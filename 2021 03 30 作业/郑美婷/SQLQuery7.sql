use master
go
create database Student
on
(
name='Student',
filename='D:\SQLcunchu\Student.mdf',
size=5mb,
maxsize=100mb,
filegrowth=10%
) 
log on
(
name='Student_log',
filename='D:\SQLcunchu\Student_log.ldf',
size=5mb,
maxsize=100mb,
filegrowth=10%
) 
go
use Student
go
create table stuInfo
(
stuID int primary key identity(1,1),
stuName nvarchar(20) , 
stuAge char(150) ,
stuSex char(1) check(stuSex in (1,0)),
time datetime
)
create table courseInfo
(
courseID int primary key identity(1,1),
courseName nvarchar(10), 
courseMarks char(100) check(courseMarks>=0 and courseMarks<=10)
)
create table scoreInfo
(
scoreID int primary key identity(1,1),
stuID int references stuInfo(stuID),
courseID int references courseInfo(courseID),
score int check(score>=0 and score<=100)
)
insert into stuInfo(stuName,stuAge,stuSex,time)values
('Tom',19,1,null),
('jack',20,0,null),
('Rose',21,1,null),
('Lulu',19,1,null),
('Lili',21,0,null),
('abc',20,1,'2007-01-07 01:11:36.590')
insert into courseInfo(courseName,courseMarks)values
('JavaBase',4 ),
('HTML',2),
('JavaScript',2),
('SqlBase',2)
insert into scoreInfo(stuID,courseID,score)values 
(1,1,80),(1,2,85),(1,4,50),(2,1,75),(2,3,45),(2,4,75),
(3,1,45), (4,1,95),(4,2,75 ),(4,3,90), (4,4,45 )
select *from stuInfo
select *from courseInfo
select *from scoreInfo
--1.查询出每个学生所选修的课程的数量和所选修的课程的考试的平均分
select stuName 学生名字,count(*)课程数量,Avg(score)平均分 from courseInfo,scoreInfo,stuInfo where courseInfo.courseID=scoreInfo.courseID group by stuName
--2.查询出每门课程的选修的学生的个数和学生考试的总分
select courseName 课程名称,count(*)学生个数,sum(score)学生考试总分 from courseInfo inner join scoreInfo on courseInfo.courseID=scoreInfo.courseID group by courseName 
--3.查询出性别一样并且年龄一样的学生的信息
select A.*from stuInfo A inner join (select stuAge,stuSex from stuInfo group by stuAge,stuSex having count(stuAge)>1 and count(stuSex)>1) B on A.stuAge=B.stuAge and A.stuSex=B.stuSex
--4.查询出学分一样的课程信息
select A.courseName,A.courseMarks from courseInfo A,courseInfo B where A.courseMarks=B.courseMarks and A.courseID !=B.courseID group by A.courseMarks,A.courseName
--5.查询出参加了考试的学生的学号，姓名，课程号和分数
select stuInfo.stuID 学生学号,stuName 学生姓名,courseID 课程编号,score 分数 from stuInfo right join scoreInfo on stuInfo.stuID = scoreInfo.stuID 
--6.查询出参加了考试的学生的学号，课程号，课程名，课程学分和分数
select stuName 学生的姓名,scoreInfo.stuID 学生的学号,scoreInfo.courseID 课程号,courseName 课程名,courseMarks 课程学分,score 分数 from  scoreInfo 
inner join courseInfo on scoreInfo.courseID=courseInfo.courseID
inner join stuInfo on stuInfo.stuID=scoreInfo.stuID
--7.查询出没有参加考试的学生的学号和姓名
select stuInfo.stuID 学生学号,stuName 学生姓名 from stuInfo full join scoreInfo on stuInfo.stuID =scoreInfo.stuID where score is null
--8.查询出是周六周天来报到的学生
select stuName 学生姓名 from stuInfo where time=7 and time=6
--9.查询出姓名中有字母a的学生的信息
select * from stuInfo where stuName like '%a%'
--10.查询出选修了2门课程以上的并且考试平均分在70以上的学生的学号和考试平均分以及选修课程的数量
select stuID 学号,count(stuID)选修课程数,avg(score)考试平均分 from scoreInfo group by stuID having count(stuID)>2 and avg(score)>70
