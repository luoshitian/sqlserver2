create database Student
on
(
name='Student',
filename='D:\Student.mdf',
size=5MB,
filegrowth=2MB,
maxsize=100MB
)
log on
(
name='Student-db',
filename='D:\Student-db.ldf',
size=5MB,
filegrowth=2MB,
maxsize=20MB
)
use student
go
create table Students
(
StuId int primary key identity(1,1) not null,
StuName nvarchar(20) not null,
stuage varchar(2) not null,
StuSex nvarchar(1) default('男')check(StuSex='男'or StuSex='女') not null,
time smalldatetime
)
create table Course
(
CourseID int primary key identity(1,1) not null,
CourseName nvarchar(50)  not null,
coursemarks int not null
)
create table Score
(
ScoreID int identity(1,1),
StuID int references Students(StuId) not null,
CourseID int references Course(CourseID) not null,
Score int not null
)
insert into Students (StuName,stuage,StuSex,time)
select 'TOM',19,'男',null union
select 'Jack',20,'女',null union
select 'Rose',21,'男',null union
select 'Lulu',19,'男',null union
select 'Lili',21,'女',null union
select 'abc',20,'男','2007-01-07 01:11:36.590' 
insert into Course (CourseName,coursemarks) values ('Javabase',4),('html',2),('javascript',2),('sqlbase',2)
insert into Score(StuID,CourseID,Score)values (1,1,80),(1,2,85),(1,4,50),(2,1,75),(2,3,45),(2,4,75),(3,1,45),(4,1,95),
(4,2,75),(4,3,90),(4,4,45)
--有如图所示的三张表结构，学生信息表（Students），课程信息表（Course）,分数信息表（Score）
select * from Students
select * from Course
select * from Score
--题目：
--1.查询出 每个学生 所选修的 课程的数量 和 所选修的课程的考试的平均分
select StuName 学生姓名,count(Score.StuID) 所选课程数量,avg(Score) 平均分 from Students inner join Score on Students.StuId = Score.StuID  group by StuName
--2.查询出每门课程的选修的学生的个数 和学生考试 的总分
select CourseName,count(Score.CourseID) 选择该课程的学生个数,sum(score) 总分 from Course inner join Score on Course.CourseID=Score.CourseID group by coursename
--3.查询出性别一样并且年龄一样的学生的信息
select A.* from Students A,Students B where A.StuSex = B.StuSex AND A.stuage=B.stuage AND a.StuId<>b.StuId
--4.查询出学分一样的课程信息
select a.CourseID,a.CourseName,a.coursemarks from Course A,Course B WHERE A.coursemarks=B.coursemarks and a.CourseID !=b.CourseID group by a.CourseID,a.CourseName,a.coursemarks
--5.查询出参加了考试的学生的学号，姓名，课程号和分数
select Students.stuid,stuname,SCORE.CourseID,Score From Students inner join Score on Score.StuID=Students.StuId
--6.查询出参加了考试的学生的学号，课程号，课程名，课程学分和分数
select Score.stuid,Course.CourseID,CourseName,coursemarks,Score From Course inner join Score on Score.CourseID=Course.CourseID
--7.查询出没有参加考试的学生的学号和姓名
SELECT Students.StuId,StuName FROM Students LEFT JOIN Score ON Students.StuId=Score.StuID
except
SELECT Students.StuId,StuName FROM Students inner JOIN Score ON Students.StuId=Score.StuID
--8.查询出是周六周天来报到的学生
select * from Students where datepart(weekday,time)=1 or datepart(weekday,time)=7
--9.查询出姓名中有字母a的学生的信息
select StuId,StuName,stuage,StuSex from Students where StuName like '%a%'
--10.查询出选修了2门课程以上的并且考试平均分在70以上的学生的学号和考试平均分以及选修课程的数量
select Score.StuID,avg(Score)平均成绩,count(Score.Courseid)选修课数量 from Score inner join Course on Score.courseId=Course.courseId group by Score.StuID 
having count(Score.Courseid)>2 and avg(Score)>70
