create database Stu
on
(
   name='Stu',
   filename='E:\SQL课堂\Stu.mdf',
   size=5,
   maxsize=100,
   filegrowth=10
)
log on 
(
   name='Stu_log',
   filename='E:\SQL课堂\Stu_log.ldf',
   size=5,
   maxsize=100,
   filegrowth=10
)
go
use Stu
go
create table stuInfo
(
   stuID int not null primary key identity(1,1),
   stuName char(10) not null,
   stuAge int not null,
   stuSex nchar(2) not null check(stuSex='男' or stuSex='女'),
   time datetime
)
insert into stuInfo values('Tom','19','男',null),('Jack','20','女',null),('Rose','21','男',null),('Lulu','19','男',null),('Lili','21','女',null),('abc','20','男','2007-01-07 01:11:36.590')
create table courseInfo
(
   courseID int not null primary key identity(1,1),
   courseName char(10) not null,
   courseMarks int not null
)
insert into courseInfo values('JavaBase','4'),('HTML','2'),('JavaScript','2'),('SqlBase','2')
create table scoreInfo
(
   scoreID int not null primary key identity(1,1),
   stuID int references stuInfo(stuID),
   courseID int references courseInfo(courseID),
   score int not null
)
insert into scoreInfo values('1','1','80'),('1','2','85'),('1','4','50'),('2','1','75'),('2','3','45'),('2','4','75'),('3','1','45'),('4','1','95'),('4','2','75'),('4','3','90'),('4','4','45')

--题目：
select * from stuInfo  --学生信息表
select * from courseInfo   --课程信息表
select * from scoreInfo   --分数信息表
--1.查询出每个学生所选修的课程的数量和所选修的课程的考试的平均分
select stuName 学生,SUM(courseMarks)课程的数量,AVG(score)考试的平均分 from scoreInfo inner join stuInfo on scoreInfo.stuID = stuInfo.stuID inner join courseInfo on scoreInfo.courseID = courseInfo.courseID group by stuName
--2.查询出每门课程的选修的学生的个数和学生考试的总分
select courseName 课程,COUNT(scoreInfo.courseID)学生的个数,SUM(score)考试的总分 from scoreInfo inner join stuInfo on scoreInfo.stuID = stuInfo.stuID inner join courseInfo on scoreInfo.courseID = courseInfo.courseID group by courseName
--3.查询出性别一样并且年龄一样的学生的信息
select A. * from stuInfo A,stuInfo B where A.stuSex = B.stuSex and A.stuAge = B.stuAge and A.stuID != B.stuID
--4.查询出学分一样的课程信息
select  A. * from courseInfo A,courseInfo B where A.courseMarks = B.courseMarks and A.courseID != B.courseID 
union
select * from courseInfo where courseID = 1 and courseID = 2 and courseID = 3
--5.查询出参加了考试的学生的学号，姓名，课程号和分数
select scoreInfo.stuID 学号,stuName 姓名,scoreInfo.courseID 课程号,score 分数 from scoreInfo inner join stuInfo on scoreInfo.stuID = stuInfo.stuID inner join courseInfo on scoreInfo.courseID = courseInfo.courseID
--6.查询出参加了考试的学生的学号，课程号，课程名，课程学分和分数
select scoreInfo.stuID 学号,scoreInfo.courseID 课程号,courseMarks 课程学分,score 分数 from scoreInfo inner join stuInfo on scoreInfo.stuID = stuInfo.stuID inner join courseInfo on scoreInfo.courseID = courseInfo.courseID
--7.查询出没有参加考试的学生的学号和姓名
select stuInfo.stuID 没有参加考试的学生的学号,stuName 姓名 from stuInfo left join scoreInfo on scoreInfo.stuID = stuInfo.stuID where stuInfo.stuID > 0
except 
select stuInfo.stuID 没有参加考试的学生的学号,stuName 姓名 from stuInfo left join scoreInfo on scoreInfo.stuID = stuInfo.stuID where score > 0
--8.查询出是周六周天来报到的学生

--9.查询出姓名中有字母a的学生的信息
select * from stuInfo where stuName like '%a%'
--10.查询出选修了2门课程以上的并且考试平均分在70以上的学生的学号和考试平均分以及选修课程的数量
select stuInfo.stuID 学生的学号,AVG(score)考试平均分在70以上,courseMarks 选修课程的数量 from scoreInfo inner join stuInfo on scoreInfo.stuID = stuInfo.stuID inner join courseInfo on scoreInfo.courseID = courseInfo.courseID group by stuInfo.stuID,courseMarks 
having COUNT(stuInfo.stuID)>=2 and AVG(score)>=70
