use master
go

create database aaa
on(
	name=aaa,
	filename='D:\aaa.mdf',
	size=10MB,
	filegrowth=10%
)
log on(
	name=aaa_log,
	filename='D:\aaa_log.ldf',
	size=10MB,
	filegrowth=10%
)
go

use aaa
go

create table stuInfo
(
	stuID int primary key identity(1,1),
	stuName varchar(10) unique not null,
	stuAge int not null,
	stuSex int check(stuSex in('1','0')) not null,
	stime datetime
)
create table courseInfo
(
	courseId int primary key identity(1,1),
	courseName varchar(10) unique not null,
	courseMarks int not null,
)
create table scoreInfo
(
	scoreId int identity(1,1),
	stuID int references stuInfo(stuID),
	courseID int references courseInfo(courseId),
	score int
)

insert into stuInfo(stuName,stuAge,stuSex,stime) values('Tom','19','1',null),('Jack','20','0',null),
('Rose','21','1',null),('Lulu','19','1',null),('Lili','21','',null),('abc','20','1','2007-01-07 01:11:36.590')
insert into courseInfo(courseName,courseMarks) values('JacaBase','4'),('HTML','2'),('JavaScript','2'),('SqlBase','2')
insert into scoreInfo(stuID,courseID,score) values('1','1','80'),('1','2','85'),('1','4','50'),('2','1','75'),
('2','3','45'),('2','4','75'),('3','1','40'),('4','1','95'),('4','2','75'),('4','3','90'),('4','4','45')

select * from stuInfo
select * from courseInfo
select * from scoreInfo

select stuInfo.stuID,count(scoreInfo.courseID)数量,avg(score)平均分 from scoreInfo 
inner join stuInfo on stuInfo.stuID=scoreInfo.courseId group by stuName,stuInfo.stuID

select CourseName,count(courseInfo.CourseID) 数量,sum(score) 总分 from courseInfo 
inner join scoreInfo on courseInfo.CourseID=scoreInfo.CourseID group by coursename

select A.* from stuInfo A,stuInfo B 
where A.StuSex = B.StuSex AND A.stuage=B.stuage AND a.StuId<>b.StuId

select a.CourseName,a.coursemarks from courseInfo A,courseInfo B 
WHERE A.coursemarks=B.coursemarks and a.CourseID !=b.CourseID group by a.CourseName,a.coursemarks

select stuInfo.stuid,stuname,scoreInfo.CourseID,score From stuInfo 
inner join scoreInfo on scoreInfo.StuID=stuInfo.StuId

select scoreInfo.stuid,courseInfo.CourseID,CourseName,coursemarks,Score From courseInfo 
inner join scoreInfo on scoreInfo.CourseID=courseInfo.CourseID

SELECT stuInfo.StuId,stuName FROM stuInfo LEFT JOIN scoreInfo ON stuInfo.StuId=scoreInfo.StuID
except
SELECT stuInfo.StuId,stuName FROM stuInfo inner JOIN scoreInfo ON stuInfo.StuId=scoreInfo.StuID

select StuId,stuName,stuAge,stuSex from stuInfo where StuName like '%a%'

select scoreInfo.StuID,avg(Score)平均分,count(scoreInfo.Courseid)课程 from scoreInfo 
inner join courseInfo on scoreInfo.courseId=courseInfo.courseId group by scoreInfo.StuID having count(scoreInfo.Courseid)>2 and avg(Score)>70