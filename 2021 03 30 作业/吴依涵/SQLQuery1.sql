use master
go
create database Students
on
(
 name='Students',
 filename='D:\新建文件夹6.mdf',
 size=6MB,
 maxsize=100MB,
 filegrowth=10Mb
)
log on
(
 name='Students_log',
 filename='D:\新建文件夹6_1og.ldf',
 size=6MB,
 maxsize=100MB,
 filegrowth=10Mb
)
go
use Students
go
create table stuInfo
(
stuID int primary key identity,
stuName varchar(10),
stuAge varchar(100),
stuSex varchar(5),
time datetime
)
go
use Students
go
create table courseInfo
(
courseID int,
courseName varchar(10),
courseMarks varchar(10)
)
go
use Students
go
create table scoreInfo
(
scoreID int,
stuID int,
courseID int,
score int
)
insert into stuInfo(stuName,stuAge,stuSex,time) values 
('Tom',19,1,null),
('Jake',20,0,null),
('Rose',21,1,null),
('Lulu',19,1,null),
('Lili',21,0,null),
('abc',20,1,'2007-01-07')

insert into courseInfo values 
(1,'JavaBase',4),
(2,'HTML',2),
(3,'JavaScript',2),
(4,'SqlBsse',2)

insert into scoreInfo values 
(1,1,1,80),
(2,1,2,85),
(3,1,4,50),
(4,2,1,75),
(5,2,3,45),
(6,2,4,75),
(7,3,1,45),
(8,4,1,95),
(9,4,2,75),
(10,4,3,90),
(11,4,4,45)


--1.查询出 每个学生 所选修的课程的数量 和 所选修的课程 的 考试的平均分
select T.stuName 学生,T.stuID,count(*)选修的课程的数量,avg(score)考试的平均分 from scoreInfo S inner join courseInfo C on S.courseID=C.courseID inner join stuInfo T on S.stuID=T.stuID group by stuName,T.stuID
 
--2.查询出 每门课程 的 选修的学生的个数 和 学生考试的总分
select C.courseName 课程,COUNT(C.courseID) 选修的学生的个数,sum(score)考试的总分 from scoreInfo S inner join courseInfo C on S.courseID=C.courseID inner join stuInfo T on S.stuID=T.stuID group by C.courseName, C.courseID 

--3.查询出性别一样并且年龄一样的学生的信息
select distinct A.* from stuInfo A,stuInfo B where A.stuSex=B.stuSex and  A.stuAge=B.stuAge and A.stuID!=B.stuID

--4.查询出学分一样的课程信息
select distinct A.* from courseInfo A,courseInfo B where A.courseMarks=B.courseMarks and A.courseID!=B.courseID 

--5.查询出参加了考试的学生的学号，姓名，课程号和分数
select S.stuID 学号,T.stuName 姓名,S.courseID 课程号,S.score 分数 from scoreInfo S left join stuInfo T on S.stuID=T.stuID 

--6.查询出参加了考试的学生的学号，课程号，课程名，课程学分和分数
select S.stuID 学号,S.courseID 课程号,C.courseName 课程名,S.score 分数 from scoreInfo S left join stuInfo T on S.stuID=T.stuID left join courseInfo C on S.courseID=C.courseID

--7.查询出没有参加考试的学生的学号和姓名
select stuID 学号,stuName 姓名 from stuInfo 
except
select S.stuID 学号,stuName 姓名 from scoreInfo S inner join stuInfo T on S.stuID=T.stuID

--8.查询出是周六周天来报到的学生
select * from stuInfo where time>='2007-01-06' and  time<='2007-01-07'

--9.查询出姓名中有字母a的学生的信息
select * from stuInfo where stuName like '%a%'

--10.查询出 选修了2门课程以上的 并且 考试平均分在70以上的 学生的学号 和 考试平均分 以及选修课程的数量
select stuID 学号,avg(score) 考试平均分,count(*) 选修课程的数量 from scoreInfo group by stuID having count(*)>2 and avg(score)>=70 


select * from stuInfo
select * from courseInfo
select * from scoreInfo