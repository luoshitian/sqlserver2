use master
go

create database student
on
(
	name='student',
	filename='D:\student.mdf',
	size=5,
	maxsize=50,
	filegrowth=10%
)
log on
(
	name='student_log',
	filename='D:\test\student_log.ldf',
	size=5,
	maxsize=50,
	filegrowth=10%
)
use student
go
create table stuInfo
(
	stuID int identity not null,
	stuName varchar(10) not null,
	stuAge int not null,
	stuSex varchar not null,
	time datetime default(null)
)
create table courseInfo
(
	courseID int identity not null,
	courseName varchar(20) not null,
	courseMarks varchar(10) not null
)
create table scoreInfo
(
	scoreID int identity not null,
	stuiD varchar(10) not null,
	courseID varchar(10) not null,
	score int not null
)
insert stuInfo values
('Tom' , 19 , 1 , ''),
('Jack' , 20 , 0 , ''),
('Rose' , 21 , 1 , ''),
('Lulu' , 19 , 1 , ''),
('Lili' , 21 , 0 , ''),
('abc' , 20 , 1 , '2007-01-07 01:11:36.590')
go

insert courseInfo values
('JavaBase' , 4),
('HTML' , 2),
('JavaScipt' , 2),
('SqlBse' , 2)
go

insert scoreInfo values
(1,1,80),(1,2,85),
(1,4,50),(2,1,75),
(2,3,45),(2,4,75),
(3,1,45),(4,1,95),
(4,2,75),(4,3,90),
(4,4,45)
go

select * from stuInfo
select * from courseInfo
select * from scoreInfo

--1.查询出每个学生所选修的课程的数量和所选修的课程的考试的平均分
select stuName , COUNT(*) as 课程数量 , AVG(score) as 平均数 from scoreInfo
inner join stuInfo on stuInfo.stuID = scoreInfo.stuID
group by scoreInfo.stuID,stuName
go

--2.查询出每门课程的选修的学生的个数和学生考试的总分
select courseName , COUNT(*) as 个数 , SUM(score) as 总分 from scoreInfo
inner join courseInfo on courseInfo.courseID = scoreInfo.courseID
group by scoreInfo.courseID , courseName
go

--3.查询出性别一样并且年龄一样的学生的信息
select A . * from stuInfo A , stuInfo B where A.stuAge=B.stuAge AND A.stuSex=B.stuSex AND A.stuID !=B.stuID

--4.查询出学分一样的课程信息
select A.courseID,A.courseName,A.courseMarks from courseInfo A ,courseInfo B 
where A.courseMarks = B.courseMarks and A.courseID != B.courseID
go

--5.查询出参加了考试的学生的学号，姓名，课程号和分数
select A.stuID as 学号,stuName as 姓名, A.courseID as 课程号,score as 分数 from scoreInfo A
inner join courseInfo B on A.courseID = B.courseID
inner join stuInfo C on A.stuID = C.stuID
go

--6.查询出参加了考试的学生的学号，课程号，课程名，课程学分和分数
select A.stuID as 学号,A.courseID as 课程号,B.courseName as 课程名,B.courseMarks as 课程学分,score as 分数 from scoreInfo A
inner join courseInfo B on A.courseID = B.courseID
inner join stuInfo C on A.stuID = C.stuID
go

--7.查询出没有参加考试的学生的学号和姓名
select stuID as 学号,stuName as 姓名 from stuInfo
except
select A.stuID as 学号,stuName as 姓名 from scoreInfo A
inner join stuInfo C on A.stuID = C.stuID

--8.查询出是周六周天来报到的学生
select stuName from stuInfo 
where datepart(weekday,Time)=6 or datepart(weekday,Time)=7
go

--9.查询出姓名中有字母a的学生的信息
select * from stuInfo 
where stuName like '%a%'
go

--10.查询出选修了2门课程以上的并且考试平均分在70以上的学生的学号和考试平均分以及选修课程的数量
select A.stuID as 学号,AVG(score) as 平均分,COUNT(*) as 课程数量 from scoreInfo A
inner join stuInfo C on A.stuID = C.stuID
group by A.stuID
having COUNT(*) > 2 and AVG(score) >70
go