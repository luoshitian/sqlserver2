--有如图所示的三张表结构，学生信息表（stuInfo），课程信息表（courseInfo）,分数信息表（scoreInfo）

use master
go 

create database stuInfo
go
use stuInfo
go
create table StuUser
(
	stuID int primary key identity,
	stuName nvarchar(10) ,
	stuAge int ,
	stuSex int check(stuSex in('0','1')),
	stuTime datetime null
)
create table Course
(
	courseID int primary key identity,
	courseName nvarchar(10),
	courseMarks int
)
create table Score
(
	scoreID int primary key identity ,
	stUID int references StuUser(stuID) ,
	courseID int references Course(courseID),
	score int 
)
go
insert into StuUser(stuName,stuAge,stuSex) values('Tom',19,1),('Jack',20,0),('Rose',21,1)
,('Lulu',19,1),('Lili',21,0)
insert into StuUser(stuName,stuAge,stuSex,stuTime) values('abc',20,1,'2007-01-07 01:11:36.590')

insert into Course (courseName,courseMarks)
select 'JavaBase',4 union
select 'HTML',2 union
select 'JavaScripy',2 union
select 'SqlBase',2 

insert into Score (stuID,courseID,score)
select 1,1,80 union
select 1,2,85 union
select 1,4,50 union
select 2,1,75 union
select 2,3,45 union
select 2,4,75 union
select 3,1,45 union
select 4,1,95 union
select 4,2,75 union
select 4,3,90 union
select 4,4,45
--题目：


--1.查询出每个学生所选修的课程的数量和所选修的课程的考试的平均分
select stuName,count(Score.stuID),avg(score) from StuUser inner join Score on StuUser.stuID = score.stUID group by stuName
--2.查询出每门课程的选修的学生的个数和学生考试的总分
select Course.courseID,count(Course.courseID),sum(score) from Course 
inner join Score on Course.courseID = Score.courseID group by Course.courseID
--3.查询出性别一样并且年龄一样的学生的信息
select A.* from StuUser A inner join
(select stuAge,stuSex from StuUser group by stuAge,stuSex having count(stuAge)>1 and count(stuSex)>1) B 
on A.stuAge = B.stuAge and A.stuSex =B.stuSex
--4.查询出学分一样的课程信息
select A.courseName,A.courseMarks from Course A,Course B where 
A.courseMarks = B.courseMarks and A.courseID != B.courseID group by A.courseMarks,A.courseName
--5.查询出参加了考试的学生的学号，姓名，课程号和分数
select S.stuName,S.stUID,S.stuName,C.courseID,score from Score C 
left join StuUser S on C.stUID = S.stuID
--6.查询出参加了考试的学生的学号，课程号，课程名，课程学分和分数
select stuName,O.stUID,C.courseID,C.courseMarks,courseMarks,score from Score O 
left join Course C on C.courseID = O.courseID
left join StuUser on O.stUID = StuUser.stuID
--7.查询出没有参加考试的学生的学号和姓名
select stUID,stuName from StuUser group by stuID,stuName
except
select Score.stUID,stuName from Score  inner join  StuUser on Score.stUID = StuUser.stuID group by Score.stUID,stuName
--8.查询出是周六周天来报到的学生

--9.查询出姓名中有字母a的学生的信息
select * from StuUser where stuName like '%a%'
--10.查询出选修了2门课程以上的并且考试平均分在70以上的学生的学号和考试平均分以及选修课程的数量
select stUID,avg(score),count(*) from Score group by stUID having count(*)>1 and avg(score)>70
select * from Course
select * from Score
select * from StuUser