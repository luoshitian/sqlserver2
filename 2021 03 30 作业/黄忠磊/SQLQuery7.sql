use master
go
create database Student
on
(
	name='Student',
	filename='D:\Student.mdf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%
)
log on
(
	name='Student_log',
	filename='D:\Student_log.ldf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%
)
go

use Student
go

create table stuInfo
(
	StuId int primary key identity(1,1),
	StuName nvarchar(20) not null,
	StuAge int,
	StuSex nvarchar(1) default(1) check(StuSex=1 or StuSex=0),
	time datetime 
)
insert into stuInfo(StuName,StuAge,StuSex,time) values 
('TOP',19,1,null),
('jack',20,0,null),
('Rose',21,1,null),
('Lulu',19,1,null),
('Lili',21,0,null),
('abc',20,1,'2007-01-07')

select *from stuInfo

create table courseInfo
(
	CourseId int primary key identity(1,1),
	courseName nvarchar(20) unique not null,
	courseMarks nvarchar(10)
)

insert into courseInfo (courseName,courseMarks) values ('javabase',4),('HTML',2),('JAVAscript',2),('SQL',2)

select *from courseInfo 

create table Score
(
	ScoreId int primary key identity(1,1),
	StuId int references stuInfo(StuId),
	CourseId int references courseInfo(CourseId),
	Score int  not null
)
insert into Score(StuId,CourseId,Score) values (1,1,80),
(1,2,85),(1,4,50),(2,1,75),(2,3,45),(2,4,75),(1,1,80),(3,1,45),(4,1,95),(4,2,75),
(4,3,90),(4,4,45)
select *from Score

--有如图所示的三张表结构，学生信息表（stuInfo），课程信息表（courseInfo）,分数信息表（scoreInfo）
select *from stuInfo
select *from courseInfo 
select *from Score
--题目：
--1.查询出每个学生所选修的课程的数量和所选修的课程的考试的平均分

select StuName,count(*) 数量,avg (Score)平均分 from Score 
 inner join stuInfo on stuInfo.StuId=Score.StuId    group by   StuName
--2.查询出每门课程的选修的学生的个数和学生考试的总分
select StuName,count(*)个数,sum(Score)总分 from Score 
 inner join stuInfo on stuInfo.StuId=Score.StuId   group by   StuName
--3.查询出性别一样并且年龄一样的学生的信息
select * from stuInfo A, stuInfo b   where A.StuAge=B.StuAge AND A.StuSex=B.StuSex AND A.StuId !=B.StuId
--4.查询出学分一样的课程信息
select * from courseInfo A, courseInfo b   where A.courseMarks=B.courseMarks AND A.CourseId !=B.CourseId
--5.查询出参加了考试的学生的学号，姓名，课程号和分数
select stuInfo.StuId  学号,StuName 姓名,courseInfo.CourseId 课程号,Score 分数 from Score inner join courseInfo on Score.CourseId=courseInfo.CourseId 
 inner join stuInfo on stuInfo.StuId=Score.StuId    group by   stuInfo.StuId,courseInfo.CourseId,StuName,Score
--6.查询出参加了考试的学生的学号，课程号，课程名，课程学分和分数
select stuInfo.StuId  学号,StuName 姓名,courseInfo.CourseId 课程号,courseMarks 课程学分 ,Score 分数 from Score inner join courseInfo on Score.CourseId=courseInfo.CourseId 
 inner join stuInfo on stuInfo.StuId=Score.StuId    group by   stuInfo.StuId,courseInfo.CourseId,StuName,courseMarks,Score
--7.查询出没有参加考试的学生的学号和姓名
select StuId 学号,StuName  姓名 from stuInfo where StuId=StuId
except
select Score.StuId,StuName from stuInfo ,Score  where Score.StuId =stuInfo.StuId  group by 
Score.StuId,StuName 
--8.查询出是周六周天来报到的学生
select * from  stuInfo where datepart(weekday,Time)=6 or datepart(weekday,Time)=7
--9.查询出姓名中有字母a的学生的信息
select * from stuInfo where StuName like 'a%'
--10.查询出选修了2门课程以上的并且考试平均分在70以上的学生的学号和考试平均分以及选修课程的数量

select StuId 学号,avg(Score)平均分,count (*)数量 from courseInfo,Score where courseInfo.CourseId>2  group by StuId  having avg(Score)>70