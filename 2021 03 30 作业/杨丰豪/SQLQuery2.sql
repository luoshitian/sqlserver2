use master
go

create database dong
on
(
name='dong',
filename='D:\test\dong.mdf',
size=5mb,
maxsize=50mb,
filegrowth=10%
)
log on
(
name='dong_log',
filename='D:\test\dong_log.ldf',
size=5mb,
maxsize=50mb,
filegrowth=10%
)

go
use dong

go
create table stuInfo
(
stuID int primary key identity(1,1),
stuName nvarchar(20) not null,
stuAge int not null,
stuSex nchar(2) default('1') check(stuSex='1' or stuSex='0'),
time datetime
)

go
create table courseInfo
(
courseID int primary key identity(1,1),
courseName nvarchar(20) not null,
courseMarks int

)

go
create table scoreInfo
(
scoreID int primary key identity(1,1),
stuID int references stuInfo(stuID),
courseID int references courseInfo(courseID),
score int 
)

go
insert into stuInfo (stuName,stuAge,stuSex) values ('Tom',19,1),('Jack',20,0),('Rose',21,0),('Lulu',19,1),('Lili',21,0),('abc',20,1)

go
update stuInfo set time='2007-01-07 01:11:36.590' where stuID=6

go
insert into courseInfo (courseName,courseMarks) values ('JavaBase',4),('HTML',2),('JavaScript',2),('SqlBase',2)

go
insert into scoreInfo (stuID,courseID,score) values (1,1,80),(1,2,85),(1,4,50),(2,1,75),(2,3,45),(2,4,75),(3,1,45),(4,1,95),(4,2,75),(4,3,90),(4,4,45)

go
select * from stuInfo
select * from courseInfo
select * from scoreInfo

--题目：
--1.查询出每个学生所选修的课程的数量和所选修的课程的考试的平均分
select stuName 学生名字,count(scoreInfo.stuID) 选修课程数量,avg(score) 选修的课程平均分 from stuInfo inner join scoreInfo on scoreInfo.stuID = stuInfo.stuID group by stuName
--2.查询出每门课程的选修的学生的个数和学生考试的总分
select courseName 课程名字,count(scoreInfo.courseID) 学生的个数,sum(score) 总分 from courseInfo inner join scoreInfo on courseInfo.courseID = scoreInfo.courseID group by courseName
--3.查询出性别一样并且年龄一样的学生的信息
select A. * from stuInfo A,stuInfo B where A.stuAge=B.stuAge and A.stuSex=B.stuSex and A.stuID!=B.stuID
--4.查询出学分一样的课程信息
select A. * from courseInfo A,courseInfo B where A.courseMarks=B.courseMarks and A.courseID!=B.courseID
--5.查询出参加了考试的学生的学号，姓名，课程号和分数
select stuInfo.stuID 学号,stuName 姓名,courseInfo.courseID 课程号,score 分数 from stuInfo inner join scoreInfo on scoreInfo.stuID=stuInfo.stuID inner join courseInfo on courseInfo.courseID=scoreInfo.courseID
--6.查询出参加了考试的学生的学号，课程号，课程名，课程学分和分数
select stuInfo.stuID 学号,courseInfo.courseID 课程号,courseName 课程名,courseMarks 课程学分,score 分数 from stuInfo inner join scoreInfo on scoreInfo.stuID=stuInfo.stuID inner join courseInfo on courseInfo.courseID=scoreInfo.courseID
--7.查询出没有参加考试的学生的学号和姓名
select stuID 学号,stuName 姓名 from stuInfo except select stuInfo.stuID 学号,stuName 姓名 from scoreInfo  inner join stuInfo  on scoreInfo.stuID=stuInfo.stuID  group by stuInfo.stuID,stuName
--8.查询出是周六周天来报到的学生
select * from stuInfo where time between '2007-01-05' and  '2007-01-08'
--9.查询出姓名中有字母a的学生的信息
select * from stuInfo where stuName like'%a%'
--10.查询出选修了2门课程以上的并且考试平均分在70以上的学生的学号和考试平均分以及选修课程的数量
select stuInfo.stuID 学生ID,COUNT(*) 课程数量,AVG(score) 考试平均分数 from stuInfo inner join scoreInfo on scoreInfo.stuID=stuInfo.stuID group by stuInfo.stuID having AVG(score)>70 and COUNT(*)>2