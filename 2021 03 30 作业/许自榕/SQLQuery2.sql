 create database  student
on
(
	name='student',
	filename='D:\student.mdf',
	size=20,
	maxsize=300,
	filegrowth=50
)
log on
(
	name='student_log',
	filename='D:\student_log.ldf',
	size=20,
	maxsize=300,
	filegrowth=50
)
create table stuInfo
(
	stuID int primary key  identity(1,1),
	stuNmae nvarchar(20),
	stuAge char(10),
	stuSex  varchar(1)  ,
	time datetime null
)
create table courseInfo
(
	courseID int primary key identity(1,1),
	courseName nvarchar(20) not null ,
	courseMarks int 
)
create table scoreInfo
(
	scoreID int primary key identity(1,1),
	stuID int foreign key references stuInfo(stuID) not null,
	courseID int foreign key references courseInfo(courseID),
	score int not null check(score>=0 and score<=100)
)
insert into stuInfo(stuNmae,stuAge,stuSex,time)values('TOM',19,'1',''),('Jack',20,'0',''),
('Rose',21,'1',''),('Lulu',19,'1',''),('Lili',21,'0',''),('abc',20,'1','2007-01-07 01:11:36.590')
insert into courseInfo(courseName,courseMarks)values('JavaBase', 4),('HTML', 2),('JavaScript', 2),('SqlBase', 4)
insert into scoreInfo(stuID,courseID,score)values(1,1,80),(1,2,85),
(1,4,50),(2,1,75),(2,3,45),(2,4,75),(3,1,45),(4,1,95),(4,2,75)



select *from courseInfo
select *from scoreInfo
select *from stuInfo

--1.查询出每个学生所选修的课程的数量和所选修的课程的考试的平均分
select courseID,count(courseName),avg(courseMarks) from courseInfo group by courseID

--2.查询出每门课程的选修的学生的个数和学生考试的总分
select courseName,count(courseName),sum(courseMarks) from courseInfo group by courseName
--3.查询出性别一样并且年龄一样的学生的信息
select A * from StudenInfo A,StudenInfo B where A.stuAge=B.stuAge and A.stuAge and A.stuID! =B.stuID
--4.查询出学分一样的课程信息
select courseName,count(courseName),sum(courseMarks) from courseInfo group by courseName
--5.查询出参加了考试的学生的学号，姓名，课程号和分数
select A * from StudenInfo A,StudenInfo B where A.stuAge=B.stuAge and A.stuAge and A.stuID! =B.stuID
--6.查询出参加了考试的学生的学号，课程号，课程名，课程学分和分数
select courseID,count(courseName),avg(courseMarks) from courseInfo group by courseID
--7.查询出没有参加考试的学生的学号和姓名
select courseID,count(courseName),avg(courseMarks) from courseInfo group by courseID
--8.查询出是周六周天来报到的学生
select courseName,count(courseName),sum(courseMarks) from courseInfo group by courseName
--9.查询出姓名中有字母a的学生的信息
select A * from StudenInfo A,StudenInfo B where A.stuAge=B.stuAge and A.stuAge and A.stuID! =B.stuID
--10.查询出选修了2门课程以上的并且考试平均分在70以上的学生的学号和考试平均分以及选修课程的数量
select courseID,count(courseName),avg(courseMarks) from courseInfo group by courseID
