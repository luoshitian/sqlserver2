		use master 
		go
		create database Students04
		on
		(
		name = 'Students04 ',
		filename = 'D:\Students04.mdf',
		size = 5MB,
		maxsize = 50MB,
		filegrowth = 10%
		)   
		log on
		(
		name = 'Students04_log ',
		filename = 'D:\Students04_log.mdf',
		size = 5MB,
		maxsize = 50MB,
		filegrowth = 10%
		)
		go
		use Students04
		go
		create table StuInfo
		(
		StuId int primary key identity(1,1),
		StuName varchar(10) not null,
		StuAge int,
		StuSex char(1) default(1) check(StuSex in(1,0)) not null,
		Time datetime
		)

		create table CourseInfo
		(
		CourseId int primary key identity(1,1),
		CourseName varchar(20) not null,
		CourseMarks int
		)

		create table ScoreInfo
		(
		ScoreId int primary key identity(1,1),
		StuId int references StuInfo(StuId),
		CourseId int references CourseInfo(CourseId),
		Score int
		)


		insert into StuInfo values('Tom',19,1,NULL),('Jack',20,1,NULL),
								('Rose',21,1,NULL),('LUlu',19,1,NULL),
								('Lili',21,0,NULL),('abc',20,1,'2007-01-07 01:11:36.590')

		insert into CourseInfo values('JavaBase',4),('HTML',2),
									  ('JavaScript',2),('SqlBase',2)

		insert into ScoreInfo values(1,1,80),(1,2,85),(1,4,50),(2,1,75),
									(2,3,45),(2,4,75),(3,1,45),(4,1,95),
									(4,2,75),(4,3,90),(4,4,45)

		--题目：
		--1.查询出每个学生所选修的课程的数量和所选修的课程的考试的平均分
			select StuName 姓名,count(CourseId)课程数量,avg(Score)平均分 from ScoreInfo 
			inner join StuInfo on ScoreInfo.StuId = StuInfo.StuId
			group by StuName
		--2.查询出每门课程的选修的学生的个数和学生考试的总分
			select CourseName 课程,count(CourseInfo.CourseId),sum(Score)平均分  from CourseInfo
			inner join ScoreInfo  on CourseInfo.CourseId = ScoreInfo .CourseId
			group by CourseName
		--3.查询出性别一样并且年龄一样的学生的信息
			select A . * from StuInfo A inner join
			(select StuAge,StuSex from StuInfo group by StuAge,StuSex having count(StuAge)>1  and count(StuSex) > 1) 
			B on A .StuAge = B .StuAge and A .StuSex = B .StuSex
		--4.查询出学分一样的课程信息
			select A . CourseName, A .CourseMarks from CourseInfo A,CourseInfo B where A .CourseMarks = B .CourseMarks
			and A .CourseId != B.CourseId group by  A . CourseName, A .CourseMarks
		--5.查询出参加了考试的学生的学号，姓名，课程号和分数
			select StuInfo.StuId 学号, StuName 姓名,  ScoreInfo.CourseId 课程号,Score 分数 from ScoreInfo
			inner join StuInfo on  ScoreInfo.CourseId = StuInfo.StuId
			inner join CourseInfo on ScoreInfo.StuId = CourseInfo.CourseId
		--6.查询出参加了考试的学生的学号，课程号，课程名，课程学分和分数
			select StuInfo.StuId 学号, StuName 姓名,  ScoreInfo.CourseId 课程号,CourseName 课程名,CourseMarks 课程学分,Score 分数 from ScoreInfo
			inner join StuInfo on  ScoreInfo.CourseId = StuInfo.StuId
			inner join CourseInfo on ScoreInfo.StuId = CourseInfo.CourseId
		--7.查询出没有参加考试的学生的学号和姓名
			select StuInfo.StuId 学号, StuName 姓名 from ScoreInfo
			inner join StuInfo on ScoreInfo.StuId = StuInfo.StuId
			where Score = ''
		--8.查询出是周六周天来报到的学生
		    select * from StuInfo where Time = '' and Time = ''
		--9.查询出姓名中有字母a的学生的信息
			select * from  StuInfo where StuName like '%a%'
		--10.查询出选修了2门课程以上的并且考试平均分在70以上的学生的学号和考试平均分以及选修课程的数量
		    select ScoreInfo.StuId 学号,avg(Score)平均分,count(ScoreInfo.CourseId)课程数量 from ScoreInfo
			inner join  CourseInfo on ScoreInfo.StuId = CourseInfo.CourseId
			inner join StuInfo on StuInfo.StuId = ScoreInfo.StuId
			group by ScoreInfo.StuId
			having avg(Score) > 70 and count(ScoreInfo.CourseId)>2

		select * from StuInfo
		select * from CourseInfo 
		select * from ScoreInfo
