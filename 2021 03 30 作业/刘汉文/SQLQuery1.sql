create database Students
on
(
name='Students_data',
filename='D:\新建文件夹.mdf',
size=5mb,
filegrowth=10%
)
go
use Students
go
create table stuInfo
(
Stuid int not null,
 Stuname char(20) not null,
stusex bit not null default('1')check(stusex=1 or stusex=0) ,
stuage int not null,
time datetime
)
go
create table courseInfo
(
courseld int not null,
courseName  char(20)  not null,
coursemarks int not null,

)
go
create table scoreInfo
(
scoreID int  not null,
stuid int not null,
courseld int not null,
score int not null
)
go
select * from stuInfo
select * from courseInfo
select * from scoreInfo
insert into stuInfo(Stuid ,Stuname,stuage,stusex) values (1,'Tom',19, 1),(2,'Jack',20,0),(3,'Rose',20,0),(4,'Lulu',10,1),(5,'Lili',21,0),(6,'abc',20,1)
 insert into courseInfo(courseld, courseName, coursemarks)values(1,'JavaBase',4),(2,'HTML',2),(3,'JavaScript',2),(4,'SqlBase',2)
 insert into scoreInfo (scoreID,stuid,courseld ,score )values(1,1,1,80),(2,1,2,85),(3,1,4,50),(4,2,1,75),(5,2,3,45),(6,2,4,75),(7,3,1,45),(8,4,1,95),(9,4,2,75),(10,4,3,90),(11,4,4,45)
 
 
 --题目：
--1.查询出每个学生所选修的课程的数量和所选修的课程的考试的平均分
select  courseld,count(courseName)课程数量,avg(courseMarks)平均分 from courseInfo group by courseld
--2.查询出每门课程的选修的学生的个数和学生考试的总分
select scoreInfo.courseld,coursename,COUNT(stuid),SUM(score) from scoreInfo inner join courseInfo on scoreInfo.courseld=courseInfo.courseld group by scoreInfo.courseld,coursename
--3.查询出性别一样并且年龄一样的学生的信息
select a.* from stuInfo a , stuInfo b where a.stusex=b.stusex and a.stuage=b.stuage and a.stuid!=b.stuid
--4.查询出学分一样的课程信息
select distinct a.courseld,a.coursename,a.coursemarks from courseInfo a,courseInfo b where a.coursemarks=b.coursemarks and a.courseld!=b.courseld 
--5.查询出参加了考试的学生的学号，姓名，课程号和分数
select stuid,stuname,courseld,score from scoreInfo inner join stuInfo on stuInfo.stuid=scoreInfo.Stuid group by stuid,stuname,courseld,score

--6.查询出参加了考试的学生的学号，课程号，课程名，课程学分和分数
 select stuid,courseInfo.courseld,coursename,coursemarks,score from scoreInfo inner join courseInfo on scoreInfo.courseld=courseInfo.courseld  group by stuid,courseInfo.courseld,coursename,coursemarks,score
--7.查询出没有参加考试的学生的学号和姓名
select * from  stuInfo left join scoreInfo on scoreInfo.stuid=stuInfo.stuId WHERE score IS NULL
--8.查询出是周六周天来报到的学生
select * from stuInfo where datepart(weekday,time)=1 or datepart(weekday,time)=7
--9.查询出姓名中有字母a的学生的信息
select * from stuInfo where stuname like '%a%'
--10.查询出选修了2门课程以上的并且考试平均分在70以上的学生的学号和考试平均分以及选修课程的数量
 select stuid,count(courseld),AVG(score) from scoreInfo group by stuid having COUNT(courseld)>2 and AVG(score)>70
