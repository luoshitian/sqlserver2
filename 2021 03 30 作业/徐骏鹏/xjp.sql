use master 
go
create database bbs
on
(
	name='bbs',
	filename='D:\bbs.mdf',
	size=10,
	maxsize=200,
	filegrowth=20
)
log on
(
	name='bbs_log',
	filename='D:\bbs_log.ldf ',
	size=10,
	maxsize=200,
	filegrowth=20
)
go
use bbs

create table stuInfo
(  
    stuld int primary key identity not null,
	stuname varchar(20) not null,
	stuage varchar(20) not null,
	stusex varchar(2) check (stusex in ('1','0'))not null,
	time datetime default('null')
)
go
create table courseInfo
(
   
   courseid int primary key identity  not null,
   coursename varchar(20) not null,
   coursemarks int not null
)
go
create table scoreInfo
(
    
	scoreid int primary key identity not null,
	stuid int references stuInfo(stuld)  not null,
	courseld int references courseInfo(courseid) not null,
	score int not null


)
go

 insert into stuInfo values ('tom','19','1',''),('jack','20','0',''),('rose','21','1',''),('lulu','19','1',''),('lili','21','0',''),('abc','20','1','2007-01-07')


 select * from stuInfo

 insert into courseInfo values('javabase','4'),('html','2'),('javascript','2'),('sqlbase','2')

 select * from courseInfo

 insert into scoreInfo values('1','1','80'),('1','2','85'),('1','4','50'),('2','1','75'),('2','3','45'),('2','4','75'),('3','1','45'),('4','1','95'),('4','2','75'),('4','3','90'),('4','4','45')

 select * from scoreInfo

--1.查询出每个学生所选修的课程的数量和所选修的课程的考试的平均分
 select stuid,stuname,count(courseld),avg(score) from scoreInfo inner join stuInfo on scoreInfo.stuid=stuInfo.stuld group by  stuid,stuname
--2.查询出每门课程的选修的学生的个数和学生考试的总分

 select scoreInfo.courseld,coursename,COUNT(stuid),SUM(score) from scoreInfo inner join courseInfo on scoreInfo.courseld=courseInfo.courseid group by scoreInfo.courseld,coursename
--3.查询出性别一样并且年龄一样的学生的信息
selecyA.* from bbUsers A. bbUsers B where A. uAge AND A.uSex=B.uSex and A.UID=B.UID
--4.查询出学分一样的课程信息
select distinct a.courseid,a.coursename,a.coursemarks from courseInfo a,courseInfo b where a.coursemarks=b.coursemarks and a.courseid!=b.courseid 

--5.查询出参加了考试的学生的学号，姓名，课程号和分数

--6.查询出参加了考试的学生的学号，课程号，课程名，课程学分和分数

--7.查询出没有参加考试的学生的学号和姓名

--8.查询出是周六周天来报到的学生
select * from stuInfo where datepart(weekday,time)=1 or datepart(weekday,time)=7
--9.查询出姓名中有字母a的学生的信息
select * from stuInfo where stuname like'%a%'
--10.查询出选修了2门课程以上的并且考试平均分在70以上的学生的学号和考试平均分以及选修课程的数量
select stuid, countr(courseld),AVG(scorr)from scoreInfo group by stuid having COUNT(courseld)>2 and AVG(score)>70