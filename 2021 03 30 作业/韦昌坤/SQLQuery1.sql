use master
go
create database lnl
on
(
	name='list',
	filename='D:\lnl.mdf',
	size=5,
	maxsize=50,
	filegrowth=10%
)
log on
(
	name='list_log',
	filename='D:\lnl_log.ldf',
	size=5,
	maxsize=50,
	filegrowth=10%
)
use lnl
go
create table Suddny
(
stuid int identity(1,1) primary key not null,
stuName varchar(5) not null,
stuAge int not null,
stuSex int not null,
time datetime 
)
go
create table Abcde
(
courseld int identity(1,1) primary key not null,
courseName varchar(11) not null,
courseMarks int not null
)
go
create table Fsese
(
scoreid int identity(1,1) not null,
stuid int references Suddny(stuid) not null,
courseid int references Abcde(courseld) not null,
score int not null
)
go
insert into Suddny(stuName,stuAge,stuSex,time) values
('Tom',19,1,NULL),('Jack',20,0,NULL),('Rose',21,1,NULL),
('Lulu',19,1,NULL),('Lili',21,0,NULL),('abc',20,1,'2007-01-07 01:11:36.590')
select * from Suddny

insert into Abcde(courseName,courseMarks) values
('JavaBase',4),('HTML',2),('JavaScript',2),('SqlBase',2)
select * from Abcde

insert into Fsese(stuid,courseid,score) values
(1,1,80),(1,2,85),(1,4,50),
(2,1,75),(2,3,45),(2,4,75),
(3,1,45),(4,1,95),(4,2,75),
(4,3,90),(4,4,45)
select * from Fsese
--1.查询出每个学生所选修的课程的数量和所选修的课程的考试的平均分
select stuName 学生名字,count(Fsese.stuID) 选修课程数量,avg(score) 选修的课程平均分 from Suddny inner join Fsese on Suddny.stuID = Fsese.stuID group by stuName
--2.查询出每门课程的选修的学生的个数和学生考试的总分
select courseName 课程名字,count(Fsese.courseID) 学生的个数,sum(score) 总分 from Abcde inner join Fsese on Abcde.courseld = Fsese.courseID group by courseName
--3.查询出性别一样并且年龄一样的学生的信息
select A. * from Suddny A,Suddny B where A.stuAge=B.stuAge and A.stuSex=B.stuSex and A.stuID!=B.stuID
--4.查询出学分一样的课程信息
select A. * from Abcde A,Abcde B where A.courseMarks=B.courseMarks and A.courseld!=B.courseld
--5.查询出参加了考试的学生的学号，姓名，课程号和分数
select Suddny.stuID 学号,stuName 姓名,Abcde.courseld 课程号,score 分数 from Suddny inner join Fsese on Fsese.stuID=Suddny.stuID inner join Abcde on Abcde.courseld=Fsese.courseID
--6.查询出参加了考试的学生的学号，课程号，课程名，课程学分和分数
select Suddny.stuID 学号,Abcde.courseld 课程号,courseName 课程名,courseMarks 课程学分,score 分数 from Suddny inner join Fsese on Fsese.stuID=Suddny.stuID inner join Abcde on Abcde.courseld=Fsese.courseID
--7.查询出没有参加考试的学生的学号和姓名
select stuID 学号,stuName 姓名 from Suddny except select Suddny.stuID 学号,stuName 姓名 from Fsese  inner join Suddny  on Fsese.stuID=Suddny.stuID  group by Suddny.stuID,stuName
--8.查询出是周六周天来报到的学生
select * from Suddny where time between '2007-01-05' and  '2007-01-08'
--9.查询出姓名中有字母a的学生的信息
select * from Suddny where stuName like'%a%'
--10.查询出选修了2门课程以上的并且考试平均分在70以上的学生的学号和考试平均分以及选修课程的数量
select Suddny.stuID 学生ID,COUNT(*) 课程数量,AVG(score) 考试平均分数 from Suddny inner join Fsese on Fsese.stuID=Suddny.stuID group by Suddny.stuID having AVG(score)>70 and COUNT(*)>2

