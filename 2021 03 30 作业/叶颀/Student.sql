use master
go

create database Student
on
(
	name ='Student',
	filename='D:\test\Student.mdf',
	size=5,
	maxsize=50,
	filegrowth=10
)
log on
(
	name ='Student_log',
	filename='D:\test\Student_log.ldf',
	size=5,
	maxsize=50,
	filegrowth=10
)
go

use Student
go

create table StuInfo 
(
	StuId int primary key identity ,
	StuName varchar(10) not null ,
	StuAge int ,
	StuSex int check (StuSex in (0,1)) ,
	Time datetime default(null)
)
go

create table CourseInfo
(
	CourseId int primary key identity ,
	CourseName varchar(10) not null ,
	CourseMarks int ,
)
go

create table ScoreInfo
(
	ScoreId int primary key identity ,
	StuId int references StuInfo(StuId) ,
	CourseId int references CourseInfo(CourseId),
	Score int 
)
go

insert StuInfo values 
('Tom',19,1,''),
('Jack',20,0,''),
('Rose',21,1,''),
('Lulu',19,1,''),
('Lili',21,0,''),
('abc',20,1,'2007-01-07 01:11:36.590')
go

insert CourseInfo values 
('JavaBase',4),
('HTML',2),
('JavaScript',2),
('SqlBase',2)
go

insert ScoreInfo values 
(1,1,80),
(1,2,85),
(1,4,50),
(2,1,75),
(2,3,45),
(2,4,75),
(3,1,45),
(4,1,95),
(4,2,75),
(4,3,90),
(4,4,45)
go

select StuName as 姓名 ,count(*) as 课程数量,AVG(Score) as 平均分 from ScoreInfo 
inner join StuInfo on StuInfo.StuId = ScoreInfo.StuId
group by ScoreInfo.StuId ,StuName
go

select courseName as 课程, count( *) as 个数 ,sum(Score) as 总分 from ScoreInfo
inner join CourseInfo on CourseInfo.CourseId = ScoreInfo.CourseId
group by ScoreInfo.CourseId, courseName
go

select A.StuId,A.StuName,A.StuAge,A.StuSex,A.Time from StuInfo A ,StuInfo B 
where A.StuAge = B.StuAge and A.StuSex = B.StuSex and A.StuId != B.StuId
go

select A.CourseId,A.CourseName,A.CourseMarks from CourseInfo A ,CourseInfo B 
where A.CourseMarks = B.CourseMarks and A.CourseId != B.CourseId
go

select A.StuId as 学号,StuName as 姓名, A.CourseId as 课程号,Score as 分数 from ScoreInfo A
inner join CourseInfo B on A.CourseId = B.CourseId
inner join StuInfo C on A.StuId = C.StuId
go

select A.StuId as 学号,A.CourseId as 课程号,B.CourseName as 课程名,B.CourseMarks as 课程学分,Score as 分数 from ScoreInfo A
inner join CourseInfo B on A.CourseId = B.CourseId
inner join StuInfo C on A.StuId = C.StuId
go

select StuId as 学号,StuName as 姓名 from StuInfo
except
select A.StuId as 学号,StuName as 姓名 from ScoreInfo A
inner join StuInfo C on A.StuId = C.StuId

select stuName from StuInfo 
where datepart(weekday,Time)=6 or datepart(weekday,Time)=7
go

select * from StuInfo 
where StuName like '%a%'
go

select * from ScoreInfo 

select A.StuId as 学号,AVG(Score) as 平均分,count(*) as 课程数量 from ScoreInfo A
inner join StuInfo C on A.StuId = C.StuId
group by A.StuId
having count(*) > 2 and AVG(Score) >70
go