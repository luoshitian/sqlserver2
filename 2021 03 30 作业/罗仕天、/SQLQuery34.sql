create database Student
on(
name=Student,
filename='D:\sql.mdf',
size=10mb,
maxsize=50mb,
filegrowth=10%
)
log on
(
name=sql_log,
filename='D:\sql_log.mdf',
size=10mb,
maxsize=50mb,
filegrowth=10%

)

create table stuInfo
(
stuID int not null primary key identity(1,1),
stuName nvarchar(10) not null,
stuAge int not null,
stuSex	nchar   check(stuSex='男' or stuSex	='女') ,
time char(50)
)
insert into stuInfo values ('TOM',19,'男',NULL),('JACK',20,'女',NULL),('ROSE',21,'男',NULL),('LULU',19,'男',NULL),('LILI',21,'女',NULL),('ABC',20,'男','2007-01-07 01:11:36:590')
select * from stuInfo
create table courseInfo
(
courseID int primary key identity(1,1) not null,
sourseName nvarchar(10) not null,
courseMarks int

)
insert into courseInfo values ('SQIBASE',4),('JAVASCRIPT',2),('HTML',2),('JAVABASE',2) 
select * from courseInfo
create table scoreInfo
(
scoreID int primary key identity(1,1) not null,
stuID int references stuInfo(stuID) not null,
courseID int references courseInfo(courseID) not null,
score int not null
)
insert into scoreInfo values (1,1,80),
(1,2,85),
(1,4,50),
(2,1,75),
(2,3,45),
(2,4,75),
(3,1,45),
(4,1,95),
(4,2,75),
(4,3,90),
(4,4,45)
select * from scoreInfo

select * from scoreInfo
select * from stuInfo
select *from courseInfo 


--1.查询出每个学生所选修的课程的数量和所选修的课程的考试的平均分
select stuname 学生,count(*)课程数量,AVG(score) 平均分 from scoreinfo
 inner join stuInfo on  stuInfo.stuid=scoreInfo.stuID
 inner join courseInfo on courseInfo.  courseID=scoreinfo.courseID 
 group by  stuname 
--2.查询出每门课程的选修的学生的个数和学生考试的总分
select soursename 课程,count(*)学生个数,SUM(score) 总分 from scoreInfo
inner join stuInfo on  stuInfo.stuid=scoreInfo.stuID
 inner join courseInfo on courseInfo.  courseID=scoreinfo.courseID 
 group by  sourseName
--3.查询出性别一样并且年龄一样的学生的信息
select A.* from stuInfo A,stuInfo B 
where A.stuAge=B.stuAge and A.stuSex=B.stuSex and A.stuID !=B.stuID
--4.查询出学分一样的课程信息
select A.* from courseInfo A, courseInfo B
where A.courseMarks=B.courseMarks and A.courseID != B.courseID
--5.查询出参加了考试的学生的学号，姓名，课程号和分数
select stuInfo.stuID,stuName,courseID,score from stuInfo 
inner join scoreInfo on scoreInfo.stuID=stuInfo.stuID
--6.查询出参加了考试的学生的学号，课程号，课程名，课程学分和分数


--7.查询出没有参加考试的学生的学号和姓名

--8.查询出是周六周天来报到的学生

--9.查询出姓名中有字母a的学生的信息

--10.查询出选修了2门课程以上的并且考试平均分在70以上的学生的学号和考试平均分以及选修课程的数量
select stuInfo.stuID 学号,avg(score) 考试平均分,count(*) 选修课程的数量 from stuInfo