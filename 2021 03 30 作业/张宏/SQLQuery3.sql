create database  Student02
on
(
	name='Student02',
	filename='D:\Student02.mdf',
	size=20,
	maxsize=300,
	filegrowth=50
)
log on
(
	name='Student02_log',
	filename='D:\Student02_log.ldf',
	size=20,
	maxsize=300,
	filegrowth=50
)
go
use   Student02
go
create table StudenInfo
(
	stuID int identity(1,1) primary key,
	stuName nvarchar(20) not null unique, 
	stuAge int   null ,
	stuSex int check(stuSex in('1','0')) default('0'),
	time datetime 
)
insert into StudenInfo(stuName,stuAge,stuSex) values('Tom',19,1),('Jack',20,0),('Rose',21,1),('Lulu',19,1),('Lili',21,0)
insert into StudenInfo(stuName,stuAge,stuSex,time) values('abc',20,1,2007-01-07)

create table courseInfo
(
	courseID int identity(1,1) primary key,
	courseName nvarchar(20) not null,
	courseMarks int not null
)
insert into courseInfo(courseName,courseMarks) values ('JavaBase',4),('HTML',2),('Javascript',2),('SqlBase',2)

create table scoreInfo
(
	scoreID int primary key identity(1,1),
	stuID int references StudenInfo(stuID),
	courseID int references courseInfo(courseID),
	score int not null
)

insert into scoreInfo values(1,1,80),(1,2,85),(1,4,50),(2,1,75),(2,3,45),(2,4,75),(3,1,45),(4,1,95),(4,2,75),(4,3,90),(4,4,45)


--题目：
select * from StudenInfo
select * from courseInfo
select * from scoreInfo
--1.查询出 每个学生 所选修的课程的数量和 所选修的课程的考试的平均分
select stuName 姓名,count(distinct courseID)所选修的课程的数量和,avg(score)平均分 from scoreInfo SC inner join StudenInfo ST on SC.stuID=ST.stuID group by stuName

--2.查询出 每门课程的选修的学生的个数 和 学生考试的总分
select courseName 课程名,count(*)每门课程的选修的学生的个数,sum(score)学生考试的总分 from scoreInfo SC inner join courseInfo CO on SC.courseID=CO.courseID group  by courseName 

--3.查询出性别一样并且年龄一样的学生的信息
select A. * from StudenInfo A,StudenInfo B where A.stuSex  = B.stuSex and A.stuAge=B.stuAge and A.stuName!=B.stuName

--4.查询出学分一样的课程信息 
select A. * from scoreInfo A,scoreInfo B where A.score=B.score and A.stuID!=B.stuID
--5.查询出参加了考试的学生的学号，姓名，课程号和分数
select SC. stuID,stuName,scoreID,score from scoreInfo SC inner join StudenInfo ST on SC.stuID=ST.stuID group by SC. stuID,stuName,scoreID,score
--6.查询出参加了考试的学生的学号，课程号，课程名，课程学分和分数
select SC.scoreID,stuID,score,courseMarks,courseName from scoreInfo SC inner join courseInfo CO on SC.courseID=CO.courseID group by SC. scoreID,stuID,score,courseMarks,courseName
--7.查询出没有参加考试的学生的学号和姓名
select * from StudenInfo ST left join scoreInfo SC on ST.stuID=SC.stuID where score is null
--8.查询出是周六周天来报到的学生
 select * from StudenInfo where datepart(WEEKDAY,time)=1 or datepart(WEEKDAY,time)=7
--9.查询出姓名中有字母a的学生的信息
select * from StudenInfo where stuName like '%a%'
--10.查询出 选修了2门课程以上的 并且考试平均分在70以上的学生的学号 和 考试平均分以及                  选修课程的数量
select ST. stuID,AVG(score),count(ST.stuID) from StudenInfo ST inner join scoreInfo SC on ST.stuID=SC.stuID  group by ST.stuID having avg(score)>70