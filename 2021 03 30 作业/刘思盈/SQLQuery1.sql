use master 
go
create database Students
on
(
name=' Students',
filename='D:\Students.mdf',
size=10,
maxsize=100,
filegrowth=10%
)
log on 
(
name=' Students_log',
filename='D:\Students_log.ldf',
size=10,
maxsize=100,
filegrowth=10%

)
go
use Students
go
create table stuInfo
(
stuID INT PRIMARY KEY IDENTITY(1,1),
stuName varchar(7) unique not null,
stuAge int ,
stuSex int check(stuSex in(1,0)) not null,
time datetime 
)
create table courseInfo
(
courseID int primary key identity(1,1),
courseName varchar(10) not null,
courseMarks int
)
create table scoreInfo
(
scoreID INT PRIMARY KEY IDENTITY(1,1),
stuID int references stuInfo(stuID) ,
courseID int references courseInfo(courseID),
score int not null
)
insert into stuInfo values('tom',19,1,null),('jack',20,0,null),
						  ('rose',21,1,null),('lulu',19,1,null),
						  ('lili',21,0,null),('abc',20,1,'2007-01-07 11:36.590')
insert into courseInfo values('javabase',4),('html',2),('javascipt',2),('sqbase',2)
insert into scoreInfo values(1,1,80),(1,2,85),(1,4,50),(2,1,75),(2,3,45),(2,4,75),
							(3,1,45),(4,1,95),(4,2,75),(4,3,90),(4,4,45)
--题目：
--1.查询出每个学生所选修的课程的数量和所选修的课程的考试的平均分
select stuName 学生, count(C.courseID) 课程的数量,avg(score)平均分 from scoreInfo 
inner join stuInfo S on scoreInfo.stuID=S.stuID 
inner join courseInfo C ON scoreInfo.courseID=C.courseID
group by stuName ORDER BY stuName
--2.查询出每门课程的选修的学生的个数和学生考试的总分
SELECT C.courseName 课程,count(scoreInfo.courseID) 学生的个数,sum(score)总分 FROM scoreInfo
 inner join courseInfo C on scoreInfo.courseID=C.courseID GROUP BY  C.courseName
 
--3.查询出性别一样并且年龄一样的学生的信息
SELECT *FROM stuInfo
select A.* from stuInfo A inner join
(SELECT stuAge, stuSex from stuInfo group by stuAge, stuSex having count(stuAge)>1 AND COUNT(stuSex)>1) B
ON A.stuAge=B.stuAge AND A.stuSex=B.stuSex

--4.查询出学分一样的课程信息
select A.courseName, A.courseMarks from  courseInfo A , courseInfo B WHERE A.courseMarks=B.courseMarks AND A.courseID!=B.courseID 
--5.查询出参加了考试的学生的学号，姓名，课程号和分数

SELECT S.stuID 学号,stuName 姓名,C.courseID 课程号,score 分数 FROM scoreInfo 
INNER JOIN courseInfo C ON scoreInfo.courseID=C.courseID
INNER JOIN stuInfo S ON scoreInfo.stuID=S.stuID
--6.查询出参加了考试的学生的学号，课程号，课程名，课程学分和分数
SELECT S.stuID 学号,stuName 姓名,C.courseID 课程号, courseName 课程名,courseMarks 课程学分,score 分数  FROM scoreInfo 
INNER JOIN courseInfo C ON scoreInfo.courseID=C.courseID
INNER JOIN stuInfo S ON scoreInfo.stuID=S.stuID 
--7.查询出没有参加考试的学生的学号和姓名
select S.stuID 学号,stuName 姓名 from stuInfo S  inner join scoreInfo ON S.stuID=scoreInfo.stuID where score='' 
--8.查询出是周六周天来报到的学生
select *from stuInfo where time=''
--9.查询出姓名中有字母a的学生的信息
select *from stuInfo where stuName like '%a%' or stuName like '%a' or stuName like 'a%'
--10.查询出选修了2门课程以上的并且考试平均分在70以上的学生的学号和考试平均分以及选修课程的数量
select S.stuID 学生的学号, COUNT(C.courseID) 选修课程的数量,AVG(score)考试平均分 from scoreInfo SI 
inner join courseInfo C on SI.courseID=C.courseID
inner join stuInfo S on SI.stuID=S.stuID
group by S.stuID
having COUNT(C.courseID)>2 and AVG(score)>70