 use master
 go
 create database student
 on
 (
	name=Student,
	filename='D:\Student.mdf',
	size=10MB,
	maxsize=50MB,
	filegrowth=10%
 )
 log on
 (
	name=Student_log,
	filename='D:\Student_log.ldf',
	size=10MB,
	maxsize=50MB,
	filegrowth=10%
 )
 go
 use student
 go 
 create table stuInfo
 (
 stuID int primary key identity(1,1),
 stuName nvarchar(5),
 stuAge int ,
 stuSex int check(stuSex=1or stuSex=0) not null,
 time  datetime 
 )
 go
 create table courseInfo
 (
 courseID int primary key identity(1,1),
 courseName nvarchar(10),
 courseMakes char(1)
 )
 go
 create table scoreInfo
 (
 scoreID int primary key identity(1,1),
 stuID int ,
 courseID int ,
 score int 
 )
 go
 insert into stuInfo values ('Tom',19,1,null)
  insert into stuInfo values ('Jack',20,0,null),
  ('Rose',21,1,null),('Lulu',19,1,null),('Lil',21,0,null),
  ('abc',20,1,'2007-01-07')
  insert into courseInfo values ('JavaBase',4)
  insert into courseInfo values ('HTML',2),('Javascript',2),
  ('sqlBase',2)
  insert into scoreInfo values (1,1,80)
  
  insert into scoreInfo values (1,2,85),(1,4,50),(1,4,50),(2,1,75),
  (2,3,45),(2,4,75),(3,1,45),(4,1,95),(4,2,75),(4,3,90),(4,4,45)

  --1.查询出每个学生所选修的课程的数量和所选修的课程的考试的平均分
  select  stuName as 名字 ,COUNT(*) as 选修数量, AVG(score) from scoreInfo 
  inner join stuInfo on scoreInfo.stuID=stuInfo.stuID
  group by stuName
  select * from courseInfo
  select * from stuInfo
   select * from scoreInfo
   --2.查询出每门课程的选修的学生的个数和学生考试的总分
  select courseName,COUNT(scoreInfo.courseID),SUM(score) from scoreInfo
  inner join courseInfo on scoreInfo.courseID=courseInfo.courseID
  group by courseName ,scoreInfo.courseID
  
--3.查询出性别一样并且年龄一样的学生的信息
select A.stuName,A.stuAge,A.stuSex from stuInfo A,stuInfo B where A.stuAge=B.stuAge and A.stuSex=B.stuSex and A.stuName!=B.stuName

--4.查询出学分一样的课程信息
select  distinct A.* from  courseInfo A,courseInfo B where A.courseMakes=B.courseMakes and A.courseID!=B.courseID
--5.查询出参加了考试的学生的学号，姓名，课程号和分数
select st.stuID as 学号,st.stuName as 姓名,c.courseID as 课程号,s.score as 分数 from scoreInfo S
inner join  courseInfo C on s.courseID=c.courseID
inner join stuInfo st on st.stuID=s.stuID

--6.查询出参加了考试的学生的学号，课程号，课程名，课程学分和分数
select st.stuID as 学号,s.courseID as 课程号,c.courseName as 课程名 ,c.courseMakes as 课程学分,s.score as 分数 from scoreInfo S
inner join  courseInfo C on s.courseID=c.courseID
inner join stuInfo st on st.stuID=s.stuID
--7.查询出没有参加考试的学生的学号和姓名
 select * from  stuInfo left join scoreInfo on scoreInfo.stuid=stuInfo.stuID WHERE score IS NULL
--8.查询出是周六周天来报到的学生
select * from stuInfo where datepart(weekday,time)=1 or datepart(weekday,time)=7
--9.查询出姓名中有字母a的学生的信息
select * from stuInfo where stuName like '%a%'
--10.查询出选修了2门课程以上的并且考试平均分在70以上的学生的学号和考试平均分以及选修课程的数量
select stuid,count(courseID),AVG(score) from scoreInfo group by stuid having COUNT(courseID)>2 and AVG(score)>70
