use master
go

create database students
go

use students
go
 
create table stuInfo
(
	stuID int primary key identity,
	stuName varchar(20),
	stuAge int,
	stuSex varchar(2),
	time datetime
)
go

create table courseInfo
(
	courseID int primary key identity,
	courseName varchar(20),
	courseMarks int
)
go

create table scoreInfo
(
	scoreID int primary key identity,
	stuID int references stuInfo(stuID),
	courseID int references courseInfo(courseID),
	score int
)
go

insert into stuInfo values('Tom','19','1',null),('Jack','20','0',null),('Rose','21','1',null),('Lulu','19','1',null),
('Lili','21','0',null),('abc','20','1','2007-01-07 01:11:36')
go

insert into	courseInfo values('JavaBase',4),('HTML',2),('JavaScript',2),('SqlBase',2)
go

insert into scoreInfo values(1,1,80),(1,2,85),(1,4,50),(2,1,75),(2,3,45),(2,4,75),(3,1,45),(4,1,95),(4,2,75),(4,3,90),(4,4,45)
go


--1.查询出每个学生所选修的课程的数量和所选修的课程的考试的平均分
select SC.courseID,count(SC.courseID)课程的数量,avg(score)平均分
from  stuInfo ST inner join scoreInfo SC on ST.stuID=SC.stuID
inner join  courseInfo CI on CI.courseID=SC.courseID
group by SC.courseID,courseMarks

--2.查询出每门课程的选修的学生的个数和学生考试的总分
select CI.courseID,sum(score)总分
from scoreInfo SI inner join courseInfo CI on SI.courseID=CI.courseID
group by CI.courseID

--3.查询出性别一样并且年龄一样的学生的信息
select * from stuInfo A
where(select count(*) from stuInfo where stuSex=A.stuSex and stuAge=A.stuAge)>1

--4.查询出学分一样的课程信息
select A.* from courseInfo A
where(select count(*) from courseInfo where courseMarks=A.courseMarks)>1

--5.查询出参加了考试的学生的学号，姓名，课程号和分数
select ST.stuID,stuName,courseID,score 
from scoreInfo SC inner join stuInfo ST on ST.stuID=SC.stuID
group by ST.stuID,stuName,courseID,score
	
--6.查询出参加了考试的学生的学号，课程号，课程名，课程学分和分数
select ST.stuID,CI.courseID,courseName,courseMarks,SI.score  
from courseInfo CI inner join scoreInfo SI on CI.courseID=SI.courseID
inner join stuInfo ST on SI.stuID=ST.stuID

--7.查询出没有参加考试的学生的学号和姓名
select ST.stuID,stuName 
from stuInfo ST left join scoreInfo SI on ST.stuID=SI.stuID
except
select SI.stuID,stuName 
from stuInfo A inner join scoreInfo SI on A.stuID=SI.stuID

--8.查询出是周六周天来报到的学生
select * from stuInfo where datepart(weekday,time+@@datefirst-1) in (6,7)

--9.查询出姓名中有字母a的学生的信息
select * 
from stuInfo ST full join scoreInfo SI on  ST.stuID=SI.stuID
where stuName like '%a%'

--10.查询出选修了2门课程以上的并且考试平均分在70以上的学生的学号和考试平均分以及选修课程的数量
select SI.stuID,avg(score) 平均分,count(courseID) 选修课程的数量
from scoreInfo SI inner join stuInfo ST on  ST.stuID=SI.stuID
group by SI.stuID 
having count(courseID)>2 and avg(score)>70


select * from stuInfo
select * from courseInfo
select * from scoreInfo