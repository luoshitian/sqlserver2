use master 
go 
create database Studen
on
(
	name='Studen',
	filename='D:\Studen.mdf',
	size=10,
	maxsize=200,
	filegrowth=15%
)
log on
(
	name='Studen_log',
	filename='D:\Studen_log.ldf',
	size=10,
	maxsize=200,
	filegrowth=15%
)
go
use Studen
go
create table stuInfo
(
	stuID int primary key identity,
	stuName varchar(20) not null,
	stuAge varchar(10) ,
	stuSex int ,
	time  datetime 
)
create table courseInfo
(
	courseID int primary key identity,
	courseName varchar(20) not null,
	courseMark varchar(10) 
)
create table scoreInfo
(
	scoreID int primary key identity,
	stuID int ,
	courseID int ,
	score int 
)
insert into stuInfo(stuName,stuAge,stuSex,time) values ('Tom','19',1,null),('Jack','20',0,null),('Rose','21',1,null),('Lulu','19',1,null),('L','21',0,null),('abc','20',1,getdate())
select * from stuInfo
insert into courseInfo(courseName,courseMark) values  ('JavaBase','4'),('HTML','2'),('JavaScript','2'),('SqlBase','2')
select * from courseInfo
insert into scoreInfo(stuID,courseID,score) values(1,1,'80'),  (1,2,'85'),  (1,4,'50'),  (2,1,'75'),  (2,3,'45'),  (2,4,'75'),  (3,1,'45'),  (4,1,'95'),  (4,2,'75'),  (4,3,'90'),  (4,4,'45')
select * from stuInfo
select * from courseInfo
select * from scoreInfo
--1.查询出每个学生所选修的课程的数量和所选修的课程的考试的平均分
select ST.stuName 学生姓名,count(courseID) 课程数量,avg(score) 考试平均分  from stuInfo ST inner join  scoreInfo SC on ST.stuID=SC.stuID group by SC.stuID,st.stuName
--2.查询出每门课程的选修的学生的个数和学生考试的总分
select CO.courseName 课程名称, count(SO.courseID) 选修的学生的个数 ,sum(score) 考试的总分 from courseInfo CO left  join scoreInfo SO on  CO.courseID=SO.courseID group by SO.courseID ,CO.courseName
--3.查询出性别一样并且年龄一样的学生的息信
select * from stuInfo A  , stuInfo B  where A.stuAge=B.stuAge and A.stuSex=B.stuSex and A.stuID!=B.stuID
--4.查询出学分一样的课程信息
select distinct * from courseInfo A inner join courseInfo B on A.courseMark=B.courseMark and A.courseName!=B.courseName 
--5.查询出参加了考试的学生的学号，姓名，课程号和分数
select  S.stuID 学号 ,stuName 姓名, C.courseID 课程号, score 分数 from stuInfo S ,courseInfo C ,scoreInfo SC where S.stuID=SC.stuID and C.courseID=SC.courseID
--6.查询出参加了考试的学生的学号，课程号，课程名，课程学分和分数
select  S.stuID 学号 , C.courseID 课程号,courseName 课程名,courseMark 课程学分, score 分数 from stuInfo S ,courseInfo C ,scoreInfo SC where S.stuID=SC.stuID and C.courseID=SC.courseID
--7.查询出没有参加考试的学生的学号和姓名

--8.查询出是周六周天来报到的学生

--9.查询出姓名中有字母a的学生的信息
select * from stuInfo where stuName like '%a%'
--10.查询出选修了2门课程以上的并且考试平均分在70以上的学生的学号和考试平均分以及选修课程的数量
select S.stuID 学号, avg(score) 考试平均分 , count(SO.stuID)  选修课程的数量 from stuInfo S inner join scoreInfo SO on S.stuID=SO.stuID group by S.stuID
having avg(score)>70 and count(SO.stuID)>2