create database student
on
(
 name='student',
 filename='D:\student.mdf',
 size=10,
 maxsize=200,
 filegrowth=20
)
log on
(
  name='student_log',
 filename='D:\student_log.ldf',
 size=10,
 maxsize=200,
 filegrowth=20
)
go
use student
go

create table stuInfo
(
 stuId  int identity(1,1)  primary key not null ,
 stuName   nvarchar(10) not null,
 stuSex   varchar(2) not null ,
 stuAge  int not null,
 time datetime 
)
create table courseInfo
(
 courseId int identity(1,1) primary key  not null,
 courseName nvarchar(10),
 courseMarks int not null
)
create table scoreInfo
(
 scoreId int identity(1,1) primary key,
 stuId int references stuInfo(stuId),
 courseId int  references courseInfo(courseId),
 score int
) 
insert into stuInfo(stuName,stuAge,stuSex,time)
values('Tom','19','1',null),('jack','20','0',null),('Rose','21','1',null),('Lulu','19','1',null),('Lili','21','0',null),('abc','20','1','2007-01-07 01:11:36.590')

insert into courseInfo(courseName,courseMarks)
values('JavtBase','4'),('HTML','2'),('JavtScript','2'),('SqlBase','2')

insert into scoreInfo  
values ('1','1','80'),('1','2','85'),('1','4','50'),('2','1','75'),('2','3','45'),('2','4','75'),('3','1','45'),('4','1','95'),('4','2','75'),('4','3','90'),('4','4','45')

select * from stuInfo 
select * from courseInfo 
select * from scoreInfo 

--1.查询出每个学生 所选修的课程的数量 和所选修的课程的考试的平均分
select stuName 学生,count(courseId)选修的课程的数量,avg(score)考试的平均分  from stuInfo SU inner join scoreInfo SR on SU.stuId=SR.stuId group by stuName
--2.查询出每门课程的选修的学生的个数和学生考试的总分
select courseName 课程,count(stuId)选修的学生的个数,sum(score) 学生考试的总分  from courseInfo  CI INNER JOIN scoreInfo SR on CI.courseId=SR.courseId group by courseName
--3.查询出性别一样并且年龄一样的学生的信息
select A. *  from stuInfo A , stuInfo B WHERE A.stuAge=B.stuAge and A.stuSex=B.stuSex AND A.stuId!=B.stuId

--4.查询出学分一样的课程信息
select A. * from courseInfo A, courseInfo B where A.courseMarks=B.courseMarks AND A.courseName!=B.courseName 

--5.查询出参加了考试的学生的学号，姓名，课程号和分数
select  SU.stuId 学号,stuName 姓名,courseId 课程号,score 分数 from stuInfo SU inner join scoreInfo SR on SU.stuId=SR.stuId 

--6.查询出参加了考试的学生的学号，课程号，课程名，课程学分和分数
select  A.stuId,A.stuName,B.courseName,C.score FROM  stuInfo A,courseInfo B,scoreInfo C WHERE A.stuId=C.stuId AND B.courseId=C.courseId

--7.查询出没有参加考试的学生的学号和姓名
select StuId  学号,StuName  姓名 from StuInfo
except
select A.StuId  学号,StuName  姓名 from ScoreInfo A
inner join StuInfo C on A.StuId = C.StuId
--8.查询出是周六周天来报到的学生
select stuName from StuInfo where datepart(weekday,Time)=1 or datepart(weekday,Time)=7

--9.查询出姓名中有字母a的学生的信息
select * from StuInfo where StuName like '%a%' 
--10.查询出选修了2门课程以上的并且考试平均分在70以上的学生的学号和考试平均分以及选修课程的数量
select A.StuId  学号,AVG(Score) 平均分,count(*) 课程数量 from ScoreInfo A
inner join StuInfo C on A.StuId = C.StuId
group by A.StuId
having count(*) > 2 and AVG(Score) >70






