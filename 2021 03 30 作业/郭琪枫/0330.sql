use master
go
create database Student
on
(
 name='Student',
 filename='D:\Student.mdf',
 size=6MB,
 maxsize=100MB,
 filegrowth=10Mb
)
log on
(
 name='Student_log',
 filename='D:\Student_log.ldf',
 size=6MB,
 maxsize=100MB,
 filegrowth=10Mb
)
go
use Student
go

create table stuInfo
(
	stuID int primary key identity(1,1),
	stuName nvarchar(20),
	stuAge varchar(20),
	StuSex char(2) default('1') check(StuSex in('1','0')),
	time datetime
)

create table courseInfo
(
	courseID int primary key identity(1,1),
	courseName varchar(20),
	courseMarks varchar(20)
)

create table scoreInfo
(
	scoreID int primary key identity(1,1),
	stuID int references stuInfo(stuID),
	courseID int references courseInfo(courseID),
	score int
)

insert into stuInfo(stuName,stuAge,StuSex) values('Tom',19,1),
('Jack',20,0),
('Rose',21,1),
('Lulu',19,1),
('Lili',21,0),
('abc',20,1)
update stuInfo set time='2007-01-07 01:11:36.590'  where stuID=6
--'2007-01-07 01:11:36.590'

insert into courseInfo values ('JavaBase',4),
('HTML',2),
('JavaScript',2),
('SqlBase',2)

insert into scoreInfo values (1,1,80),
(1,2,85),
(1,4,50),
(2,1,75),
(2,3,45),
(2,4,75),
(3,1,45),
(4,1,95),
(4,2,75),
(4,3,90),
(4,4,45)

select * from stuInfo
select * from courseInfo
select * from scoreInfo

--有如图所示的三张表结构，学生信息表（stuInfo），课程信息表（courseInfo）,分数信息表（scoreInfo）

--题目：
--1.查询出每个学生所选修的课程的数量和所选修的课程的考试的平均分

select SI.stuName 学生姓名,COUNT(*) 课程的数量, AVG(score) 课程的考试的平均分 from scoreInfo SC inner join stuInfo SI on SC.stuID=SI.stuID
inner join courseInfo CI on CI.courseID=SC.courseID group by SI.stuName

--2.查询出每门课程的选修的学生的个数和学生考试的总分

select CI.courseName 课程名称,COUNT(*) 选修的学生的个数, SUM(score)学生考试的总分 from scoreInfo SC inner join stuInfo SI on SC.stuID=SI.stuID
inner join courseInfo CI on CI.courseID=SC.courseID group by CI.courseName

--3.查询出性别一样并且年龄一样的学生的信息

select A. * from stuInfo A ,stuInfo B where A.stuAge=B.stuAge AND A.StuSex=B.StuSex AND A.stuID != B.stuID

--4.查询出学分一样的课程信息

select distinct  A. * from courseInfo A ,courseInfo B where A.courseMarks=B.courseMarks AND A.courseID != B.courseID 

--5.查询出参加了考试的学生的学号，姓名，课程号和分数

select SI.stuID 学号,stuName 姓名,CI.courseID 课程号,score 分数 from  scoreInfo SC inner join stuInfo SI on SC.stuID=SI.stuID
inner join courseInfo CI on CI.courseID=SC.courseID group by SI.stuID,stuName,CI.courseID,score

--6.查询出参加了考试的学生的学号，课程号，课程名，课程学分和分数

select SI.stuID 学号,CI.courseID 课程号,courseName 课程名,courseMarks 课程学分,score 分数 from  scoreInfo SC inner join stuInfo SI on SC.stuID=SI.stuID
inner join courseInfo CI on CI.courseID=SC.courseID group by SI.stuID,CI.courseID,courseName,courseMarks,score

--7.查询出没有参加考试的学生的学号和姓名
select stuID 学号,stuName 姓名 from stuInfo
except
select SI.stuID 学号,stuName 姓名 from scoreInfo SC inner join stuInfo SI on SC.stuID=SI.stuID  group by SI.stuID,stuName 

--8.查询出是周六周天来报到的学生

select * from stuInfo where time between '2007-01-05' and  '2007-01-08'

--9.查询出姓名中有字母a的学生的信息

select * from stuInfo where stuName like '%a%'

--10.查询出选修了2门课程以上的并且考试平均分在70以上的学生的学号和考试平均分以及选修课程的数量

select SI.stuID 学号,AVG(score) 考试平均分,COUNT(*)选修课程的数量 from  scoreInfo SC inner join stuInfo SI on SC.stuID=SI.stuID
inner join courseInfo CI on CI.courseID=SC.courseID group by SI.stuID having count(*)>2 and avg(score)>=70 
