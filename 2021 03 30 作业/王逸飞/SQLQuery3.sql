create database dididada
on(
        name='dididada',
		filename='D:\dididada.mdf',
		size=10,
		maxsize=100,
		filegrowth=10
		

)
log on
(             
        name='dididada_log',
		filename='D:\dididada_log.ldf',
		size=10,
		maxsize=100,
		filegrowth=10
      


)
go 
create table stuInfo(
stuID int identity(1,1) primary key,
stuName nvarchar(10)not	null ,
stuAge varchar(10) not	null,
stuSex varchar(2) check(stuSex=1 or stuSex=0),
times datetime 
)
go
create table courseInfo(
courseID int identity(1,1) primary key,
courseName nvarchar(10) not null,
courseMarks varchar(5)
)
go
create table scoreInfo(
scoreID int identity(1,1) primary key,
stuID int references stuInfo(stuID),
courseID int references courseInfo(courseID),
score int not null

)
insert into stuInfo
select 'Tom',19,1,null union
select 'Jack',20,0 ,null union
select 'Rose',21,1,null  union
select 'Lulu',19,1 ,null  union
select 'Lili',21,0,null union
select 'abc',20,1 ,'2007-01-07 '
insert into courseInfo
select 'JavaBase',4 union
select 'HTML',2 union
select 'JavaScript',2 union
select 'SqlBase',2 

insert into scoreInfo
select 1,1 , 80union
select 1,2,85 union
select 1,4,50 union
select 2,1,75 union
select 2,3 ,45union
select 2,4 ,75union
select 3,1,45 union
select 4,1, 95union
select 4,2,75 union
select 4,3,90 union
select 4,4 ,45

--1.查询出每个学生所选修的课程的数量和所选修的课程的考试的平均分
select stuID, courseName, courseMarks,avg(score)  from  courseInfo c inner join scoreInfo s on c.courseID=s.courseID group by courseName, courseMarks,score,stuID ORDER BY courseName
--2.查询出每门课程的选修的学生的个数和学生考试的总分
 select scoreInfo.courseID,coursename,COUNT(stuid),SUM(score) from scoreInfo inner join courseInfo on scoreInfo.courseID=courseInfo.courseid group by scoreInfo.courseld,coursename
 --3.查询出性别一样并且年龄一样的学生的信息
 select a.* from stuInfo a , stuInfo b where a.stusex=b.stusex and a.stuage=b.stuage and a.stuID!=b.stuID  --4.查询出学分一样的课程信息
 select distinct a.courseid,a.coursename,a.coursemarks from courseInfo a,courseInfo b where a.coursemarks=b.coursemarks and a.courseid!=b.courseid 
  --5.查询出参加了考试的学生的学号，姓名，课程号和分数
 select stuInfo.stuID,stuname,courseID,score from scoreInfo inner join stuInfo on stuInfo.stuID=scoreInfo.stuid group by stuInfo.stuID,stuname,courseID,score
  --6.查询出参加了考试的学生的学号，课程号，课程名，课程学分和分数
 select stuid,courseInfo.courseid,coursename,coursemarks,score from scoreInfo inner join courseInfo on scoreInfo.courseID=courseInfo.courseid  group by stuid,courseInfo.courseid,coursename,coursemarks,score
  --7.查询出没有参加考试的学生的学号和姓名
 select * from  stuInfo left join scoreInfo on scoreInfo.stuid=stuInfo.stuID WHERE score IS NULL
  --8.查询出是周六周天来报到的学生
select * from stuInfo where datepart(weekday,times)=1 or datepart(weekday,times)=7
--9.查询出姓名中有字母a的学生的信息
 select * from stuInfo where stuname like '%a%'
 
--10.查询出选修了2门课程以上的并且考试平均分在70以上的学生的学号和考试平均分以及选修课程的数量
 select stuid,count(courseID),AVG(score) from scoreInfo group by stuid having COUNT(courseID)>2 and AVG(score)>70















