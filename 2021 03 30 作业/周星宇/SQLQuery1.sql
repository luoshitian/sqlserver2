use master
go

create database student
on
(
     name='student',
	 filename='E:\新建文件夹\作业\SQLstudent.mdf',
	 size=5MB,
	 maxsize=50MB,
	 filegrowth=10%
)

log on
(
     name='student_log',
	 filename='E:\新建文件夹\作业\SQLstudent_log.ldf',
	 size=5MB,
	 maxsize=50MB,
	 filegrowth=10%
)
go

use student 
go

create table stuInfo
(
     stuID int primary key not null , 
	 stuName varchar(10) not null,
	 stuAge int,
	 stuSex varchar(1) check(stuSex='0' or stuSex='1'),
	 stuTime datetime 
)
go

create table courseInfo
(
     courseID int primary key identity(1,1) not null,
	 courseName nvarchar(10) not null,
	 courseMarks int 
)
go


create table scoreInfo
(
     scoreID int primary key identity(1,1) not null,
	 stuID int references stuInfo(stuID),
	 courseID int references courseInfo(courseID),
	 score int
)


insert into stuInfo(stuID,stuName,stuAge,stuSex,stuTime)
select '1','Tom','19','1',null union
select '2','Jack','20','0',null union
select '3','Rose','21','1',null union
select '4','Lulu','19','1',null union
select '5','Lili','21','0',null union
select '6','abc','20','1','2007-01-07 01:11:36.590' 
go


insert into courseInfo(courseName,courseMarks)
select 'JavaBase','4' union
select 'HTML','2' union
select 'JavaScript','2' union
select 'SqlBase','2' 
go



insert into scoreInfo(stuID,courseID,score)
select '1','1','80'union
select '1','2','85'union
select '1','4','50'union
select '2','1','75'union
select '2','3','45'union
select '2','4','75'union
select '3','1','45'union
select '4','1','95'union
select '4','2','75'union
select '4','3','90'union
select '4','4','45'
go




select*from stuInfo
select*from courseInfo
select*from scoreInfo


--有如图所示的三张表结构，学生信息表（stuInfo），课程信息表（courseInfo）,分数信息表（scoreInfo）


--1.查询出每个学生所选修的课程的数量和所选修的课程的考试的平均分
select A.stuName 学生,COUNT(B.stuId) 选课数量,AVG(score) 考试平均分 from stuInfo A
inner join scoreInfo B on A.stuId = B.stuId
group by A.stuId,stuName


--2.查询出每门课程的选修的学生的个数和学生考试的总分
select courseName as 课程, count( *) as 个数 ,sum(Score) as 总分 from scoreInfo
inner join courseInfo on courseInfo.courseId = scoreInfo.courseID
group by scoreInfo.courseID, courseName


--3.查询出性别一样并且年龄一样的学生的信息
select*from stuInfo A,stuInfo B
where A.stuAge=B.StuAge and A.stuSex=B.StuSex and A.StuId != B.StuId


--4.查询出学分一样的课程信息
select A.courseID,A.courseName,A.courseMarks from courseInfo A ,courseInfo B 
where A.courseMarks = B.courseMarks and A.courseID != B.courseID


--5.查询出参加了考试的学生的学号，姓名，课程号和分数
select A.stuID as 学号,stuName as 姓名, A.courseID as 课程号,score as 分数 from scoreInfo A
inner join courseInfo B on A.courseId = B.courseID
inner join stuInfo C on A.stuID = C.stuID


--6.查询出参加了考试的学生的学号，课程号，课程名，课程学分和分数
select C.stuId 学号,C.courseId 课程号,courseName 课程名,courseMarks 课程学分,score 分数 from scoreInfo B
inner join courseInfo C on C.courseId = B.courseId
inner join stuInfo A on A.stuId = B.stuId



--7.查询出没有参加考试的学生的学号和姓名
select * from  stuInfo A
left join scoreInfo B on A.stuId=B.stuId 
where score IS NULL


--8.查询出是周六周天来报到的学生
select * from stuInfo 
where datepart(weekday,stuTime)=1 or datepart(weekday,stuTime)=7


--9.查询出姓名中有字母a的学生的信息
select * from stuInfo  where stuName like '%a%'


--10.查询出选修了2门课程以上的并且考试平均分在70以上的学生的学号和考试平均分以及选修课程的数量
select A.stuId 学号,AVG(score) 考试平均分,COUNT(B.stuId) 选课数量 from stuInfo A
inner join scoreInfo B on A.stuId = B.stuId 
group by A.stuId
