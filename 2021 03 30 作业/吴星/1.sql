use master
go

create database  Student
on
(
	name='Student',
	filename='D:\Student.mdf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%
)
log on
(
	name='Student_log',
	filename='D:\Student_log.ldf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%
)
go

use Student
go


create table stuInfo
(
	stuID int primary key identity(1,1),
	stuName char(10),
	stuAge int,
	stuSex int check(stuSex=1 or stuSex=0),
	time datetime,
)
go
create table courseInfo
(
	courseID int primary key identity(1,1),
	courseName char(20),  
	courseMarks int,
)
go

create table scoreInfo
(
	scoreID int primary key identity(1,1),
	stuID int references stuInfo(stuID),
	courseID int references courseInfo(courseID),
	score int
)
go

insert into stuInfo (stuName,stuAge,stuSex,time) values ('Tom',19,1,null),
('Jack',20,0,null),
('Rose',21,1,null),
('Lulu',19,1,null),
('Lili',21,0,null),
('abc',20,1,'2007-01-07')

select * from stuInfo 


insert into courseInfo (courseName,courseMarks) values ('JavaBase',4),
('HTML',2),
('JavaScript',2),
('SqlBase',2)

select * from courseInfo 

insert into scoreInfo (stuID,courseID,score) values (1,1,80),
(1,2,85),
(1,4,50),
(2,1,75),
(2,3,45),
(2,4,75),
(3,1,45),
(4,1,95),
(4,2,75),
(4,3,90),
(4,4,45)




--1.查询出 每个学生 所 选修的课程的数量和 所选修的课程的 考试的平均分
select stuName as 学生姓名,count(scoreInfo.stuID) as 选修的课程数 ,avg(score) as 考试的平均分 from scoreInfo  
full join stuInfo on scoreInfo.stuID=stuInfo.stuID
group by stuName


--2.查询出 每门课程的 选修的学生的个数和 学生考试的总分
select courseName,count(scoreInfo.courseID)选修的学生的个数,sum(score)学生考试的总分 from scoreInfo 
inner join courseInfo on scoreInfo.courseID=courseInfo.courseID 
group by courseName


--3.查询出性别一样并且年龄一样的学生的信息
select A.* from stuInfo A,stuInfo B 
where A.stuAge=B.stuAge and A.stuSex=B.stuSex and A.stuID !=B.stuID

--4.查询出学分一样的课程信息
select distinct  B.* from courseInfo A,courseInfo B
where A.courseMarks=B.courseMarks and A.courseID !=B.courseID and A.courseName !=B.courseName

--5.查询出参加了考试的学生的学号，姓名，课程号和分数
select stuInfo.stuID,stuName,courseID,score from stuInfo 
right join scoreInfo on scoreInfo.stuID=stuInfo.stuID

--6.查询出参加了考试的学生的学号，课程号，课程名，课程学分和分数
select stuName,scoreInfo.stuID,scoreInfo.courseID,courseName,courseMarks,score  from  scoreInfo 
inner join courseInfo on scoreInfo.courseID=courseInfo.courseID
inner join stuInfo on stuInfo.stuID=scoreInfo.stuID



--7.查询出没有参加考试的学生的学号和姓名
select  stuInfo.stuID,stuName from stuInfo full join scoreInfo on stuInfo.stuID =scoreInfo.stuID 
where score is null


--8.查询出是周六周天来报到的学生
select * from stuInfo
where datepart(weekday,time)=1 or datepart(WEEKDAY,time)=7

--9.查询出姓名中有字母a的学生的信息
select * from stuInfo where stuName like '%a%'


--10.查询出选修了2门课程以上的并且考试平均分在70以上的学生的学号和考试平均分以及选修课程的数量
select stuID as 学号,count(stuID)选修课程的数量,avg(score)考试平均分 from scoreInfo group by stuID having count(stuID)>2 and avg(score)>70