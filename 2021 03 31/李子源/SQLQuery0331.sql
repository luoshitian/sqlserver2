use master
go

create database bbs
on
(
	name='bbs',
	filename='D:\bbs.mdf',
	size=5,
	maxsize=50,
	filegrowth=10%
)
log on
(
	name='bbs_log',
	filename='D:\bbs_log.ldf',
	size=5,
	maxsize=50,
	filegrowth=10%
)
use bbs
go
create table tbl_card
(
	cardID varchar(10) primary key,
	passWord varchar(20),
	balance int ,
	userName varchar(10)
)
create table tbl_computer
(
	computerID varchar(10) primary key,
	onUse varchar check(onUse='0' or onUse='1'),
	note int
)
create table  tbl_record
(
	recordID int primary key,
	cardId varchar(10),
	ComputerId varchar(10),
	beginTime datetime,
	endTime datetime,
	fee int 
)


insert tbl_card values
('0023_ABC' , '555' , 98 , '张军'),
('0025_bbd' , 'abe' , 300 , '朱俊'),
('0036_CCD' , '何柳' , 100 , '何柳'),
('0045_YGR' , '0045_YGR' , 58 , '证验'),
('0078_RJV' , '55885fg' , 600 , '校庆'),
('0089_EDE' , 'zhang' , 134 , '张峻')
go
insert tbl_computer values
('02' , 0 , 25555),('03' , 1 , 55555),
('04' , 0 , 66666),('05' , 1 , 88888),
('06' , 0 , 688878),('B01' , 0 , 558558)
go
insert tbl_record values
(23 , '0078_RJV' , 'B01' , '2007-07-15 19:00:00' , '2007-07-15 21:00' , 20),
(34 , '0025_bbd' , '02' , '2006-12-25 18:00:00' , '2006-12-25 22:00:00' , 23),
(45 , '0023_ABC' , '03' , '2006-12-23 15:26:00' , '2006-12-23 22:55:00' , 50),
(46 , '0023_ABC' , '03' , '2006-12-22 15:26:00' , '2006-12-22 22:55:00' , 6),
(47 , '0023_ABC' , '03' , '2006-12-23 15:26:00' , '2006-12-23 22:55:00' , 50),
(48 , '0023_ABC' , '03' , '2007-01-06 15:26:00' , '2007-01-06 22:55:00' , 6),
(55 , '0023_ABC' , '03' , '2006-07-21 15:26:00' , '2006-07-21 22:55:00' , 50),
(64 , '0045-Y' , '04' , '2006-12-24 18:00:00' , '2006-12-24 22:00:00' , 3),
(65 , '0025_bbd' , '02' , '2006-12-28 18:00:00' , '2006-12-28 22:00:00' , 23),
(98 , '0025_bbd' , '02' , '2006-12-26 18:00:00' , '2006-12-26 22:00:00' , 23)
go


--1. 查询出用户名为'张军'的上网卡的上网记录，要求显示卡号，用户名，机器编号、开始时间、结束时间，和消费金额，并按消费金额降序排列
select A.cardID as 卡号 , userName as 用户名 , ComputerId as 机器编号 , beginTime as 开始时间 , endTime as 结束时间 , fee as 和消费金额
from tbl_record A inner join tbl_card B on A.cardID = B.cardID
where userName = '张军' order by fee desc
go

--2. 查询出每台机器上的上网次数和消费的总金额
select ComputerId , COUNT(ComputerId) , SUM(fee) from tbl_record group by ComputerId 
go

--3. 查询出所有已经使用过的上网卡的消费总金额
select A.cardID , SUM(fee) from tbl_record A 
inner join tbl_card B on A.cardID=B.cardID
group by A.cardID
go

--4. 查询出从未消费过的上网卡的卡号和用户名
select B.cardID , B.userName from tbl_record A
right join tbl_card B on A.cardID = B.cardID
except
select A.cardID , userName from tbl_record A
inner join tbl_card B on A.cardID = B.cardID
go

--5. 将密码与用户名一样的上网卡信息查询出来
select A.cardID , A.passWord , A.balance ,A.userName from tbl_card A , tbl_card B
where A.passWord = B.userName and A.cardID = B.cardID
go

--6. 查询出使用次数最多的机器号和使用次数
select top 1 ComputerId , COUNT(recordID) from tbl_record
group by ComputerId
order by COUNT(recordID) desc
go

--7. 查询出卡号是以'ABC'结尾的卡号，用户名，上网的机器号和消费金额
select A.cardId , userName , ComputerId , fee from tbl_record A
inner join tbl_card B on A.cardId = B.cardId
where A.cardId like('%ABC')
go

--8. 查询出是周六、周天上网的记录，要求显示上网的卡号，用户名，机器号，开始时间、结束时间和消费金额
select A.cardId , ComputerId , beginTime , endTime , fee from tbl_record A
inner join tbl_card B on A.cardId = B.cardId
where datepart(weekday,beginTime)=1 or datepart(weekday,beginTime)=7
go

--9. 查询出一次上网时间超过12小时的的上网记录，要求显示上网的卡号，用户名，机器号，开始时间、结束时间和消费金额
select A.cardId , ComputerId , beginTime , endTime , fee from tbl_record A
inner join tbl_card B on A.cardId = B.cardId
where DATEDIFF(HH,beginTime,endTime)>12
go

--10. 查询除消费金额排列前三名(最高)的上网记录，要求显示上网的卡号，用户名，机器号，开始时间、结束时间和消费金额
select top 7 cardId 卡号, userName 用户名,  ComputerId 机器号, beginTime 开始时间,endTime 结束时间 , fee 消费金额  from tbl_record 
	left join tbl_card C on cardId=C.cardID  
		order by fee