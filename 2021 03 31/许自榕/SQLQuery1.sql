create database tbl
on(
name='tbl',
FILENAME='D:\tbl.mdf',
SIZE=5MB,
MAXSIZE=100MB,
filegrowth=10%

)
log on 
(
name='tbl_log',
FILENAME='D:\tbl_log.ldf',
SIZE=5MB,
MAXSIZE=100MB,
filegrowth=10%
)
go
use tbl
 create table tbl_card
 (
 cardid nvarchar(20)  primary key ,
 passWord nvarchar(20),
 balance nvarchar(20),
 userName nvarchar(20)
 )
 create table tbl_computer
 (
 computerid nvarchar(20) primary key ,
 onUse nvarchar check(onUse=0 or onUse=1),
 note text,
 )
 create table tbl_record
 (
 id nvarchar(20) primary key ,
 cardId nvarchar(20) references tbl_card(cardid),
 ComputerId nvarchar(20) references  tbl_computer(computerid),
 beginTime datetime,
 endTime datetime,
 fee money
 )
 
 insert into tbl_card (cardid,passWord,balance,userName) values('0023_ABC','555','98','张军'
 ),('0025_bbd','abe','300','朱俊'),('0036_ccd','何柳','100','何柳'),('0045_YGR','0045_YGR','58','验证'),
 ('0078_RJV','55885fg','600','校庆'), ('0089_EDE','zhang','134','张峻')
  insert into  tbl_computer (computerid,onUse,note) values('02','0','25555'),('03','1','55555'),
  ('04','0','666666'),('05','1','888888'),('06','0','688878'),('B01','0','558558')
  insert into   tbl_record (id,cardid,computerid,beginTime,endTime,fee) values('23','0078_RJV','B01','2007-7-15 19:00:00','2007-7-15 21:00','20'),
  ('34','0025_bbd','02','2006-12-25 18:00','2006-12-25 22:00','23'),
  ('45','0023_ABC','03','2006-12-23 15:26:00','2006-12-23 22:55:00','50')
  ,('46','0023_ABC','03','2006-12-22 15:26:00','2006-12-22 22:55:00','6'),
  ('47','0023_ABC','03','2006-12-23 15:26:00','2006-12-23 22:55:00','50'),
  ('48','0023_ABC','03','2006-01-06 15:26:00','2006-01-06 22:55:00','6'),
  ('55','0023_ABC','03','2006-07-21 15:26:00','2006-07-21 22:55:00','50'),
  ('64','0045_YGR','04','2006-12-24 20:00:00','2006-12-24 22:00:00','3')
  ,('65','0025_bbd','02','2006-12-28 20:00:00','2006-12-28 22:00:00','23'),
  ('98','0025_bbd','02','2006-12-26 20:00:00','2006-12-26 22:00:00','23')

--请完成以下题目：
select * from tbl_card
select * from tbl_computer
select * from tbl_record

--1. 查询出用户名为'张军'的上网卡的上网记录，要求显示卡号，用户名，机器编号、开始时间、结束时间，和消费金额，并按消费金额降序排列
select tbl_card.cardid,userName,ComputerId,beginTime,endTime,fee from  tbl_record inner join tbl_card on tbl_record.cardId=tbl_card.cardid where userName='张军' group by tbl_card.cardid,userName,ComputerId,beginTime,endTime,fee  order by fee desc
--2. 查询出每台机器上的上网次数和消费的总金额
select ComputerId, count (*) 次数,sum(fee) 总金额 from tbl_record group by ComputerId
--3. 查询出所有已经使用过的上网卡的消费总金额
select cardId,SUM(fee) 总金额 from tbl_record  inner join tbl_computer on tbl_record.ComputerId=tbl_computer.ComputerId where onUse=1 group by cardId
--4. 查询出从未消费过的上网卡的卡号和用户名
select cardId,SUM(fee) 总金额 from tbl_record  inner join tbl_computer on tbl_record.ComputerId=tbl_computer.ComputerId where onUse=0 group by cardId
--5. 将密码与用户名一样的上网卡信息查询出来
 
select  A.* from tbl_card A,tbl_card B where A.cardid=B.cardid  AND A.passWord=B.userName 
--6. 查询出使用次数最多的机器号和使用次数
select top 1 ComputerId 机械号,count (*) 使用次数 from tbl_record group by ComputerId order by 使用次数 desc
--7. 查询出卡号是以'ABC'结尾的卡号，用户名，上网的机器号和消费金额
select tbl_record.cardid 卡号,userName 用户名,ComputerId 机械号,fee 消费金额  from tbl_record inner join tbl_card on tbl_record.cardId=tbl_card.cardid where tbl_card.cardid like '%ABC'
--8. 查询出是周六、周天上网的记录，要求显示上网的卡号，用户名，机器号，开始时间、结束时间和消费金额
select * from tbl_record
where datepart (WEEKDAY,beginTime)=1 or DATEPART (WEEKDAY,endTime)=7
UNION
select tbl_record.cardid 卡号,userName 用户名,ComputerId 机械号,beginTime 开始时间,endTime 结束时间,  fee 消费金额  from tbl_record inner join tbl_card on tbl_record.cardId=tbl_card.cardid
--9. 查询成一次上网时间超过12小时的的上网记录，要求显示上网的卡号，用户名，机器号，开始时间、结束时间和消费金额
select tbl_record.cardid 卡号,userName 用户名,ComputerId 机械号,beginTime 开始时间,endTime 结束时间,  fee 消费金额  from tbl_record inner join tbl_card on tbl_record.cardId=tbl_card.cardid WHERE endTime-beginTime>12
--10. 查询除消费金额排列前三名(最高)的上网记录，要求显示上网的卡号，用户名，机器号，开始时间、结束时间和消费金额
select top 3 tbl_record.cardid 卡号,userName 用户名,ComputerId 机械号,beginTime 开始时间,endTime 结束时间,  fee 消费金额  from tbl_record inner join tbl_card on tbl_record.cardId=tbl_card.cardid  order by fee desc
