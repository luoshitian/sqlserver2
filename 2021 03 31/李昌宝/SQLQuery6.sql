create database lzl
on primary
(
	name = 'lzl',
	size = 5,
	maxsize = 500,
	filename = 'D:\新建文件夹\.mdf',
	filegrowth = 10%
)
log on
(
	name = 'lzl_log',
	size = 5,
	maxsize = 500,
	filename = 'D:\新建文件夹\.ldf',
	filegrowth = 10%
)
use lzl
go
create table tbl_card
(
	id varchar(10) primary key,
	password varchar(10) not null,
	balance money check(balance>0),
	username varchar(10)
)
go
create table tbl_computer
(
     id varchar(5) primary key,
	 onsure varchar(1) check(onsure=0 or onsure=1),
	 note varchar(10)
)
go
create table tbl_record
(
	id varchar(5) primary key,
	cardId varchar(10) references tbl_card(id),
	ComputerId varchar(5) references tbl_computer(id),
	beginTime smalldatetime,
	endTime smalldatetime,
	fee money
)
--表一为上网卡信息表(tbl_card)
--id: 卡号，主键
--passWord：密码
--balance：卡上的余额
--userName：该上网卡所属的用户名
insert into tbl_card(id,password,balance,userName) values ('0023_ABC','555',98,'张军'),('0025_CCD','abc',300,'朱柳'),('0036_CCD','何柳',100,'何柳'),
('0045_YGR','0045_YGR',58,'证言'),('0078_RJV','55885fg',600,'校庆'),('0089_EDE','zhang',134,'校庆')
--表2为网吧机器说明表(tbl_computer)
--id：机器编号，主键
--onUse：该机器是否正在使用，0为未使用，1为正在使用
--note：该机器的说明
insert into tbl_computer(id,onsure,note) values('02',0,25555),('03',1,55555),('04',0,66666),('05',1,88888),('06',0,688878),('B01',0,558558)
--表3为上网记录表(tbl_record)：
--id：记录编号，主键
--cardId：本次上网的卡号，外键
--ComputerId：本次上网记录所使用的机器号，外键
--beginTime：本次上网记录的开始时间
--endTime：本次上网记录的结束时间
--fee：本次上网的费用
insert into tbl_record(id,cardId,ComputerId,beginTime,endTime,fee)
select 23,'0078_RJV','B01','2007-07-15 19:00:00','2007-07-15 21:00:00',20  union
select 34,'0025_CCD','02','2006-12-25 18:00:00','2006-12-25 22:00:00',23  union
select 45,'0023_ABC','03','2006-12-23 15:26:00','2006-12-23 22:55:00',50  union
select 46,'0023_ABC','03','2006-12-22 15:26:00','2006-12-22 22:55:00',6  union
select 47,'0023_ABC','03','2006-12-23 15:26:00','2006-12-23 22:55:00',50  union
select 48,'0023_ABC','03','2007-01-06 15:26:00','2007-01-06 22:55:00',6  union
select 55,'0023_ABC','03','2006-07-21 15:26:00','2006-7-21 22:55:00',50  union
select 64,'0045_YGR','04','2006-12-24 18:00:00','2006-12-24 18:00:00',300  union
select 65,'0025_CCD','02','2006-12-28 18:00:00','2006-12-28 18:00:00',23  union
select 98,'0025_CCD','02','2006-12-26 18:00:00','2006-12-26 22:00:00',23  
insert into tbl_record(id,cardId,ComputerId,beginTime,endTime,fee) values (100,'0078_RJV','B01','2007-07-15 19:00:00','2007-07-18 21:00:00',20)
select * from tbl_card
select * from tbl_record
select * from tbl_computer
--1. 查询出用户名为'张军'的上网卡的上网记录，要求显示卡号，用户名，机器编号、开始时间、结束时间，和消费金额，并按消费金额降序排列
select a.id 卡号,a.username 用户名, b.ComputerId 机器编号,b.beginTime 开始时间,b.endTime 结束时间,fee 消费金额 from tbl_card A inner join tbl_record B 
on a.id=b.cardId where username='张军' order by fee desc
--2. 查询出每台机器上的上网次数和消费的总金额
select b.ComputerId,count(b.ComputerId) 上网总次数, sum(fee) 消费总金额 from tbl_computer a inner join tbl_record b on a.id=b.ComputerId group by b.ComputerId
--3. 查询出所有已经使用过的上网卡的消费总金额)
select cardId 卡号, count(cardId) 该卡使用次数, sum(fee) 消费总金额 from tbl_record group by cardId
--4. 查询出从未消费过的上网卡的卡号和用户名
select id 卡号,userName 用户名 from tbl_card 
except
select cardId 卡号,userName from tbl_record a inner join tbl_card b on a.cardId=b.id
--5. 将密码与用户名一样的上网卡信息查询出来
select a.* from tbl_card a,tbl_card b where a.password=b.username 
--6. 查询出使用次数最多的机器号和使用次数
select top 1 cardId,count(cardId) 使用次数 from tbl_record  group by cardId order by count(cardId) desc
--7. 查询出卡号是以'ABC'结尾的卡号，用户名，上网的机器号和消费金额
select a.id,a.username 用户名, b.ComputerId 上网机器号, fee from tbl_card a inner join tbl_record b on a.id=b.cardId where a.id like '%ABC'
--8. 查询出是周六、周天上网的记录，要求显示上网的卡号，用户名，机器号，开始时间、结束时间和消费金额
select a.id 卡号,a.username 用户名,b.ComputerId 机器号,b.beginTime,b.endTime,b.fee 消费金额 from tbl_card a inner join tbl_record b on a.id=b.cardId
where datename(DW,beginTime)='星期六' or datename(DW,beginTime)='星期日'
--9. 查询成一次上网时间超过12小时的的上网记录，要求显示上网的卡号，用户名，机器号，开始时间、结束时间和消费金额
select a.id,a.username,b.beginTime,b.endTime,fee 消费金额 from tbl_card a inner join tbl_record b on a.id=b.cardId where datediff(hour,beginTime,endTime)>12
--10. 查询除消费金额排列前三名(最高)的上网记录，要求显示上网的卡号，用户名，机器号，开始时间、结束时间和消费金额
select top 3 a.id 上网卡号,a.username 用户名,b.ComputerId 机器号, b.beginTime 开始时间 ,b.endTime 结束时间,fee 消费金额 from tbl_card a inner join tbl_record b 
on a.id=b.cardId order by fee desc




