create database WB
on
(
	name='WB',
	filename='D:\WB.mdf',
	size=20,
	maxsize=300,
	filegrowth=50
)
log on
(
	name='WB_log',
	filename='D:\WB_log.ldf',
	size=20,
	maxsize=300,
	filegrowth=50
)
GO
use WB
go

create table tbl_card
(
	id nvarchar(20) primary key ,
	password nvarchar(20) not null,
	balance money,
	userName nvarchar(10) not null
)

insert into tbl_card (id,password,balance,userName) values('0023_ABC','555','98','张军'),('0025_bbd','abc','300','朱俊'),('0036_CCD','何柳','100','何柳'),('0045_YGR','0045_YGR','58','证验'),('0078_RJV','55885fg','600','校庆'),('0089_EDE','zhang','134','张俊')

select * from tbl_card

create table tbl_computer
(
	id nvarchar(20) primary key ,
	onUse nvarchar(1) check(onUse='0' or onUse='1'),
	note int
)

insert into tbl_computer (id,onUse,note) values('02','0',25555),
('03','1',55555),
('04','0',66666),
('05','1',88888),
('06','0',688878),
('B01','0',558558)

select * from tbl_computer

create table tbl_record
(
	id nvarchar(20) primary key ,
	cardId nvarchar(20) references tbl_card(id),
	ComputerId nvarchar(20)references tbl_computer(id),
	beginTime datetime,
	endTime datetime,
	fee money
)

select * from tbl_card
select * from tbl_computer
select * from tbl_record

insert into tbl_record (id,cardId,ComputerId,beginTime,endTime,fee) values
('23','0078_RJV','B01','2007-07-15','2007-07-15','20'),
('34','0025_bbd','02','2006-12-25','2006-12-25','23'),
('45','0023_ABC','03','2006-12-23','2006-12-23','50'),
('46','0023_ABC','03','2006-12-22','2006-12-22','6'),
('47','0023_ABC','03','2006-12-23','2006-12-23','50'),
('48','0023_ABC','03','2007-01-06','2007-01-06','6'),
('55','0023_ABC','03','2006-07-21','2006-07-21','50'),
 ('64','0045_YGR','04','2006-12-24','2006-12-24','3'),
('65','0025_bbd','02','2006-12-28','2006-12-28','23'),
('98','0025_bbd','02','2006-12-26','2006-12-26','23')

select * from tbl_card
select * from tbl_computer
select * from tbl_record

--1. 查询出用户名为'张军'的上网卡的上网记录，要求显示卡号，用户名，机器编号、开始时间、结束时间，和消费金额，并按消费金额降序排列
select CA.id 卡号,ComputerId 机器编号,userName 用户名,beginTime 开始时间,endTime 结束时间,fee 费用 from tbl_record RE
inner join tbl_card CA on RE.id=CA.id  
where userName='张军' order by fee DESC 
--2. 查询出 每台机器上的上网次数 和 消费的总金额
select ComputerId,count(ComputerId)上网次数,sum(fee)总金额 from tbl_record group by ComputerId
--3. 查询出所有已经使用过的上网卡的消费总金额
select ComputerId,sum(fee)总金额 from tbl_record group by ComputerId 
--4. 查询出从未消费过的上网卡的卡号和用户名
select CA.id,userName ,fee from tbl_record RE left join tbl_card CA on RE.id=CA.id where fee is null
--5. 将密码与用户名一样的上网卡信息查询出来
select * from tbl_card A,tbl_card B where A.userName=B.password
--6. 查询出使用次数最多的机器号和使用次数
select top 1 ComputerId,count(ComputerId) 次数 from tbl_record group by ComputerId order by count(ComputerId) DESC
--7. 查询出卡号是以'ABC'结尾的卡号，用户名，上网的机器号和消费金额
select CA.id,userName,ComputerId,fee from tbl_card CA inner join tbl_record RE on CA.id=RE.id where CA.id like '%ABC'
--8. 查询出是周六、周天上网的记录，要求显示上网的卡号，用户名，机器号，开始时间、结束时间和消费金额
select * from tbl_record where datepart(WEEKDAY,beginTime)=1 or datepart(WEEKDAY,beginTime)=7
--9. 查询成一次上网时间超过12小时的的上网记录，要求显示上网的卡号，用户名，机器号，开始时间、结束时间和消费金额
select * from tbl_record where is (beginTime - endTime)>12
--10. 查询除消费金额排列前三名(最高)的上网记录，要求显示上网的卡号，用户名，机器号，开始时间、结束时间和消费金
select TOP 7 * from tbl_record order by fee ASC