create database A
go
create table stuinfo
(
	stuID int primary key identity,
	stuName varchar(20),
	stuAge int,
	stuSex int,
	Time datetime
)
create table courseinfo
(
	courseID int primary key identity,
	courseName varchar(20),
	courseMarks int
)
create table scoreinfo
(
	scoreID int primary key identity,
	stuID int references stuinfo(stuID),
	courseID int references courseinfo(courseID),
	score int
)


insert into stuinfo(stuName,stuAge,stuSex,time)
select 'Tom',19,1,NULL union
select 'Jack',20,0,NULL union
select 'Rose',21,1,NULL union
select 'Lulu',19,1,NULL union
select 'Lili',21,0,NULL union
select 'abc',20,1,'2007-01-07'


insert into courseinfo(courseName,courseMarks)
select 'JavaBase',4 union
select 'HTML',2 union
select 'JavaScipt',2 union
select 'SqlBase',2

insert into scoreinfo(stuID,courseID,score)
select 1,1,80 union
select 1,2,85 union
select 1,4,50 union
select 2,1,75 union
select 2,3,45 union
select 2,4,75 union
select 3,1,45 union
select 4,1,95 union
select 4,2,75 union
select 4,3,90 union
select 4,4,45

select * from stuinfo
select * from courseinfo
select * from scoreinfo
--avg(score)
--题目：
--1.查询出每个学生所选修的课程的数量和所选修的课程的考试的平均分
select stuName,count(*),avg(score) from stuinfo a inner join scoreinfo b on a.stuID=b.stuID inner join courseinfo c on a.stuID=c.courseID 
group by stuName

--2.查询出每门课程的选修的学生的个数和学生考试的总分
select courseName,count(*) 个数,SUM(score) 总分 from courseinfo a inner join scoreinfo b on a.courseID=b.courseID group by courseName
--3.查询出性别一样并且年龄一样的学生的信息
select * from stuinfo a,stuinfo b where a.stuAge=b.stuAge and a.stuSex=b.stuSex and a.stuID!=b.stuID
--4.查询出学分一样的课程信息
select * from courseinfo a,courseinfo b where a.courseMarks=b.courseMarks and a.courseID!=b.courseID
--5.查询出参加了考试的学生的学号，姓名，课程号和分数
select a.stuID,stuName,b.courseID,score from stuinfo a inner join scoreinfo b on a.stuID=b.stuID inner join courseinfo c on a.stuID=c.courseID
--6.查询出参加了考试的学生的学号，课程号，课程名，课程学分和分数
select a.stuID,b.courseID,courseName,courseMarks,score from stuinfo a inner join scoreinfo b on a.stuID=b.stuID inner join courseinfo c on a.stuID=c.courseID
--7.查询出没有参加考试的学生的学号和姓名
select a.stuID,stuName from stuinfo a left join scoreinfo b on a.stuID=b.stuID left join courseinfo c on a.stuID=c.courseID

--8.查询出是周六周天来报到的学生

--9.查询出姓名中有字母a的学生的信息

--10.查询出选修了2门课程以上的并且考试平均分在70以上的学生的学号和考试平均分以及选修课程的数量
