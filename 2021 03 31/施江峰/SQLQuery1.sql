use master
go

create database WB
on
(
name='WB',
filename='D:\test\WB.mdf',
size=10mb,
maxsize=50mb,
filegrowth=10%
)
log on
(
name='WB_log',
filename='D:\test\WB_log.ldf',
size=10mb,
maxsize=50mb,
filegrowth=10%
)

go
use WB

go
create table tbl_card
(
id nvarchar(20) primary key,
passWord varchar(20),
balance money,
userName nvarchar(20)
)

go
create table tbl_computer
(
id varchar(20) primary key,
onUse varchar(10) check(onUse=1 or onUse=0),
note text
)

go
create table tbl_record
(
id int primary key,
cardId nvarchar(20) references tbl_card(id),
ComputerId varchar(20) references tbl_computer(id),
beginTime datetime,
endTime datetime,
fee money
)

go
insert into tbl_card values
('0023_ABC','555',98,'张军'),
('0025_bbd','abe',300,'朱俊'),
('0036_CCD','何柳',100,'何柳'),
('0045_YGR','0045_YGR',58,'验证'),
('0078_RJV','55885fg',600,'校庆'),
('008_EDE','zhang',134,'张峻')

go
insert into tbl_computer values
('02',0,'25555'),
('03',1,'55555'),
('04',0,'66666'),
('05',1,'88888'),
('06',0,'688878'),
('B01',0,'558558')

go
insert into tbl_record values
(23,'0078_RJV','B01','2007-07-15 19:00:00','2007-07-15 21:00:00',20),
(34,'0025_bbd','02','2006-12-25 18:00:00','2006-12-25 22:00:00',23),
(45,'0023_ABC','03','2006-12-23 15:26:00','2006-12-23 22:55:00',50),
(46,'0023_ABC','03','2006-12-22 15:26:00','2006-12-22 22:55:00',6),
(47,'0023_ABC','03','2006-12-23 15:26:00','2006-12-23 22:55:00',50),
(48,'0023_ABC','03','2007-01-06 15:26:00','2007-01-06 22:55:00',6),
(55,'0023_ABC','03','2006-07-21 15:26:00','2006-07-21 15:26:00',50),
(64,'0045_YGR','04','2006-12-24 18:00:00','2006-12-24 22:00:00',3),
(65,'0025_bbd','02','2006-12-28 18:00:00','2006-12-28 22:00:00',23),
(98,'0025_bbd','02','2006-12-26 18:00:00','2006-12-26 22:00:00',23)

select * from tbl_card
select * from tbl_computer
select * from tbl_record

--请完成以下题目：
--1. 查询出用户名为'张军'的上网卡的上网记录，要求显示卡号，用户名，机器编号、开始时间、结束时间，和消费金额，并按消费金额降序排列
select userName 用户名,tbl_card.id 卡号,tbl_record.ComputerId 机器编号,beginTime 开始时间,endTime 结束时间,fee 消费金额 from tbl_card inner join tbl_record on tbl_card.id = tbl_record.cardId where userName='张军' order by tbl_record.fee desc 

--2. 查询出每台机器上的上网次数和消费的总金额
select tbl_computer.id 机器编号, COUNT(ComputerId) 上网次数, SUM(fee) 消费总金额 from tbl_computer left join tbl_record on tbl_record.ComputerId =tbl_computer.id group by tbl_computer.Id 

--3. 查询出所有已经使用过的上网卡的消费总金额
select tbl_card.id 网卡号,SUM(fee) 消费总金额 from tbl_card inner join tbl_record on tbl_card.id = tbl_record.cardId group by tbl_card.id

--4. 查询出从未消费过的上网卡的卡号和用户名
select id 网卡号,userName 用户名 from tbl_card 
except
select tbl_record.cardId 网卡号,userName 用户名 from tbl_record inner join tbl_card on tbl_card.id = tbl_record.cardId

select tbl_card.id 卡号, userName 用户名,fee 消费金额 from tbl_card  left join tbl_record tbl_record on tbl_card.id=tbl_record.cardId where fee is null

--5. 将密码与用户名一样的上网卡信息查询出来
select A. * from tbl_card A,tbl_card B where A.passWord=B.userName

--6. 查询出使用次数最多的机器号和使用次数
select top 1 ComputerId 机器号,COUNT(cardId) 使用次数 from tbl_record group by ComputerId order by COUNT(cardId) desc

--7. 查询出卡号是以'ABC'结尾的卡号，用户名，上网的机器号和消费金额
select tbl_card.id 卡号,userName 用户名,ComputerId 机器号,fee 消费金额 from tbl_card inner join tbl_record on tbl_card.id=tbl_record.cardId where tbl_card.id like'%ABC%'

--8. 查询出是周六、周天上网的记录，要求显示上网的卡号，用户名，机器号，开始时间、结束时间和消费金额
select cardId 网卡号,userName 用户名,ComputerId 机器号,beginTime 开始时间,endTime 结束时间,fee 消费金额 from tbl_record inner join tbl_card on tbl_record.cardId=tbl_card.id where datename(DW,beginTime)='星期六' or datename(DW,beginTime)='星期天'

--9. 查询成一次上网时间超过12小时的的上网记录，要求显示上网的卡号，用户名，机器号，开始时间、结束时间和消费金额
select tbl_card.id 卡号,userName 用户名,ComputerId 机器号,beginTime 开始时间,endTime 结束时间,fee 消费金额 from tbl_card inner join tbl_record on tbl_card.id = tbl_record.cardId where datediff(hour,endTime,beginTime)>12

--10. 查询除消费金额排列前三名(最高)的上网记录，要求显示上网的卡号，用户名，机器号，开始时间、结束时间和消费金额
select top 3 tbl_card.id 卡号,userName 用户名,ComputerId 机器号,beginTime 开始时间,endTime 结束时间,fee 消费金额  from tbl_record 
left join  tbl_card on tbl_card.id = tbl_record.cardId
order by fee desc