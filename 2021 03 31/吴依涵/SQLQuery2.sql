
use master
go
create database WangBa
on
(
 name='WangBa',
 filename='D:\新建文件夹3.mdf',
 size=6MB,
 maxsize=100MB,
 filegrowth=10Mb
)
log on
(
 name='WangBa_log',
 filename='D:\新建文件夹3_1og.ldf',
 size=6MB,
 maxsize=100MB,
 filegrowth=10Mb
)
go
use WangBa
go
create table tbl_card
(
id varchar(20) primary key,
passWord varchar(20),
balance money,
userName varchar(20)
)
go
use WangBa
go
create table tbl_computer
(
id varchar(20) primary key,
onUse int check(onUse=0 or onUse=1),
note text
)
go
use WangBa
go
create table tbl_record
(
id varchar(20) primary key,
cardId varchar(20) references tbl_card(id),
ComputerId varchar(20) references tbl_computer(id),
beginTime datetime,
endTime datetime,
fee money
)
insert into tbl_card values
('0023_ABC','555',98,'张军'),
('0025_bbd','abe',300,'朱俊'),
('0036_CCD','何柳',100,'何柳'),
('0045_YGR','0045_YGR',58,'证验'),
('0078_RJV','55885fg',600,'校庆'),
('0089_EDE','zhang',134,'张峻')

insert into tbl_computer values
('02',0,'25555'),
('03',1,'55555'),
('04',0,'66666'),
('05',1,'88888'),
('06',0,'688878'),
('B01',0,'558558')

insert into tbl_record values
('23','0078_RJV','B01','2007-7-15 19:00:00','2007-7-15 21:00:00',20),
('34','0025_bbd','02','2006-12-25 18:00:00','2006-12-25 22:00:00',23),
('45','0023_ABC','03','2006-12-23 15:26:00','2006-12-25 22:55:00',50),
('46','0023_ABC','03','2006-12-23 15:26:00','2006-12-25 22:55:00',6),
('47','0023_ABC','03','2006-12-23 15:26:00','2006-12-25 22:55:00',50),
('48','0023_ABC','03','2006-12-23 15:26:00','2006-12-25 22:55:00',6),
('55','0023_ABC','03','2006-12-23 15:26:00','2006-12-25 22:55:00',50),
('64','0045_YGR','04','2006-12-24 18:00:00','2006-12-25 22:00:00',3),
('65','0025_bbd','02','2006-12-28 18:00:00','2006-12-25 22:00:00',23),
('98','0025_bbd','02','2006-12-26 18:00:00','2006-12-25 22:00:00',23)

--1. 查询出用户名为'张军'的上网卡的上网记录，要求显示卡号，用户名，机器编号、开始时间、结束时间，和消费金额，并按消费金额降序排列
select C.id 卡号,C.userName 用户名,D.id 机器编号,L.beginTime 开始时间,L.endTime 结束时间,L.fee 消费金额 from tbl_record L inner join tbl_card C on L.cardId=C.id inner join tbl_computer D on L.ComputerId=D.id
where C.userName='张军' order by L.fee DESC

--2. 查询出 每台机器 上的 上网次数 和 消费的总金额
select D.id 机器,count(*)上网次数,sum(fee)消费的总金额  from tbl_computer D left join tbl_record L on D.id=L.ComputerId group by D.id

--3. 查询出所有已经使用过的上网卡的消费总金额
select cardId 卡号,sum(fee) 消费总金额 from tbl_record group by cardId

--4. 查询出从未消费过的上网卡的卡号和用户名
select id 卡号,userName 用户名 from tbl_card 
except
select C.id 卡号,C.userName 用户名 from tbl_record L inner join tbl_card C on C.id=L.cardId

--5. 将密码与用户名一样的上网卡信息查询出来
select A.passWord 密码,A.userName 用户名 from tbl_card A,tbl_card B where A.passWord=B.userName

--6. 查询出使用次数最多的机器号和使用次数
select top 1 L.ComputerId 机器号, count(*)使用次数 from tbl_record L inner join tbl_computer D on L.ComputerId=D.id group by ComputerId order by count(*) DESC

--7. 查询出卡号是以'ABC'结尾的卡号，用户名，上网的机器号和消费金额
select * from tbl_card C left join tbl_record L on C.id=L.cardId where C.id like '%ABC'  

--8. 查询出是周六、周天上网的记录，要求显示上网的卡号，用户名，机器号，开始时间、结束时间和消费金额
select C.id 卡号,C.userName 用户名,D.id 机器号,L.beginTime 开始时间,L.endTime 结束时间,fee 消费金额 from tbl_record L inner join tbl_card C on L.cardId=C.id inner join tbl_computer D on L.ComputerId=D.id
where datename(dw,beginTime)='星期六'or datename(dw,beginTime)='星期日'

--9. 查询成一次上网时间超过12小时的的上网记录，要求显示上网的卡号，用户名，机器号，开始时间、结束时间和消费金额
select C.id 卡号,C.userName 用户名,D.id 机器号,L.beginTime 开始时间,L.endTime 结束时间,L.fee 消费金额 from tbl_record L inner join tbl_card C on L.cardId=C.id inner join tbl_computer D on L.ComputerId=D.id
where datediff(hh,beginTime,endTime)>12

--10. 查询除消费金额排列前三名(最高)的上网记录，要求显示上网的卡号，用户名，机器号，开始时间、结束时间和消费金额
select C.id 卡号,C.userName 用户名,D.id 机器号,L.beginTime 开始时间,L.endTime 结束时间,L.fee 消费金额 from tbl_record L inner join tbl_card C on L.cardId=C.id inner join tbl_computer D on L.ComputerId=D.id
except
select C.id 卡号,C.userName 用户名,D.id 机器号,L.beginTime 开始时间,L.endTime 结束时间,L.fee 消费金额 from tbl_record L inner join tbl_card C on L.cardId=C.id inner join tbl_computer D on L.ComputerId=D.id
where fee=(select max(fee) from tbl_record)

select * from tbl_card
select * from tbl_computer
select * from tbl_record