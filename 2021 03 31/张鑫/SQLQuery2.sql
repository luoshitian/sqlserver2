use master
go

create database Tbl
go

use Tbl
go

create table Tbl_card
(
	CardId varchar(10) primary key ,
	PassWord varchar(10) not null ,
	Balance int ,
	UserName varchar(10) not null 
) 
go

create table Tbl_computer
(
	ComputerId varchar(10) primary key ,
	OnUse int check(OnUse in(0,1)) not null ,
	Note varchar(100)
)
go

create table Tbl_record
(
	RecordId int primary key ,
	CardId varchar(10) references Tbl_Card(CardId) ,
	ComputerId varchar(10) references Tbl_computer(ComputerId) ,
	BeginTime datetime ,
	EndTime datetime ,
	Fee int 
)
go

insert Tbl_card values
('0023_ABC','555',98,'张军'),
('0025_bbd','abe',300,'朱俊'),
('0036_CCD','何柳',100,'何柳'),
('0045_YGR','0045_YGR',58,'证验'),
('0078_RJV','55885fg',600,'校庆'),
('0089_EDE','zhang',134,'张峻')
go

insert Tbl_computer values 
('02',0,'25555'),
('03',1,'55555'),
('04',0,'66666'),
('05',1,'88888'),
('06',0,'688878'),
('B01',0,'558558')
go

insert Tbl_record values 
(23,'0078_RJV','B01','2007-07-15 19:00:00','2007-07-15 21:00:00',20),
(34,'0025_bbd','02','2006-12-25 18:00:00','2006-12-25 22:00:00',23),
(45,'0023_ABC','03','2006-12-23 15:26:00','2006-12-23 22:55:00',50),
(46,'0023_ABC','03','2006-12-22 15:26:00','2006-12-23 22:55:00',6),
(47,'0023_ABC','03','2006-12-23 15:26:00','2006-12-22 22:55:00',50),
(48,'0023_ABC','03','2007-01-06 15:26:00','2007-01-06 22:55:00',6),
(55,'0023_ABC','03','2006-07-21 15:26:00','2006-07-21 22:55:00',50),
(64,'0045_YGR','04','2006-12-24 18:00:00','2006-12-24 22:00:00',30),
(65,'0025_bbd','02','2006-12-28 18:00:00','2006-12-28 22:00:00',23),
(98,'0025_bbd','02','2006-12-26 18:00:00','2006-12-26 22:00:00',23)
go

select * from Tbl_card
select * from Tbl_computer
select * from Tbl_record

select Tbl_record.CardId,userName,ComputerId,beginTime,endTime,fee from tbl_record 
inner join tbl_card on Tbl_card.CardId=Tbl_record.CardId where UserName='张军'ORDER BY fee DESC

select CardId,count(ComputerId)次数,sum(Fee)总消费 from Tbl_record 
group by ComputerId,CardId

select Tbl_computer.ComputerId,sum(Fee)总消费 from tbl_record 
inner join Tbl_computer on Tbl_computer.ComputerId=tbl_record.ComputerId group by Tbl_computer.ComputerId

select tbl_card.CardId,userName from tbl_card 
left join  tbl_record on  tbl_record.cardId=tbl_card.CardId where fee is null

select PassWord,UserName from tbl_card where PassWord=UserName

select top 1 computerID,count(computerID)次数 from Tbl_record
group by ComputerId order by count(computerID)desc

select Tbl_card.CardId,UserName,ComputerId,Fee from Tbl_card
inner join Tbl_record on Tbl_record.CardId=Tbl_card.CardId
where Tbl_card.cardId like '%_ABC'

select Tbl_card.CardId,UserName,ComputerId,BeginTime,EndTime,Fee from Tbl_card
inner join Tbl_record on Tbl_record.CardId=Tbl_card.CardId
where datepart(weekday,endTime)=1 or datepart(weekday,endTime)=7

select Tbl_card.CardId,UserName,ComputerId,BeginTime,EndTime,Fee from Tbl_card
inner join Tbl_record on Tbl_record.CardId=Tbl_card.CardId
where datediff(HH,beginTime,endTime) >12

select Tbl_card.CardId,UserName,ComputerId,BeginTime,EndTime,Fee from Tbl_card
inner join Tbl_record on Tbl_record.CardId=Tbl_card.CardId
except
select top 3 Tbl_card.CardId,userName,ComputerId,beginTime,endTime,fee from tbl_record inner join tbl_card on tbl_record.cardId=tbl_card.cardId order by fee desc