--首先创建网吧计费系统的数据库，表结构和数据如2.bmp所示，表的说明如下：
create database WB
on
(
   name='WB',
   filename='D:\WB.mdf',
   size=50,
   maxsize=100,
   filegrowth=10
)
log on
(
   name='WB_log',
   filename='D:\WB_log.ldf',
   size=50,
   maxsize=100,
   filegrowth=10
)
go
--表一为上网卡信息表(tbl_card)
--ID: 卡号，主键
--PassWord：密码
--Balance：卡上的余额
--UserName：该上网卡所属的用户名
use WB
go
create table tbl_card
(
   ID char(10) not null primary key,
   PassWord nchar(10) not null,
   Balance int  not null,
   UserName nchar(5) not null
)
insert into tbl_card values ('0023_ABC','555','98','张军'),('0025_bbd','abe','300','朱俊'),('0036_CCD','何柳','100','何柳'),('0045_YGR','0045_YGR','58','证验'),('0078_RJV','55885fg','600','校庆'),('0089_EDE','zhang','134','张峻')
--表2为网吧机器说明表(tbl_computer)
--ID：机器编号，主键
--OnUse：该机器是否正在使用，0为未使用，1为正在使用
--NOte：该机器的说明
create table tbl_computer
(
   ID char(10) not null primary key,
   OnUse int not null check(OnUse='0' or OnUse='1'),
   NOte text not null
)
insert into tbl_computer values ('02','0','25555'),('03','1','55555'),('04','0','66666'),('05','1','88888'),('06','0','688878'),('B01','0','558558')
--表3为上网记录表(tbl_record)：
--ID：记录编号，主键
--CardID：本次上网的卡号，外键
--ComputerID：本次上网记录所使用的机器号，外键
--BeginTime：本次上网记录的开始时间
--EndTime：本次上网记录的结束时间
--Fee：本次上网的费用
create table tbl_record
(
   ID int not null primary key,
   CardID char(10) not null references tbl_card(ID),
   ComputerID char(10) not null references tbl_computer(ID),
   BeginTime datetime not null,
   EndTime datetime not null,
   Fee float not null
)
insert into tbl_record values ('23','0078_RJV','B01','2007-07-15 19:00:00','2007-07-15 21:00:00','20'),('34','0025_bbd','02','2006-12-25 18:00:00','2006-12-25 22:00:00','23'),('45','0023_ABC','03','2006-12-23 15:26:00','2006-12-23 22:55:00','50'),('46','0023_ABC','03','2006-12-22 15:26:00','2006-12-22 22:55:00','6'),('47','0023_ABC','03','2006-12-23 15:26:00','2006-12-23 22:55:00','50'),('48','0023_ABC','03','2007-01-06 15:26:00','2007-01-06 22:55:00','6'),('55','0023_ABC','03','2006-07-21 15:26:00','2006-07-21 22:55:00','50'),('64','0045_YGR','04','2006-12-24 18:00:00','2006-12-24 22:00:00','3'),('65','0025_bbd','02','2006-12-28 18:00:00','2006-12-28 22:00:00','23'),('98','0025_bbd','02','2006-12-26 18:00:00','2006-12-26 22:00:00','23')

--请完成以下题目：
select * from tbl_card
select * from tbl_computer
select * from tbl_record

--1. 查询出用户名为'张军'的上网卡的上网记录，要求显示卡号，用户名，机器编号、开始时间、结束时间，和消费金额，并按消费金额降序排列
select tbl_card.ID 显示卡号,UserName 用户名,tbl_computer.ID 机器编号,BeginTime 开始时间,EndTime 结束时间,Fee 消费金额 from tbl_record inner join tbl_card on tbl_record.CardID = tbl_card.ID inner join tbl_computer on tbl_record.ComputerID = tbl_computer.ID
where UserName='张军' order by Fee DESC

--2. 查询出每台机器上的上网次数和消费的总金额
select tbl_computer.ID 机器,COUNT(ComputerID)上网次数,SUM(Fee)总金额 from tbl_computer left join tbl_record on tbl_computer.ID = tbl_record.ComputerID group by tbl_computer.ID

--3. 查询出所有已经使用过的上网卡的消费总金额
select tbl_record.CardID 上网卡,SUM(Fee)消费总金额 from tbl_record group by tbl_record.CardID

--4. 查询出从未消费过的上网卡的卡号和用户名
select tbl_card.ID 上网卡号,UserName 用户名 from tbl_record right join tbl_card on tbl_record.CardID = tbl_card.ID
except
select tbl_card.ID 上网卡号,UserName 用户名 from tbl_record right join tbl_card on tbl_record.CardID = tbl_card.ID where Fee>0

--5. 将密码与用户名一样的上网卡信息查询出来
select * from tbl_card where PassWord = UserName

--6. 查询出使用次数最多的机器号和使用次数
select Top 1 tbl_computer.ID 机器号,COUNT(ComputerID)使用次数 from tbl_record inner join tbl_computer on tbl_record.ComputerID = tbl_computer.ID group by tbl_computer.ID 
order by COUNT(ComputerID) DESC

--7. 查询出卡号是以'ABC'结尾的卡号，用户名，上网的机器号和消费金额
select tbl_card.ID 卡号,UserName 用户名,tbl_computer.ID 机器号,Fee 消费金额 from tbl_record inner join tbl_card on tbl_record.CardID = tbl_card.ID inner join tbl_computer on tbl_record.ComputerID = tbl_computer.ID
where tbl_card.ID like '%ABC'

--8. 查询出是周六、周天上网的记录，要求显示上网的卡号，用户名，机器号，开始时间、结束时间和消费金额
select tbl_card.ID 卡号,UserName 用户名,ComputerID 机器号,BeginTime 开始时间,EndTime 结束时间,Fee 消费金额  from tbl_record inner join tbl_card on tbl_record.CardID = tbl_card.ID
where DATENAME(DW,BeginTime)='星期六' or DATENAME(DW,BeginTime)='星期日'

--9. 查询成一次上网时间超过12小时的的上网记录，要求显示上网的卡号，用户名，机器号，开始时间、结束时间和消费金额
select tbl_card.ID 卡号,UserName 用户名,ComputerID 机器号,BeginTime 开始时间,EndTime 结束时间,Fee 消费金额 from tbl_record inner join tbl_card on tbl_record.CardID = tbl_card.ID
where DATEDIFF(HH,BeginTime,EndTime)>5

--10. 查询除消费金额排列前三名(最高)的上网记录，要求显示上网的卡号，用户名，机器号，开始时间、结束时间和消费金额
select tbl_card.ID 卡号,UserName 用户名,ComputerID 机器号,BeginTime 开始时间,EndTime 结束时间,Fee 消费金额 from tbl_record inner join tbl_card on tbl_record.CardID = tbl_card.ID group by tbl_card.ID,UserName,ComputerID,BeginTime,EndTime,Fee
except
select Top 3 tbl_card.ID 卡号,UserName 用户名,ComputerID 机器号,BeginTime 开始时间,EndTime 结束时间,Fee 消费金额 from tbl_record inner join tbl_card on tbl_record.CardID = tbl_card.ID group by tbl_card.ID,UserName,ComputerID,BeginTime,EndTime,Fee
order by Fee DESC




