use master
go

create database Tbl
on
(
	name ='Tbl',
	filename='D:\test\Tbl.mdf',
	size=5,
	maxsize=50,
	filegrowth=10
)
log on
(
	name ='Tbl_log',
	filename='D:\test\Tblt_log.ldf',
	size=5,
	maxsize=50,
	filegrowth=10
)
go

use Tbl
go

create table Tbl_card
(
	CardId varchar(10) primary key ,
	PassWord varchar(10) not null ,
	Balance int ,
	UserName varchar(10) not null 
) 
go

create table Tbl_computer
(
	ComputerId varchar(10) primary key ,
	OnUse int check(OnUse in(0,1)) not null ,
	Note varchar(100)
)
go

create table Tbl_record
(
	RecordId int primary key ,
	CardId varchar(10) references Tbl_Card(CardId) ,
	ComputerId varchar(10) references Tbl_computer(ComputerId) ,
	BeginTime datetime ,
	EndTime datetime ,
	Fee int 
)
go

insert Tbl_card values
('0023_ABC','555',98,'张军'),
('0025_bbd','abe',300,'朱俊'),
('0036_CCD','何柳',100,'何柳'),
('0045_YGR','0045_YGR',58,'证验'),
('0078_RJV','55885fg',600,'校庆'),
('0089_EDE','zhang',134,'张峻')
go

insert Tbl_computer values 
('02',0,'25555'),
('03',1,'55555'),
('04',0,'66666'),
('05',1,'88888'),
('06',0,'688878'),
('B01',0,'558558')
go

insert Tbl_record values 
(23,'0078_RJV','B01','2007-07-15 19:00:00','2007-07-15 21:00:00',20),
(34,'0025_bbd','02','2006-12-25 18:00:00','2006-12-25 22:00:00',23),
(45,'0023_ABC','03','2006-12-23 15:26:00','2006-12-23 22:55:00',50),
(46,'0023_ABC','03','2006-12-22 15:26:00','2006-12-23 22:55:00',6),
(47,'0023_ABC','03','2006-12-23 15:26:00','2006-12-22 22:55:00',50),
(48,'0023_ABC','03','2007-01-06 15:26:00','2007-01-06 22:55:00',6),
(55,'0023_ABC','03','2006-07-21 15:26:00','2006-07-21 22:55:00',50),
(64,'0045_YGR','04','2006-12-24 18:00:00','2006-12-24 22:00:00',30),
(65,'0025_bbd','02','2006-12-28 18:00:00','2006-12-28 22:00:00',23),
(98,'0025_bbd','02','2006-12-26 18:00:00','2006-12-26 22:00:00',23)
go

select 
A.CardId as 卡号,UserName as 用户名,ComputerId as 机器编号,
BeginTime as 开始时间,EndTime as 结束时间,Fee as 消费金额 
from Tbl_record A 
inner join Tbl_card B on A.CardId = B.CardId
where UserName = '张军'
order by Fee desc
go

select ComputerId,count(ComputerId) as 上网次数,sum(Fee) as 总金额 from Tbl_record 
group by ComputerId 
go

select A.CardId,sum(Fee) as 总金额 from Tbl_record A
inner join Tbl_card B on A.CardId = B.CardId
group by A.CardId
go

select B.CardId,B.UserName as 总金额 from Tbl_record A
right join Tbl_card B on A.CardId = B.CardId
except
select A.CardId,UserName as 总金额 from Tbl_record A
inner join Tbl_card B on A.CardId = B.CardId
go

select A.CardId,A.PassWord,A.Balance,A.UserName from Tbl_card A ,Tbl_card B 
where A.PassWord = B.PassWord and A.PassWord = B.UserName
go

select top 1 ComputerId as 机器号,count(RecordId) as 使用次数 from Tbl_record
group by ComputerId
order by count(RecordId) desc
go

select A.CardId,UserName as 用户名,ComputerId as 卡号,Fee as 消费金额 from Tbl_record A
inner join Tbl_card B on A.CardId = B.CardId
where A.CardId like ('%ABC')
go

select 
A.CardId,UserName as 用户名,ComputerId as 机器号,
BeginTime as 开始时间,EndTime as 结束时间,Fee as 消费金额  
from Tbl_record A
inner join Tbl_card B on A.CardId = B.CardId
where datepart(weekday,BeginTime)=1 or datepart(weekday,BeginTime)=7
go

select 
A.CardId,UserName as 用户名,ComputerId as 机器号,
BeginTime as 开始时间,EndTime as 结束时间,Fee as 消费金额  
from Tbl_record A
inner join Tbl_card B on A.CardId = B.CardId
where DATEDIFF(HH,BeginTime,EndTime) > 12
go

select 
A.CardId,UserName as 用户名,ComputerId as 机器号,
BeginTime as 开始时间,EndTime as 结束时间,Fee as 消费金额  
from Tbl_record A
inner join Tbl_card B on A.CardId = B.CardId
except
select top 3 
A.CardId,UserName as 用户名,ComputerId as 机器号,
BeginTime as 开始时间,EndTime as 结束时间,Fee as 消费金额  
from Tbl_record A
inner join Tbl_card B on A.CardId = B.CardId
order by Fee desc
