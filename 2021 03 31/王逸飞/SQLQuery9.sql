use master
go

create database  wang
on
(
	name='wang.mdf',
	filename='D:\wang.mdf',
	maxsize =50,
	size=20,
	filegrowth=10%
)
	log on
	(
	name='wang_log.ldf',
	filename='D:\wang_log.ldf',
	size=20,
	maxsize=50,
	filegrowth=10%

	)
	go
	use wang
	go
create table tbl_card
(
ID varchar(10) primary key ,
passWord char(15),
balance char(15),
userName nvarchar(10)
)
go 
create table tbl_computer
(
ID varchar(10) primary key,
onUse int check(onUse=0 or onUse=1),
note nvarchar(50)
)
go 
create table tbl_record
(
ID int primary key,
cardId varchar(10) references tbl_card(ID),
ComputerId varchar(10) references tbl_computer(ID),
beginTime  datetime,
endTime datetime,
fee money
)
insert into tbl_card values ('0023_ABC','555','98','张军')
insert into tbl_card values ('0025_bbd','abe','300','朱骏'),('0036_CCD','何柳','100','何柳'),
('0045_YGR','0045_YGR','58','验证'),('0078_RJV','5588fg','600','校庆'),('0089_EDE','zhang','134','张俊')
insert into tbl_computer values ('02',0,'25555')
insert into tbl_computer values ('03',1,'55555'),('04',0,'66666'),('05',1,'888888'),('06',0,'6888878'),('01',0,'558558')


insert Tbl_record values 
(23,'0078_RJV','01','2007-07-15 19:00:00','2007-07-15 21:00:00',20),
(34,'0025_bbd','02','2006-12-25 18:00:00','2006-12-25 22:00:00',23),
(45,'0023_ABC','03','2006-12-23 15:26:00','2006-12-23 22:55:00',50),
(46,'0023_ABC','03','2006-12-22 15:26:00','2006-12-23 22:55:00',6),
(47,'0023_ABC','03','2006-12-23 15:26:00','2006-12-22 22:55:00',50),
(48,'0023_ABC','03','2007-01-06 15:26:00','2007-01-06 22:55:00',6),
(55,'0023_ABC','03','2006-07-21 15:26:00','2006-07-21 22:55:00',50),
(64,'0045_YGR','04','2006-12-24 18:00:00','2006-12-24 22:00:00',30),
(65,'0025_bbd','02','2006-12-28 18:00:00','2006-12-28 22:00:00',23),
(98,'0025_bbd','02','2006-12-26 18:00:00','2006-12-26 22:00:00',23)
go
--1. 查询出用户名为'张军'的上网卡的上网记录，要求显示卡号
select * from tbl_card
select * from tbl_record
--用户名，机器编号、开始时间、结束时间，和消费金额，并按消费金额降序排列
select C.ID,S.userName,C.ID,C.ComputerId,C.beginTime,C.endTime,C.fee from tbl_card S inner join tbl_record C on S.ID=C.cardID WHERE S.userName LIKE '张军'
--2. 查询出每台机器上的上网次数和消费的总金额
select S.computerID,SUM(S.fee),COUNT(*) from tbl_record S inner join tbl_record C ON S.ID=C.ID group by  S.computerID
--3. 查询出所有已经使用过的上网卡的消费总金额
select S.cardId,SUM(S.fee),COUNT(*) from tbl_record S inner join tbl_record C ON S.ID=C.ID group by  S.cardId
--4. 查询出从未消费过的上网卡的卡号和用户名
select S.cardId,C.userName from tbl_record S right join tbl_card C on S.cardId=C.ID where S.cardId IS NULL
--5. 将密码与用户名一样的上网卡信息查询出来
select userName,PassWord,B.* from tbl_card A LEFT join tbl_record B ON A.ID=B.cardId WHERE passWord = userName 
--6. 查询出使用次数最多的机器号和使用次数
SELECT  top 1 ComputerId ,count(*) from tbl_record A left join tbl_computer B on A.cardId=B.ID GROUP BY A.ID,ComputerId 
--7. 查询出卡号是以'ABC'结尾的卡号，用户名，上网的机器号和消费金额
SELECT A.cardId,B.userName,A.ComputerId,A.fee FROM tbl_record A inner join  tbl_card B on A.cardId = B.ID  where cardId LIKE '%ABC%'
--8. 查询出是周六、周天上网的记录，要求显示上网的卡号，用户名，机器号，开始时间、结束时间和消费金额
select cardId,userName,ComputerId,beginTime,endTime,fee from tbl_record inner join tbl_card on tbl_record.cardId=tbl_card.id  where datediff(HH,beginTime,endTime) >12
--9. 查询成一次上网时间超过12小时的的上网记录，要求显示上网的卡号，用户名，机器号，开始时间、结束时间和消费金额

 select cardId,userName,ComputerId,beginTime,endTime,fee from tbl_record inner join tbl_card on tbl_record.cardId=tbl_card.id
 except
 
--10. 查询除消费金额排列前三名(最高)的上网记录，要求显示上网的卡号，用户名，机器号，开始时间、结束时间和消费金额


 select top 3 cardId,userName,ComputerId,beginTime,endTime,fee from tbl_record inner join tbl_card on tbl_record.cardId=tbl_card.id order by fee desc
