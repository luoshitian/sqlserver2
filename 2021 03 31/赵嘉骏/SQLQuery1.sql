--首先创建网吧计费系统的数据库，
--表结构和数据如2.bmp所示，表的说明如下：
create database Wanba
on(
name=Wanba,
filename='D:\Wanba.mdf',
size=10mb,
maxsize=50mb,
filegrowth=10%
)
log on(
name=Wanba_log,
filename='D:\Wanba_log.mdf',
size=10mb,
maxsize=50mb,
filegrowth=10%
)
go
--表一为上网卡信息表(
create table tbl_card
(
id varchar(30) primary key  not null,
 --卡号，主键
passWord char(30) not null, 
--密码
balance int not null, 
--卡上的余额
userName varchar(30) not null 
--该上网卡所属的用户名
)
insert into tbl_card values('0023_ABC','555',98,'张军'),('0025_bbd','abe',300,'朱俊'),
('0036_CCD','何柳',100,'何柳'),('0045_YGR','0045_YGR', 58,'证验'),
('0078_RJV','55885fg',600,'校庆'),('0089_ede','zhang',134,'张峻')
select * from tbl_card
--表2为网吧机器说明表(
create table tbl_computer
(
id varchar primary key  not null,
--机器编号，主键
onUse int check(onuse='1' or onuse='0') not null,
--该机器是否正在使用，0为未使用，1为正在使用
note char(50) not null
--该机器的说明
)
insert into tbl_computer values
 (02,0,25555),(03,1,55555),
 (04,0,66666),(05,1,88888),
 (06,0,688878),(01,0,558558)
 select * from tbl_computer
--表3为上网记录表(
create table tbl_record
(
id int primary key not null,
--记录编号，主键
cardId varchar(30) references tbl_card(id) not null,
--本次上网的卡号，外键
ComputerId varchar  references tbl_computer(id) not null,
--本次上网记录所使用的机器号，外键
beginTime char(30) not null, 
--本次上网记录的开始时间
endTime char(30) not null,
--本次上网记录的结束时间
fee int not null
--本次上网的费用
)
INSERT into tbl_record values
(23,'0078_RJV',01,'2007-07-15 19:00:00','2007-07-15 21:00:00',20),
(34,'0025_bbd',02,'2006-12-25 18:00:00','2006-12-25 22:00:00',23),
(45,'0023_ABC',03,'2006-12-23 15:26:00','2006-12-23 22:55:00',50),
(46,'0023_ABC',03,'2006-12-22 15:26:00','2006-12-22 22:55:00',6),
(47,'0023_ABC',03,'2006-12-23 15:26:00','2006-12-23 22:55:00',50),
(48,'0023_ABC',03,'2007-01-06 15:26:00','2007-01-06 22:55:00',6),
(55,'0023_ABC',03,'2006-07-21 15:26:00','2006-7-21 22:55:00',50),
(64,'0045_YGR',04,'2006-12-24 18:00:00','2006-12-24 22:00:00',3),
(65,'0025_bbd',02,'2006-12-28 18:00:00','2006-12-28 22:00:00',23),
(98,'0025_bbd',02,'2006-12-26 18:00:00','2006-12-26 22:00:00',23)
select * from tbl_record


select * from tbl_card
select * from tbl_computer
select * from tbl_record
--1. 查询出用户名为'张军'的上网卡的上网记录，要求显示卡号，用户名，机器编号、开始时间、结束时间，和消费金额，并按消费金额降序排列
select tbl_card.id 卡号,username,tbl_computer.id,begintime,endtime,fee from tbl_record
 inner join tbl_card on tbl_record.CardID = tbl_card.ID 
 inner join tbl_computer on tbl_record.ComputerID = tbl_computer.ID
where UserName='张军' order by Fee DESC
--2. 查询出每台机器上的上网次数和消费的总金额
select tbl_computer.id,count(*)上网次数,sum(fee) 消费的总金额 from tbl_record
 inner join tbl_card on tbl_record.CardID = tbl_card.ID 
 inner join tbl_computer on tbl_record.ComputerID = tbl_computer.ID
 group by tbl_computer.id
--3. 查询出所有已经使用过的上网卡的消费总金额
select tbl_card.id,sum(fee) 消费总金额 from tbl_record
inner join tbl_card on tbl_record.cardId = tbl_card.id
group by tbl_card.id
--4. 查询出从未消费过的上网卡的卡号和用户名
select tbl_card.id,username from tbl_card
full join tbl_record on tbl_record.cardId =tbl_card.id
where fee is null
--5. 将密码与用户名一样的上网卡信息查询出来
select A.* from tbl_card A,tbl_card B
where A.passWord=A.userName and A.id != B.id

select *FROM tbl_card where passWord = userName
--6. 查询出使用次数最多的机器号和使用次数
select top 1 tbl_computer.id,count(*) from tbl_record
inner join tbl_computer on tbl_record.ComputerId=tbl_computer.id
group by tbl_computer.id order by count(*) desc
--7. 查询出卡号是以'ABC'结尾的卡号，用户名，上网的机器号和消费金额
select  tbl_card.id ,username,tbl_computer.id,fee from tbl_record
inner join tbl_card on tbl_record.cardId=tbl_card.id
inner join tbl_computer on tbl_record.ComputerId=tbl_computer.id
where tbl_card.id like '%ABC' 

--8. 查询出是周六、周天上网的记录，要求显示上网的卡号，用户名，机器号，开始时间、结束时间和消费金额
select tbl_card.id,username,tbl_computer.id,begintime,endtime,fee from tbl_record
inner join tbl_card on tbl_record.cardId=tbl_card.id
inner join tbl_computer on tbl_record.ComputerId=tbl_computer.id
where  DATENAME(DW,BeginTime)='星期六' or DATENAME(DW,BeginTime)='星期日'
--9. 查询成一次上网时间超过12小时的的上网记录，要求显示上网的卡号，用户名，机器号，开始时间、结束时间和消费金额
select tbl_card.id,username,tbl_computer.id,begintime,endtime,fee from tbl_record
inner join tbl_card on tbl_record.cardId=tbl_card.id
inner join tbl_computer on tbl_record.ComputerId=tbl_computer.id
where DATEDIFF(HH,BeginTime,EndTime)>5
--10. 查询出消费金额排列前三名(最高)的上网记录，要求显示上网的卡号，用户名，机器号，开始时间、结束时间和消费金额
select top 3 tbl_card.id,username,tbl_computer.id,begintime,endtime,fee  from tbl_record
inner join tbl_card on tbl_record.cardId=tbl_card.id
inner join tbl_computer on tbl_record.ComputerId=tbl_computer.id 
order by fee desc

