use master 
go 

create database InternetBar
on
(
     name='InternetBar',
	 filename='D:\InternetBar.mdf',
	 size=5MB,
	 maxsize=50MB,
	 filegrowth=10%
)

log on
(
     name='InternetBar_log',
	 filename='D:\InternetBar_log.ldf',
	 size=5MB,
	 maxsize=50MB,
	 filegrowth=10%
)
go

use InternetBar
go

create table tbl_card
(
    ID varchar(20) primary key,
	PassWord varchar(10),
	Balance int,
	UseName varchar(5)
)



create table tbl_computer
(
    ID varchar(5) primary key,
	OnUse varchar(1) check(OnUse=0 or OnUse=1) not null,
	Note nvarchar(20)
)


create table tbl_record
(
    ID int primary key,
	cardId varchar(20) references tbl_card(ID),
	ComputerId varchar(5) references tbl_computer(ID),
	beginTime datetime,
	endTime datetime,
	fee money
)



insert into tbl_card(ID,PassWord,Balance,UseName)
select'0023_ABC','555','98','张军' union
select'0025_bbd','abe','300','朱俊' union
select'0036_CCD','何柳','100','何柳' union
select'0045_YGR','0045_YGR','58','验证' union
select'0078_RJV','55885fg','600','校庆' union
select'0089_EDE','zhang','1234','张峻' 



insert into tbl_computer(ID,OnUse,Note)
select'02','0','25555' union
select'03','1','55555' union
select'04','0','66666' union
select'05','1','88888' union
select'06','0','688878' union
select'B01','0','558558' 


insert into tbl_record values('23','0078_RJV','B01','2007-07-15 19:00:00','2007-07-15 21:00:00','20'),
 ('34','0025_bbd','02','2006-12-25 18:00:00','2006-12-25 22:00:00','23'),
 ('45','0023_ABC','03','2006-12-23 15:26:00','2006-12-23 22:55:00','50'),
 ('46','0023_ABC','03','2006-12-22 15:26:00','2006-12-22 22:55:00','6'),
 ('47','0023_ABC','03','2006-12-23 15:26:00','2006-12-23 22:55:00','50'),
 ('48','0023_ABC','03','2007-01-06 15:26:00','2007-01-06 22:55:00','6'),
 ('55','0023_ABC','03','2006-07-21 15:26:00','2006-07-21 22:55:00','50'),
 ('64','0045_YGR','04','2006-12-24 18:00:00','2006-12-24 22:00:00','30'),
 ('65','0025_bbd','02','2006-12-28 18:00:00','2006-12-28 22:00:00','23'),
 ('98','0025_bbd','02','2006-12-26 18:00:00','2006-12-26 22:00:00','23')
 


 --1. 查询出用户名为'张军'的上网卡的上网记录，要求显示卡号，用户名，机器编号、开始时间、结束时间，和消费金额，并按消费金额降序排列
 select A.cardId as 卡号 , UseName as 用户名 , ComputerId as 机器编号 , beginTime as 开始时间 , endTime as 结束时间 , fee as 和消费金额
from tbl_record A inner join tbl_card B on A.cardID = A.cardId
where UseName = '张军' order by fee desc


 --2. 查询出每台机器上的上网次数和消费的总金额
 select ComputerId , COUNT(ComputerId) , SUM(fee) from tbl_record group by ComputerId 


 --3. 查询出所有已经使用过的上网卡的消费总金额
 select A.cardId , SUM(fee) from tbl_record A 
inner join tbl_card B on A.cardID=B.cardId
group by A.cardID


 --4. 查询出从未消费过的上网卡的卡号和用户名



 --5. 将密码与用户名一样的上网卡信息查询出来
select A.ID , A.passWord , A.balance ,A.UseName from tbl_card A , tbl_card B
where A.passWord = B.UseName and A.ID = B.ID


 --6. 查询出使用次数最多的机器号和使用次数
 select top 1 ComputerId , COUNT(cardId) from tbl_record
group by ComputerId
order by COUNT(cardId) desc


 --7. 查询出卡号是以'ABC'结尾的卡号，用户名，上网的机器号和消费金额
 select A.cardId , UseName , ComputerId , fee from tbl_record A
inner join tbl_card B on A.cardId = B.ID
where A.cardId like('%ABC')


 --8. 查询出是周六、周天上网的记录，要求显示上网的卡号，用户名，机器号，开始时间、结束时间和消费金额


 --9. 查询成一次上网时间超过12小时的的上网记录，要求显示上网的卡号，用户名，机器号，开始时间、结束时间和消费金额


--10. 查询除消费金额排列前三名(最高)的上网记录，要求显示上网的卡号，用户名，机器号，开始时间、结束时间和消费金额
select top 7 cardId 卡号, UseName 用户名,  ComputerId 机器号, beginTime 开始时间,endTime 结束时间 , fee 消费金额  from tbl_record 
left join tbl_card C on cardId=C.ID  
order by fee