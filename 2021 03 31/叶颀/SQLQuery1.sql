use master
go
create database WB
on
(
name = 'WB',
filename = 'D:\ WB.maf',
size = 5MB,
maxsize = 50MB,
filegrowth = 10%
)
log on
(
name = 'WB_log',
filename = 'D:\ WB_log.maf',
size = 5MB,
maxsize = 50MB,
filegrowth = 10%
)
go
use WB
go

create table Tbl_card
(
cardId varchar(20) primary key not null,
passWord varchar(20) unique not null,
balance  money not null,
userName varchar(10)
)

create table Tbl_computer
(
ComputerId varchar(10) primary key  not null,
onUse int check(onUse in(0,1)),
note text
)

create table Tbl_record
(
record int primary key not null,
cardId varchar(20) references Tbl_card(cardId) not null,
ComputerId varchar(10) references Tbl_computer(ComputerId),
beginTime datetime,
endTime datetime,
fee money
)

insert into Tbl_card values ('0023_ABC','555',98,'张军'),('0025_bbd','abe',300,'朱俊'),
							('0036_CCD','何柳',100,'何柳'),('0045_YGR','0045_YGR',58,'证验'),
							('0078_RJV','55885fg',600,'校庆'),('0089_EDE','zhang',134,'张峻')

insert into Tbl_computer values ('02',0,'25555'),('03',1,'55555'),('04',0,'66666'),
								('05',1,'88888'),('06',0,'688878'),('B01',0,'558558')

insert into  Tbl_record values (23,'0078_RJV','B01','2007-07-15 19:00:00','2007-07-15 21:00:00',20),
								(34,'0025_bbd','02','2006-12-25 18:00:00','2006-12-25 22:00:00',23),
								(45,'0023_ABC','03','2006-12-23 15:26:00','2006-12-25 22:55:00',50),
								(46,'0023_ABC','03','2006-12-22 15:26:00','2006-12-25 22:55:00',6),
								(47,'0023_ABC','03','2006-12-23 15:26:00','2006-12-25 22:55:00',50),
								(48,'0023_ABC','03','2007-01-06 15:26:00','2007-01-06 22:55:00',6),
								(55,'0023_ABC','03','2006-07-21 15:26:00','2006-07-21 22:55:00',50),
								(64,'0045_YGR','04','2006-12-24 18:00:00','2006-12-24 22:00:00',3),
								(65,'0025_bbd','02','2006-12-28 18:00:00','2006-12-28 22:00:00',23),
								(98,'0025_bbd','02','2006-12-26 18:00:00','2006-12-26 22:00:00',23)
select * from Tbl_card
select * from Tbl_computer
select * from Tbl_record
--请完成以下题目：
--1. 查询出用户名为'张军'的上网卡的上网记录，要求显示卡号，用户名，机器编号、开始时间、结束时间，和消费金额，并按消费金额降序排列
select Tbl_card.cardId 卡号, userName 用户名,ComputerId 机器编号, beginTime 开始时间,endTime 结束时间, fee 消费金额 from Tbl_record
inner join Tbl_card on Tbl_record.cardId = Tbl_card.cardId
where userName= '张军'
order by fee DESC 

--2. 查询出每台机器上的上网次数和消费的总金额
select computerid ,count(cardid),sum(fee) 消费总金额 from Tbl_record
group by ComputerId
select * from Tbl_card
select * from Tbl_computer
select * from Tbl_record
--3. 查询出所有已经使用过的上网卡的消费总金额
		select cardId ,sum(fee) 消费总金额 from Tbl_record
		group by cardId
	--4. 查询出从未消费过的上网卡的卡号和用户名
		select Tbl_record.cardId , userName from Tbl_record
		right join Tbl_card on Tbl_record.cardId = Tbl_card.cardId 
		where fee is null
	--5. 将密码与用户名一样的上网卡信息查询出来
		select * from Tbl_card where passWord = userName
	--6. 查询出使用次数最多的机器号和使用次数
		select TOP 1 ComputerId,count(cardId) from Tbl_record
		group by ComputerId
		order by count(cardId) DESC
	--7. 查询出卡号是以'ABC'结尾的卡号，用户名，上网的机器号和消费金额
		select  Tbl_record.cardId,userName,ComputerId,fee from Tbl_card
		inner join Tbl_record on Tbl_card.cardId = Tbl_record.cardId
		where Tbl_record.cardId  like '%ABC'
	--8. 查询出是周六、周天上网的记录，要求显示上网的卡号，用户名，机器号，开始时间、结束时间和消费金额
	    select Tbl_card.cardId,userName,ComputerId,beginTime ,endTime ,fee from Tbl_record
		inner join Tbl_card on Tbl_record.cardId = Tbl_card.cardId
		where datename(DW,beginTime )= '星期六' and datename(DW,beginTime )= '星期日'
	--9. 查询成一次上网时间超过12小时的的上网记录，要求显示上网的卡号，用户名，机器号，开始时间、结束时间和消费金额
		select Tbl_card.cardId,userName,ComputerId,beginTime ,endTime ,fee from Tbl_record
		inner join Tbl_card on Tbl_record.cardId = Tbl_card.cardId
		where datediff(HH,beginTime,endTime) > 12
	--10. 查询除消费金额排列前三名(最高)的上网记录，要求显示上网的卡号，用户名，机器号，开始时间、结束时间和消费金额
		select top 3 Tbl_card.cardId,userName,ComputerId,beginTime ,endTime ,fee from Tbl_record
		inner join Tbl_card on Tbl_record.cardId = Tbl_card.cardId
		order by fee DESC