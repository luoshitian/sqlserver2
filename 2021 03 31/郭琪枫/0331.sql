use master
go
create database Wangba
on
(
 name='Wangba',
 filename='D:\Wangba.mdf',
 size=6MB,
 maxsize=100MB,
 filegrowth=10Mb
)
log on
(
 name='Wangba_log',
 filename='D:\Wangba_log.ldf',
 size=6MB,
 maxsize=100MB,
 filegrowth=10Mb
)
go
use Wangba
go

create table tbl_card
(
	ID varchar(20) Primary key,
	PassWord varchar(20),
	Balance money,
	UseName nvarchar(20)
)

create table tbl_computer
(
	ID varchar(20) primary key,
	OnUse char(2) check(OnUse in (1,0)),
	Note int
)

create table tbl_record
(
	ID varchar(20) primary key,
	cardId varchar(20) references tbl_card(ID),
	ComputerId varchar(20) references tbl_computer(ID),
	beginTime datetime,
	endTime datetime,
	fee money
)

insert into tbl_card values('0023_ABC','555',98,'张军'),
('0025_bbd','abe',300,'朱俊'),
('0036_CCD','何柳',100,'何柳'),
('0045_YGR','0045_YGR',58,'证验'),
('0078_RJV','55885fg',600,'校庆'),
('0089_EDE','zhang',134,'张峻')

insert into tbl_computer values('02',0,25555),
('03',1,55555),
('04',0,66666),
('05',1,88888),
('06',0,688878),
('B01',0,558558)

insert into tbl_record values(23,'0078_RJV','B01','2007-07-15 19:00:00','2007-07-15 21:00:00',20),
(34,'0025_bbd','02','2006-12-25 18:00:00','2006-12-25 22:00:00',23),
(45,'0023_ABC','03','2006-12-23 15:26:00','2006-12-23 22:55:00',50),
(46,'0023_ABC','03','2006-12-22 15:26:00','2006-12-22 22:55:00',6),
(47,'0023_ABC','03','2006-07-15 15:26:00','2006-07-15 22:55:00',50),
(48,'0023_ABC','03','2007-07-15 15:26:00','2007-07-15 22:55:00',6),
(55,'0023_ABC','03','2006-07-15 15:26:00','2006-07-15 22:55:00',50),
(64,'0045_YGR','04','2006-07-15 18:00:00','2006-07-15 22:00:00',30),
(65,'0025_bbd','02','2006-07-15 18:00:00','2006-07-15 22:00:00',23),
(98,'0025_bbd','02','2006-07-15 18:00:00','2006-07-15 22:00:00',23)

select * from tbl_card
select * from tbl_computer
select * from tbl_record

--请完成以下题目：
--1. 查询出用户名为'张军'的上网卡的上网记录，要求显示卡号，用户名，机器编号、开始时间、结束时间，和消费金额，
--并按消费金额降序排列

select TD.ID 卡号,UseName 用户名,TR.ID 机器编号,beginTime 开始时间,endTime 结束时间,fee 消费金额 from tbl_record TR inner join tbl_card TD on TR.cardId=TD.ID
inner join tbl_computer TC on TR.ComputerId=TC.ID where UseName='张军' group by TD.ID ,UseName, TR.ID ,beginTime ,endTime ,fee order by fee DESC

--2. 查询出每台机器上的上网次数和消费的总金额

select ComputerId 机器编号,COUNT(*) 上网次数,SUM(fee) 消费的总金额 from tbl_record group by ComputerId

--3. 查询出所有已经使用过的上网卡的消费总金额

select TD.ID 卡号,SUM(fee) 消费总金额 from tbl_record TR inner join tbl_card TD on TR.cardId=TD.ID group by TD.ID


--4. 查询出从未消费过的上网卡的卡号和用户名
select ID 卡号,UseName 用户名 from tbl_card
except
select cardId 卡号,UseName 用户名 from tbl_record TR inner join tbl_card TD on TR.cardId=TD.ID
--5. 将密码与用户名一样的上网卡信息查询出来

select A.* from tbl_card A,tbl_card B where A.PassWord=B.PassWord AND A.UseName=B.UseName

--6. 查询出使用次数最多的机器号和使用次数

select top 1 ComputerId 机器号 , COUNT(*) 使用次数  from tbl_record group by ComputerId order by COUNT(*) DESC
--7. 查询出卡号是以'ABC'结尾的卡号，用户名，上网的机器号和消费金额

select TD.ID 卡号,UseName 用户名,ComputerId 机器号,fee 消费金额 from tbl_record TR inner join tbl_card TD on TR.cardId=TD.ID
 where TD.ID like '%ABC' 

--8. 查询出是周六、周天上网的记录，要求显示上网的卡号，用户名，机器号，开始时间、结束时间和消费金额

select TD.ID 卡号,UseName 用户名,ComputerId 机器号,beginTime 开始时间,endTime 结束时间,fee 消费金额 from tbl_record TR inner join tbl_card TD on TR.cardId=TD.ID
where datename(DW,beginTime)='星期六' or datename(DW,beginTime)='星期日'
select datename(DW,'2007-07-15 19:00:00.000')

--9. 查询成一次上网时间超过12小时的的上网记录，要求显示上网的卡号，用户名，机器号，开始时间、结束时间和消费金额

select TD.ID 卡号,UseName 用户名,ComputerId 机器号,beginTime 开始时间,endTime 结束时间,fee 消费金额 from tbl_record TR inner join tbl_card TD on TR.cardId=TD.ID
where datediff(HOUR,endTime,endTime)>12

--10. 查询除消费金额排列前三名(最高)的上网记录，要求显示上网的卡号，用户名，机器号，开始时间、结束时间和消费金额

select TD.ID 卡号,UseName 用户名,ComputerId 机器号,beginTime 开始时间,endTime 结束时间,fee 消费金额 
from tbl_record TR inner join tbl_card TD on TR.cardId=TD.ID 
inner join tbl_computer TC ON TC.ID=TR.ComputerId
except 
select TD.ID 卡号,UseName 用户名,ComputerId 机器号,beginTime 开始时间,endTime 结束时间,fee 消费金额   
from tbl_record TR inner join tbl_card TD on TR.cardId=TD.ID
inner join tbl_computer TC ON TC.ID=TR.ComputerId
where  fee=(select MAX(fee) from tbl_record)



