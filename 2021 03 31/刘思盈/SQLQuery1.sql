use master
go
create database WanBa
on
(
name='WanBa',
filename='D:\WanBa.mdf',
size=10,
maxsize=100,
filegrowth=10%
)
log on
(
name='WanBa_log',
filename='D:\WanBa_log.ldf',
size=10,
maxsize=100,
filegrowth=10%
)
go
use WanBa
go
create table tbl_card
(
id varchar(10) primary key ,
password varchar(10) ,
balance int ,
userName nvarchar(5)
)
create table tbl_computer 
(
id char(5) primary key,
onUse int check(onUse in(0,1)),
note text
)
create table tbl_record
(
id int primary key ,
cardId varchar(10) references tbl_card(id),
ComputerId char(5) references tbl_computer(id),
beginTime datetime ,
endTime datetime,
fee int
)
insert into tbl_card values('0023_ABC','555',98,'张军'),('0025_bbd','abe',300,'朱俊'),
							('0036_DDD','何柳',100,'何柳'),('0045_YGR','0045_YGR',58,'证验'),
							('0078_RIV','55885g',600,'校庆'),('0089_EDE','zhang',134,'张峻')
insert into tbl_computer values('02',0,'25555'),('03',1,'55555'),
							   ('04',0,'66666'),('05',1,'88888'),
							   ('06',0,'688879'),('B01',0,'558558')
insert into tbl_record values (23,'0078_RIV','B01','2007-07-15 19:00:00','2007-07-15 21:00:00',20),	
							  (34,'0025_bbd','02','2006-12-25 18:00:00','2006-12-25 22:00:00',23),
							  (45,'0023_ABC','03','2006-12-23 15:26:00','2006-12-23 22:55:00',50),
							  (46,'0023_ABC','03','2006-12-22 15:26:00','2006-12-22 15:55:00',6),
							  (47,'0023_ABC','03','2006-12-23 15:26:00','2006-12-23 22:55:00',50),
							  (48,'0023_ABC','03','2007-01-06 15:26:00','2007-01-06 15:55:00',6),
							  (55,'0023_ABC','03','2006-07-21 15:26:00','2006-07-21 22:55:00',50),
							  (64,'0045_YGR','04','2006-12-24 15:26:00','2006-12-23 22:55:00',3),
							  (65,'0025_bbd','02','2006-12-28 18:00:00','2006-12-28 22:00:00',23),
							  (98,'0025_bbd','02','2006-12-26 18:00:00','2006-12-26 22:00:00',23) 
--		请完成以下题目：
--1. 查询出用户名为'张军'的上网卡的上网记录，要求显示卡号，用户名，机器编号、开始时间、结束时间，和消费金额，并按消费金额降序排列
SELECT cardId ,ComputerId,userName,beginTime,endTime,fee FROM tbl_record  
inner join tbl_card on tbl_record.cardId=tbl_card.id 
where userName='张军'order by fee desc
--2. 查询出每台机器上的上网次数和消费的总金额
select ComputerId, count(id)上网次数,sum(fee)消费的总金额 from tbl_record group by ComputerId
--3. 查询出所有已经使用过的上网卡的消费总金额
select cardId,sum(fee)消费总金额 from tbl_record group by cardId 
--4. 查询出从未消费过的上网卡的卡号和用户名
select tbl_card.id,userName from tbl_record right join tbl_card on tbl_record.cardId=tbl_card.id where fee is null
--5. 将密码与用户名一样的上网卡信息查询出来
select passWord,userName from tbl_card where userName=passWord
--6. 查询出使用次数最多的机器号和使用次数
select top 1 ComputerId,count(ComputerId)  from tbl_record group by ComputerId order by count(ComputerId) desc
--7. 查询出卡号是以'ABC'结尾的卡号，用户名，上网的机器号和消费金额
SELECT tbl_card.id,userName,ComputerId,fee FROM tbl_card inner join tbl_record on tbl_card.id=tbl_record.cardId where tbl_card.id='%ABC'
--8. 查询出是周六、周天上网的记录，要求显示上网的卡号，用户名，机器号，开始时间、结束时间和消费金额
SELECT TOP 3 cardId,userName,ComputerId ,beginTime,endTime,fee FROM tbl_record INNER JOIN tbl_card ON tbl_record.cardId=tbl_card.id where beginTime='' and beginTime=''
--9. 查询成一次上网时间超过12小时的的上网记录，要求显示上网的卡号，用户名，机器号，开始时间、结束时间和消费金额
SELECT cardId,userName,ComputerId ,beginTime,endTime,fee FROM tbl_record INNER JOIN tbl_card ON tbl_record.cardId=tbl_card.id WHERE endTime-beginTime>12
--10. 查询除消费金额排列前三名(最高)的上网记录，要求显示上网的卡号，用户名，机器号，开始时间、结束时间和消费金额
SELECT TOP 3 cardId,userName,ComputerId ,beginTime,endTime,fee FROM tbl_record INNER JOIN tbl_card ON tbl_record.cardId=tbl_card.id ORDER BY fee desc

         