use master
go

create database NBA
on
(
	name='NBA',
	filename='D:\NBA.mdf',
	size=5,
	maxsize=50,
	filegrowth=10%
)
log on 
(
	name='NBA_log',
	filename='D:\NBA_log.ldf',
	size=5,
	maxsize=50,
	filegrowth=10%
)
go

use NBA
go

create table UsersInfo
(
	UsersId int primary key identity not null,	--编号
	UsersName varchar(20) not null ,			--名字
	UsersSex varchar(2) ,						--性别
	jingli int not null ,						--经历
	Team varchar(20) not null ,					--队伍
)
go

create table Team
(
	TeamId int primary key identity not null ,	--编号
	TeamName varchar(20) not null ,				--名字
	Competition varchar(20) check(Competition in('东部','西部'))not null ,--赛区
	victory int not null ,						--胜场
	coach varchar(20) not null ,				--教练
)
go

create table Game
(
	GameId int primary key identity not null ,		--编号
	TeamId1 int references Team(TeamId) not null ,	--主场
	defen1 int not null,							--得分
	TeamId2 int references Team(TeamId) not null ,	--客场
	defen2 int not null,							--得分
	date date not null  							--日期
)
go

create table UsersData
(
	UsersId int references UsersInfo(UsersId) not null,--球员编号
	TeamId int references Team(TeamId) not null ,		--队伍编号
	chuchang int not null ,								--出场次数
	mingzhong varchar(10) not null ,					--命中率
	trisection varchar(10) not null ,					--三分命中率
	faqiu varchar(10) not null ,						--罚球率
	lanban float not null ,								--篮板
	zhugong float not null ,							--助攻
	defen float not null ,								--得分
	qiangduan float not null ,							--抢断
	gaimao float not null ,								--盖帽
)
go

create table champion
(
	TeamId int references Team(TeamId) not null ,		--队伍编号
	Mvp nvarchar(20) not null,							--球员编号
	duoguan int not null,                               --那一年夺冠
	victory int not null ,								--胜场
	failing int not null ,								--负场
)
go



insert UsersInfo values
('埃里克-戈登','男',12,'火箭'),
('姚明','男',8,'火箭'),
('凯文·加内特','男',20,'凯尔特人'),
('雷·阿伦','男',18,'凯尔特人'),
('迈克尔·乔丹','男',15,'公牛'),
('斯科蒂·皮蓬','男',20,'公牛'),
('科比·布莱恩特','男',20,'湖人'),
('沙奎尔·奥尼尔','男',18,'湖人'),
('约翰-沃尔','男',12,'奇才'),
('德怀特·霍华德','男',16,'奇才'),
('达米恩·利拉德','男',10,'开拓者'),
('C.J.麦科勒姆','男',9,'开拓者'),
('韦恩-埃灵顿','男',11,'活塞'),
('科里-约瑟夫','男',9,'活塞')
go

insert Team values 
('雄鹿','东部',32,'迈克-布登霍尔泽'),
('马刺','西部',28,'波波维奇'),
('公牛','东部',70,'菲尔·杰克逊'),
('湖人','西部',62,'菲尔·杰克逊'),
('凯尔特人','东部',40,'布拉德-史蒂文斯'),
('火箭','西部',40,'斯蒂芬-塞拉斯'),
('活塞','东部',55,'德维恩-凯西'),
('奇才','东部',20,'斯科特-布鲁克斯'),
('开拓者','西部',30,'特里-斯托茨')
go

insert Game values
(2,112,1,102,'2020-12-13'),
(1,121,5,122,'2020-12-24'),
(3,115,4,117,'2021-01-09'),
(4,121,5,122,'2021-01-31'),
(6,125,3,120,'2021-01-19')
go

insert UsersData values 
(1,6,692,'42.5%','36.8%','66.7%',2.5,2.8,17.8,4.9,5.3),
(2,6,486,'52.4%','20.0%','52.7%',9.2,1.6,19.3,2.4,1.9),
(3,5,1462,'0.5%','0.3%','66.7%',10.0,3.7,16.6,1.3,1.4),
(4,5,1300,'45.2%','40.0%','95.2%',4.1,3.4,18.9,1.1,3.2),
(5,1,522,'52.5%','28.5%','72.4%',8.9,4.3,20.0,1.2,1.3),
(6,1,801,'44.4%','36.6%','77.6%',5.7,1.1,10.3,2.7,4.4),
(7,2,126,'44.3%','32.2%','73.3%',8.5,7.1,24.4,1.1,0.3),
(8,2,867,'43.1%','35.5%','78.2%',3.3,8.2,17.7,1.6,0.1),
(9,3,1750,'43.3%','32.4%','92.1%',4.3,9.2,19.3,1.7,2),
(10,3,1348,'58.6%','13.2%','85.5%',12.3,1.4,16.8,2,1.9),
(11,4,1562,'43.6%','37.1%','88.9%',4.2,6.5,24.4,5.3,7.3),
(12,4,1105,'18.7%','45.4%','84.4%',3.3,3.2,18.7,2.9,8.4),
(13,7,681,'40.8%','37.8%','84.6%',2.2,1.1,8.3,2.5,3.1),
(14,7,592,'44.1%','33.1%','76.6%',2.6,2.9,6.9,1.8,2.2)
go
insert champion values

('3','迈克尔·乔丹','1991','4','1'),
('3','迈克尔·乔丹','1992','4','2'),
('3','迈克尔·乔丹','1993','4','2'),
('6','哈基姆·奥拉朱旺','1994','4','3'),
('6','哈基姆·奥拉朱旺','1995','4','0'),
('3','迈克尔·乔丹','1996','4','2'),
('3','迈克尔·乔丹','1997','4','2'),
('3','迈克尔·乔丹','1998','4','2'),
('2','蒂姆·邓肯','1999','4','1'),
('4','沙奎尔·奥尼尔','2000','4','2'),
('4','沙奎尔·奥尼尔','2001','4','1'),
('4','沙奎尔·奥尼尔','2002','4','0'),
('2','蒂姆·邓肯','2003','4','2'),
('7','昌西·比卢普斯','2004','4','1'),
('2','蒂姆·邓肯','2005','4','3'),
('5','保罗·皮尔斯','2008','4','2')


go



select * from UsersInfo
select * from Team
select * from Game
select * from UsersData
select * from champion

select 
TeamId as 队伍编号 ,
TeamName as 队伍名称 ,
Competition as 队伍区部 ,
victory as 胜场 ,
coach as 教练  
from Team

select
UsersId as 球员编号 ,
UsersName as 球员名称 ,
UsersSex as 性别 ,
jingli as 经历 ,
Team as 队伍  
from UsersInfo

select  
GameId as 比赛编号 ,
TeamId1 as 主场 ,
defen1 as 得分 ,
TeamId2 as 客场 ,
defen2 as 得分 ,
date as 时间
from Game

select 
UsersName as 球员名称 ,
TeamName as 队伍名称 ,
chuchang as 出场次数 ,
mingzhong as 命中率 ,
trisection as 三分命中率 ,
faqiu as 罚球率 ,
lanban as 篮板 ,
zhugong as 助攻 ,
defen as 得分 ,
qiangduan as 抢断 ,
gaimao as 盖帽 
from UsersData A
inner join UsersInfo B on A.UsersId = B.UsersId
inner join Team C on A.TeamId = C.TeamId

select duoguan as 赛季,TeamName,Mvp,A.victory as 胜场,failing as 负场 from champion A
inner join Team C on A.TeamId = C.TeamId