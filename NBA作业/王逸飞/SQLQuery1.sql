create database  nba
on
(
	name='nba.mdf',
	filename='D:\nba.mdf',
	maxsize =50,
	size=20,
	filegrowth=10%
)
	log on
	(
	name='nba_log.ldf',
	filename='D:\nba_log.ldf',
	size=20,
	maxsize=50,
	filegrowth=10%

	)
	create table teamtable(
	teamID int primary key identity(1,1),
	teamName nvarchar(20) unique not null,
	teamcreationtime datetime default(getdate()) not null,
	teambriefintroduction  ntext 
	)


create table playertable(
playerID int identity(1,1) primary key ,
playName nvarchar(20) not null,
playerSex nvarchar(1) check(playerSex='男' or playerSex='女'),
playerStature varchar(3) not null,
playerWeight varchar(3) not null,
team int references teamtable(teamID),
nationality nvarchar(10) not null,
location nvarchar(5) check(location='得分后卫' or location='控球后卫' or location='小前锋' or location='大前锋' or location='中锋' ),
sservice nvarchar(2) check(sservice='服役' or sservice='现役')
)
create table playerdatainfo(
PDID  int not null primary key identity(1,1),
PDNAMEid int not null references playertable(playerID),
PDSHOAT int not null,
PDST int not null,
PDFOUL int not null,
PDTHREE int not null,
PDTWO int not null,
PDdunk int not null


)
create table champion
(
championID int primary key identity(1,1),
championTime datetime not null,
championteam int references teamtable(teamID),

)
create table gametable(
gameD int identity(1,1) primary key,
teamID int references teamtable(teamID),
gametime datetime not null,
gamesite nvarchar(20) not null,
score  varchar(10) not null
)
