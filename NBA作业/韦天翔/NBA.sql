create table qiuyuan --球员信息表
(
userid int identity(1,1) primary key not null, --id
username nvarchar(10) not null, --名字
userage int not null, --年龄
brithday date not null,--生日
stature float ,--身高
weight float,--体重
nationality nvarchar(10) not null,--国籍
teamid int not null,--战队id
nbaage int not null --nba球龄
)

create table zhandui--战队表
(
teamid int identity(1,1) primary key not null,--球队id
teamname nvarchar(10) not null,--球队名字
teamcoach nvarchar(10),--球队教练
location nvarchar(10),--所在地
area nvarchar(10)--所属赛区
) 

alter table qiuyuan add constraint FK_qiuyuan_teamid foreign key(teamid) references zhandui(teamid)

insert into zhandui (teamname,teamcoach,location,area) values
('华盛顿奇才','斯科特·布鲁克斯','华盛顿特区华盛顿','东部联盟-东南区赛区'),
('夏洛特黄蜂','詹姆斯·博雷戈','北卡罗来纳州夏洛特','东部联盟-东南区赛区'),
('亚特兰大老鹰','劳埃德·皮尔斯','乔治亚州亚特兰大','东部联盟-东南区赛区'),
('迈阿密热火','埃里克·斯波尔斯特拉','佛罗里达州迈阿密','东部联盟-东南区赛区'),
('奥兰多魔术','史蒂夫·克利福德','佛罗里达州奥兰多','东部联盟-东南区赛区')

insert into qiuyuan (username,userage,brithday,stature,weight,nationality,teamid,nbaage) values
('钱德勒-哈奇森','25','1996-04-26','1.99','96','美国 ','1','2'),
('科迪-泽勒','29','1992-10-05','2.11','108.9','美国','2','7'),
('约翰-科林斯','24','1997-09-23','2.06','106.6','美国','3','3'),
('德维恩-戴德蒙','32','1989-08-12','2.13','111.1','美国','4','7'),
('奥托-波特','28','993-06-03','2.03','89.8','美国','5','7')

create table saicheng--赛程表
(
scid int primary key not null,--比赛编号
riqi date not null,--日期
zc nvarchar not null ,--主场
kc nvarchar not null ,--客场
zcdf int not null,--主场得分
kcdf int not null,--客场得分
vit nvarchar not null--胜利队伍
)
insert into saicheng (scid,riqi,zc,kc,zcdf,kcdf,vit) values 
('202104131700','2021-04-13','奇才','爵士','125','121','奇才'),
('202104121800','2021-04-12','黄蜂','老鹰','101','105','老鹰'),
('202104101800','2021-04-10','老鹰','公牛','120','108','老鹰'),
('202104092000','2021-04-09','热火','湖人','110','104','热火'),
('202104132100','2021-04-13','魔术','马刺','97','120','马刺') 

create table vt--赛季冠军表
(
sj nvarchar not null, --赛季
guanjun nvarchar not null--冠军
)
insert into vt(sj,guanjun) values
('2015赛季','软件1班'),
('2016赛季','软件2班'),
('2017赛季','软件3班'),
('2018赛季','软件4班'),
('2019赛季','软件5班'),
('2010赛季','软件1班')


create table sj--球员数据表
(
userid int primary key not null,--球员id
saichang nvarchar not null,--赛季
changci int not null,--上场次数
shijian float not null,--上场时间
defen float not null,--总得分
)
alter table sj add constraint FK_sj_userid foreign key(userid) references qiuyuan(userid)

insert into sj(userid,saichang,changci,shijian,defen) values 
('1','2021','6','18.7','5.8'),
('2','2021','33','709','304'),
('3','2021','47','1422','855'),
('4','2021','0','0','0'),
('5','2021','3','66','24')