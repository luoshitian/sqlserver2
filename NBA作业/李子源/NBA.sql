use master 
go 
create database BB
on
(
 name='BB',
 filename='D:\BB.mdf',
 size=10,
 maxsize=200,
 filegrowth=10%
)
log on
(
 name='BB_log',
 filename='D:\BB_log.ldf',
 size=10,
 maxsize=200,
 filegrowth=10%
)
go
use BB 
create table Team 
(
	Id int primary key identity,
	TeamName varchar(20) not null,
	victory int not null ,
	chief varchar(20) not null 
)
create table AA
(
  Id int primary key identity,
  name varchar(20) not null,
  Sex  varchar(2) not null,
  location  varchar(20) not null,
  height  varchar(10) not null,
  weight varchar(20) not null ,
  number varchar(20) not null,
  TeamId int not null references Team(Id)
)

create table Game
(
	GameID int primary key identity not null ,
	home  int not null references Team(Id) ,
	visiting int references Team(Id) not null ,
	date date  not null,
	site varchar(50) not null

)
create table UserDate
(
	UserName int   references AA(Id),
	TeamId  int references Team(Id),
	appearances  int  not null,
	MVPTime  int ,
	hit varchar(10),
	faqiu varchar(10) not null ,						
	lanban float not null ,								
	zhugong float not null ,							
	defeng float not null ,							
	qiangduan float not null ,							
	gaimao float not null ,
)
create table  champion
(
	Id int primary key ,
	TeamID  int references Team(Id) ,
	data  datetime  not null ,

)
insert into AA values ('LeBron James','��','ǰ��','203CM','113KG','23',3),('Michael Jordan ','��','����','198CM','88KG','23',4),('Kobe Bean Bryant','��','����','198CM','96KG','24',2),
('Yao Ming ','��','�з�','229CM','141KG','11',1)
insert into Team values ('Houston Rockets',50,'Mike D Antoni'),('Los Angeles Lakers',33,'Frank Vogel  '),('Cleveland Cavaliers',50,'John Beilein'),
('Chicago Bulls',50,'Jim Boylen')
insert into Game values (1,2,getdate(),'��˹��'),(2,3,'1999-12-12','��ɼ��'),(3,1,'1999-12-25','�������'),(4,3,'1999-12-28','֥�Ӹ�')
insert into UserDate values (1,3,6,8,'1000','120',50,20,10,5,10),(3,2,8,12,'11000','100',50,30,50,8,13),(2,4,10,15,'500','220',40,66,20,8,16),
(4,1,10,8,'500','100',30,66,11,5,12)
insert into champion values (1,3,getdate()),(2,2,'2016-10-5'),(3,4,'2017-8-12'),(4,1,'2018-10-12')
select * from Team
select * from AA
select * from Game 
select * from  UserDate
select * from  champion
