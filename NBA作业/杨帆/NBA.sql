use master
go

create database NBA
on
(
	name='NBA',
	filename='D:\NBA.mdf',
	size=5,
	maxsize=50,
	filegrowth=10%
)
log on 
(
	name='NBA_log',
	filename='D:\NBA_log.ldf',
	size=5,
	maxsize=50,
	filegrowth=10%
)
go

use NBA
go

create table UsersInfo
(
	UsersId int primary key identity not null,	--编号
	UsersName varchar(20) not null ,			--名字
	UsersSex varchar(2) ,						--性别
	jingli int not null ,						--经历
	Team varchar(20) not null ,					--队伍
)
go

create table Team
(
	TeamId int primary key identity not null ,	--编号
	TeamName varchar(20) not null ,				--名字
	Competition varchar(20) check(Competition in('东部','西部'))not null ,--赛区
	victory int not null ,						--胜场
	coach varchar(20) not null ,				--教练
)
go

create table Game
(
	GameId int primary key identity not null ,		--编号
	TeamId1 int references Team(TeamId) not null ,	--主场
	defen1 int not null,							--得分
	TeamId2 int references Team(TeamId) not null ,	--客场
	defen2 int not null,							--得分
	date date not null  							--日期
)
go

create table UsersData
(
	UsersId int references UsersInfo(UsersId) not null,--球员编号
	TeamId int references Team(TeamId) not null ,		--队伍编号
	chuchang int not null ,								--出场次数
	mingzhong varchar(10) not null ,					--命中率
	trisection varchar(10) not null ,					--三分命中率
	faqiu varchar(10) not null ,						--罚球率
	lanban float not null ,								--篮板
	zhugong float not null ,							--助攻
	defen float not null ,								--得分
	qiangduan float not null ,							--抢断
	gaimao float not null ,								--盖帽
)
go

create table champion
(
	TeamId int references Team(TeamId) not null ,		--队伍编号
	Mvp nvarchar(20) not null,							--球员编号
	duoguan int not null,                               --哪一年夺冠
	victory int not null ,								--胜场
	failing int not null ,								--负场
)
go



insert UsersInfo values
	('埃里克-戈登','男',12,'火箭'),
	('姚明','男',8,'火箭'),
	('凯文·加内特','男',20,'凯尔特人'),
	('雷·阿伦','男',18,'凯尔特人'),
	('甘尼斯·阿德托昆博','男',8,'雄鹿'),
	('艾森·伊利亚索瓦','男',14,'雄鹿'),
	('卢卡·东契奇','男',3,'独行侠'),
	('小蒂姆·哈达威','男',7,'独行侠'),
	('约翰-沃尔','男',12,'奇才'),
	('德怀特·霍华德','男',16,'奇才'),
	('达米恩·利拉德','男',10,'开拓者'),
	('C.J.麦科勒姆','男',9,'开拓者'),
	('韦恩-埃灵顿','男',11,'活塞'),
	('科里-约瑟夫','男',9,'活塞')
go

insert Team values 
	('雄鹿','东部',32,'迈克-布登霍尔泽'),
	('独行侠','西部',28,'里克-卡莱尔'),
	('奇才','东部',17,'斯科特-布鲁克斯'),
	('开拓者','西部',30,'里克-卡莱尔'),
	('凯尔特人','东部',25,'布拉德-史蒂文斯'),
	('火箭','西部',13,'斯蒂芬-塞拉斯'),
	('活塞','东部',15,'德维恩-凯西')
go

insert Game values
	(2,112,1,102,'2020-12-13'),
	(3,96,7,99,'2020-12-20'),
	(1,121,5,122,'2020-12-24'),
	(6,126,4,128,'2020-12-27')
go

insert UsersData values 
	(1,6,692,'42.5%','36.8%','66.7%',2.5,2.8,17.8,0.9,0.3),
	(2,6,486,'52.4%','20.0%','52.7%',9.2,1.6,19.0,0.4,1.9),
	(3,5,1462,'0.5%','0.3%','66.7%',10.0,3.7,16.6,1.3,1.4),
	(4,5,1300,'45.2%','40.0%','95.2%',4.1,3.4,18.9,1.1,0.2),
	(5,1,522,'52.5%','28.5%','72.4%',8.9,4.3,20.0,1.2,1.3),
	(6,1,801,'44.4%','36.6%','77.6%',5.7,1.1,10.3,0.7,0.4),
	(7,2,126,'44.3%','32.2%','73.3%',8.5,7.1,24.4,1.1,0.3),
	(8,2,867,'43.1%','35.5%','78.2%',3.3,8.2,17.7,1.6,0.1),
	(9,3,573,'43.3%','32.4%','78.1%',4.3,9.2,19.0,1.7,0.7),
	(10,3,1106,'58.6%','13.2%','56.5%',12.3,1.4,16.8,0.9,1.9),
	(11,4,607,'43.6%','37.1%','88.9%',4.2,6.5,24.0,1.0,0.3),
	(12,4,506,'18.7%','45.4%','84.4%',3.3,3.2,18.7,0.9,0.4),
	(13,7,681,'40.8%','37.8%','84.6%',2.2,1.1,8.0,0.5,0.1),
	(14,7,592,'44.1%','33.1%','76.6%',2.6,2.9,6.9,0.8,0.2)
go
insert champion values
	('5','保罗·皮尔斯','2008','4','2'),
	('7','昌西·比卢普斯','2004','4','1'),
	('6','哈基姆·奥拉朱旺','1994','4','3'),
	('1','贾巴尔','1971','4','0')
go



select * from UsersInfo
select * from Team
select * from Game
select * from UsersData
select * from champion

select 
	TeamId as 队伍编号 ,
	TeamName as 队伍名称 ,
	Competition as 队伍区部 ,
	victory as 胜场 ,
	coach as 教练  
from Team

select
	UsersId as 球员编号 ,
	UsersName as 球员名称 ,
	UsersSex as 性别 ,
	jingli as 经历 ,
	Team as 队伍  
from UsersInfo

select  
	GameId as 比赛编号 ,
	TeamId1 as 主场 ,
	defen1 as 得分 ,
	TeamId2 as 客场 ,
	defen2 as 得分 ,
	date as 时间
from Game

select 
	UsersName as 球员名称 ,
	TeamName as 队伍名称 ,
	chuchang as 出场次数 ,
	mingzhong as 命中率 ,
	trisection as 三分命中率 ,
	faqiu as 罚球率 ,
	lanban as 篮板 ,
	zhugong as 助攻 ,
	defen as 得分 ,
	qiangduan as 抢断 ,
	gaimao as 盖帽 
from UsersData A
inner join UsersInfo B on A.UsersId = B.UsersId
inner join Team C on A.TeamId = C.TeamId

select 
	duoguan as 赛季,
	TeamName,
	Mvp,
	A.victory as 胜场,
	failing as 负场 
from champion A
inner join Team C on A.TeamId = C.TeamId