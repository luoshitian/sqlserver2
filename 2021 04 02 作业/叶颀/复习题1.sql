use master
go

create database GoodsDB 
on
(
	name='GoodsDB',
	filename='D:\test\GoodsDB.mdf',
	size=5,
	maxsize=50,
	filegrowth=10%
)
log on
(
	name='GoodsDB_log',
	filename='D:\test\GoodsDB_log.ldf',
	size=5,
	maxsize=50,
	filegrowth=10%
)
go

use GoodsDB
go

create table GoodsType 
(
	TypeID int primary key identity not null ,
	TypeName nvarchar(20) not null ,
)
go

create table GoodsInfo 
(
	GoodsId int primary key identity not null ,
	GoodsName nvarchar(20) not null ,
	GoodsColor nvarchar(20) not null ,
	GoodsBrand nvarchar(20) ,
	GoodsMoney money not null ,
	TypeID int references GoodsType(TypeID)
)
go

insert GoodsType values 
('服装内衣'),
('鞋包配饰'),
('手机数码')
go

insert GoodsInfo values 
('提花小西装','红色','菲曼琪',300,1),
('百搭短裤','绿色','哥弟',100,1),
('无袖背心','白色','阿依莲',700,1),
('低帮休闲鞋','红色','菲曼琪',900,2),
('中跟单鞋','绿色','阿依莲',400,2),
('平底鞋','白色','阿依莲',200,2),
('迷你照相机','红色','尼康',500,3),
('硬盘','黑色','希捷',600,3),
('显卡','黑色','技嘉',800,3)
go

select top 1 
GoodsName as 商品名称,
GoodsColor as 商品颜色,
GoodsMoney as 商品价格 
from GoodsInfo
order by GoodsMoney desc
go

select 
max(GoodsMoney) as 最高价格,
min(GoodsMoney) as 最低价格,
avg(GoodsMoney) as 平均价格
from GoodsInfo
group by TypeID
go

select * from GoodsInfo
where GoodsMoney >= 300 and GoodsMoney <= 600 and GoodsColor = '红色' 

