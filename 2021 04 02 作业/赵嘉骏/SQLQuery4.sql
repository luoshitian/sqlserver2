create database StarManagerDB
on
(
 name=StarManagerDB,
 filename='D:\StarManagerDB.mdf',
 size=10mb,
 maxsize=50mb,
 filegrowth=10%
)
log on
(
 name=StarManagerDB_log,
 filename='D:\StarManagerDB_log.mdf',
 size=10mb,
 maxsize=50mb,
 filegrowth=10%
)
go
create table StarType--（明星类型表）
--字段名	说明	类型	长度	可否为空	约束
(
T_NO	int not null primary key identity(1,1),
--明星类型编号	int		否	主键约束，自增，标识种子和标识增量都是1
T_NAME	NVARCHAR(20)
--明星类型	nvarchar	20		
)
--明星类型表（StarType） 
--明星类型编号	明星类型
--1	体育明星
--2	IT明星
--3	相声演员
insert into StarType values
('体育演员'),('IT明星'),('相声演员')
select * from StarType
create table StarInfo--（明星信息表）
--字段名	说明	类型	长度	可否为空	约束
(
S_NO	int not null primary key identity(1,1),
--明星编号	int		否	主键约束，自增，标识种子和标识增量都是1
S_NAME	nvarchar(20) not null, 
--明星姓名	nvarchar	20	否	
S_AGE	int not null,
--明星年龄	int		否	
S_HOBBY	nvarchar(20),
--特技	nvarchar	20		
S_NATIVE	nvarchar(20) default('中国大陆'),
--明星籍贯	nvarchar	20		默认约束：中国大陆
S_T_NO	int references StarType(T_NO) ,
--明星类型编号	int			外键，参照StarType表的主键T_NO
)
--明星表（StarInfo） 
--明星编号	姓名	年龄	特技	籍贯	明星类型编号
--1	梅西	30	射门	阿根廷	1
--2	科比	35	过人	美国	1
--3	蔡景现	40	敲代码	中国	2
--4	马斯克	36	造火箭	外星人	2
--5	郭德纲	50	相声	中国	3
--6	黄铮	41	拼多多	中国	2
insert into StarInfo values
('梅西','30','射门','阿根廷','1'),('科比','35','过人','美国','1'),
('蔡景现','40','敲代码','中国','2'),('马斯克','36','造火箭','外星人','2'),
('郭德纲','50','相声','中国','3'),('黄峥','41','拼多多','中国','2')
select * from StarInfo

select * from StarInfo
SELECT * FROM StarType

--3、查询年龄最大的3个明星的姓名，特技和籍贯信息，要求使用别名显示列名。
select top 3 S_name 明星的姓名,s_hobby 特技,s_native 籍贯信息 from StarType
inner join StarInfo on StarType.T_NO=StarInfo.S_T_NO
order by S_age desc

--4、按明星类型编号分类查询明星人数，明星平均年龄，显示明星人数大于2的分组信息，要求使用别名显示列名。
select T_NO 明星类型,count(*)明星人数,avg(S_age)明星平均年龄 from StarType
INNER join StarInfo on StarInfo.S_T_NO=StarType.T_NO
 group by t_no having count(*)>2

--5、查询明星类型为“体育明星”中年龄最大的姓名、特技、籍贯信息，要求显示列别名。
select  top 1 t_no,t_name,s_name,s_hobby,s_native, max(s_age) from StarType
inner join StarInfo on StarInfo.S_T_NO=StarType.T_NO group by 
t_no,t_name,s_name,s_hobby,s_native
having T_NO=1 