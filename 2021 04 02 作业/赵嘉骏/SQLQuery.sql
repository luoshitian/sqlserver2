create database HOUSE_DB
on
(
  name=HOUSE_DB,
  filename='D:\HOUSE_DB.mdf',
  size=10mb,
  maxsize=50mb,
  filegrowth=10%
)
log on
(
  name=HOUSE_DB_log,
  filename='D:\HOUSE_DB_log.mdf',
  size=10mb,
  maxsize=50mb,
  filegrowth=10%
)
go
--房屋类型表（
create table HOUSE_TYPE
--字段名	类型	是否可为空	约束	说明
(type_id int not null primary key identity(1,1),	
--int	N	主键，自增长	类型编号
type_name	varchar(50) not null
--varchar(50)	N	唯一约束	类型名称
)
insert into HOUSE_TYPE values ('小户型'),('经济型'),('别墅')
select * from  HOUSE_TYPE
--房屋信息表
create table HOUSE
--字段名	类型	是否可为空	约束	说明
(
house_id	int not null primary key identity(1,1),
--int	N	主键，自增长	房屋编号
house_name	varchar(50) not null, 
--varchar(50)	N		房屋名称
house_price	float not null default('0'),
--float	N	默认值0	房租
type_id	int not null references HOUSE_TYPE(type_id)
--int	N	外键依赖HOUSE_TYPE表	房屋类型
)
insert into HOUSE values ('林佳元','20','1'),('刘毅','10','2'),('刘金海','25','3')
select * from HOUSE

SELECT * FROM HOUSE
SELECT * FROM HOUSE_TYPE
--查询所有房屋信息
select *from House
--使用模糊查询包含”型“字的房屋类型信息
select * from HOUSE_TYPE inner join HOUSE on HOUSE_TYPE.type_id=HOUSE.type_id
where type_name like '%型%' 
--查询出房屋的名称和租金，并且按照租金降序排序
select house_name 房屋的名称,house_price 房屋租金 from HOUSE
order by house_price desc
--使用连接查询，查询信息，显示房屋名称和房屋类型名称
select house_name 房屋名称,TYPE_NAME 房屋类型名称 from HOUSE
inner join HOUSE_TYPE on HOUSE_TYPE.type_id=HOUSE.type_id
--查询所有房屋中月租最高的房屋，显示最高的租金和房屋名称
select top 1 house_id, house_price,house_name from HOUSE
inner join HOUSE_TYPE on HOUSE_TYPE.type_id=HOUSE.type_id
order by house_price desc
