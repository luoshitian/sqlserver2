create database GoodsDB
on
(
name='GoodsDB',
filename='D:\GoodsDB.mdf',
size=10mb,
maxsize=50mb,
filegrowth=10%
)
log on
(
name='GoodsDB_log',
filename='D:\GoodsDB_log.mdf',
size=10mb,
maxsize=50mb,
filegrowth=10%
)
go
create table GoodsType
(
TypeID 	int not null primary key identity(1,1),
--商品类型编号	int		否	主键约束，自增约束，标识种子和标识增量都是1
TypeName	nvarchar(20) not null
--商品类型名称	nvarchar	20	否	

)
insert into GoodsType values
('服装内衣'),('鞋包配饰'),('手机数码')
select * from GoodsType
create table GoodsInfo
(
GoodsID int not null primary key identity(1,1),	
--商品编号	int		否	主键约束，自增约束，标识种子和标识增量都是1
GoodsName	nvarchar(20) not null,
--商品名称	nvarchar	20	否	
GoodsColor	nvarchar(20) not null,
--商品颜色	nvarchar	20	否	
GoodsBrand	nvarchar(20), 
--商品品牌	nvarchar	20		
GoodsMoney	money not null,
--商品价格	money		否	
TypeID	int references GoodsType(TypeID)
--商品类型编号	int			外键，参照GoodsType中的TypeID

)
insert into GoodsInfo values
('提花小西装','红色','菲曼琪',300,1),('百搭短裤','绿色','哥弟',100,1),('无袖背心','白色','阿依莲',700,1),
('低帮休闲鞋','红色','菲曼琪',900,2),('中跟单鞋','绿色','哥弟',400,2),('平底鞋','白色','阿依莲',200,2),
('迷你照相机','红色','尼康',500,3),('硬盘','黑色','希捷',600,3),('显卡','黑色','技嘉',800,3)
select * from GoodsInfo


select * from GoodsInfo
select * from GoodsType

--3、查询价格最贵的商品名称，商品颜色和商品价格，要求使用别名显示列名
 select  goodsname 商品名称,goodscolor 商品颜色,goodsmoney 商品价格 from GoodsInfo
 inner join GoodsType on GoodsInfo.TypeID=GoodsType.TypeID
 where goodsmoney=(select max(GoodsMoney) from GoodsInfo)
 --4、按商品类型编号分组查询商品最高价格，最低价格和平均价格，要求使用别名显示列名
 select GoodsInfo.TypeID, max(goodsmoney) 商品最高价格,MIN(goodsmoney ) 最低价格,avg(goodsmoney) 平均价格 from GoodsInfo
 group by GoodsInfo.TypeID
--5、查询商品信息所有列，要求商品颜色为红色，价格在300~600之间
select * from GoodsInfo
where GoodsColor='红色' AND GoodsMoney>=300 and GoodsMoney<=600