
CREATE database HOUSE_DB
on
(
 name='HOUSE_DB',
 filename='D:\HOUSE_DB.mdf',
 size=5,
 maxsize=50,
 filegrowth=1
)
log on
(
 name='HOUSE_DB_log',
 filename='D:\HOUSE_DB_log.ldf',
 size=5,
 maxsize=50,
 filegrowth=1
)
go
use HOUSE_DB
go
create table HOUSE_TYPE
(
 type_id int primary key identity(1,1) not null,
 type_name	varchar(50) not null unique
)
create table HOUSE
(
house_id int primary key identity(1,1),
house_name	varchar(50)	 not null ,
house_price	float not null default(0),
type_id	int references HOUSE_TYPE(type_id)
)
insert into HOUSE_TYPE values('小户型'),('经济型'),('别墅' )
insert into HOUSE values('小红屋',500,'1'),('经济屋',1000,'2'),('别墅屋',5000,'3')
select * from HOUSE_TYPE
select * from HOUSE
select * from HOUSE_TYPE where type_name like '%型%'
select * from HOUSE order by house_price desc
select house_name  房屋名称,type_name 房屋类型名称 from HOUSE A INNER JOIN HOUSE_TYPE B ON A.type_id=B.type_id
select top 1 house_price 租金,house_name 房屋名称 from HOUSE order by house_price desc

