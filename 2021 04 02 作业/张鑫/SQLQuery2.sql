use master
go

create database HOUSE_DB
on
(
	name=HOUSE_DB,
	filename='D:\ATM.mdf',
	size=5mb,
	maxsize=50mb,
	filegrowth=1%
)
log on
(
	name=HOUSE_DB_log,
	filename='D:\ATM_log.mdf',
	size=5mb,
	maxsize=50mb,
	filegrowth=1%
)
go

use HOUSE_DB
go

create table HOUSE_TYPE
(
	type_id int not null primary key identity(1,1),
	type_name varchar(50) not null 
)
go

create table HOUSE
(
	house_id int not null primary key identity,
	house_name varchar(50) not null,
	house_price float not null default('0'),
	type_id int not null references HOUSE_TYPE(type_id)
)

insert HOUSE_TYPE values ('小户型'),('经济型'),('别墅')
insert HOUSE values
('小屋','500','1'),('中屋','300','2'),('大屋','200','3')

select * from HOUSE

select type_name from HOUSE_TYPE where type_name like '%型'

select house_name,house_price from HOUSE order by house_price desc

select distinct house_name,type_name from HOUSE_TYPE
inner join HOUSE on HOUSE_TYPE.type_id=HOUSE_TYPE.type_id

select top 1 house_name,house_price from HOUSE
order by  house_price desc