use master
go

create database GoodsDB
go

use GoodsDB
go

create table GoodsType
(
	TypeID int not null  primary key identity(1,1),
	TypeName nvarchar(20) not null
)

create table GoodsInfo
(
	GoodsID int not null primary key identity(1,1),
	GoodsName nvarchar(20) not null,
	GoodsColor nvarchar(20) not null,
	GoodsBrand nvarchar(20),
	GoodsMoney money not null,
	TypeID int references GoodsType(TypeID)
)
go

insert GoodsType values
('��װ����'),
('Ь������'),
('�ֻ�����')
go

insert GoodsInfo values
('�ỨС��װ','��ɫ','������','300','1'),
('�ٴ�̿�','��ɫ','���','100','1'),
('���䱳��','��ɫ','������','700','1'),
('�Ͱ�����Ь','��ɫ','������','900','2'),
('�и���Ь','��ɫ','���','400','2'),
('ƽ��Ь','��ɫ','������','200','2'),
('���������','��ɫ','�῵','500','3'),
('Ӳ��','��ɫ','ϣ��','600','3'),
('�Կ�','��ɫ','����','800','3')
go

select top 1 GoodsName,GoodsColor,GoodsMoney ��Ʒ�۸�
from GoodsInfo order by GoodsMoney desc 

select TypeID,max(GoodsMoney)��߼۸�,min(GoodsMoney)��ͼ۸�,AVG(GoodsMoney)ƽ���۸� from GoodsInfo
group by TypeID

select GoodsName,GoodsColor,GoodsBrand,GoodsMoney,TypeID from GoodsInfo
where GoodsColor='��ɫ' and GoodsMoney>=300 and GoodsMoney<=600