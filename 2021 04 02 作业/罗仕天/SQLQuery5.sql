create database HOUSE_DB
go
use HOUSE_DB
go

create table HOUSE_TYPE
(
type_id int not null primary key identity ,
type_name varchar(50) not null unique 
)
go

create table HOUSE
(
house_id int not null primary key identity ,
house_name	varchar(50)	not null,
house_price	float	not null default ('0'),
type_id	int	not null foreign key references  HOUSE_TYPE(type_id)
)

go


select * from  HOUSE_TYPE

insert into  HOUSE_TYPE(type_name) values ('小户型'),('经济型'),('别墅')
go
insert into HOUSE(house_name,house_price,type_id) values('皇家','10000','3'),
                                                        ('大宅','1000','2'),
                                                        ('小窝','100','1')
insert into HOUSE(house_name,house_price,type_id) values('天帝型','100000','3')

select * from  HOUSE_TYPE
select max (house_price) from   HOUSE


--查询所有房屋信息

select * from   HOUSE

--使用模糊查询包含”型“字的房屋类型信息

select * from HOUSE where house_name like '%%型'

--查询出房屋的名称和租金，并且按照租金降序排序

select house_name,house_price from  HOUSE group by house_price,house_name order by house_price desc

--使用连接查询，查询信息，显示房屋名称和房屋类型名称

select house_name,type_name from HOUSE_TYPE e inner join HOUSE r on e.type_id=r.type_id

--查询所有房屋中月租最高的房屋，显示最高的租金和房屋名称

select house_price,house_name from  HOUSE where house_price=(select max (house_price) from   HOUSE)