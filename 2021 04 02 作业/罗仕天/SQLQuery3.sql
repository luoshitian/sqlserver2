create database GoodsDB
go
use  GoodsDB
go
create table GoodsType
(
TypeID int not null primary key identity ,
TypeName nvarchar(20) not null
)
go

create table GoodsInfo
(
GoodsID int  not null primary key identity,
GoodsName nvarchar(20) not null,
GoodsColor nvarchar(20) not null,
GoodsBrand nvarchar(20) not null,
GoodsMoney money  not null,
TypeID int foreign key references GoodsType(TypeID)
)
go

insert into GoodsType(TypeName) values ('服装内衣'),('鞋包配饰'),('手机数码')
go

insert into GoodsInfo(GoodsName ,GoodsColor,GoodsBrand,GoodsMoney,TypeID) 
values('提花小西装','红色','菲曼琪','300','1'),

      ('百搭短裤','绿色',' 哥弟','100','1'),

	  (' 无袖背心','白色 ','阿依莲','700','1'),

	  ('低帮休闲鞋 ','红色','菲曼琪','900','2'),

	  ('中跟单鞋','绿色','哥弟','400','2'),

	  ('平底鞋',' 白色',' 阿依莲','200','2'),

	  ('迷你照相机','红色','尼康','500','3'),

	  ('硬盘 ','黑色','希捷','600','3'),

	  ('显卡 ','黑色 ',' 技嘉','800','3')

go

select * from GoodsType
select max(GoodsMoney) from GoodsInfo


--3、查询价格最贵的商品名称，商品颜色和商品价格，要求使用别名显示列名
select GoodsName, GoodsColor ,GoodsMoney from  GoodsInfo where GoodsMoney=(select max (GoodsMoney) from GoodsInfo)

--4、按商品类型编号  分组查询  商品最高价格，最低价格和平均价格，要求使用别名显示列名

select  TypeID,max(GoodsMoney) 最高,min(GoodsMoney) 最低 ,avg(GoodsMoney) 平均 from GoodsInfo group by TypeID

--5、查询商品信息所有列，要求商品颜色为红色，价格在300~600之间

select * from GoodsInfo where GoodsColor='红色' and GoodsMoney>300 and GoodsMoney<600

