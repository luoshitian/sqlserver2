create database StarManagerDB
go
use StarManagerDB
go

create table StarType
(
T_NO int primary key identity ,
T_NAME	nvarchar(20)		
)
go


create table StarInfo
(
S_NO int  not null primary key identity ,
S_NAME nvarchar(20) not null,
S_AGE int null ,
S_HOBBY nvarchar(20) not null ,
S_NATIVE nvarchar(20) default ('中国大陆'),
S_T_NO int foreign key references  StarType(T_NO )
)
go

insert into StarType(T_NAME) values ('体育明星'), ('IT明星'), ('相声演员')

go

insert into  StarInfo(S_NAME,S_AGE,S_HOBBY,S_NATIVE,S_T_NO) values('梅西','30','射门','阿根廷','1'),

                                                                  ('科比','35','过人','美国','1'),

																  ('蔡景现','40','敲代码','中国','2'),

																  ('马斯克','36','造火箭','外星人','2'),

																  ('郭德纲','50','相声','中国','3'),

																  ('黄铮','34','拼多多','中国','1')
go



select * from StarType
select * from StarInfo


--3、查询年龄最大的3个明星的姓名，特技和籍贯信息，要求使用别名显示列名。

select  top 3 S_AGE 年龄,S_HOBBY 特技,S_NATIVE 籍贯信息 , S_NAME 姓名
 from StarInfo order by S_AGE DESC

--4、按明星类型编号分类查询明星人数，明星平均年龄，显示明星人数大于2的分组信息，要求使用别名显示列名。

select count(T_NO) 分组的明星人数,avg(S_AGE) 平均年龄 from
  StarType e inner join  StarInfo r on e.T_NO=r.S_T_NO group by e.T_NO 
  having count(T_NO)>=2


--5、查询明星类型为“体育明星”中年龄最大的姓名、特技、籍贯信息，要求显示列别名。

select top 1 S_AGE,S_NAME 姓名,S_HOBBY 特技,S_NATIVE 籍贯信息 from  StarType e inner join  StarInfo r on  e.T_NO=r.S_T_NO where T_NO=1 order by S_AGE desc