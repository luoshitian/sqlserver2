create database GoodDB
go
use GoodDB
go

create table GoodsType
(
	TypeID int not null primary key identity ,
	TypeName nvarchar(20) not null
)

create table GoodInfo
(
	GoodsID int primary key identity not null,
	GiidsName nvarchar(20) not null,
	GoodsColor nvarchar(20) not null,
	GoodsBrand nvarchar(20) ,
	GoodsMoney money not null,
	TypeId int foreign key references GoodsType(TypeID)
)
go
insert into GoodsType(TypeName)
select '服装内衣' union
select '鞋包配饰' union
select '手机数码'

insert into GoodInfo(GiidsName,GoodsColor,GoodsBrand,GoodsMoney,TypeId)
values('提花小西装','红色','菲曼琪',300,1),('百搭短裤','绿色','哥弟',100,1),('无袖背心','白色','阿依莲',700,1),
('低帮休闲鞋','红色','菲曼琪',900,2),('中跟单鞋','绿色','哥弟',400,2),('平底鞋','白色','阿依莲',200,2),
('迷你照相机','红色','尼康',500,3),('硬盘','黑色','希捷',600,3),('显卡','黑色','技嘉',800,3)
go
--3、查询价格最贵的商品名称，商品颜色和商品价格，要求使用别名显示列名
select GiidsName 商品名称,GoodsColor 商品颜色,GoodsMoney 商品价格 from GoodInfo where GoodsMoney = (select max(GoodsMoney) from GoodInfo)

--4、按商品类型编号分组查询商品最高价格，最低价格和平均价格，要求使用别名显示列名
select TypeID 商品类型,max(GoodsMoney) 最高价格,min(GoodsMoney) 最低价格 ,avg(GoodsMoney) 平均价格 from GoodInfo group by TypeID

--5、查询商品信息所有列，要求商品颜色为红色，价格在300~600之间
select * from GoodInfo where GoodsColor='红色' and GoodsMoney between 300 and 600

