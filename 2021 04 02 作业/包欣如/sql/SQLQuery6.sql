use master 
go
create database  StarManagerDB
on
(
name='StarManagerDB',
size=5mb,
filename='c:\sql\StarManagerDB.mdf',
maxsize=50mb,
filegrowth=1mb
)
log on
(
name='StarManagerDB_log',
size=5mb,
filename='c:\sql\StarManagerDB_log.ldf',
maxsize=50mb,
filegrowth=1mb
)
go
use StarManagerDB
create table StarType
(
T_NO int not null primary key identity,
T_NAME nvarchar(20)
)
create table StarInfo
(
S_NO int not null primary key identity,
S_NAME nvarchar(20) not null,
S_AGE int not null,
S_HOBBY nvarchar(20),
S_NATIVE nvarchar(20) default('中国大陆'),
S_T_NO int foreign key references StarType(T_NO)
)
go
insert into StarType values('体育明星'),
('IT明星'),
('相声演员')
insert into StarInfo(S_NAME,S_AGE,S_HOBBY,S_NATIVE,S_T_NO)
values('梅西',30,'射门','阿根廷',1),
('科比',25,'过人','美国',1),
('蔡景现',40,'敲代码','中国',2),
('马斯克',36,'造火箭','外星人',2),
('郭德纲',50,'相声','中国',3),
('黄铮',41,'拼多多','中国',2)

select*from StarType
select*from StarInfo
--查询年龄最大的3个明星的姓名，特技和籍贯信息，要求使用别名显示列名。
select top 3 S_NAME 姓名,S_HOBBY 特技,S_NATIVE 籍贯,S_AGE,max(S_AGE)
from StarInfo group by S_NAME,S_HOBBY,S_NATIVE,S_AGE order by S_AGE desc
--按明星类型编号分类查询明星人数，明星平均年龄，显示明星人数大于2的分组信息，要求使用别名显示列名。
select S_NO 明星编号,S_AGE 年龄,count(S_NO), avg (S_AGE) from StarInfo where S_NO>2 group by  S_NO,S_AGE
--查询     明星类型为“体育明星”中年龄最大     的     姓名、特技、籍贯信息，要求显示列别名。
select * from StarInfo where S_AGE = (select MAX(S_AGE) from  StarInfo T inner join StarType S on T.S_T_NO=S.T_NO where T_NAME='体育明星' )
--S_NAME,S_AGE,S_HOBBY,S_NATIVE 