use master
go
create database GoodDB
on
(
name='GoodDB',
filename='C:\sql\GoodDB.mdf',
size=5mb,
maxsize=100mb,
filegrowth=10mb
)
log on
(
name='GoodDB_log',
filename='C:\sql\GoodDB_log.ldf',
size=5mb,
maxsize=100mb,
filegrowth=10mb
)
go
use GoodDB
go
create table GoodsType
(
TypeID int primary key identity not null,
TypeName nvarchar(20) not null
)
create table GoodsInfo
(
GoodsID int primary key identity not null,
GoodsName nvarchar(20)not null,
GoodsColor  nvarchar(20)not null,
GoodsBrand nvarchar(20) null,
GoodsMoney money not null,
TypeID int foreign key references GoodsType(TypeID)
)
go
insert into GoodsType(TypeName)
 values('服装内衣'),
 ('鞋包服饰'),
 ('手机数码')
insert into GoodsInfo(GoodsName,GoodsColor,GoodsBrand,GoodsMoney,TypeID)
values ('提花小西装','红色',' 菲曼琪', 300, 1),
('百搭短裤 ','绿色 ','哥弟' ,100 ,1),
('无袖背心',' 白色', '阿依莲', 700, 1),
('低帮休闲鞋','红色', '菲曼琪', 900, 2),
('中跟单鞋',' 绿色',' 哥弟' ,400, 2),
('平底鞋', '白色', '阿依莲',200,2),
('迷你照相机',' 红色',' 尼康', 500, 3),
('硬盘',' 黑色',' 希捷', 600, 3),
('显卡',' 黑色',' 技嘉', 800, 3)

select*from GoodsType
select*from GoodsInfo
--查询价格最贵的商品名称，商品颜色和商品价格，要求使用别名显示列名
select top 1 goodsname 商品名称,goodscolor 商品颜色,GoodsMoney 商品价格,max(GoodsMoney)from GoodsInfo group by goodsname,goodscolor,GoodsMoney order by GoodsMoney desc
--按商品类型编号分组查询商品最高价格，最低价格和平均价格，要求使用别名显示列名
select TypeID 商品类型编号 ,max(GoodsMoney),min(GoodsMoney),avg(GoodsMoney)from GoodsInfo group by TypeID
--查询商品信息所有列，要求商品颜色为红色，价格在300~600之间	
select  goodsid 商品编号 ,GoodsColor 商品颜色,GoodsMoney 商品价格 from GoodsInfo  where GoodsColor='红色' and GoodsMoney>=300 and GoodsMoney<=600