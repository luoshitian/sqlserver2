use master 
go
create database  HOUSE_DB
on
(
name='HOUSE_DB',
size=5mb,
filename='c:\sql\HOUSE_DB.mdf',
maxsize=50mb,
filegrowth=1mb
)
log on
(
name='HOUSE_DB_log',
size=5mb,
filename='c:\sql\HOUSE_DB_log.ldf',
maxsize=50mb,
filegrowth=1mb
)
go
use HOUSE_DB
create table HOUSE_TYPE
(
type_id int not null primary key identity,
type_name varchar(50) not null unique
)
create table HOUSE
(
house_id int not null primary key identity,
house_name varchar(50) not null,
house_price float not null default(0),
type_id int foreign key references  HOUSE_TYPE(type_id)
)
go
insert into HOUSE_TYPE(type_name)
values('小户型'),('经济型'),('别墅')
insert into  HOUSE(house_name,house_price,type_id )
values('北京四合院',80000000,1),
('上海海景房',2000000,3),
('浙江大别墅',40000000,2)
delete  HOUSE where house_id>=4
select*from HOUSE
select*from HOUSE_TYPE
--查询所有房屋信息
select*from HOUSE
--使用模糊查询包含”型“字的房屋类型信息
select TYPE_NAME from HOUSE_TYPE  where TYPE_NAME like'%型%'
--查询出房屋的名称和租金，并且按照租金降序排序
select house_name,house_price ,count(house_price)from HOUSE group by house_name,house_price order by house_price
--使用连接查询，查询信息，显示房屋名称和房屋类型名称
select *from HOUSE H inner join HOUSE_TYPE T on H.type_id=T.type_id 
--查询所有房屋中月租最高的房屋，显示最高的租金和房屋名称
select  top 1 house_name,house_price, max(house_price) from HOUSE group by house_name,house_price order by house_price DESC