use master
go
create database wb
on
(
name='wb',
filename='c:\SQL\wb.mdf',
size=5mb,
maxsize=100mb,
filegrowth=10mb
)
log on
(
name='wb_log',
filename='c:\SQL\wb_log.mdf',
size=5mb,
maxsize=100mb,
filegrowth=10mb
)
go
use wb
go
create table tbl_card
(
ID varchar (20)primary key not null,
passWord varchar(20) not null,
balance int not null,
userName nvarchar(2) not null
)
create table tbl_computer
(
ID  varchar(20) primary key  not null,
onuse bit check(onuse=0 or onuse=1),
note int not null
)
create table tbl_record
(
ID int primary key  not null,
cardId varchar(20) foreign key references tbl_card (ID),
ComputerId  varchar(20)foreign key references tbl_computer(ID),
beginTime datetime not null,
endTime datetime not null,
fee int not null
)
go
insert into tbl_card(ID,passWord,balance,userName)
 values ('0023_ABC','555',98,'张军'),
('0025_bbd','abe',300,'朱俊'),
('0036_CCD','何柳',100,'何柳'),
('0045_YGR','0045_YGR',58,'证验'),
('0078_RJV','55885fg',600,'校庆'),
('0089_EDE','zhang',134,'张骏' )
insert into tbl_computer(ID,onuse,note) values ('02',0,25555),('03',1,55555),('04',0,66666),('05',1,88888),
('06',0,688878),('b01',0,558558)
insert into tbl_record(id ,cardid ,computerid,begintime,endtime,fee)
values(23,'0078_RJV','B01','2007-07-15 19:00:00','2007-07-15 21:00:00',20),
(34,'0025_bbd','02','2006-12-25 18:00:00','2006-12-25 22:00:00',23),
(45,'0023_ABC','03','2006-12-23 15:26:00','2006-12-23 22:55:00',50),
(46,'0023_ABC','03','2006-12-22 15:26:00','2006-12-23 22:55:00',6),
(47,'0023_ABC','03','2006-12-23 15:26:00','2006-12-23 22:55:00',50),
(48,'0023_ABC','03','2007-01-06 15:26:00','2007-01-06 22:55:00',6),
(55,'0023_ABC','03','2006-07-21 15:26:00','2006-07-21 22:55:00',50),
(64,'0045_YGR','04','2006-12-24 18:00:00','2006-12-24 22:55:00',3),
(65,'0025_bbd','02','2006-12-28 18:00:00','2006-12-28 22:00:00',23),
(98,'0025_bbd','02','2006-12-26 18:00:00','2006-12-26 22:00:00',23)
go
select *from tbl_card
select *from tbl_computer
select *from tbl_record

--1. 查询出用户名为'张军'的上网卡的上网记录，要求显示卡号，用户名，机器编号、开始时间、结束时间，和消费金额，并按消费金额降序排列
select username,cardId,computerid,begintime,endtime,fee from tbl_card inner join tbl_record on tbl_card .ID=tbl_record.cardId where userName='张军'
group by cardId,computerid,begintime,endtime,username ,fee order by fee desc
--2. 查询出每台机器上的上网次数和消费的总金额
select computerid 每台机器  ,fee 消费金额,count (computerid)上网次数,sum (fee) 消费总金额 from tbl_computer inner join tbl_record on tbl_computer.ID=tbl_record.ComputerId group by computerid ,fee

--3. 查询出所有已经使用过的上网卡的消费总金额
select
--4. 查询出从未消费过的上网卡的卡号和用户名


--5. 将密码与用户名一样的上网卡信息查询出来


--6. 查询出使用次数最多的机器号和使用次数


--7. 查询出卡号是以'ABC'结尾的卡号，用户名，上网的机器号和消费金额


--8. 查询出是周六、周天上网的记录，要求显示上网的卡号，用户名，机器号，开始时间、结束时间和消费金额


--9. 查询成一次上网时间超过12小时的的上网记录，要求显示上网的卡号，用户名，机器号，开始时间、结束时间和消费金额


--10. 查询除消费金额排列前三名(最高)的上网记录，要求显示上网的卡号，用户名，机器号，开始时间、结束时间和消费金额




