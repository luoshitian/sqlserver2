use master
go
create database StarManagerDB
on
(
 name='StarManagerDB',
 filename='D:\StarManagerDB.mdf',
 size=6MB,
 maxsize=100MB,
 filegrowth=10Mb
)
log on
(
 name='StarManagerDB_log',
 filename='D:\StarManagerDB_log.ldf',
 size=6MB,
 maxsize=100MB,
 filegrowth=10Mb
)
go
use StarManagerDB
go

create table StarType
(
	T_NO int primary key identity(1,1),
	T_NAME nvarchar(20)
)

create table StarInfo
(
	S_NO int primary key identity(1,1),
	S_NAME nvarchar(20) not null,
	S_AGE int not null,
	S_HOBBY nvarchar(20),
	S_NATIVE nvarchar(20) default('中国大陆'),
	S_T_NO int references StarType(T_NO)
)

insert into StarType values('体育明星'),('IT明星'),('相声演员')

insert into StarInfo values('梅西',30,'射门','阿根廷',1),
('科比',35,'过人','美国',1),
('蔡景现',40,'敲代码','中国',2),
('马斯克',36,'造火箭','外星人',2),
('郭德纲',50,'相声','中国',3),
('黄峥',41,'拼多多','中国',2)

--3、查询年龄最大的3个明星的姓名，特技和籍贯信息，要求使用别名显示列名。

select top 3 S_AGE 年龄,S_NAME 姓名,S_HOBBY 特技,S_NATIVE 籍贯 from StarInfo order by S_AGE DESC

--4、按明星类型编号分类查询明星人数，明星平均年龄，显示明星人数大于2的分组信息，要求使用别名显示列名。

select T_NO 类型编号,  COUNT(*) 人数,AVG(S_AGE) 平均年龄 from StarType ST INNER JOIN StarInfo SI ON ST.T_NO=SI.S_T_NO
group by T_NO having COUNT(*)>2

--5、查询明星类型为“体育明星”中年龄最大的姓名、特技、籍贯信息，要求显示列别名。

select top 1 S_AGE 年龄,S_NAME 姓名,S_HOBBY 特技,S_NATIVE 籍贯 from StarInfo where S_T_NO=1 order by S_AGE DESC

