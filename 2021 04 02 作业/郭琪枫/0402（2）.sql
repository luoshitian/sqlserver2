use master
go
create database HOUSE_DB
on
(
 name='HOUSE_DB',
 filename='D:\HOUSE_DB.mdf',
 size=5MB,
 maxsize=50MB,
 filegrowth=1Mb
)
log on
(
 name='HOUSE_DB_log',
 filename='D:\HOUSE_DB_log.ldf',
 size=5MB,
 maxsize=50MB,
 filegrowth=1Mb
)
go
use HOUSE_DB
go

create table HOUSE_TYPE
(
	type_id int primary key identity(1,1),
	type_name varchar(50) unique not null
)

create table HOUSE
(
	house_id int primary key identity(1,1),
	house_name varchar(50) not null,
	house_price float default(0) not null,
	type_id int references HOUSE_TYPE(type_id)
)

insert into HOUSE_TYPE values('小户型'),('经济型'),('别墅')

insert into HOUSE values('宿舍',1000,1),('旺铺',500000,2),('避暑山庄',100000000,3)

--查询所有房屋信息

select * from HOUSE

--使用模糊查询包含”型“字的房屋类型信息

select  * from HOUSE H inner join HOUSE_TYPE HT on H.type_id=HT.type_id where type_name like '%型%'

--查询出房屋的名称和租金，并且按照租金降序排序

select  house_name 房屋的名称,house_price 租金 from HOUSE ORDER BY house_price

--使用连接查询，查询信息，显示房屋名称和房屋类型名称

select  house_name 房屋的名称,type_name 房屋类型名称 from HOUSE H inner join HOUSE_TYPE HT on H.type_id=HT.type_id

--查询所有房屋中月租最高的房屋，显示最高的租金和房屋名称

select  house_name 房屋的名称,MAX(house_price)租金 from HOUSE 
where house_price=(select MAX(house_price) from HOUSE ) group by house_name