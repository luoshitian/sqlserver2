create database GoodsDB
use GoodsDB
go
create table GoodsType
(
	TypeID int not null primary key identity(1,1),		--商品类型编号
	TypeName nvarchar(20) not null						--商品类型名称
)
go
create table GoodsInfo
(
	GoodsID int not null primary key identity(1,1),
	GoodsName nvarchar(20) not null,
	GoodsColor nvarchar(20) not null,
	GoodsBrand nvarchar(20),
	GoodsMoney money not null,
	TypeID int references GoodsType(TypeID)
)
insert into GoodsType(TypeName) values ('服饰内衣'),('鞋包服饰'),('手机数码')
insert into GoodsInfo(GoodsName,GoodsColor,GoodsBrand,GoodsMoney,TypeID)
select '提花小西装','红色','菲曼斯',300,1 union
select '百搭短袖','绿色','哥弟',100,1 union
select '无袖背心','白色','阿依莲',700,1 union
select '低帮休闲鞋','红色','菲曼斯',900,2 union
select '中跟单鞋','绿色','哥弟',400,2 union
select '平底鞋','白色','阿依莲',200,2 union
select '迷你照相机','红色','尼康',500,3 union
select '硬盘','黑色','希捷',600,3 union
select '显卡','黑色','技嘉',800,3 

select * from GoodsType
select * from GoodsInfo
--3.查询价格最贵的商品名称，商品颜色和商品价格，要求使用别名显示列名
select top 1 GoodsName 商品名称,GoodsColor 商品颜色,GoodsMoney 商品价格 from GoodsInfo order by GoodsMoney desc
select GoodsName 商品名称,GoodsColor 商品颜色,GoodsMoney 商品价格 from GoodsInfo where GoodsMoney=(select max(GoodsMoney) from GoodsInfo)
--4、按商品类型编号分组查询商品最高价格，最低价格和平均价格，要求使用别名显示列名
select max(GoodsMoney) 最高价格,min(GoodsMoney) 最低价格,avg(GoodsMoney) 平均价格 from GoodsInfo group by TypeID
--5、查询商品信息所有列，要求商品颜色为红色，价格在300~600之间
select * from GoodsInfo where GoodsColor='红色' and GoodsMoney between 300 and 600