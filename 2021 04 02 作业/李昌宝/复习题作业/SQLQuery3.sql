create database StarManagerDB
on
(
	name = 'StarManagerDB',
	size = 5,
	maxsize =500,
	filename ='D:\新建文件夹\StarManagerDB.mdf',
	filegrowth=10%
)
log on
(
	name = 'StarManagerDB_log',
	size = 5,
	maxsize =500,
	filename ='D:\新建文件夹\StarManagerDB_log.ldf',
	filegrowth=10%
)
use StarManagerDB
go
create table StarType
(
	T_NO int not null primary key identity(1,1),
	T_NAME nvarchar(20)
)
go
create table StarInfo
(
	S_NO int not null primary key identity(1,1),
	S_NAME nvarchar(20) not null,
	S_AGE int not null,
	S_HOBBY nvarchar(20),
	S_NATIVE nvarchar(20) default('中国大陆'),
	S_T_NO int references StarType(T_NO)
)

insert into StarType(T_NAME) values ('体育明星'),('IT体育明星'),('相声演员')
insert into StarInfo(S_NAME,S_AGE,S_HOBBY,S_NATIVE,S_T_NO)
select '梅西',30,'射门','阿根廷',1  union
select '科比',35,'过人','美国',1  union
select '茶经先',40,'敲代码','中国',2  union
select '马什克',36,'找火箭','外星人',2  union
select '郭德纲',50,'相声','中国',3  union
select '黄峥',41,'拼多多','中国',2  
select * from StarType
select * from StarInfo
--3、查询年龄最大的3个明星的姓名，特技和籍贯信息，要求使用别名显示列名。
select top 3 S_NAME 姓名,S_HOBBY 特技,S_NATIVE 籍贯信息  from StarInfo order by S_AGE desc
--4、按明星类型编号分类查询明星人数，明星平均年龄，显示明星人数大于2的分组信息，要求使用别名显示列名。
select avg(S_AGE) 平均年龄,count(S_T_NO) 该类型人数,S_T_NO 明星类型编号 from StarInfo group by S_T_NO having count(S_T_NO)>2 
--5、查询明星类型为“体育明星”中年龄最大的姓名、特技、籍贯信息，要求显示列别名。
select  top 1 a.S_NAME 姓名,a.S_HOBBY 特技,a.S_NATIVE 籍贯,a.S_AGE 年龄 from StarInfo a inner join StarType b on a.S_T_NO=b.T_NO where b.T_NAME='体育明星' order by S_AGE desc
