create database HOUSE_DB
on
(
	name = 'HOUSE_DB',
	size = 5,
	maxsize = 50,
	filegrowth = 1,
	filename = 'D:\新建文件夹\HOUSE_DB.mdf'
)
log on
(
	name = 'HOUSE_DB_log',
	size = 5,
	maxsize = 50,
	filegrowth = 1,
	filename = 'D:\新建文件夹\HOUSE_DB_log.ldf'
)
use HOUSE_DB
go
create table HOUSE_TYPE
(
	type_id int not null primary key identity(1,1),
	type_name varchar(50) not null unique,
)
go
create table HOUSE
(
	house_id int not null primary key identity(1,1),
	house_name varchar(50) not null ,
	house_price float not null default(0),
	type_id int not null references HOUSE_TYPE(type_id)
)
insert into HOUSE_TYPE(type_name) values('小户型'),('经济型'),('别墅')
insert into HOUSE(house_name,house_price,type_id) values ('别',1000,3),('墅',100,3),('经',89.2,2),('济',223.3,2),('小',43.2,1),('户',1222.2,1)
select * from HOUSE_TYPE
select * from HOUSE

--查询所有房屋信息
select a.*,b.type_name from HOUSE a inner join HOUSE_TYPE b on a.type_id=b.type_id
--使用模糊查询包含”型“字的房屋类型信息
select a.*,b.type_name from HOUSE a inner join HOUSE_TYPE b on a.type_id=b.type_id where type_name like '%型%'
--查询出房屋的名称和租金，并且按照租金降序排序
select house_name,house_price from HOUSE order by house_price desc
--使用连接查询，查询信息，显示房屋名称和房屋类型名称
select a.house_name,b.type_name from HOUSE a inner join HOUSE_TYPE b on a.type_id=b.type_id
--查询所有房屋中月租最高的房屋，显示最高的租金和房屋名称
select house_price,house_name from HOUSE where house_price=(select max(house_price) from HOUSE)
