--1、创建商品数据库（GoodsDB），然后建立两张表，GoodsType（商品类型表），GoodsInfo（商品信息表），表结构分别如下：
--商品类型表（GoodsType）
--字段名	说明	类型	长度	可否为空	约束
--TypeID	商品类型编号	int		否	主键约束，自增约束，标识种子和标识增量都是1
--TypeName	商品类型名称	nvarchar	20	否	

create database GoodsDB
go

use GoodsDB
go

create table GoodsType
(
	TypeID int not null primary key identity(1,1),
	TypeName nvarchar(20) not null
)

--商品信息表（GoodsInfo）
--字段名	说明	类型	长度	可否为空	约束
--GoodsID	商品编号	int		否	主键约束，自增约束，标识种子和标识增量都是1
--GoodsName	商品名称	nvarchar	20	否	
--GoodsColor	商品颜色	nvarchar	20	否	
--GoodsBrand	商品品牌	nvarchar	20		
--GoodsMoney	商品价格	money		否	
--TypeID	商品类型编号	int			外键，参照GoodsType中的TypeID

create table GoodsInfo 
(
	GoodsID int not null primary key identity(1,1),
	GoodsName nvarchar(20) not null,
	GoodsColor nvarchar(20) not null,
	GoodsBrand nvarchar(20),
	GoodsMoney money not null,
	TypeID int references GoodsType(TypeID)
)

--2、使用插入语句为两张表添加数据
--商品类型表（GoodsType）
--商品类型编号 商品类型名称
--1 服装内衣
--2 鞋包配饰
--3 手机数码

insert into GoodsType(TypeName)
select '服装内衣' union
select '鞋包配饰' union
select '手机数码'

--商品信息表（GoodsType）
--商品编号 商品名称 商品颜色 商品品牌 商品价格 商品类型编号
--1 提花小西装 红色 菲曼琪 300 1
--2 百搭短裤 绿色 哥弟 100 1
--3 无袖背心 白色 阿依莲 700 1
--4 低帮休闲鞋 红色 菲曼琪 900 2
--5 中跟单鞋 绿色 哥弟 400 2
--6 平底鞋 白色 阿依莲 200 2
--7 迷你照相机 红色 尼康 500 3
--8 硬盘 黑色 希捷 600 3
--9 显卡 黑色 技嘉 800 3

insert into GoodsInfo(GoodsName,GoodsColor,GoodsBrand,GoodsMoney,TypeID)
select '提花小西装','红色','菲曼琪',300,1 union
select '百搭短裤','绿色','哥弟',100,1 union
select '无袖背心','白色','阿依莲',700,1 union
select '低帮休闲鞋','红色','菲曼琪',900,2 union
select '中跟单鞋','绿色','哥弟',400,2 union
select '平底鞋','白色','阿依莲',200,2 union
select '迷你照相机','红色','尼康',500,3 union
select '硬盘','黑色','希捷',600,3 union
select '显卡','黑色','技嘉',800,3 

--3、查询价格最贵的商品名称，商品颜色和商品价格，要求使用别名显示列名

select GoodsName 商品名称,GoodsColor 商品颜色,GoodsMoney 商品价格  from GoodsInfo
	where GoodsMoney = (select MAX(GoodsMoney) from GoodsInfo)

--4、按商品类型编号分组查询商品最高价格，最低价格和平均价格，要求使用别名显示列名

select MAX(GoodsMoney) 最高价格,MIN(GoodsMoney) 最低价格,AVG(GoodsMoney) 平均价 from GoodsInfo 
	group by TypeID

--5、查询商品信息所有列，要求商品颜色为红色，价格在300~600之间

select * from GoodsInfo 
	where GoodsColor = '红色' and GoodsMoney between 300 and 600
