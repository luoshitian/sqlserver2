create database HOUSE_DB
on 
(
   name='HOUSE_DB',
   filename='D:\HOUSE_DB.mdf',
   size=5,
   maxsize=50,
   filegrowth=1
)
log on
(
   name='HOUSE_DB_log',
   filename='D:\HOUSE_DB_log.ldf',
   size=5,
   maxsize=50,
   filegrowth=1
)
go
create table HOUSE_TYPE
(
   type_id int not null primary key identity(1,1),
   type_name varchar(50) not null unique
)
create table HOUSE
(
   house_id int not null primary key identity(1,1),
   house_name varchar(50) not null,
   house_price float not null default('0'),
   type_id int not null references HOUSE_TYPE(type_id)
)

insert into HOUSE_TYPE values ('小户型'),('经济型'),('别墅')
insert into HOUSE values ('小一','400','1'),('小二','500','2'),('小三','450','3')


--4、添加查询
select * from HOUSE_TYPE
select * from HOUSE
--查询所有房屋信息
select * from HOUSE inner join HOUSE_TYPE on HOUSE.type_id = HOUSE_TYPE.type_id
--使用模糊查询包含”型“字的房屋类型信息
select HOUSE.* from HOUSE inner join HOUSE_TYPE on HOUSE.type_id = HOUSE_TYPE.type_id
where type_name like '%型%'
--查询出房屋的名称和租金，并且按照租金降序排序
select house_name 名称,house_price 租金 from HOUSE 
order by  house_price DESC
--使用连接查询，查询信息，显示房屋名称和房屋类型名称
select house_name 房屋名称,type_name 房屋类型 from HOUSE inner join HOUSE_TYPE on HOUSE.type_id = HOUSE_TYPE.type_id
--查询所有房屋中月租最高的房屋，显示最高的租金和房屋名称
select Top 1 house_price 最高的租金,house_name 房屋名称 from HOUSE
order by house_price DESC
