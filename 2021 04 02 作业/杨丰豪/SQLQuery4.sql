use master
go

create database HOUSE_DB
on
(
name='HOUSE_DB',
filename='D:\test\HOUSE_DB.mdf',
size=5MB,
maxsize=50MB,
filegrowth=1
)
log on
(
name='HOUSE_DB_log',
filename='D:\test\HOUSE_DB_log.ldf',
size=5MB,
maxsize=50MB,
filegrowth=10%
)

go
use HOUSE_DB

go
create table HOUSE_TYPE
(
type_id int not null primary key identity(1,1),
type_name varchar(50) unique not null,
)

go
create table HOUSE
(
house_id	int primary key identity(1,1) not null,
house_name	varchar(50) not null,
house_price	float default(0) not null,
type_id	int references HOUSE_TYPE(type_id)
)

--HOUSE_TYPE表中添加3条数据，例如：小户型、经济型、别墅
--HOUSE表中添加至少3条数据，不能全都为同一类型
go
insert into HOUSE_TYPE (type_name) values ('小户型'),('经济型'),('别墅')
insert into HOUSE (house_name,house_price,type_id) values('单身公寓',500,1),('商品房',1200,2),('海边别墅',5000,3)

go
select * from HOUSE_TYPE 
select * from HOUSE
--添加查询
--查询所有房屋信息
select * from HOUSE
--使用模糊查询包含”型“字的房屋类型信息
select * from HOUSE_TYPE  where  type_name like'%型%'
--查询出房屋的名称和租金，并且按照租金降序排序
select house_name 房屋的名称,house_price 租金 from HOUSE order by house_price desc
--使用连接查询，查询信息，显示房屋名称和房屋类型名称
select type_name 房屋类型,HOUSE.house_name 房屋名称,house_price 房屋租金 from HOUSE_TYPE
inner join HOUSE on HOUSE.type_id=HOUSE_TYPE.type_id
--查询所有房屋中月租最高的房屋，显示最高的租金和房屋名称
select house_name 房屋名称,house_price 最高租金 from HOUSE where house_price=(select MAX(house_price) from HOUSE)  group by house_name,house_price
