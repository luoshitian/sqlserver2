use master
go

create database StarManagerDB
on
(
name='StarManagerDB',
filename='D:\test\StarManagerDB.mdf',
size=5MB,
maxsize=50mb,
filegrowth=10%
)
log on
(
name='StarManagerDB_log',
filename='D:\test\StarManagerDB_log.ldf',
size=5MB,
maxsize=50mb,
filegrowth=10%
)

go
use StarManagerDB

go
create table StarType
(
T_NO int primary key identity(1,1) not null,
T_NAME	nvarchar(20)
)

go
create table StarInfo
(
S_NO int primary key identity(1,1),
S_NAME nvarchar(20) not null,
S_AGE int not null,
S_HOBBY	nvarchar(20),
S_NATIVE nvarchar(20) default('中国大陆'),
S_T_NO	int	references StarType(T_NO)
)

go
insert into StarType (T_NAME) values ('体育明星'),('IT明星'),('相声演员')

--明星表（StarInfo）
--明星编号	姓名	年龄	特技	籍贯	明星类型编号
--1	梅西	30	射门	阿根廷	1
--2	科比	35	过人	美国	1
--3	蔡景现	40	敲代码	中国	2
--4	马斯克	36	造火箭	外星人	2
--5	郭德纲	50	相声	中国	3
--6	黄铮	41	拼多多	中国	2


go
insert into StarInfo (S_NAME,S_AGE,S_HOBBY,S_NATIVE,S_T_NO) values 
('梅西',30,'射门','阿根延',1),
('科比',35,'过人','美国',1),
('蔡景现',40,'敲代码','中国',2),
('蔡景现',40,'敲代码','中国',2),
('马斯克',36,'造火箭','外星人',2),
('郭德纲',50,'相声','中国',3),
('黄铮',41,'拼多多','中国',2)

--3、查询年龄最大的3个明星的姓名，特技和籍贯信息，要求使用别名显示列名。
select top 3 S_NAME 姓名,S_AGE 年龄,S_HOBBY 特技,S_NATIVE 籍贯信息 from StarInfo order by S_AGE desc

--4select S_T_NO as 类型,count(*) as 人数,avg(S_Age) as 平均年龄 from StarInfo
select S_T_NO as 类型,count(*) 人数,avg(S_Age) 平均年龄 from StarInfo group by S_T_NO

--5、查询明星类型为“体育明星”中年龄最大的姓名、特技、籍贯信息，要求显示列别名。
