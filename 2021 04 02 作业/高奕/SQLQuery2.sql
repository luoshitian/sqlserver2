create database HOUSE_DB
on
(
	name=house,
	filename='E:\Demo\house.mdf',
	size=5mb,
	maxsize=50mb,
	filegrowth=1mb
)
log on
(
	name=house_log,
	filename='E:\Demo\house_log.ldf',
	size=5mb,
	maxsize=50mb,
	filegrowth=1mb
)
go

use HOUSE_DB
go

create table HOUSE_TYPE
(
	type_id	int	primary key identity(1,1) not null, 
	type_name varchar(50) unique not null
)
go

create table HOUSE
(
	house_id int  primary key identity(1,1),
	house_name	varchar(50) not null,
	house_price	float default(0) not null,
	type_id	int references HOUSE_TYPE(type_id) not null
)
go


insert into HOUSE_TYPE values ('小户型'),
('经济型'),
('别墅')


insert into HOUSE (house_name,house_price,type_id) values ('大平层海景型别墅',5000,3),
('精装三户',2500,1),
('简易三户',2000,2),
('独栋海景',5000,3)



--查询所有房屋信息
select * from HOUSE full join HOUSE_TYPE on HOUSE.type_id=HOUSE_TYPE.type_id

--使用模糊查询包含”型“字的房屋类型信息
select * from HOUSE full join HOUSE_TYPE on HOUSE.type_id=HOUSE_TYPE.type_id 
where house_name like '%型%'  

--查询出房屋的名称和租金，并且按照租金降序排序
select house_name,house_price from HOUSE order by house_price DESC

--使用连接查询，查询信息，显示房屋名称和房屋类型名称
select HOUSE_TYPE.type_name,HOUSE.house_name from HOUSE 
inner join HOUSE_TYPE on HOUSE_TYPE.type_id=HOUSE.type_id


--查询所有房屋中月租最高的房屋，显示最高的租金和房屋名称
select max(house_price) from HOUSE 

select top 1  house_name,house_price from HOUSE
where house_price=(select max(house_price) from HOUSE )

select top 1  house_name,house_price from HOUSE
order by house_price DESC

































