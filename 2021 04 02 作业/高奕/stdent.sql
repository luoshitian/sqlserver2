create database ClassicDb

GO



use ClassicDb

GO



create table StudentInfo

(

    Id int PRIMARY key not null IDENTITY,

    StudentCode nvarchar(80),

    StudentName nvarchar(80),

    Birthday date not null,

    Sex nvarchar(2),

    ClassId int not null

)



GO



-- select * from StudentInfo



insert into StudentInfo (StudentCode,StudentName,Birthday,Sex,ClassId) values('01' , '赵雷' , '1990-01-01' , 'm',1)

insert into StudentInfo (StudentCode,StudentName,Birthday,Sex,ClassId) values('02' , '钱电' , '1990-12-21' , 'm',1)

insert into StudentInfo (StudentCode,StudentName,Birthday,Sex,ClassId) values('03' , '孙风' , '1990-12-20' , 'm',1)

insert into StudentInfo (StudentCode,StudentName,Birthday,Sex,ClassId) values('04' , '李云' , '1990-12-06' , 'm',1)

insert into StudentInfo (StudentCode,StudentName,Birthday,Sex,ClassId) values('05' , '周梅' , '1991-12-01' , 'f',1)

insert into StudentInfo (StudentCode,StudentName,Birthday,Sex,ClassId) values('06' , '吴兰' , '1992-01-01' , 'f',1)

insert into StudentInfo (StudentCode,StudentName,Birthday,Sex,ClassId) values('07' , '郑竹' , '1989-01-01' , 'f',1)

insert into StudentInfo (StudentCode,StudentName,Birthday,Sex,ClassId) values('09' , '张三' , '2017-12-20' , 'f',1)

insert into StudentInfo (StudentCode,StudentName,Birthday,Sex,ClassId) values('10' , '李四' , '2017-12-25' , 'f',1)

insert into StudentInfo (StudentCode,StudentName,Birthday,Sex,ClassId) values('11' , '李四' , '2012-06-06' , 'f',1)

insert into StudentInfo (StudentCode,StudentName,Birthday,Sex,ClassId) values('12' , '赵六' , '2013-06-13' , 'f',1)

insert into StudentInfo (StudentCode,StudentName,Birthday,Sex,ClassId) values('13' , '孙七' , '2014-06-01' , 'f',1)





GO





CREATE TABLE Teachers

(

    Id int PRIMARY key not null IDENTITY,

    TeacherName nvarchar(80)

)



go

-- select * from Teachers



insert into Teachers (TeacherName) values('张三')

insert into Teachers (TeacherName) values('李四')

insert into Teachers (TeacherName) values('王五')



GO



create table CourseInfo

(

    Id int PRIMARY key not null IDENTITY,

    CourseName NVARCHAR(80) not null,

    TeacherId int not null

)



go

-- select * from CourseInfo



insert into CourseInfo (CourseName,TeacherId) values( '语文' , 2)

insert into CourseInfo (CourseName,TeacherId) values( '数学' , 1)

insert into CourseInfo (CourseName,TeacherId) values( '英语' , 3)



GO



create table StudentCourseScore

(

    Id int PRIMARY key not null IDENTITY,

    StudentId int not null,

    CourseId int not null,

    Score int not null

)

go

-- select * from StudentCourseScore



insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='01') , 1 , 80)

insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='01') , 2 , 90)

insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='01') , 3 , 99)

insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='02') , 1 , 70)

insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='02') , 2 , 60)

insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='02') , 3 , 80)

insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='03') , 1 , 80)

insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='03') , 2 , 80)

insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='03') , 3 , 80)

insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='04') , 1 , 50)

insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='04') , 2 , 30)

insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='04') , 3 , 20)

insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='05') , 1 , 76)

insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='05') , 2 , 87)

insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='06') , 1 , 31)

insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='06') , 3 , 34)

insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='07') , 2 , 89)

insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='07') , 3 , 98)



go

select * from StudentCourseScore
select * from StudentInfo
select * from CourseInfo
select * from Teachers

-- 1.查询"数学 "课程比" 语文 "课程成绩高的  学生的信息  及课程分数
 
--select AA.*,SI.StudentName from (select yu.StudentId,yu.CourseId yuCourseId,yu.Score yuScore,shu.CourseId shuCourseId,shu.Score shuScore
--from (SELECT * FROM  StudentCourseScore SC WHERE SC.CourseId=1) AS yu,
--(SELECT * FROM StudentCourseScore SC WHERE SC.CourseId=2) AS shu  
--WHERE shu.Score > yu.Score AND yu.StudentId = shu.StudentId) AA 
--inner join StudentInfo SI ON SI.Id=AA.StudentId

--select * from (SELECT * FROM  StudentCourseScore SC WHERE SC.CourseId=1) AS yu,
--(SELECT * FROM StudentCourseScore SC WHERE SC.CourseId=2) AS shu  
--WHERE shu.Score > yu.Score AND yu.StudentId = shu.StudentId


SELECT * FROM StudentCourseScore A,StudentCourseScore B 
where A.StudentId=B.StudentId AND A.CourseId=2 AND B.CourseId=1 AND A.Score>B.Score

-- 1.1 查询同时存在" 数学 "2课程和" 语文 "1课程的情况
select StudentId,count(CourseId) from StudentCourseScore
where CourseId = 1 or CourseId =2
group by StudentId
having count(CourseId)=2
-- 1.2 查询存在" 数学 "课程但可能不存在" 语文 "课程的情况(不存在时显示为 null )
select * from 
(select * from StudentCourseScore where CourseId =2)as A 
left join(select * from StudentCourseScore where CourseId =1) as B on A.StudentId= B.StudentId


-- 1.3 查询不存在" 数学 "课程但存在" 语文 "课程的情况
select * from 
(select * from StudentCourseScore where CourseId =1)as A 
left join(select * from StudentCourseScore where CourseId =2) as B on A.StudentId= B.StudentId


-- 2.查询 平均成绩大于等于 60 分 的同学的 学生编号和 学生姓名和 平均成绩
select StudentId,StudentName,AVG(Score)平均成绩 from StudentCourseScore 
inner join StudentInfo on StudentInfo.StudentCode=StudentCourseScore.StudentId 
group by StudentId,StudentName having AVG(Score)>=60


-- 3.查询在 成绩 表存在成绩的学生信息
select * from StudentInfo 
right join StudentCourseScore on StudentCourseScore.StudentId=StudentInfo.StudentCode 


-- 4.查询所有同学的学生编号、学生姓名、选课总数、所有课程的总成绩(没成绩的显示为 null )
select StudentInfo.Id 学生编号,StudentName 学生姓名,count(StudentId)选课总数,sum(Score)所有课程的总成绩 from StudentInfo 
left join StudentCourseScore  on StudentInfo.StudentCode= StudentCourseScore.StudentId
group by StudentInfo.Id,StudentName order by StudentInfo.Id

-- 4.1 查有成绩的学生信息
select * from StudentInfo inner join StudentCourseScore on StudentCourseScore.StudentId=StudentInfo.StudentCode 


-- 5.查询「李」姓老师的数量
select * from Teachers where TeacherName like '李%'


-- 6.查询学过「张三」老师授课的同学的信息
select * from StudentInfo 
inner join  StudentCourseScore on StudentCourseScore.StudentId=StudentInfo.StudentCode
where StudentCourseScore.CourseId=2 


-- 7.查询没有学全所有课程的同学的信息
select StudentId,StudentName,Birthday,Sex,ClassId from StudentCourseScore 
inner join StudentInfo on StudentInfo.StudentCode=StudentCourseScore.StudentId
group by StudentId,StudentName,Birthday,Sex,ClassId having count(StudentId)<3

-- 8.查询至少有一门课与学号为" 01 "的同学所学相同的同学的信息
select distinct StudentId,StudentName from StudentCourseScore 
inner join StudentInfo on StudentCourseScore.StudentId=StudentInfo.StudentCode 
where CourseId=1 or CourseId=2 or CourseId=3

-- 9.查询和" 01 "号的同学学习的课程 完全相同的其他同学的信息
select StudentId from StudentCourseScore group by StudentId having count(StudentId)=3 


-- 10.查询没学过"张三"老师讲授的任一门课程的学生姓名
select * from StudentCourseScore 
right join StudentInfo on StudentInfo.StudentCode=StudentCourseScore.StudentId

-- 11.查询两门及其以上不及格课程的同学的学号，姓名及其平均成绩

select StudentId,StudentName,avg(Score)平均成绩 from StudentCourseScore sc
inner join StudentInfo  B on sc.StudentId = B.StudentCode
Where (Select Count(*) From StudentCourseScore s1 Where s1.StudentId=sc.StudentId And Score<60)>=2 
Group By StudentId ,StudentName

-- 12.检索" 数学 "课程分数小于 60，按分数降序排列的学生信息
select StudentId,Score from StudentCourseScore 
where StudentId=(select * from StudentCourseScore where CourseId=2 and Score<60)



-- 13.按平均成绩从高到低显示所有学生的所有课程的成绩以及平均成绩
select StudentId 学生号,sum(Score)总成绩,avg(Score)平均成绩 from StudentCourseScore group by StudentId order by avg(Score) DESC


-- 14.查询各科成绩最高分、最低分和平均分：
select max(Score)最高分,min(Score)最低分,avg(Score)平均分 from StudentCourseScore group by CourseId


-- 15.以如下形式显示：课程 ID，课程 name，最高分，最低分，平均分，及格率，中等率，优良率，优秀率

/*

	及格为>=60，中等为：70-80，优良为：80-90，优秀为：>=90



	要求输出课程号和选修人数，查询结果按人数降序排列，若人数相同，按课程号升序排列



	按各科成绩进行排序，并显示排名， Score 重复时保留名次空缺

*/	

	

-- 15.1 按各科成绩进行排序，并显示排名， Score 重复时合并名次



-- 16.查询学生的总成绩，并进行排名，总分重复时保留名次空缺



-- 16.1 查询学生的总成绩，并进行排名，总分重复时不保留名次空缺



-- 17.统计各科成绩各分数段人数：课程编号，课程名称，[100-85]，[85-70]，[70-60]，[60-0] 及所占百分比



-- 18.查询各科成绩前三名的记录



-- 19.查询每门课程被选修的学生数
select CourseId 课程名,count(CourseId)每门课程被选修的学生数 from StudentCourseScore group by CourseId


-- 20.查询出只选修两门课程的学生学号和姓名
select StudentId,StudentName from StudentCourseScore
inner join StudentInfo on StudentCourseScore.StudentId=StudentInfo.StudentCode
group by StudentId,StudentName having count(StudentId)=2

-- 21.查询男生、女生人数
select sex,count(Sex) from StudentInfo group by Sex


-- 22.查询名字中含有「风」字的学生信息
select * from StudentInfo where StudentName like '%风%'


-- 23.查询同名同性学生名单，并统计同名人数
SELECT StudentName,
COUNT(1) 
FROM StudentInfo st 
GROUP BY StudentName,Sex HAVING COUNT(1)>1


-- 24.查询 1990 年出生的学生名单
select * from StudentInfo where datepart(yyyy,Birthday)=1990


-- 25.查询 每门课程的 平均成绩，结果按 平均成绩降序排列，平均成绩相同时，按 课程编号升序排列
select CourseId,avg(Score) from StudentCourseScore group by CourseId order by avg(Score)DESC,CourseId ASC


-- 26.查询平均成绩大于等于 85 的所有学生的学号、姓名和平均成绩
select StudentCode,StudentName,avg(Score) from StudentCourseScore 
inner join StudentInfo on StudentInfo.StudentCode=StudentCourseScore.StudentId
group by StudentCode,StudentName having avg(Score)>=85


-- 27.查询课程名称为「数学」，且分数低于 60 的学生姓名和分数
select StudentName,Score from StudentInfo 
inner join StudentCourseScore on StudentCourseScore.StudentId=StudentInfo.StudentCode 
where CourseId=2 and Score<60 group by StudentName,Score


-- 28.查询所有学生的课程及分数情况（存在学生没成绩，没选课的情况）
select  StudentName 姓名,CourseName 科目,Score 成绩 from StudentCourseScore 
full join StudentInfo on StudentInfo.StudentCode=StudentCourseScore.StudentId
full join CourseInfo on StudentCourseScore.CourseId=CourseInfo.Id

-- 29.查询任何一门课程成绩在 70 分以上的姓名、课程名称和分数
select StudentName 姓名,CourseId 课程,Score 分数 from StudentCourseScore 
inner join StudentInfo on StudentCourseScore.StudentId=StudentInfo.StudentCode
where Score>70
group by StudentName,CourseId,Score


-- 30.查询不及格的课程
select StudentName 姓名,CourseName 课程,Score 分数 from StudentCourseScore 
inner join StudentInfo on StudentCourseScore.StudentId=StudentInfo.StudentCode
inner join CourseInfo on StudentCourseScore.CourseId=CourseInfo.Id
where Score<60
group by StudentName,CourseId,CourseName,Score


-- 31.查询课程编号为 01 且课程成绩在 80 分以上的学生的学号和姓名
select StudentId 学号,StudentName 姓名 from StudentCourseScore
inner join StudentInfo on StudentCourseScore.StudentId=StudentInfo.StudentCode
where CourseId=1 and Score>=80
group by StudentId,StudentName


-- 32.求每门课程的学生人数
select CourseId,count(CourseId) from StudentCourseScore group by CourseId


-- 33.成绩不重复，查询选修「张三」2老师所授课程的学生中，成绩最高的学生信息及其成绩


select distinct StudentId 学号,Score 成绩 from StudentCourseScore 
where score=(select max(Score)  from StudentCourseScore where CourseId=2)



--34.成绩有重复的情况下，查询选修「张三」老师所授课程的学生中，成绩最高的学生信息及其成绩
select StudentId 学号,Score 成绩 from StudentCourseScore 
where score=(select max(Score)  from StudentCourseScore where CourseId=2)



-- 35.查询不同课程成绩相同的学生的学生编号、课程编号、学生成绩
SELECT StudentId,CourseId,score,
COUNT(1) 
FROM StudentCourseScore st 
GROUP BY StudentId,CourseId,Score HAVING COUNT(1)>1
-- 36.查询每门功成绩最好的前两名
select CourseId,Score from StudentCourseScore group by CourseId,Score order by Score DESC


-- 37.统计每门课程的学生选修人数（超过 5 人的课程才统计）。
select CourseId from StudentCourseScore 
group by CourseId having count(CourseId)>5


-- 38.检索至少选修两门课程的学生学号
select StudentId from StudentCourseScore
group by StudentId having count(StudentId)>=2


-- 39.查询选修了全部课程的学生信息
select StudentId from StudentCourseScore
group by StudentId having count(StudentId)=3


-- 40.查询各学生的年龄，只按年份来算
select * from StudentInfo where datediff(YY,GETDATE,Birthday) as shengri


-- 41.按照出生日期来算，当前月日 < 出生年月的月日则，年龄减一



-- 42.查询本周过生日的学生



-- 43.查询下周过生日的学生



-- 44.查询本月过生日的学生



-- 45.查询下月过生日的学生
























































