use master
go

create database HOUSE_DB
on
(
	name=HOUSE_DB_mbf,
	filename='D:\HOUSE_DB.mbf',
	size=5,
	filegrowth=1MB
)
log on
(
	name=HOUSE_DB_dbf,
	filename='D:\HOUSE_DB_log.lbf',
	size=5,
	filrgrowth=1MB
)
go

create table HOUSE_TYPE
(
	type_id int not null primary key identity,
	type_name varchar(50) not null 
)
go

create table HOUSE
(
	house_id int not null primary key identity,
	house_name varchar(50) not null,
	house_price float not null default(0),
	type_id int not null references HOUSE_TYPE(type_id)
)
go

--HOUSE_TYPE表中添加3条数据，例如：小户型、经济型、别墅
insert into HOUSE_TYPE values('小户型'),('经济型'),('别墅')
go

--HOUSE表中添加至少3条数据，不能全都为同一类型
insert into HOUSE values
('房1',1000,1),
('房2',2000,2),
('房3',3000,3)


--查询所有房屋信息
select * 
from HOUSE HO inner join HOUSE_TYPE HT on HO.type_id=HT.type_id

--使用模糊查询包含”型“字的房屋类型信息
select *
from HOUSE HO inner join HOUSE_TYPE HT on HO.type_id=HT.type_id
where type_name like '%型%'

--查询出房屋的名称和租金，并且按照租金降序排序
select type_name,house_price 
from HOUSE HO inner join HOUSE_TYPE HT on HO.type_id=HT.type_id
order by house_price desc

--使用连接查询，查询信息，显示房屋名称和房屋类型名称
select house_name,type_name
from HOUSE HO inner join HOUSE_TYPE HT on HO.type_id=HT.type_id

--查询所有房屋中月租最高的房屋，显示最高的租金和房屋名称
select house_name,house_price 
from HOUSE HO inner join HOUSE_TYPE HT on HO.type_id=HT.type_id
where house_price=(select max(house_price) from HOUSE)