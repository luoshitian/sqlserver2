use master
go

create database HOUSE_DB 
on
(
	name='HOUSE_DB',
	filename='D:\test\HOUSE_DB.mdf',
	size=5,
	maxsize=50,
	filegrowth=1
)
log on
(
	name='HOUSE_DB_log',
	filename='D:\test\HOUSE_DB_log.ldf',
	size=5,
	maxsize=50,
	filegrowth=1
)
go

use HOUSE_DB
go

create  table HOUSE_TYPE 
(
	type_id int primary key identity not null ,
	type_name varchar(50) not null unique 
)
go

create table HOUSE
(
	house_id int primary key identity not null ,
	house_name varchar(50) not null ,
	house_price float not null ,
	type_id int references HOUSE_TYPE(type_id)
)
go

insert HOUSE_TYPE values 
('小户型'),('经济型'),('别墅')
go

insert HOUSE values 
('asd',2000,1),
('dsa',1500,3),
('das',1800,2)
go

select * from HOUSE
go

select B.* from HOUSE A
inner join HOUSE_TYPE B on A.type_id = B.type_id
where type_name like '%型'
go

select house_name,house_price from HOUSE
order by house_price desc
go


select A.*,B.type_name from HOUSE A
inner join HOUSE_TYPE B on A.type_id = B.type_id
go

select top 1 house_name,house_price from HOUSE
order by house_price desc
go