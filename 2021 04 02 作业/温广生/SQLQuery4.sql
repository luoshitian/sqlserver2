create database HOUSE_DB
on
(
	name='HOUSE_DB',
	filename='d:\temp\HOUSE_DB_mdf',
	size=5,
	maxsize=50,
	filegrowth=1
)
log on
(
	name='HOUSE_DB_log',
	filename='d:\temp\HOUSE_DB_log_ldf',
	size=5,
	maxsize=50,
	filegrowth=1
)
go
create table HOUSE_TYPE
(
	type_id int not null primary key identity(1,1),
	type_name varchar(50) not null,

)
create table HOUSE
(
	house_id int not null primary key identity(1,1),
	house_name varchar(50) not null,
	house_price float not null default(0),
	type_id int not null references HOUSE_TYPE(type_id)
)

insert into HOUSE_TYPE(type_name)
select '小户型' union
select '经济型' union
select '别墅'

insert into HOUSE(house_name,house_price,type_id)
select '兴国型',500,1 union
select '小狗区',300,2 union
select '上都会型',1500,3
--4、添加查询
--查询所有房屋信息
select * from HOUSE_TYPE
select * from HOUSE
--使用模糊查询包含”型“字的房屋类型信息
select * from HOUSE where house_name like '%型'
--查询出房屋的名称和租金，并且按照租金降序排序
select house_name 名称,house_price 租金 from HOUSE order by house_price desc
--使用连接查询，查询信息，显示房屋名称和房屋类型名称
select house_name,type_name from HOUSE a inner join HOUSE_TYPE b on a.house_id=b.type_id
--查询所有房屋中月租最高的房屋，显示最高的租金和房屋名称
select *from HOUSE where house_price=(select max(house_price) from HOUSE)
