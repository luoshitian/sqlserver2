create database GoodsDB
go
create table GoodsType
(
	TypeID int not null primary key identity(1,1),
	TypeName nvarchar(20) not null 
)
create table GoodsInfo
(
	GoodsID int not null primary key identity(1,1),
	GoodsName nvarchar(20) not null,
	GoodsColor nvarchar(20) not null, 
	GoodsBrand nvarchar(20),  
	GoodsMoney money not null,
	TypeID int references GoodsType(TypeID)
)
insert into GoodsType(TypeName)
select '服装内衣' union
select '鞋包配饰' union
select '手机数码'

insert into GoodsInfo(GoodsName,GoodsColor,GoodsBrand,GoodsMoney,TypeID)
select '提花小西装','红色','菲曼琪',300,1 union
select '百搭短裤','绿色','哥弟',100,1 union
select '无袖背心','白色' ,'阿依莲' ,700, 1 union
select '低帮休闲鞋','红色', '菲曼琪' ,900, 2 union
select '中跟单鞋','绿色', '哥弟' ,400 ,2 union
select '平底鞋','白色', '阿依莲', 200, 2 union
select '迷你照相机','红色' ,'尼康', 500 ,3 union
select '硬盘','黑色' ,'希捷' ,600 ,3 union
select '显卡','黑色' ,'技嘉',800 ,3 

select * from GoodsInfo
select * from GoodsType

--3、查询价格最贵的商品名称，商品颜色和商品价格，要求使用别名显示列名
select GoodsName 商品名称 ,GoodsColor 商品颜色,GoodsMoney 商品价格 from GoodsInfo where GoodsMoney=(select MAX(GoodsMoney) from GoodsInfo)
--4、按商品类型编号分组查询商品最高价格，最低价格和平均价格，要求使用别名显示列名
select TypeID,min(GoodsMoney) 最低价格,MAX(GoodsMoney) 最高价格,AVG(GoodsMoney) 平均价格 from GoodsInfo group by typeID
--5、查询商品信息所有列，要求商品颜色为红色，价格在300~600之间
select * from GoodsInfo where GoodsColor='红色' and GoodsMoney>=300 and GoodsMoney<600