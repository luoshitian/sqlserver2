create database StarManagerDB
go
use StarManagerDB
go
create table StarType
(
	T_NO int not null primary key identity(1,1),
	T_NAME nvarchar(20)
)
create table StarInfo
(
	S_NO int not null primary key identity(1,1),
	S_NAME nvarchar(20) not null,
	S_AGE int not null,
	S_HOBBY nvarchar(20),
	S_NATIVE nvarchar(20) default('中国大陆'),
	S_T_NO int references StarType(T_NO)
)
insert into StarType(T_NAME)
select '体育明星' union
select 'IT明星' union
select '相声演员'

insert into StarInfo(S_NAME,S_AGE,S_HOBBY,S_NATIVE,S_T_NO)
select '梅西',30,'射门','阿根廷',1 union
select '科比',35,'过人','美国',1 union
select '菜景观',40,'敲代码','中国',2 union
select '马克思',36,'造火箭','外星人',2 union
select '郭德纲',50,'相声','中国',3 union
select '黄铮',41,'拼多多','中国',2

select * from StarInfo
select * from StarType
--3、查询年龄最大的3个明星的姓名，特技和籍贯信息，要求使用别名显示列名。
select top 3 S_NAME 姓名,S_HOBBY 特技,S_NATIVE 籍贯 from StarInfo order by S_AGE desc
--4、按 明星类型编号 分类查询   明星人数，明星平均年龄 ，显示明星人数大于2的分组信息，要求使用别名显示列名。
select S_T_NO,count(*) 人数,AVG(S_AGE) from StarType a inner join StarInfo b on a.T_NO=b.S_T_NO  group by S_T_NO HAVING COUNT(S_NAME)>2
--5、查询明星类型为“体育明星”中年龄最大的姓名、特技、籍贯信息，要求显示列别名。
select S_NAME,S_HOBBY,S_NATIVE,max(S_AGE) from StarInfo a inner join StarType b on a.S_T_NO=b.T_NO where S_AGE = (select MAX(S_AGE) from StarInfo where S_T_NO = 2) group by S_NAME,S_HOBBY,S_NATIVE