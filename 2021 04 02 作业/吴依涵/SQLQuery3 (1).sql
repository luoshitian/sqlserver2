use master
go
create database HOUSE_DB
on
(
 name='HOUSE_DB',
 filename='D:\新建文件夹 (2).mdf',
 size=5MB,
 maxsize=50MB,
 filegrowth=1Mb
)
log on
(
 name='HOUSE_DB_log',
 filename='D:\新建文件夹 (2)_1og.ldf',
 size=5MB,
 maxsize=50MB,
 filegrowth=1Mb
)
go
use HOUSE_DB
go
create table HOUSE_TYPE
(
type_id int primary key identity not null,
type_name nvarchar(50) unique not null,
)
go
use HOUSE_DB
go
create table HOUSE
(
house_id int primary key identity not null,
house_name	varchar(50) not null,
house_price float default(0) not null,
type_id	int references HOUSE_TYPE(type_id)
)
insert into HOUSE_TYPE(type_name) values 
('小户型'),
('经济型'),
('别墅')
insert into HOUSE(house_name,house_price,type_id) values 
('平顶房',100000,1),
('豪华套房',200000,2),
('空中豪华别墅',1000000,3)

--查询所有房屋信息
select * from HOUSE

--使用模糊查询包含”型“字的房屋类型信息
select * from HOUSE_TYPE where type_name like '%型%'

--查询出房屋的名称和租金，并且按照租金降序排序
select house_name 房屋的名称,house_price 租金 from HOUSE order by house_price DESC

--使用连接查询，查询信息，显示房屋名称和房屋类型名称
select X.house_name 房屋的名称,L.type_name 房屋类型名称 from HOUSE_TYPE L inner join HOUSE X on L.type_id=X.type_id 

--查询所有房屋中月租最高的房屋，显示最高的租金和房屋名称
select house_name 房屋名称,house_price 最高的租金 from HOUSE where house_price=(select max(house_price) from HOUSE)

select * from HOUSE_TYPE
select * from HOUSE

