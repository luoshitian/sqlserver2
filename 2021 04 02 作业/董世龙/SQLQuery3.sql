--1、创建数据库HOUSE_DB，
--要求：
--指定数据文件大小：5兆，
--指定文件最大值：50兆，
--文件增长率：1兆
use master 
go 

create database HOUSE_DB
on
(
	name = HOUSE_DB,
	filename ='D:\HOUSE_DB.mfg',
	size = 5MB,
	maxsize = 50MB,
	filegrowth = 1MB
)
use HOUSE_DB

--2、创建数据表
--房屋类型表（HOUSE_TYPE）
--字段名	类型	是否可为空	约束	说明
--type_id	int	N	主键，自增长	类型编号
--type_name	varchar(50)	N	唯一约束	类型名称

create table HOUSE_TYPE
(
	type_id int primary key identity ,
	type_name varchar(20) unique 
)

--房屋信息表（HOUSE）
--字段名	类型	是否可为空	约束	说明
--house_id	int	N	主键，自增长	房屋编号
--house_name	varchar(50)	N		房屋名称
--house_price	float	N	默认值0	房租
--type_id	int	N	外键依赖HOUSE_TYPE表	房屋类型

create table HOUSE
(
	house_id int primary key identity ,
	house_name varchar(50) ,
	house_price float default('0'),
	type_id int references HOUSE_TYPE(type_id)
)

--3、添加表数据
--HOUSE_TYPE表中添加3条数据，例如：小户型、经济型、别墅
--HOUSE表中添加至少3条数据，不能全都为同一类型

insert into HOUSE_TYPE(type_name)
select '小户型' union
select '经济型' union
select '别墅' 

insert into HOUSE(house_name,house_price,type_id)
select '蔡','5000','3' union
select '东','10000','2' union
select '生','50000','1' 

--4、添加查询
--查询所有房屋信息
	
select * from HOUSE A
	inner join HOUSE_TYPE B on A.type_id = B.type_id

--使用模糊查询包含”型“字的房屋类型信息

select * from HOUSE A
	inner join HOUSE_TYPE B on a.type_id = B.type_id
		where type_name like '%型%'

--查询出房屋的名称和租金，并且按照租金降序排序

select house_name 名字,house_price 租金 from HOUSE
	order by house_price desc

--使用连接查询，查询信息，显示房屋名称和房屋类型名称

select house_name 名称,type_name 类型名称 from HOUSE A
	inner join HOUSE_TYPE B on A.type_id = B.type_id

--查询所有房屋中月租最高的房屋，显示最高的租金和房屋名称

select house_price ,house_name from HOUSE where house_price = (select MAX(house_price) from HOUSE)

