		-- 练习题目：

			select * from StudentInfo
			select * from Teachers
			select * from CourseInfo
			select * from StudentCourseScore
		-- 1.查询"数学 "课程比" 语文 "课程成绩高的  学生的信息  及  课程分数
			select * from StudentCourseScore A,StudentCourseScore B where A.StudentId=B.StudentId AND A.CourseId=2 and B.CourseId=1 and A.Score>B.Score

		-- 1.1 查询同时存在" 数学 "课程和" 语文 "课程的情况
			select * from StudentCourseScore A,StudentCourseScore B where A.StudentId=B.StudentId AND A.CourseId=2 and B.CourseId=1

			select * from (select StudentId ,CourseId from StudentCourseScore WHERE CourseId=1) A ,
			(SELECT StudentId,CourseId from StudentCourseScore where CourseId=2 ) B
			where A.StudentId=B.StudentId
		-- 1.2 查询存在" 数学 "课程但可能不存在" 语文 "课程的情况(不存在时显示为 null )
			select * from (select StudentId ,CourseId from StudentCourseScore where CourseId=1) A left join
		   (select StudentId,CourseId from StudentCourseScore where CourseId=2 ) B on A.StudentId=B.StudentId
		-- 1.3 查询不存在" 数学 "课程但存在" 语文 "课程的情况
			select * from (select StudentId ,CourseId from StudentCourseScore where CourseId=1) A left join 
		   (select StudentId ,CourseId from StudentCourseScore where CourseId=2)B ON A.StudentId=B.StudentId
		   where B.CourseId is null and B.StudentId is null

		-- 2.查询平均成绩大于等于 60 分的同学的学生编号和学生姓名和平均成绩
			select StudentId,StudentName,avg(Score)平均成绩 from StudentCourseScore
			inner join StudentInfo on StudentCourseScore.StudentId = StudentInfo.Id
			group by  StudentId,StudentName
			having avg(Score) >=60

		-- 3.查询在成绩表存在成绩的学生信息
			select  distinct StudentInfo. * from StudentCourseScore
			inner join StudentInfo on StudentCourseScore.StudentId = StudentInfo.Id

		-- 4.查询所有同学的学生编号、学生姓名、选课总数、所有课程的总成绩(没成绩的显示为 null )
			select StudentInfo.Id,StudentName,count(CourseId)选课总数,sum(Score)总成绩 from StudentCourseScore
			inner join StudentInfo on StudentCourseScore.StudentId = StudentInfo.Id
			group by StudentName,StudentInfo.Id

		-- 4.1 查有成绩的学生信息
			select distinct StudentInfo. * from StudentCourseScore
			inner join StudentInfo on StudentCourseScore.StudentId = StudentInfo.Id

		-- 5.查询「李」姓老师的数量
			select count(TeacherName)李姓老师的数量 from Teachers where TeacherName like '李%'

		-- 6.查询学过「张三」老师授课的同学的信息
			select distinct StudentInfo . * from StudentCourseScore
			inner join CourseInfo on StudentCourseScore.CourseId = CourseInfo.TeacherId
			inner join StudentInfo on StudentInfo.Id=StudentCourseScore.StudentId 
			where TeacherId = 1

		-- 7.查询没有学全所有课程的同学的信息
			select count(courseId) , StudentInfo. *  from StudentCourseScore
			inner join StudentInfo on StudentCourseScore.StudentId = StudentInfo.Id
			group by StudentInfo.Id ,StudentCode,StudentName,Birthday,Sex,ClassId
			having count(courseId)<3 

		-- 8.查询至少有一门课与学号为" 01 "的同学所学相同的同学的信息
			select count(courseId), StudentInfo. * from StudentCourseScore inner join StudentInfo on StudentCourseScore.StudentId = StudentInfo.Id
			where StudentInfo.Id!=01
			group by StudentInfo.Id ,StudentCode,StudentName,Birthday,Sex,ClassId
			having count(courseId)=2 or  count(courseId)=1 or count(courseId)=3

		-- 9.查询和" 01 "号的同学学习的课程 完全相同的其他同学的信息
			select count(courseId), StudentInfo. * from StudentInfo inner join StudentCourseScore on StudentInfo.Id=StudentCourseScore.StudentId  
			where StudentInfo.Id!=01
			group by StudentInfo.Id ,StudentCode,StudentName,Birthday,Sex,ClassId
			having count(courseId)=3  

		-- 10.查询没学过"张三"老师讲授的任一门课程的学生姓名
			select StudentName from StudentCourseScore
			full join StudentInfo on StudentCourseScore.StudentId = StudentInfo.Id
			full join CourseInfo on StudentCourseScore.CourseId = CourseInfo.Id
			where Score is null 