use master
go
create database Houses
on
(
name = 'Houses',
filename = 'D:\ Houses.mdf',
size = 5MB,
maxsize = 50MB,
filegrowth = 10%
)
log on
(
name = ' Houses_log',
filename = 'D:\ Houses_log.ldf',
size = 5MB,
maxsize = 50MB,
filegrowth = 10%
)
go

use  Houses
go

create table HOUSE_TYPE
(
[type_id] int primary key identity not null,
[type_name] nvarchar(50) unique not null 
)

create table HOUSE
(
house_id int primary key identity not null,
house_name	varchar(50) not null,
house_price	float default(0) not null,
type_id	int  references  HOUSE_TYPE([type_id])
)


		--3、添加表数据
		--HOUSE_TYPE表中添加3条数据，例如：小户型、经济型、别墅
		--HOUSE表中添加至少3条数据，不能全都为同一类型
			insert into HOUSE_TYPE values ('小户型'),('经济型'),('别墅')

			insert into  HOUSE values ('学区房',3000,1),('地铁房',2000,2),('碧桂园',6000,3)
		--4、添加查询
		--查询所有房屋信息
			SELECT * FROM HOUSE_TYPE
		--使用模糊查询包含”型“字的房屋类型信息
			select * FROM HOUSE_TYPE where [type_name] like '%型'
		--查询出房屋的名称和租金，并且按照租金降序排序
			select house_name,house_price FROM HOUSE 
			order by house_price DESC
		--使用连接查询，查询信息，显示房屋名称和房屋类型名称
			select [type_name],house_name from  HOUSE
			inner join HOUSE_TYPE  on  HOUSE.type_id = HOUSE_TYPE.type_id
		--查询所有房屋中月租最高的房屋，显示最高的租金和房屋名称
			select top 1 house_name,house_price from HOUSE
			order by house_price DESC

			select house_name,house_price from HOUSE where house_price =
			(select max(house_price) from HOUSE )



		SELECT * FROM HOUSE_TYPE
		SELECT * FROM HOUSE
