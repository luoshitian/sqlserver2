use master
go
create database GoodsDB
on
(
	name='list',
	filename='D:\GoodsDB.mdf',
	size=5,
	maxsize=50,
	filegrowth=10%
)
log on
(
	name='list_log',
	filename='D:\GoodsDB_log.ldf',
	size=5,
	maxsize=50,
	filegrowth=10%
)
use GoodsDB
go
create table GoodsType
(
TypeID int primary key not null identity(1,1) ,
TypeName nvarchar(20) not null
)
create table GoodsInfo
(
GoodsID int primary key not null identity(1,1) ,
GoodsName nvarchar(20) not null,
GoodsColor nvarchar(20) not null,
GoodsBrand nvarchar (20) not null,
GoodsMoney money not null,
TypeID int not null foreign key references GoodsType(TypeID)
)
go
insert into GoodsType(TypeName) values 
('服装内衣'),('鞋包配饰'),('手机数码')
select * from GoodsType

insert into GoodsInfo(GoodsName,GoodsColor,GoodsBrand,GoodsMoney,TypeID) values
('提花小西装','红色', '菲曼琪',300,1),('百搭短裤','绿色','哥弟',100, 1),('无袖背心', '白色', '阿依莲', 700, 1),
('低帮休闲鞋',' 红色', '菲曼琪', 900, 2),('中跟单鞋', '绿色', '哥弟', 400, 2),('平底鞋', '白色', '阿依莲', 200, 2),
('迷你照相机', '红色', '尼康', 500, 3),('硬盘', '黑色', '希捷', 600, 3),('显卡', '黑色', '技嘉', 800, 3)
select * from GoodsInfo

go
--查询价格最贵的商品名称，商品颜色和商品价格，要求使用别名显示列名
select top 1 GoodsName,GoodsColor 商品颜色,GoodsMoney from GoodsInfo order by GoodsMoney desc
--按商品类型编号 分组查询 商品最高价格，最低价格和平均价格，要求使用别名显示列名
select TypeID 商品类型编号,max(GoodsMoney) 最高价格,min(GoodsMoney) 最低价格 ,avg(GoodsMoney) 平均价格 from GoodsInfo group by TypeID
--查询商品信息 所有列，要求商品颜色为 红色，价格在 300~600 之间
select * from GoodsInfo  where GoodsColor= '红色' and GoodsMoney between 300 and 600