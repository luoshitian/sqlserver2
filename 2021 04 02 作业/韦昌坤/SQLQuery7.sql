use master
go
create database HOUSE_DB
on
(
	name='list',
	filename='D:\HOUSE_DB.mdf',
	size=5,
	maxsize=50,
	filegrowth=1%
)
log on
(
	name='list_log',
	filename='D:\HOUSE_DB_log.ldf',
	size=5,
	maxsize=50,
	filegrowth=1%
)
use HOUSE_DB
go
create table HOUSE_TYPE
(
type_id int not null primary key identity,
type_name varchar(50) not null unique 
)
create table HOUSE
(
house_id int not null primary key identity,
house_name varchar(50) not null,
house_price float not null default(0),
type_id int not null foreign key references HOUSE_TYPE(type_id)
)
go
insert into HOUSE_TYPE(type_name) values 
('小户型'),('经济型'),('别墅')
select * from  HOUSE_TYPE

insert into HOUSE(house_name,house_price,type_id) values 
('小户型',200,1),('经济型',500,2),('别墅',1000,3)
select * from  HOUSE
go
--查询所有房屋信息
select * from  HOUSE
--使用模糊查询包含”型“字的房屋类型信息
select* from HOUSE  where house_name like '%型%'
--查询出房屋的名称和租金，并且按照租金降序排序
select house_name 房屋名称,house_price 租金 from HOUSE order by house_price desc
--使用连接查询，查询信息，显示房屋名称和房屋类型名称
select a.type_name 房屋类型名称,b.house_name 房屋名称 from  HOUSE_TYPE a  inner join  HOUSE b on a.type_id = b.type_id
--查询所有房屋中 月租最高的房屋，显示最高的 租金 和 房屋名称
select top 1 max(house_price)最高价格, house_name 房屋名称  from HOUSE  group by house_name