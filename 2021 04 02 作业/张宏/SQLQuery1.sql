-- 练习题目：



-- 1.查询"数学 "课程比" 语文 "课程成绩高的学生的信息及课程分数

select * from 
	(select * from StudentCourseScore where courseId = 1) as A
		inner join StudentInfo s on s.Id = A.Id,
	(select * from StudentCourseScore where CourseId = 2) as B
			where A.Score > B.Score and A.StudentId = B.StudentId


-- 1.1 查询同时存在" 数学 "课程和" 语文 "课程的情况

select StudentId from StudentCourseScore
	where CourseId = 1 or CourseId = 2
	 group by StudentId
		HAVING COUNT(distinct CourseId) = 2
		

-- 1.2 查询存在" 数学 "课程但可能不存在" 语文 "课程的情况(不存在时显示为 null )

select * from
	(select * from StudentCourseScore where CourseId = 2) A
	left join (select * from StudentCourseScore where CourseId = 1) B on A.StudentId = B.StudentId

-- 1.3 查询不存在" 数学 "课程但存在" 语文 "课程的情况

select * from 
	(select * from StudentCourseScore where CourseId = 1) A
	left join (select * from StudentCourseScore where CourseId = 2) B on A.StudentId = B.StudentId
		where B.CourseId is null

-- 2.查询平均成绩大于等于 60 分的同学的学生编号和学生姓名和平均成绩

select Studentcode 学生编号,StudentName 学生姓名,AVG(Score) 平均成绩 from StudentCourseScore
	inner join StudentInfo on StudentCourseScore.StudentId = StudentInfo.StudentCode
		group by Studentcode,StudentName
			HAVING AVG(Score) >= 60

-- 3.查询在 成绩 表存在成绩的学生信息

select * from StudentCourseScore 
	left join StudentInfo on StudentCourseScore.StudentId = StudentInfo.StudentCode

-- 4.查询所有同学的学生编号、学生姓名、选课总数、所有课程的总成绩(没成绩的显示为 null )

select StudentCode ,StudentName ,COUNT(CourseId) ,SUM(Score) from StudentCourseScore scs
	right join StudentInfo si on scs.StudentId = si.StudentCode
		group by StudentCode,StudentName

-- 4.1 查有成绩的学生信息

select * from StudentCourseScore scs
	inner join StudentInfo si on scs.StudentId = si.StudentCode

-- 5.查询「李」姓老师的数量

select COUNT(TeacherName) from Teachers
	where TeacherName like '李%'

-- 6.查询学过 「张三」老师授课 的 同学的信息

select * from StudentCourseScore A
	inner join CourseInfo B on A.CourseId = B.Id
	inner join StudentInfo C on A.StudentId = C.StudentCode
	inner join Teachers D on B.TeacherId = D.Id
		where TeacherName = '张三'

-- 7.查询没有学全所有课程的同学的信息
select * from CourseInfo
select * from StudentCourseScore
select * from StudentInfo
select * from Teachers

select StudentCode ,StudentName from StudentCourseScore A
	inner join StudentInfo B on A.StudentId = B.StudentCode 
		group by StudentCode,StudentName
			HAVING COUNT(CourseId) < (select COUNT(CourseName) from CourseInfo)

-- 8.查询至少有一门课与学号为" 01 "的同学所学相同的同学的信息

select StudentCode ,StudentName from StudentCourseScore A
	inner join StudentInfo B on A.StudentId = B.StudentCode
		group by  StudentCode ,StudentName
			having count(CourseId) > 0

-- 9.查询和" 01 "号的同学学习的课程 完全相同的其他同学的信息

select StudentCode ,StudentName from StudentCourseScore A
	inner join StudentInfo B on A.StudentId = B.StudentCode
		group by  StudentCode ,StudentName
			having count(CourseId)=(select COUNT(CourseName) from CourseInfo)

-- 10.查询没学过"张三"老师讲授的任一门课程的学生姓名

select distinct studentName from StudentCourseScore A
	inner join CourseInfo B on A.CourseId = B.Id
	inner join StudentInfo C on A.StudentId = C.StudentCode
	inner join Teachers D on B.TeacherId = D.Id
		where TeacherName != '张三'