
create database HOUSE_DB
on
(
	name='HOUSED_B',
	filename='D;\HOUSE_DB.mdf',
	size=5mb,
	maxsize=50mb,
	filegrowth=1mb
)
log on
(
	name='HOUSE_DBlog',
	filename='D;\HOUSE_DB_log.ldf',
	size=5mb,
	maxsize=50mb,
	filegrowth=1mb
)

go
use HOUSE_DB
go

create table HOUSE_TYPE
(
	type_id int primary key identity(1,1),
	type_name varchar(50) unique not null,
)

insert into HOUSE_TYPE (type_name) values('小型户'),('经济户'),('别墅')
select * from HOUSE_TYPE

create table HOUSE
(
	house_id int primary key identity(1,1),
	house_name varchar(50) not null,
	house_price float default('0') not null,
	type_id int references HOUSE_TYPE(type_id)
)

insert into HOUSE (house_name,house_price,type_id) values('中式屋','60000',1)
insert into HOUSE (house_name,house_price,type_id) values('法式屋','40000',2)
insert into HOUSE (house_name,house_price,type_id) values('美式屋','50000',3)


--查询所有房屋信息
select *,type_name from HOUSE HO inner join HOUSE_TYPE HT on HO.type_id=HT.type_id

--使用模糊查询包含”型“字的房屋类型信息
select HT.type_id,type_name,house_name,house_price from HOUSE_TYPE HT inner join HOUSE HO on HT.type_id=HO.type_id WHERE type_name LIKE '%型%'

--查询出房屋的名称和租金，并且按照租金降序排序
select house_name,house_price from HOUSE order by house_price DESC

--使用连接查询，查询信息，显示房屋名称和房屋类型名称
select house_name,type_name from HOUSE HO inner join HOUSE_TYPE HT on HO.type_id=HT.type_id

--查询所有房屋中月租最高的房屋，显示最高的租金和房屋名称
select top 1 house_price,house_name from HOUSE order by house_price DESC

