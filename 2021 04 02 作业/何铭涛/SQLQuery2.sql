use ClassicDb
go


select * from StudentInfo
select * from Teachers
select * from CourseInfo
select * from StudentCourseScore

select D.* from 
(select A.StudentId a,A.CourseId b,A.Score c,B.StudentId d,B.CourseId e,B.Score f from
(select * from StudentCourseScore where CourseId = 1) as A ,
(select * from StudentCourseScore where CourseId = 2) as B
where A.Score > B.Score and A.StudentId = B.StudentId) as C
inner join StudentInfo D on C.a = D.StudentCode
 



select StudentId,count(CourseId) from StudentCourseScore 
where CourseId = 1 or CourseId = 2
group by StudentId
having count(CourseId) = 2


select * from (select * from StudentCourseScore where CourseId = 1) as A
right join (select * from StudentCourseScore where CourseId = 2) as B on A.StudentId = B.StudentId 



select * from (select * from StudentCourseScore where CourseId = 1) as A
left join (select * from StudentCourseScore where CourseId = 2) as B on A.StudentId = B.StudentId 
where A.CourseId is null


select StudentId as 学生编号,StudentName as 学生姓名,avg(Score) as 平均成绩 from StudentCourseScore A
inner join StudentInfo B on A.StudentId = B.StudentCode 
group by StudentId,StudentName
having avg(Score) >= 60


select B.* from StudentCourseScore A
left join StudentInfo B on A.StudentId = B.StudentCode


select 
StudentCode as 学生编号,StudentName as 学生姓名,
count(CourseId) as 选课总数,sum(Score) as 总成绩 
from StudentCourseScore A
right join StudentInfo B on A.StudentId = B.StudentCode
group by StudentCode,StudentName
order by StudentCode



select StudentCode,StudentName,Birthday,Sex,ClassId from StudentCourseScore A
left join StudentInfo B on A.StudentId = B.StudentCode
group by StudentCode,StudentName,Birthday,Sex,ClassId



select count(*) as 姓老师的数量 from Teachers
group by TeacherName
having TeacherName like ('李%')




select B.* from StudentCourseScore A
inner join StudentInfo B on A.StudentId = B.StudentCode
inner join CourseInfo C on A.CourseId = C.Id
where A.CourseId = 1



select StudentId,StudentName,Birthday,Sex,ClassId from StudentCourseScore A 
inner join StudentInfo B on A.StudentId = B.StudentCode
group by StudentId,StudentName,Birthday,Sex,ClassId
having count(CourseId)<3 



select StudentId,StudentName,Birthday,Sex,ClassId from StudentCourseScore A
inner join StudentInfo B on A.StudentId = B.StudentCode
where CourseId = 1 or CourseId = 2 or CourseId = 3
group by StudentId,StudentName,Birthday,Sex,ClassId



select StudentCode,StudentName,Birthday,Sex,ClassId from StudentCourseScore A
inner join StudentInfo B on A.StudentId = B.StudentCode
where CourseId = 1 or CourseId = 2 or CourseId = 3
group by StudentCode,StudentName,Birthday,Sex,ClassId
having count(CourseId) = 3



select StudentName from StudentCourseScore A
inner join StudentInfo B on A.StudentId = B.StudentCode
inner join CourseInfo C on A.CourseId = C.Id
where A.CourseId != 1
group by StudentCode,StudentName

