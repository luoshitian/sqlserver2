USE MASTER 
GO
CREATE DATABASE StarManagerDB

on(
name='StarManagerDB',
filename='D:\StarManagerDB.mdf',
size=10,
maxsize=100,
filegrowth=10%
)
log on
(
name='StarManagerDB_log',
filename='D:\StarManagerDB_log.ldf',
size=10,
maxsize=100,
filegrowth=10%
)
GO
USE StarManagerDB
GO
CREATE TABLE StarType
(
T_NO INT NOT NULL PRIMARY KEY IDENTITY(1,1),
T_NAME nvarchar(20)
)
CREATE TABLE StarInfo
(
S_NO INT NOT NULL  PRIMARY KEY IDENTITY(1,1),
S_NAME nvarchar(20) not null,
S_AGE int not null,
S_HOBBY nvarchar(20),
S_NATIVE nvarchar(20) default('中国大陆'),
S_T_NO int references StarType(T_NO)
)
insert into StarType values('体育明星'),('IT明星'),('相声演员')
insert into StarInfo values('梅西',30,	'射门',	'阿根廷',1),('科比',35,'过人',	'美国',	1),
('蔡景现',40,'敲代码','中国',2),('马斯克',36,'造火箭','外星人',2),
('郭德纲',50,'相声','中国',	3),('黄铮',	41,	'拼多多','中国',2)
--3、查询年龄最大的3个明星的姓名，特技和籍贯信息，要求使用别名显示列名。
select top 3 S_NAME,S_HOBBY 特技, S_NATIVE 明星籍贯 from StarInfo order by S_AGE DESC
--4、按明星类型编号分类查询明星人数，明星平均年龄，显示明星人数大于2的分组信息，要求使用别名显示列名。
SELECT T_NO 明星类型编号,COUNT(S_NO) 明星人数,AVG(S_AGE)平均年龄 FROM StarType INNER JOIN StarInfo ON StarType.T_NO=StarInfo.S_T_NO GROUP BY T_NO HAVING COUNT(S_NO)>2
--5、查询明星类型为“体育明星”中年龄最大的姓名、特技、籍贯信息，要求显示列别名。
SELECT TOP 1 S_NAME 姓名,S_HOBBY 特技, S_NATIVE 明星籍贯 FROM StarInfo INNER JOIN StarType ON StarInfo.S_T_NO=StarType.T_NO WHERE T_NAME='体育明星' ORDER BY S_AGE DESC
