use master 
go

create database HOUSE_DB
on
(
   name='HOUSE_DB',
   filename='D:\HOUSE_DB.mdf',
   size=5MB,
   maxsize=50MB,
   filegrowth=1MB
)

log on
(
   name='HOUSE_DB_log',
   filename='D:\HOUSE_DB_log.dfl',
   size=5MB,
   maxsize=50MB,
   filegrowth=1MB
)


use HOUSE_DB
go
create table HOUSE_TYPE
(
    type_id int not null primary key identity(1,1),
    type_name varchar(50) not null unique 
)

create table HOUSE
( 
    HOUSE int not null primary key identity(1,1),
	house_name varchar(50) not null,
	house_price float not null default(0),
    type_id int not null references HOUSE_TYPE(type_id)
)


insert into HOUSE_TYPE values
('小户型'),
('经济型'),
('别墅')

insert into HOUSE values 
('大都会','800','1'),
('中心城','1000','2'),
('哥谭市','2500','3')

--查询所有房屋信息
select*from HOUSE


--使用模糊查询包含”型“字的房屋类型信息
select type_name from HOUSE_TYPE
where type_name like ('%型%')


--查询出房屋的名称和租金，并且按照租金降序排序
select house_name,house_price from HOUSE
order by house_price desc

--使用连接查询，查询信息，显示房屋名称和房屋type_name类型名称
select type_name,house_name from HOUSE_TYPE A
inner join HOUSE B on A.type_id=B.type_id


--查询所有房屋中月租最高的房屋，显示最高的租金和房屋名称
select house_name,house_price from HOUSE
order by house_price desc