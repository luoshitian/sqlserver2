use master 
go
create database StarManagerDB
on
(
	name='StarManagerDB',
	filename='D:\StarManagerDB.mdf',
	size =5,
	maxsize=50,
	filegrowth=50
)
log on
(
	name='StarManagerDB_log',
	filename='D:\StarManagerDB_log.ldf',
	size =5,
	maxsize=50,
	filegrowth=50
)
go
use StarManagerDB
go
create table StarType
(
	T_NO int primary key identity,               --明星类型编号
	T_NAME nvarchar(20)                          --明星类型
)
create table StarInfo
( 
	S_NO int primary key identity,                --明星编号
	S_NAME nvarchar(20) not null ,                --明星姓名
	S_AGE  int   not null,						  --明星年龄  
	S_HOBBY   nvarchar(20),						  --特技
	S_NATIVE	nvarchar(20) default('中国大陆'), --明星籍贯
	S_T_NO   int references StarType(T_NO)        --明星类型编号
)
insert into StarType values ('体育明星'),('IT明星'),('相声演员')
select * from StarType
insert into StarInfo values ('梅西',30,'射门','阿根廷',1),('科比',35,'过人','美国',1),('蔡景现',40,'敲代码','中国',2),('马斯克',36,'造火箭','外星人',2),
('郭德纲',50,'相声','中国',3),('黄峥',41,'拼多多','中国',2)
select * from StarInfo
--3、查询年龄最大的3个明星的姓名，特技和籍贯信息，要求使用别名显示列名。
select S_NAME 姓名,S_HOBBY 特技,S_NATIVE 籍贯 from StarInfo S join StarType P on S.S_T_NO=P.T_NO  where S_AGE in(select top 3 S_AGE from StarInfo order by  S_AGE DESC)
--4、按明星类型编号分类查询明星人数，明星平均年龄，显示明星人数大于2的分组信息，要求使用别名显示列名。
select S_T_NO 明星类型编号 , count(*) 明星人数,avg(S_AGE) 平均年龄 from StarInfo  group by S_T_NO having count(*)>=2
--5、查询明星类型为“体育明星”中年龄最大的姓名、特技、籍贯信息，要求显示列别名。
select S_NAME 姓名,S_HOBBY 特技,S_NATIVE 籍贯 from StarInfo S join StarType P on S.S_T_NO=P.T_NO  where S_AGE =(select top 1 S_AGE from StarInfo  where S_T_NO=1 order by  S_AGE DESC)
