use master 
go 
create database HOUSE_DB
on
(
	name='HOUSE_DB',
	filename='D:\HOUSE_DB。mdf',
	size =5,
	maxsize=50,
	filegrowth=50
)
log on
(
	name='HOUSE_DB_log',
	filename='D:\HOUSE_DB_log.ldf',
	size =5,
	maxsize=50,
	filegrowth=50
)
go
use HOUSE_DB
go
create table HOUSE_TYPE
(
	type_id int primary key identity ,   -- 类型编号
	type_name varchar(50) unique ,        --类型名称
)
create table HOUSE
(
	house_id int primary key identity,           --房屋编号
	house_name varchar(50) ,                     --房屋名称
	house_price  float   default(0),             --房租  
	type_id   int references HOUSE_TYPE(type_id) --房屋类型
)

--HOUSE_TYPE表中添加3条数据，例如：小户型、经济型、别墅
insert into HOUSE_TYPE values ('小户型'),('经济型'),('别墅')
select * from HOUSE_TYPE
--HOUSE表中添加至少3条数据，不能全都为同一类型
insert into HOUSE values ('小房子','500','2'),('公寓','5000','3'),('瓦房','1000','1')
select * from HOUSE

--查询所有房屋信息
select H.house_name  房屋名称 ,house_price 房租 ,type_name 类型名称 from HOUSE H ,HOUSE_TYPE HT where H.type_id=HT.Type_id 
--使用模糊查询包含”型“字的房屋类型信息
select H.house_name  房屋名称 ,house_price 房租 ,type_name 类型名称 from HOUSE H ,HOUSE_TYPE HT where H.type_id=HT.Type_id
and type_name like '%型' 
--查询出房屋的名称和租金，并且按照租金降序排序
select house_name  房屋名称 ,house_price 房租  from HOUSE  order by house_price  DESC
--使用连接查询，查询信息，显示房屋名称和房屋类型名称
select H.house_name  房屋名称 ,type_name 类型名称 from HOUSE H ,HOUSE_TYPE HT where H.type_id=HT.Type_id
--查询所有房屋中月租最高的房屋，显示最高的租金和房屋名称
select house_name  房屋名称 ,house_price 房租  from HOUSE   where house_price= (select max(house_price) from HOUSE)
