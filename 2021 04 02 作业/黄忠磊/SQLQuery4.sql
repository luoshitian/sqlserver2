--1、创建明星数据库（StarManagerDB）,然后建立两张表，StarType（明星类型表），StarInfo（明星信息表），表结构分别如下：
--StarType（明星类型表）
--字段名	说明	类型	长度	可否为空	约束
--T_NO	明星类型编号	int		否	主键约束，自增，标识种子和标识增量都是1
--T_NAME	明星类型	nvarchar	20		

use master
go

create database StarManagerDB
go

use StarManagerDB
go

create table  StarType
(
	T_NO int not null primary key identity(1,1),
	T_NAME nvarchar(20)
)

--StarInfo（明星信息表）
--字段名	说明	类型	长度	可否为空	约束
--S_NO	明星编号	int		否	主键约束，自增，标识种子和标识增量都是1
--S_NAME	明星姓名	nvarchar	20	否	
--S_AGE	明星年龄	int		否	
--S_HOBBY	特技	nvarchar	20		
--S_NATIVE	明星籍贯	nvarchar	20		默认约束：中国大陆
--S_T_NO	明星类型编号	int			外键，参照StarType表的主键T_NO

create table StarInfo
(
	S_NO int not null primary key identity(1,1),
	S_NAME nvarchar(20) not null,
	S_AGE int not null,
	S_HOBBY nvarchar(20) ,
	S_NATIVE nvarchar(20) default('中国大陆'),
	S_T_NO int references StarType(T_NO)
)

--2、使用插入语句为两张表添加数据

--明星类型表（StarType） 
--明星类型编号	明星类型
--1	体育明星
--2	IT明星
--3	相声演员

insert into StarType
select '体育明星' union
select 'IT明星' union
select '相声演员' 

--明星表（StarInfo）
--明星编号	姓名	年龄	特技	籍贯	明星类型编号
--1	梅西	30	射门	阿根廷	1
--2	科比	35	过人	美国	1
--3	蔡景现	40	敲代码	中国	2
--4	马斯克	36	造火箭	外星人	2
--5	郭德纲	50	相声	中国	3
--6	黄铮	41	拼多多	中国	2

insert into StarInfo(S_NAME,S_AGE,S_HOBBY,S_NATIVE,S_T_NO)
select '梅西',30,'射门','阿根廷',1 union
select '科比',35,'过人','美国',1 union
select '蔡景现',40,'敲代码','中国',2 union
select '马斯克',36,'造火箭','外星人',2 union
select '郭德纲',50,'相声','中国',3 union
select '黄铮',41,'拼多多','中国',2 

--3、查询年龄最大的3个明星的姓名，特技和籍贯信息，要求使用别名显示列名。

select top 3 S_NAME as 姓名,S_AGE as 年龄,S_HOBBY as 特技,S_NATIVE as 籍贯信息 from StarInfo
	order by S_AGE desc

--4、按明星类型编号分类查询明星人数，明星平均年龄，显示明星人数大于2的分组信息，要求使用别名显示列名。

select S_T_NO as 类型,count(*) as 人数,avg(S_Age) as 平均年龄 from StarInfo
	group by S_T_NO


--5、查询明星类型为“体育明星”中年龄最大的姓名、特技、籍贯信息，要求显示列别名。

select top 1 S_NAME as 姓名,S_AGE as 年龄,S_HOBBY as 特技,S_NATIVE as 籍贯信息 from StarInfo
	where S_T_NO = 1
		order by S_AGE desc


