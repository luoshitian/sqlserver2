use master
go 
create database HOUSE_DB
on
(
name='HOUSE_DB',
filename='D:\HOUSE_DB.mdf',
size=5,
maxsize=50,
filegrowth=20%
)
log on 
(
name='HOUSE_DB_log',
filename='D:\HOUSE_DB_log.ldf',
size=5,
maxsize=50,
filegrowth=20%
)
go
use HOUSE_DB
go
create table HOUSE_TYPE
(
type_id int not null primary key identity,
type_name varchar(50) unique not null ,

)
create table HOUSE
(
house_id int primary key identity not null,
house_name	varchar(50) not null,
house_price	float not null default(0),
type_id	int not null references HOUSE_TYPE(type_id)
)
--3、添加表数据

--HOUSE_TYPE表中添加3条数据，例如：小户型、经济型、别墅
insert into HOUSE_TYPE values('小户型'),('经济型'),('别墅')
--HOUSE表中添加至少3条数据，不能全都为同一类型
insert into HOUSE values('a房',2000,3),('b房',1500,2),('c房',1200,1)
--4、添加查询
--查询所有房屋信息
select*from HOUSE_TYPE
select*from HOUSE
--使用模糊查询包含”型“字的房屋类型信息
select * from HOUSE_TYPE where type_name like '%型'
--查询出房屋的名称和租金，并且按照租金降序排序
select house_name ,house_price from HOUSE order by house_price DESC
--使用连接查询，查询信息，显示房屋名称和房屋类型名称
select house_name ,type_name from HOUSE INNER JOIN HOUSE_TYPE ON HOUSE.type_id=HOUSE.type_id
--查询所有房屋中月租最高的房屋，显示最高的租金和房屋名称
SELECT TOP 1 house_name,MAX(house_price) FROM HOUSE GROUP BY house_name

