use master 
go 

create database HOUSE_DB
on
(
	name = 'HOUSE_DB',
	filename = 'D:\HOUSE_DB.mdf',
	size = 5,
	maxsize = 50,
	filegrowth = 10%
)
log on
(
	name = 'HOUSE_DB_log',
	filename = 'D:\HOUSE_DB_log.ldf',
	size = 5,
	maxsize = 50,
	filegrowth = 10% 
)
use HOUSE_DB
go

create table HOUSE_TYPE
(
	type_Id int primary key identity(1,1) not null,
	type_name varchar(50) not null
)
create table HOUSE
(
	house_id int primary key identity(1,1) not null,
	house_name varchar(50) not null,
	house_price float default(0) not null,
	type_id int references HOUSE_TYPE(type_Id) not null
)
insert HOUSE_TYPE values
('小户型'),('经济型'),('别墅')
go

insert HOUSE values
('学区房' , '5000' , 1),
('海景房' , '8000' , 3),
('安全屋' , '4000' , 2)
go 

--查询所有房屋信息
select * from HOUSE

--使用模糊查询包含”型“字的房屋类型信息
select * from HOUSE_TYPE where type_name like('%型')

--查询出房屋的名称和租金，并且按照租金降序排序
select house_name as 名称 , house_price as 租金 from HOUSE
order by house_price desc

--使用连接查询，查询信息，显示房屋名称和房屋类型名称
select house_name type_name from HOUSE A inner join HOUSE_TYPE B on A.type_id = B.type_Id

--查询所有房屋中月租最高的房屋，显示最高的租金和房屋名称
select top 1 house_name as 名称 , house_price as 租金 from HOUSE
order by house_price desc