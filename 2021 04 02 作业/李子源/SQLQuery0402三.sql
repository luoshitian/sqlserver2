use master 
go 

create database StarManagerDB
on
(
	name = 'StarManagerDB',
	filename = 'D:\StarManagerDB.mdf',
	size = 5,
	maxsize = 50,
	filegrowth = 10%
)
log on
(
	name = 'StarManagerDB_log',
	filename = 'D:\StarManagerDB_log.ldf',
	size = 5,
	maxsize = 50,
	filegrowth = 10% 
)
use StarManagerDB
go

create table StarType
(
	T_NO int primary key identity(1,1) not null,
	T_NAME nvarchar(20)
)
create table StarInfo
(
	S_NO int primary key identity(1,1) not null,
	S_NAME nvarchar(20) not null,
	S_AGE int not null,
	S_HOBBY nvarchar(20),
	S_NATIVE nvarchar(20) default('中国大陆'),
	S_T_NO int references StarType(T_NO)
)

insert StarType values
('体育明星'),
('IT明星'),
('相声演员')
go

insert StarInfo values
('梅西',30,'射门','阿根廷',1),
('科比',35,'过人','美国',1),
('蔡景现',40,'敲代码','中国',2),
('马斯克',36,'造火箭','外星人',2),
('郭德纲',50,'相声','中国',3),
('黄铮',41,'拼多多','中国',2)
go

--查询年龄最大的3个明星的姓名，特技和籍贯信息，要求使用别名显示列名。
select top 3 S_NAME as 姓名,S_AGE as 年龄,S_HOBBY as 特技,S_NATIVE as 籍贯信息 from StarInfo
order by S_AGE desc
go

--按明星类型编号分类查询明星人数，明星平均年龄，显示明星人数大于2的分组信息，要求使用别名显示列名。
select S_T_NO as 类型,count(*) as 人数,avg(S_Age) as 平均年龄 from StarInfo
group by S_T_NO
go

--查询明星类型为“体育明星”中年龄最大的姓名、特技、籍贯信息，要求显示列别名。
select top 1 S_NAME as 姓名,S_AGE as 年龄,S_HOBBY as 特技,S_NATIVE as 籍贯信息 from StarInfo
where S_T_NO = 1
order by S_AGE desc

