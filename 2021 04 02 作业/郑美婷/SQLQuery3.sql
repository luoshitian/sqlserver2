use MASTER 
GO
create database HOUSE_DB
on
(
name='HOUSE_DB',
filename='D:\HOUSE_DB.mdf',
size=5mb,
maxsize=50mb,
filegrowth=1mb
)
log on
(
name='HOUSE_DB_log',
filename='D:\HOUSE_DB_log.ldf',
size=5mb,
maxsize=50mb,
filegrowth=1mb
)
go
use HOUSE_DB
go
create table HOUSE_TYPE
(
type_id int primary key identity(1,1) not null,
type_name varchar(50) not null unique
)
create table HOUSE
(
house_id int primary key identity(1,1) not null,
house_name varchar(50) not null,
house_price float default(0) not null,
type_id int references HOUSE_TYPE(type_id) 
)
insert into HOUSE_type
select '小户型' union
select '经济型' union
select '别墅' union
select '豪华房车'
insert into HOUSE values
('聚春园','100000.12','3'),
('居民楼','120000.23','1'),
('空中花园','230000','2')
select*from HOUSE
--使用模糊查询包含”型“字的房屋类型信息
select*from HOUSE_TYPE where type_name like '%型%' 
--查询出房屋的名称和租金，并且按照租金降序排序
select house_name 房屋名称,house_price 租金 from HOUSE order by house_price desc 
--使用连接查询，查询信息，显示房屋名称和房屋类型名称
select house_name 房屋名称,type_name 房屋类型名称 from HOUSE inner join HOUSE_type on HOUSE.type_id=HOUSE_type.type_id
--查询所有房屋中月租最高的房屋，显示最高的租金和房屋名称
select top 1 house_name 房屋名称,max(house_price)租金 from HOUSE group by house_name,house_price order by house_price desc