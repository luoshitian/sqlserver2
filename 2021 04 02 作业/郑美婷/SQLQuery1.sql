use master
go
create database GOODsDB
on
(
name='GOODsDB',
filename='D:\GOODsDB.dmf',
size=5mb,
maxsize=100mb,
filegrowth=10%
)
log on
(
name='GOODsDB_log',
filename='D:\GOODsDB_log.lmf',
size=5mb,
maxsize=100mb,
filegrowth=10%
)
go
use GOODsDB
go
create table GOODsType
(
TypeId int primary key identity(1,1) not null,
TypeName nvarchar(20) not null
)
create table GOODsInfo
(
GOODsID int primary key identity (1,1) not null,
GOODsName nvarchar(20) not null,
GOODsColor nvarchar(20) not null,
GOODsBrand nvarchar(20),
GOODsMoney money not null,
TypeID int references GOODsType(TypeID)
)
insert into GOODsType
select '服装内衣'union
select '鞋子配饰'union
select '手机数码'
insert into GOODsInfo values
('提花小西裤','红色','费曼琪',300,1),
('百搭短裤','绿色','哥弟',100,1),
('无袖背心','白色','阿依莲',700,1),
('低帮休闲鞋','红色','菲曼琪',900,2),
('跟单鞋','绿色','哥弟',400,2),
('平底鞋','白色','阿依莲',200,2),
('迷你照相机','红色 ','尼康',500,3),
('硬盘','黑色','希捷',600,3),
('显卡','黑色','技嘉',800,3)
select*from GOODsInfo
select*from GOODsType
--3、查询价格最贵的商品名称，商品颜色和商品价格，要求使用别名显示列名
select top 1 GOODsName 商品名称, GOODsColor 商品颜色,max(GOODsMoney) 商品价格 from GOODsInfo
group by GOODsName,GOODsColor,GOODsMoney order by GOODsMoney desc
--4、按商品类型编号分组查询商品最高价格，最低价格和平均价格，要求使用别名显示列名
select GOODsType.TypeID 商品类型编号,TypeName 商品类型名称,max(GOODsMoney)最高价格,AVg(GOODsMoney)平均价格,min(GOODsMoney)最低价格 from
GOODsInfo inner join GOODsType on  GOODsInfo.TypeID=GOODsType.TypeId 
group by GOODsType.TypeID,TypeName
--5、查询商品信息所有列，要求商品颜色为红色，价格在300~600之间
select*from GOODsInfo where GOODsColor='红色' and GOODsMoney>=300 and GOODsMoney<=600





