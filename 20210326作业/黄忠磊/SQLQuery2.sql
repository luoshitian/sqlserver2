use master
go
--作业三
create database bbs
on
(
	name='bbs',
	filename='D:\bbs.mdf',
	size=10,
	maxsize=100,
	filegrowth=10%
)
log on
(
	name='bbs_log',
	filename='D:\bbs.ldf',
	size=5,
	maxsize=15,
	filegrowth=10%
)

go
use bbs
create table bbsUsers
(
	bbbsUID int	identity(1,1),
	uName varchar(10) not null,
	uSex  varchar(2) not null ,
	uAge  int not null ,
	uPoint  int not null 
)
go
alter table bbsUsers add constraint PK_bbbsUID primary key (bbbsUID)
alter table bbsUsers add constraint UK_uName unique(uName)
alter table bbsUsers add constraint CK_uSex check(uSex in('男','女'))
alter table bbsUsers add constraint CK_uAge check(uAge>14 and uAge<61 )
alter table bbsUsers add constraint CK_uPoint check(len(uPoint)>=0)
go
create table bbsSection
(
	bbssID  int identity(1,1),
	sName  varchar(10) not null,
	sUid   int
)

go
alter table bbsSection add constraint PK_bbssID primary key (bbssID)
alter table bbsSection add constraint FK_sUid foreign key (sUid) references bbsUsers(bbbsUID)
go
create table bbsTopic
(
	tID  int primary key identity(1,1),
	tUID  int foreign key references bbsUsers(bbbsUID),
	tSID  int foreign key references bbsSection(bbssID),
	 tTitle  varchar(100) not null,
	 tMsg  text,
	 tTime  datetime default(getdate()),
	 tCount  int
)

create table bbsReply
(
	rID  int primary key identity(1,1),
	rUID  int foreign key references bbsUsers(bbbsUID),
	rTID  int	foreign key references bbsTopic(tID),
	rMsg  text NOT NULL,
	rTime  datetime default(getdate())
)

go
insert into bbsUsers(uName,uSex,uAge,uPoint) values('小雨点','女',20,0),('逍遥','男',18,4),('七年级生','男',19,2)
select uName,uPoint into bbsPoint from bbsUsers
insert into bbsSection (sName,sUid) values('技术交流',1),('读书世界',3),('生活百科',1),('八卦区',3)

insert into bbsTopic(tUID,tSID,tTitle,tMsg,tTime,tCount) values(2 ,4,'范跑跑','谁是范跑跑 ','2008-7-8',1)
insert into bbsTopic(tUID,tSID,tTitle,tMsg,tTime,tCount) values(3 ,1,'.NET','与JAVA的区别是什么呀？ ','2008-9-1',2)
insert into bbsTopic(tUID,tSID,tTitle,tMsg,tTime,tCount) values(1 ,3,'今年夏天最流行什么','有谁知道今年夏天最流行什么呀 ','2008-9-10',0)

insert into bbsReply(rUID,rTID,rMsg,rTime) values(1 ,1,'不认识','2008-7-11')
insert into bbsReply(rUID,rTID,rMsg,rTime) values(1 ,2,'没有区别','2008-9-11')
insert into bbsReply(rUID,rTID,rMsg,rTime) values(2 ,2,'请百度','2008-9-12')
--在论坛数据库中完成以下题目
select * from bbsUsers
select * from bbsSection
select * from bbsTopic
select * from bbsReply
--1.查询出每个版块的版主编号，版主姓名和版块名称
select uName,sUid,sName from bbsSection inner join bbsUsers on bbsSection.bbssID=bbsUsers.bbbsUID
--2.查询出主贴的发帖时间在2008-9-15以后的主贴的发帖人编号，发帖人姓名，帖子的标题，帖子的内容和发帖时间
select * from bbsTopic where tTime<2008-9-15
--3.查询出年龄在20以下的版主的编号，版主的名称和版块的名称
select bbssID,uName,sName from bbsUsers inner join bbsSection on bbsUsers.bbbsUID=bbsSection.bbssID where uAge<20 group by bbssID,uName,sName
--4.查询出回复数量最多的主贴的发帖人编号，发帖人姓名，主贴标题，主贴内容和回复数量
select top 1 * from bbsTopic inner join bbsSection on bbsTopic.tUID=bbsSection.bbssID order by tCount desc
--5.在主贴表中查询每个版块中每个用户的发帖总数
select tSID 版块,uName 用户,tCount 发帖总数 from bbsTopic inner join bbsUsers on bbsTopic.tUID=bbsUsers.bbbsUID group by uName,tSID,tCount

