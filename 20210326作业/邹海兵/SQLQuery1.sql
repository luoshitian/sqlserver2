use master
go

create database ui
go

use ui
go
create table orders
(
	orderid int primary key identity(1,1),
	orderDate datetime
)
go

create table orderItem
(
	itemID int primary key identity(1,1),
	orderid int references orders(orderid),
	itemType varchar(10),
	itemName varchar(20),
	theNumber int,
	theMoney decimal(7,2)
)
go

insert into orders values('2008-01-12'),('2008-02-10'),('2008-02-15'),('2008-03-10')
go

insert into orderItem values(1,'文具','笔','72','2'),(1,'文具','尺','10','1'),(1,'体育用品','篮球','1','56'),(2,'文具','笔','36','2'),
(2,'文具','固体胶','20','3'),(2,'日常用品','透明胶','2','1'),(2,'体育用品','羽毛球','20','3'),(3,'文具','订书机','20','3'),(3,'文具','订书机','10','3'),
(3,'文具','裁纸刀','5','5'),(4,'文具','笔','20','2'),(4,'文具','信纸','50','1'),(4,'日常用品','毛巾','4','5'),(4,'日常用品','透明球','30','1'),
(4,'体育用品','羽毛球','20','3')
go



--1.查询所有的订单的订单的编号，订单日期，订购产品的类别和订购的产品名称，订购数量和订购单价
select OI.orderid,O.orderDate,itemType,itemName,theNumber,theMoney from orders O inner join orderItem OI on O.orderid=OI.orderid

--2.查询订购数量大于50的订单的编号，订单日期，订购产品的类别和订购的产品名称
select OI.orderid,O.orderDate,itemType,itemName from orders O inner join orderItem OI on O.orderid=OI.orderid where theNumber>50

--3.查询所有的订单的订单的编号，订单日期，订购产品的类别和订购的产品名称，订购数量和订购单价以及订购总价
select OI.orderid,O.orderDate,itemType,itemName,theNumber,theMoney 订购单价,sum(theMoney)订购总价
from orders O inner join orderItem OI on O.orderid=OI.orderid
group by OI.orderid,O.orderDate,itemType,itemName,theNumber,theMoney

--4.查询单价大于等于5并且数量大于等于50的订单的订单的编号，订单日期，订购产品的类别和订购的产品名称，订购数量和订购单价以及订购总价
select OI.orderid,O.orderDate,itemType,itemName,theNumber 订购数量,theMoney 订购单价,sum(theMoney)订购总价
from orders O inner join orderItem OI on O.orderid=OI.orderid
where theMoney>=5 and theNumber>=50
group by OI.orderid,O.orderDate,itemType,itemName,theNumber,theMoney

--5.查询每个订单分别订购了几个产品，例如：
--			编号   订购产品数
--           1           3
--           2           4
select OI.orderid 编号,count(itemType) 订购产品数
from orders O inner join orderItem OI on O.orderid=OI.orderid 
group by OI.orderid


--6.查询每个订单里的每个类别的产品分别订购了几次和总数量，例如：

-- 订单编号       产品类别     订购次数     总数量

--    1           文具            	2           82
--    1           体育用品        	1            1
--    2           文具            	2           56
--    2           体育用品        	1            2
--    2           日常用品        	1            20
select OI.orderid,itemType,count(itemType)订购次数,sum(theNumber)
from orderItem OI inner join orders O on OI.orderid=O.orderid
group by OI.orderid,itemType order by OI.orderid
