--统计每个班的男生数
use TestDB
go
select ClassId,count(StuSex) 男生 from StuInfo group by StuSex,ClassId having StuSex='男'
select * from StuInfo

--统计每个班的男、女生数
select ClassId,StuSex,count(StuSex)人数 from StuInfo group by StuSex,ClassId

--统计每个班的福建人数
select ClassId,count(*)人数 from StuInfo group by StuProvince,ClassId having StuProvince='福建省'
select * from StuInfo

--统计每个班的各个省的总人数
select ClassId,StuProvince,count(*)各个省的总人数 from StuInfo group by StuProvince,ClassId
select * from StuInfo

--统计每个省的女生数
select StuProvince,count(*)女生数 from StuInfo group by StuProvince,StuSex having StuSex='女'

--统计每个省的男、女生数
select StuProvince,StuSex,count(*)人数 from StuInfo group by StuProvince,StuSex

--统计每个学生的考试总分、平均分
select SUM(Score) from Scores where StuId=1
select StuId,SUM(Score)总分,avg(Score)平均分 from Scores group by StuId

--统计出考试总分大于620的学生的考试总分
select * from Scores
select StuId,SUM(Score) from Scores group by StuId having SUM(Score)>620


--统计出每门考试成绩最高分和最低分
select CourseId,max(Score)最高,min(Score)最低 from Scores group by CourseId

--统计出每个学生的各门成绩的平均分
select Score from Scores where StuId=1
select StuId,avg(Score)平均分 from Scores group by StuId 