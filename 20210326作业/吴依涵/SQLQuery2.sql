use master
go
create database biao
on
(
name='biao',
filename='D:\新建文件夹4.mdf',
size=6mb,
maxsize=100mb,
filegrowth=10mb
)
log on
(
name='biao_log',
filename='D:\新建文件夹4_log.ldf',
size=6mb,
maxsize=100mb,
filegrowth=10mb
)
go
use biao
go
create table orders
(
orderId int primary key identity,
orderDate datetime default(getDate())
)


use biao
go
create table orderItem
(
ItemiD int identity,
orderId int references orders(orderId),
itemType nvarchar(6), 
itemName nvarchar(5),
theNumber int,
theMoney money
)

insert into orders values(GETDATE()),(GETDATE()),(GETDATE()),(GETDATE())
insert into orderItem values(1,'文具','笔',72,2),
(1,'文具','尺',10,1),
(1,'体育用品','篮球',1,56),
(2,'文具','笔',36,2),
(2,'文具','固体胶',20,3),
(2,'日常用品','透明胶',2,1),
(2,'体育用品','羽毛球',20,3),
(3,'文具','订书机',20,3),
(3,'文具','订书针',10,3),
(3,'文具','裁纸刀',5,5),
(4,'文具','笔',20,2),
(4,'文具','信纸',50,1),
(4,'日常用品','毛巾',4,5),
(4,'日常用品','透明胶',30,1),
(4,'体育用品','羽毛球',20,3)

select * from orders
select * from orderItem

--1.查询所有的订单的订单的编号，订单日期，订购产品的类别和订购的产品名称，订购数量和订购单价
select O.orderId 订单编号,orderDate 订单日期,itemType 订购产品类别,itemName 订购的产品名称,theNumber 订购数量,theMoney 订购单价 from orders O inner join orderItem X on O.orderId = X.orderId

--2.查询订购数量大于50的订单的编号，订单日期，订购产品的类别和订购的产品名称
select O.orderId 订单编号,orderDate 订单日期,itemType 订购产品类别,itemName 订购的产品名称 from orders O inner join orderItem X on O.orderId = X.orderId
where theNumber>50

--3.查询所有的订单的订单的编号，订单日期，订购产品的类别和订购的产品名称，订购数量和订购单价以及订购总价
select O.orderId 订单编号,orderDate 订单日期,itemType 订购产品类别,itemName 订购的产品名称,theNumber 订购数量,theMoney 订购单价,theNumber*theMoney 订购总价 from orders O inner join orderItem X on O.orderId = X.orderId

--4.查询单价大于等于5并且数量大于等于50的订单的订单的编号，订单日期，订购产品的类别和订购的产品名称，订购数量和订购单价以及订购总价
select O.orderId 订单编号,orderDate 订单日期,itemType 订购产品类别,itemName 订购的产品名称,theNumber 订购数量,theMoney 订购单价 from orders O inner join orderItem X on O.orderId = X.orderId
where theMoney>5 and theNumber>50

--5.查询每个订单分别订购了几个产品，例如：
--			编号   订购产品数
--          1           3
--          2           4
select O.orderId 编号,count(*) 订购产品数 from orders O inner join orderItem X on O.orderId = X.orderId group by O.orderId

--6.查询每个订单里的每个类别的产品分别订购了几次和总数量，例如：
-- 订单编号       产品类别       订购次数     总数量
--    1           文具            	2           82
--    1           体育用品        	1            1
--    2           文具            	2           56
--    2           体育用品        	1            2
--    2           日常用品        	1            20

select O.orderId 订单编号,itemType 产品类别,count(*)订购次数,sum(theNumber)总数量 from orders O inner join orderItem X on O.orderId = X.orderId group by itemType,O.orderId