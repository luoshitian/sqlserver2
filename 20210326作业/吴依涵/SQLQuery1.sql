use master
go
create database Stundent
on
(
 name='Stundent',
 filename='D:\新建文件夹.mdf',
 size=6MB,
 maxsize=100MB,
 filegrowth=10Mb
)
log on
(
 name='Stundent_log',
 filename='D:\新建文件夹_1og.ldf',
 size=6MB,
 maxsize=100MB,
 filegrowth=10Mb
)
go
use Stundent
go
create table stuinfo--学生信息表
(
 stuNO varchar(10), 
 stuName nvarchar(20), 
 stuAge int, 
 stuAddress nvarchar(100),
 stuSeat char(10),
 StuSex nvarchar(1) check(StuSex in(1,0)),
 )
use Stundent
go
create table stuexam--学生成绩表
(
 examNO char(10),
 stuNO varchar(10),
 writtenExam int,
 labExam int,
)
--插入表数据
insert into stuinfo(stuNO,stuName,stuAge,stuAddress,stuSeat,StuSex)
values('s2501','张秋丽',20,'美国硅谷',1,1)
insert into stuinfo(stuNO,stuName,stuAge,stuAddress,stuSeat,StuSex)
values('s2502','李斯文',18,'湖北武汉',2,0)
insert into stuinfo(stuNO,stuName,stuAge,stuAddress,stuSeat,StuSex)
values('s2503','马文才',22,'湖南长沙',3,1)
insert into stuinfo(stuNO,stuName,stuAge,stuAddress,stuSeat,StuSex)
values('s2504','欧阳俊雄',21,'湖北武汉',4,0)
insert into stuinfo(stuNO,stuName,stuAge,stuAddress,stuSeat,StuSex)
values('s2505','梅超风',20,'湖北武汉',5,1)
insert into stuinfo(stuNO,stuName,stuAge,stuAddress,stuSeat,StuSex)
values('s2506','陈旋风',19,'美国硅谷',6,1)
insert into stuinfo(stuNO,stuName,stuAge,stuAddress,stuSeat,StuSex)
values('s2507','陈风',20,'美国硅谷',7,0)


insert into stuexam(examNO,stuNO,writtenExam,labExam)
values(1,'s2501',50,70)
insert into stuexam(examNO,stuNO,writtenExam,labExam)
values(2,'s2502',60,65)
insert into stuexam(examNO,stuNO,writtenExam,labExam)
values(3,'s2503',86,85)
insert into stuexam(examNO,stuNO,writtenExam,labExam)
values(4,'s2504',40,80)
insert into stuexam(examNO,stuNO,writtenExam,labExam)
values(5,'s2505',70,90)
insert into stuexam(examNO,stuNO,writtenExam,labExam)
values(6,'s2506',85,90)

select * from stuinfo
select * from stuexam

--1.查询学生的姓名，年龄，笔试成绩和机试成绩
select stuName 姓名,stuAge 年龄,writtenExam 笔试成绩,labExam 机试成绩 from stuinfo S inner join stuexam B on S.stuNO = B.stuNO

--2.查询笔试和机试成绩都在60分以上的学生的学号，姓名，笔试成绩和机试成绩
select S.stuNO 学号, stuName 姓名,writtenExam 笔试成绩,labExam 机试成绩 from stuinfo S inner join stuexam B on S.stuNO = B.stuNO  
where writtenExam>60 and labExam>60

--3.查询所有学生的学号，姓名，笔试成绩，机试成绩，没有参加考试的学生的成绩以NULL值填充
select S.stuNO 学号,stuName 姓名,writtenExam 笔试成绩,labExam 机试成绩 from stuinfo S left join stuexam B on S.stuNO = B.stuNO 

--4.查询年龄在20以上（包括20）的学生的姓名，年龄，笔试成绩和机试成绩，并按笔试成绩降序排列
select stuName 姓名,stuAge 年龄,writtenExam 笔试成绩,labExam 机试成绩 from stuinfo S join stuexam B on S.stuNO = B.stuNO  
where stuAge>=20 order by writtenExam DESC

--5.查询男女生的机试平均分
select StuSex 性别,avg(labExam)机试平均分 from stuinfo S inner join stuexam B on S.stuNO = B.stuNO group by StuSex 
 
--6.查询男女生的笔试总分
select StuSex 性别,sum(writtenExam)笔试总分 from stuinfo S inner join stuexam B on S.stuNO = B.stuNO group by StuSex 