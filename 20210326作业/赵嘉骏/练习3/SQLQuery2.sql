create database bbs
on(
  name=bbs,
  filename='D:\bbs.mdf', 
  maxsize=50mb,
  size=10mb,
  filegrowth=10%
)
log on(
  name=bbs_log,
  filename='D:\bbs_log.mdf', 
  maxsize=50mb,
  size=10mb,
  filegrowth=10%

)
go
create table bbsUser
(
UID int identity (1,1) not null ,
uName varchar(10) not null,
uSex varchar(2) not null,
uAge int not null,
uPoint int not null
)
create table bbsSection
(
sID int identity (1,1) not null,
sName varchar(10) not null,
sUid int
)
--添加约束
-- alter table 表名 add constraint 约束名  约束类型 
alter table bbsUser add constraint PK_bbsUsers_uID primary key(uID)
alter table bbsUser add constraint UK_bbsUsers_uName unique(uName)
alter table bbsUser add constraint CK_bbsUsers_uSex check(uSex='男' or uSex='女')
alter table bbsUser add constraint CK_bbsUsers_uAge check(uAge>=15 and uAge<=60)
alter table bbsUser add constraint CK_bbsUser_uPoint check(uPoint>=0)

alter table bbsSection add constraint PK_bbsSection_sID primary key(sID)
alter table bbsSection add constraint FK_bbsSection_sUid foreign key(sUid) references bbsUser(uID)

create table bbsTopic
(
tID int primary key identity (1,1),
tUID int references bbsUser(UID),
tSID int references bbsSection(sID),
tTitle varchar(100) not null,
tMsg text not null,
tTime datetime,
tCount int
)
create table bbsReply
(rID int primary key identity (1,1),
rUID int references bbsUser(UID),
rTID int references bbsTopic(tID),
rMsg text not null,
rTime datetime
)
insert into bbsUser(uName,uSex,uAge,uPoint)values('小雨点','女',20,0)
insert into bbsUser(uName,uSex,uAge,uPoint)values('逍遥','男',18,4)
insert into bbsUser(uName,uSex,uAge,uPoint)values('七年级生','男',19,2)
--将bbsUsers表中的用户名和积分两列备份到新表bbsPoint表中，
select uName,uPoint into  bbsPoint from bbsUser

insert bbsSection(sName,sUid)
	select '技术交流', 1 union
    select '读书世界', 3 union
	select '生活百科', 1 union
	select '八卦区', 3

	insert  bbsTopic (tUID,tSID,tTitle,tMsg,tTime,tCount)
		select 2 , 4,'范跑跑','谁是范跑跑','2008-7-8',1 union
		select 3 , 1,'.NET ','与JAVA的区别是什么呀？',' 2008-9-1 ',2 union
		select 1 , 3,'.NET ' , ' 有谁知道今年夏天最流行什么呀？','2008-9-10',0 

		 insert into bbsReply (rUID,rMsg,rTime)
	   select 1,'高奕','2021-3-15' union
	   select 2,'the name deferent','2021-3-15' union
	   select 3,'sex','2021-3-15'

--1.查询出每个版块的版主编号，版主姓名和版块名称
select sUid,uName,sName from bbsUser inner join bbsSection on bbsUser.uID=bbsSection.sUid

--2.查询出主贴的发帖时间在2008-9-15以后的主贴的发帖人编号，发帖人姓名，帖子的标题，帖子的内容和发帖时间
select tID,uName,tTitle,tMsg,tTime from bbsUser inner join bbsTopic on bbsUser.uID=bbsTopic.tID where tTime>='2008-8-15'
--3.查询出年龄在20以下的版主的编号，版主的名称和版块的名称
select sUID,uname,sname from bbsUser inner join bbsSection on bbsUser.uid=bbsSection.suid where uage<='19'
--4.查询出回复数量最多的主贴的发帖人编号，发帖人姓名，主贴标题，主贴内容和回复数量
select tid,uname,ttitle,tmsg,ttime,tcount from bbsuser inner join bbstopic on bbsuser.uid=bbstopic.tid where tcount=(select max(tCount) from bbsTopic)
--5.在主贴表中查询每个版块中每个用户的发帖总数
select * from bbssection
select tuid,count(tSID)发帖总数 from  bbsTopic group by tUID,tSID
