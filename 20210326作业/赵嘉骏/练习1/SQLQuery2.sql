create database Student
on
(
	name=Student,
	filename='D:\Student.mdf',
	size=10MB,
	maxsize=50MB,
	filegrowth=10%
)
log on
(
	name=Student_log,
	filename='D:\Student_log.ldf',
	size=10MB,
	maxsize=50MB,
	filegrowth=10%
)


create table Stuinfo
(
	stuNO varchar(5) unique not null,
	stuName nvarchar(20),
	stuAge int,
	stuAddress nvarchar(20),
	stuSeat int identity(1,1) unique not null,
	stuSex nchar(1) check(stuSex='男' or stuSex='女')
)
go
insert into Stuinfo values ('s2501','张秋利',20,'美国硅谷','女'),('s2502','李斯文',18,'湖北武汉','男'),('s2503','马文才',22,'湖南长沙','女'),
('s2504','欧阳俊雄',21,'湖北武汉','男'),('s2505','梅超风',20,'湖北武汉','女'),('s2506','陈旋风',19,'美国硅谷','女'),('s2507','陈风',20,'美国硅谷','男')

select 学号=stuNO,stuName as 姓名,stuAge as 年龄,stuAddress as 地址,stuSeat as 座位号,stuSex as 性别  from Stuinfo
select stuName,stuAge,stuAddress from Stuinfo
select stuNO,邮箱=stuName+'@'+stuAddress from Stuinfo
select stuAddress from Stuinfo
select distinct 所有年龄=stuAge from Stuinfo
select * from Stuinfo where stuSeat<=3
select stuName,stuSeat  from Stuinfo where stuSeat<=4
select top 50 percent * from Stuinfo 
select * from Stuinfo where stuAddress='湖北武汉' and stuAge=20
select * from Stuinfo where stuAddress='湖北武汉' or stuAddress='湖南长沙'
select * from Stuinfo where stuAddress in('湖北武汉' , '湖南长沙')
select * from Stuinfo where stuAge is null
select * from Stuinfo where stuAge is not null
select * from Stuinfo where stuName like '张%'
select * from Stuinfo where stuAddress like '湖%'
select * from Stuinfo where stuName like '张_'
select * from Stuinfo where stuName like '__俊_'
select * from Stuinfo order by stuAge desc
select * from Stuinfo order by stuAge desc,stuSeat asc



create table Scoreinfo
(
	examNO int primary key identity(1,1),
	stuNO varchar(5) references Stuinfo(stuNO),
	writtenExan int,
	labExam int,
)
insert into Scoreinfo values('s2501',50,70),('s2502',60,65),('s2503',86,85),('s2504',40,80),('s2505',70,90),('s2506',85,90)

select 学号=stuNO,writtenExan as 笔试 ,labExam as 机试 from Scoreinfo
select examNO,stuNO,总分=writtenExan+labExam from Scoreinfo
select labExam from Scoreinfo where labExam>=60 and labExam<=80 order by labExam desc
select writtenExan from Scoreinfo where not labExam>=70 and labExam<=90
select top 1* from Scoreinfo order by writtenExan desc
select top 1* from Scoreinfo order by labExam 
--25.查询每个地方的学生的平均年龄
select stuAddress, AVG(StuAge)平均年龄 from dbo.StuInfo group by stuAddress

--26.查询男女生的分别的年龄总和
select '男生：',SUM(StuAge)年龄总和 from StuInfo where StuSex='男'
UNION
select '女生：', SUM(StuAge)年龄总和 from StuInfo where StuSex='女'
--27.查询每个地方的男女生的平均年龄和年龄的总和
select stuAddress,AVG(StuAge)平均年龄,SUM(StuAge)年龄总和 from dbo.StuInfo  group by stuAddress 
select *from scoreinfo
select *from stuinfo
--1.查询学生的姓名，年龄，笔试成绩和机试成绩
select stuname,stuage,writtenexan,labexam from stuinfo inner join scoreinfo on stuinfo.stuno=scoreinfo.stuno
--2.查询笔试和机试成绩都在60分以上的学生的学号，姓名，笔试成绩和机试成绩
select Scoreinfo.stuNO,stuname,writtenexan,labexam from stuinfo inner join scoreinfo on stuinfo.stuno=scoreinfo.stuno where writtenexan>60 and labexam>60
--3.查询所有学生的学号，姓名，笔试成绩，机试成绩，没有参加考试的学生的成绩以NULL值填充
select Scoreinfo.stuNO,stuname,writtenexan,labexam from stuinfo left join scoreinfo on stuinfo.stuno=scoreinfo.stuno 
--4.查询年龄在20以上（包括20）的学生的姓名，年龄，笔试成绩和机试成绩，并按笔试成绩降序排列
select stuname,stuage,writtenexan,labexam from stuinfo inner join  scoreinfo on stuinfo.stuno=scoreinfo.stuno where stuage>=20 order by writtenexan desc
--5.查询男女生的机试平均分
select stusex,avg(labexam) from  stuinfo inner join scoreinfo on stuinfo.stuno=scoreinfo.stuno group by stusex
--6.查询男女生的笔试总分
select stusex,sum(writtenexan) from  stuinfo inner join scoreinfo on stuinfo.stuno=scoreinfo.stuno group by stusex