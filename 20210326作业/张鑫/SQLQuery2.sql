use TestDB
go


--统计每个班的男生数
select classid,count(stusex) from StuInfo group by classid,stusex having stusex='男'

--统计每个班的男、女生数
select classid,stusex,count(stusex)人数 from StuInfo group by classid,stusex
select * from StuInfo
--统计每个班的福建人数
select classid,stuProvince,count(StuProvince)人数 from StuInfo group by classid,StuProvince having StuProvince='福建省'
select * from StuInfo

--统计每个班的各个省的总人数
select classid,stuProvince,count(*)人数 from StuInfo group by ClassId,StuProvince

--统计每个省的女生数
select StuProvince,stusex,count(*)人数 from StuInfo group by StuProvince,StuSex having StuSex='女'

--统计每个省的男、女生数
select StuProvince,StuSex,count(StuSex)人数 from StuInfo group by StuProvince,StuSex order by StuProvince

--统计每个学生的考试总分、平均分
select stuid,sum(score)总分,avg(Score)平均分 from Scores group by StuId

--统计出考试总分大于620的学生的考试总分
select stuid,sum(Score)总分 from Scores group by StuId having sum(Score)>620

--统计出每门考试成绩最高分和最低分
select CourseId,max(score)最高分,min(score)最低分 from Scores group by CourseId

--统计出每个学生的各门成绩的平均分
select stuid,CourseId,avg(Score)平均分 from Scores group by StuId,CourseId