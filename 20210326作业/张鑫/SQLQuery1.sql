use master
go

create database Studen
on
(
	name=Studen,
	filename='D:\Studen.mdf',
	size=10MB,
	maxsize=50MB,
	filegrowth=10%
)
log on
(
	name=Studen_log,
	filename='D:\Studen_log.ldf',
	size=10MB,
	maxsize=50MB,
	filegrowth=10%
)

use Studen
go	

create table Stuinfo
(
	stuNO varchar(5) unique not null,
	stuName nvarchar(20),
	stuAge int,
	stuAddress nvarchar(20),
	stuSeat int identity(1,1) unique not null,
	stuSex nchar(1) check(stuSex='男' or stuSex='女')
)
go

insert into Stuinfo values ('s2501','张秋利',20,'美国硅谷','女'),('s2502','李斯文',18,'湖北武汉','男'),('s2503','马文才',22,'湖南长沙','女'),
('s2504','欧阳俊雄',21,'湖北武汉','男'),('s2505','梅超风',20,'湖北武汉','女'),('s2506','陈旋风',19,'美国硅谷','女'),('s2507','陈风',20,'美国硅谷','男')

create table Scoreinfo
(
	examNO int primary key identity(1,1),
	stuNO varchar(5) references Stuinfo(stuNO),
	writtenExan int,
	labExam int,
)

insert into Scoreinfo values('s2501',50,70),('s2502',60,65),('s2503',86,85),('s2504',40,80),('s2505',70,90),('s2506',85,90)

select StuName,stuAge,writtenExan,labExam from StuInfo
inner join Scoreinfo on Scoreinfo.stuNO = Stuinfo.stuNO

select StuInfo .stuNO,StuName,writtenExan,labExam from StuInfo 
inner join Scoreinfo on Scoreinfo.stuNO = Stuinfo.stuNO where writtenExan>'60' and labExam>'60'

select StuInfo .stuNO,StuName,writtenExan,labExam from StuInfo 
left join Scoreinfo on Scoreinfo.stuNO = Stuinfo.stuNO

select StuName,stuAge,writtenExan,labExam from StuInfo
inner join Scoreinfo on Scoreinfo.stuNO = Stuinfo.stuNO where stuAge>='20' order by writtenExan DESC 

select stuSex,avg(labExam) from StuInfo
inner join Scoreinfo on Scoreinfo.stuNO = Stuinfo.stuNO 
group by stuSex

select stuSex,sum(writtenExan) from StuInfo
inner join Scoreinfo on Scoreinfo.stuNO = Stuinfo.stuNO 
group by stuSex