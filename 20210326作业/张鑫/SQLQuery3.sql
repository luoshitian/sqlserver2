use master
go

create database zuoye
go

use zuoye
go
create table orders
(
	orderid int primary key identity(1,1),
	orderDate datetime
)

create table orderItem
(
	itemID int primary key identity(1,1),
	orderid int references orders(orderid),
	itemType varchar(10),
	itemName varchar(20),
	theNumber int,
	theMoney decimal(37,2)
)
insert into orders values('2008-01-12'),('2008-02-10'),('2008-02-15'),('2008-03-10')
insert into orderItem values(1,'文具','笔','72','2'),(1,'文具','尺','10','1'),(1,'体育用品','篮球','1','56'),(2,'文具','笔','36','2'),
(2,'文具','固体胶','20','3'),(2,'日常用品','透明胶','2','1'),(2,'体育用品','羽毛球','20','3'),(3,'文具','订书机','20','3'),(3,'文具','订书机','10','3'),
(3,'文具','裁纸刀','5','5'),(4,'文具','笔','20','2'),(4,'文具','信纸','50','1'),(4,'日常用品','毛巾','4','5'),(4,'日常用品','透明球','30','1'),
(4,'体育用品','羽毛球','20','3')

select itemID,orderDate,itemType,itemName,theNumber,theMoney from orderItem
inner join orders on orderItem.orderid = orders.orderid

select theNumber,itemID,orderDate,itemType,itemName from orderItem
inner join orders on orderItem.orderid = orders.orderid where theNumber>'50'

select itemID,orderDate,itemType,itemName,theMoney,(theNumber*theMoney)订购总价  from orderItem
inner join orders on orderItem.orderid = orders.orderid

select itemID,orderDate,itemType,itemName,theMoney,(theNumber*theMoney)订购总价  from orderItem
inner join orders on orderItem.orderid = orders.orderid where theMoney>='5' and theNumber>='50'

select orderid,count(orderid) 订购产品数 from orderItem
group by orderid

select orderid,itemType,count(orderid) 订购次数,sum(theNumber) from orderItem
group by orderid,itemType