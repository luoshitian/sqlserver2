create database bbs
on
(
name = 'bbs',
size = 5,
maxsize = 500,
filename = 'D:\新建文件夹2\bbs.mdf',
filegrowth = 10%
)
log on
(
name = 'bbs_log',
size = 5,
maxsize = 500,
filename = 'D:\新建文件夹2\bbs_log.ldf',
filegrowth = 10%
)
use bbs
go
create table bbsUsers   --用户信息表
(
UID int identity (1,1),    --用户编号
uName varchar(10) not null ,  --用户名
uSex  varchar(2) not null,  --性别
uAge  int not null,  --年龄
uPoint  int not null,  --积分
)
go
create table bbsSection		--板块表
(
 sID  int identity,		--板块编号
 sName  varchar(10) not null,--板块名称
 sUid   int,  --版主编号
)
go
alter table bbsUsers add constraint PK_UID primary key(UID);
alter table bbsUsers add constraint UQ_uName unique(uName);
alter table bbsUsers add constraint CK_uSex check(uSex = '男' or uSex = '女');
alter table bbsUsers add constraint CK_uAge check(uAge>=15 and uAge<=60);
alter table bbsUsers add constraint CK_uPoint check(uPoint>=0);
alter table bbsSection add constraint CK_sID primary key (sID);
alter table bbsSection add constraint FK_sUid foreign key(sUid) references bbsUsers(UID)
go
create table bbsTopic           --主贴表
(
tID  int primary key identity,		--主贴编号
tUID  int references bbsUsers(UID),--发帖人编号
tSID  int references bbsSection(sID),--板块编号
tTitle  varchar(100) not null,--贴纸标题
tMsg  text not null,--帖子内容
tTime  datetime  ,--发帖时间
tCount  int--回复数量
)
alter table bbsTopic alter column tMsg varchar(60)
go
create table bbsReply		--回贴表
(
rID  int primary key identity,--回帖编号
 rUID  int references bbsUsers(UID),--回帖人编号
 rTID  int references bbsTopic(tID),--对应主贴编号
 rMsg  text not null,--回帖内容
  rTime  datetime --回帖时间
)
go
insert into bbsUsers(uName,uSex,uAge,uPoint) values('小雨点','女',20,0),('逍遥','男',18,4),('七年级生','男',19,2)
insert into bbsSection (sName,sUid) values('技术交流',1),('读书世界',3),('生活百科',1),('八卦区',3)
insert into bbsTopic(tUID,tSID,tTitle,tMsg,tTime,tCount) values(2 ,4,'范跑跑','谁是范跑跑 ','2008-7-8',1)
insert into bbsTopic(tUID,tSID,tTitle,tMsg,tTime,tCount) values(3 ,1,'.NET','与JAVA的区别是什么呀？ ','2008-9-1',2)
insert into bbsTopic(tUID,tSID,tTitle,tMsg,tTime,tCount) values(1 ,3,'今年夏天最流行什么','有谁知道今年夏天最流行什么呀 ','2008-9-10',2)
insert into bbsReply(rTID,rMsg,rTime) values(1,'不认识',2008-7-11)
insert into bbsReply(rTID,rMsg,rTime) values(2,'没有区别',2008-9-11)
insert into bbsReply(rTID,rMsg,rTime) values(2,'请百度',2008-9-12)

--1.查询出每个版块的版主编号，版主姓名和版块名称
select sUid'版主编号',uName'版主姓名',sName'板块名称' from bbsSection inner join bbsUsers on bbsSection.sUid=bbsUsers.UID
--2.查询出主贴的发帖时间在2008-9-15以后的主贴的发帖人编号，发帖人姓名，帖子的标题，帖子的内容和发帖时间
select tUID,uName,tTitle,tMsg,tTime from bbsUsers left join bbsTopic on bbsUsers.UID=bbsTopic.tID where tTime>'2008-9-8'
--3.查询出年龄在20以下的版主的编号，版主的名称和版块的名称
select sUid'版主编号',uName'版主姓名',sName'板块名称',uAge 年龄 from bbsSection inner join bbsUsers on bbsSection.sUid=bbsUsers.UID where uAge<20
--4.查询出回复数量最多的主贴的发帖人编号，发帖人姓名，主贴标题，主贴内容和回复数量
select top 1 tUID,uName,tTitle,tMsg,tCount from bbsUsers left join bbsTopic on bbsUsers.UID=bbsTopic.tID order by tCount desc
--5.在主贴表中查询每个版块中每个用户的发帖总数
select uName 用户姓名,sName 板块名,tSID 板块编号,count(tuid)该用户发帖总数 from bbsTopic left join bbsUsers on  bbsUsers.UID=bbsTopic.tID 
left join bbsSection on bbsTopic.tSID=bbsSection.sID group by tSID,uName,sName
select * from bbsSection
select * from bbsUsers
select * from bbsReply
select * from bbsTopic