create database Student
on
(
	name='Stuinfo',
	filename='D:\新建文件夹.mdf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%

)
log on
(
	name='Stuinfo_log',
	filename='D:\新建文件夹.ldf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%
)
use Student
go

create table Stuinfo
(	
	stuNO varchar(5) unique not null,					--学生学号
	stuName nvarchar(20),								--学生姓名
	stuAge int,											--学生年龄
	stuAddress nvarchar(20),							--学生住址
	stuSeat int identity(1,1) unique not null,			--学生座位号
	stuSex nchar(1) check(stuSex='男' or stuSex='女')	--学生性别
)
go

create table Scoreinfo
(
	examNO int primary key identity(1,1),				--考试座位号
	stuNO varchar(5) references Stuinfo(stuNO),			--学生学号
	writtenExan int,									--笔试成绩
	labExam int,										--机试成绩
	)

go	
insert into Stuinfo values ('s2501','张秋利','20','美国硅谷','女'),('s2502','李斯文',18,'湖北武汉','男'),('s2503','马文才',22,'湖南长沙','女'),
('s2504','欧阳俊雄',21,'湖北武汉','男'),('s2505','梅超风',20,'湖北武汉','女'),('s2506','陈旋风',19,'美国硅谷','女'),('s2507','陈风',20,'美国硅谷','男')

insert into Scoreinfo values('s2501',50,70),('s2502',60,65),('s2503',86,85),('s2504',40,80),('s2505',70,90),('s2506',85,90)
select * from Stuinfo
select * from Scoreinfo
--1.查询学生的姓名，年龄，笔试成绩和机试成绩
select stuName,stuAge,writtenExan,labExam from Stuinfo inner join Scoreinfo on Stuinfo.stuNO=Scoreinfo.stuNO
select stuName,stuAge,writtenExan,labExam from Stuinfo left join Scoreinfo on Stuinfo.stuNO=Scoreinfo.stuNO
--2.查询笔试和机试成绩都在60分以上的学生的学号，姓名，笔试成绩和机试成绩
select Stuinfo.stuNO,stuName,writtenExan,labExam from Stuinfo left join Scoreinfo on Stuinfo.stuNO=Scoreinfo.stuNO where writtenExan>60 and labExam>60
--3.查询所有学生的学号，姓名，笔试成绩，机试成绩，没有参加考试的学生的成绩以NULL值填充
select Stuinfo.stuNO,stuName,writtenExan,labExam from Stuinfo left join Scoreinfo on Stuinfo.stuNO=Scoreinfo.stuNO
--4.查询年龄在20以上（包括20）的学生的姓名，年龄，笔试成绩和机试成绩，并按笔试成绩降序排列
select stuName,stuAge,writtenExan,labExam from Stuinfo right join Scoreinfo on Stuinfo.stuNO=Scoreinfo.stuNO where stuAge>=20 order by stuAge desc
--5.查询男女生的机试平均分
select stuSex 性别, avg(labExam)机试平均分 from Stuinfo left join Scoreinfo on Stuinfo.stuNO=Scoreinfo.stuNO group by stuSex
--6.查询男女生的笔试总分
select stuSex 性别, sum(labExam)机试平均分 from Stuinfo left join Scoreinfo on Stuinfo.stuNO=Scoreinfo.stuNO group by stuSex