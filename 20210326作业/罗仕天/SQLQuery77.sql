use master
go

create database Studen
go
use Studen
go	

create table Stuinfo
(
	stuNO varchar(5) unique not null,
	stuName nvarchar(20),
	stuAge int,
	stuAddress nvarchar(20),
	stuSeat int identity(1,1) unique not null,
	stuSex nchar(1) check(stuSex='男' or stuSex='女')
)
go

insert into Stuinfo values ('s2501','张秋利',20,'美国硅谷','女'),('s2502','李斯文',18,'湖北武汉','男'),('s2503','马文才',22,'湖南长沙','女'),
('s2504','欧阳俊雄',21,'湖北武汉','男'),('s2505','梅超风',20,'湖北武汉','女'),('s2506','陈旋风',19,'美国硅谷','女'),('s2507','陈风',20,'美国硅谷','男')
go

create table Scoreinfo
(
	examNO int primary key identity(1,1),
	stuNO varchar(5) references Stuinfo(stuNO),
	writtenExan int,
	labExam int,
)
go

insert into Scoreinfo values('s2501',50,70),('s2502',60,65),('s2503',86,85),('s2504',40,80),('s2505',70,90),('s2506',85,90)
go


--1.查询学生的姓名，年龄，笔试成绩和机试成绩
select stuName,stuAge,writtenExan,labExam from Stuinfo S inner join Scoreinfo SI on S.stuNO=SI.stuNO

--2.查询笔试和机试成绩都在60分以上的学生的学号，姓名，笔试成绩和机试成绩
select S.stuNO,stuName,writtenExan,labExam from Stuinfo S inner join Scoreinfo SI on S.stuNO=SI.stuNO where writtenExan>60 and labExam>60

--3.查询所有学生的学号，姓名，笔试成绩，机试成绩，没有参加考试的学生的成绩以NULL值填充
select S.stuNO,stuName,writtenExan,labExam from Stuinfo S left join Scoreinfo SI on S.stuNO=SI.stuNO

--4.查询年龄在20以上（包括20）的学生的姓名，年龄，笔试成绩和机试成绩，并按笔试成绩降序排列
select stuName,stuAge,writtenExan,labExam from Stuinfo S inner join Scoreinfo SI on S.stuNO=SI.stuNO order by writtenExan desc

--5.查询男女生的机试平均分
select stuSex,avg(labExam)机试平均分 from Stuinfo S inner join Scoreinfo SI on S.stuNO=SI.stuNO group by stuSex

--6.查询男女生的笔试总分
select stuSex,sum(writtenExan)笔试总分 from Stuinfo S inner join Scoreinfo SI on S.stuNO=SI.stuNO group by stuSex