create database orderskk
on(
        name='orderskk',
		filename='D:\orderskk.mdf',
		size=10,
		maxsize=100,
		filegrowth=10
		

)
log on
(             
        name='orderskk_log',
		filename='D:\orderskk_log.ldf',
		size=10,
		maxsize=100,
		filegrowth=10
      


)
go
use orderskk
go
create table orders
( orderId int primary key identity ,
  orderDate datetime 
)
go
create table orderItem
( 
    ItemiD int primary key identity ,
	orderId int ,
	itemType nvarchar(10),
	itemName nvarchar(10),
	theNumber money  ,
	theMoney money ,
)
insert  orders values('2008-01-12'),('2008-02-10'),('2008-02-15'),('2008-03-10')
insert  orderItem values
(1,'文具','笔',72,2),
(1,'文具','尺',10,1),
(1,'体育用品','篮球',1,56),
(2,'文具','笔',36,2),
(2,'文具','固体胶',20,3), 
(2,'日常用品','透明胶',2,4), 
(2,'体育用品','羽毛球',20,3), 
(3,'文具','订书机',20,3), 
(3,'文具','订书针',10,3), 
(3,'文具','裁纸刀',5,5), 
(4,'文具','笔',20,2), 
(4,'文具','信纸',50,1), 
(4,'日常用品','毛巾',4,5), 
(4,'日常用品','透明胶',30,1),
(4,'体育用品','羽毛球',20,3) 

select * from orders
select * from orderItem
select SUM (theNumber) as 总数 from dbo.orderItem
select orderId ,sum(theNumber), AVG(theMoney) from orderItem where orderId<3  group by orderId HAVING avg(theMoney)<10 
select orderId ,sum(theNumber), AVG(theMoney) from orderItem   where theNumber>50 group by orderId HAVING avg(theMoney)<10 
select itemType,COUNT(itemType)人数 from dbo.orderItem group by itemType
select itemType ,sum(theNumber), AVG(theMoney) from orderItem    group by itemType HAVING sum(theNumber)>100 
select itemName ,COUNT(itemName),SUM(theNumber),AVG(theMoney) from orderItem group by itemName



--使用上次作业的订单数据库，完成下列题目：

select * from  orders
select * from  orderItem



--1.查询所有的订单的  订单的编号，订单日期，订购产品的类别  和  订购的产品名称，订购数量  和  订购单价
select OI.orderid,O.orderDate,itemType,itemName,theNumber,theMoney from orders O inner join orderItem OI on O.orderid=OI.orderid



--2.查询订购数量大于50的 订单的编号，订单日期，订购产品的类别  和 订购的产品名称

select 1.theNumber ,2.orderDate,orderId from orders 1 inner join orderItem 2 on 1.orderId=2.orderId 


--3.查询所有的订单的订单的编号，订单日期，订购产品的类别和订购的产品名称，订购数量和订购单价以及订购总价
select * from orders



--4.查询单价大于等于5并且数量大于等于50的订单的订单的编号，订单日期，订购产品的类别和订购的产品名称，订购数量和订购单价以及订购总价

--5.查询每个订单分别订购了几个产品，例如：
--			编号   订购产品数
--                                            	  1           3
--                                            	  2           4

--6.查询每个订单里的每个类别的产品分别订购了几次和总数量，例如：

-- 订单编号       产品类别     订购次数     总数量

--    1           文具            	2           82
--    1           体育用品        	1            1
--    2           文具            	2           56
--    2           体育用品        	1            2
--    2           日常用品        	1            20