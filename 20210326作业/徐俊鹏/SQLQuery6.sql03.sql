use master 
go
create database bbs
on
(
	name='bbs',
	filename='D:\bbs.mdf',
	size=10,
	maxsize=200,
	filegrowth=20
)
log on
(
	name='bbs_log',
	filename='D:\bbs_log.ldf ',
	size=10,
	maxsize=200,
	filegrowth=20
)
use bbs
  create table bbsUsers
 (
	UIDD  int identity(1,1) not null ,
	uName   nvarchar(10) not null,
	uSex   varchar(2) not null ,
	uAge  int not null,
	uPoint int not null 
 )
 alter table bbsUsers add constraint PK_bbsUsers_UIDD primary key(UIDD )
alter table  bbsUsers add constraint UK_bbsUsers_uName unique(uName)
alter table bbsUsers add constraint CK_bbsUsers_uSex check(uSex in('女','男'))
alter table bbsUsers add constraint CK_bbsUsers_uAge  check(uAge>=15 and uAge<=60)
alter table bbsUsers add constraint CK_bbsUsers_uPoint  check(uPoint>=0)
  create table bbsTopic
 (
	tID   int primary key identity(1,1),
	tUID   int references  bbsUsers(UIDD),
	tSID  int references  bbsSection(sIDD),
	tTitle    nvarchar(100) not null,
	tMsg    text not null,
	tTime   datetime,  
	tCount  int 
 )
 create table bbsReply
 (
	rID     int primary key identity(1,1),
	rUID     int references bbsUsers(UIDD) ,
	rTID     int references  bbsTopic(tID),
	rMsg      text not null,
	rTime     datetime,   
 )
  create table bbsSection
 (
	sIDD    int identity(1,1),
	sName  nvarchar(10),
	sUid  int  , 
 )
 alter table bbsSection add constraint PK_bbsSection_sIDD primary key(sIDD)
 alter table bbsSection add constraint FK_bbsSection_sUid foreign key(sUid) references bbsUsers(UIDD)
 
 insert into bbsUsers(uName,uSex,uAge,uPoint) values ('小雨点','女','20','0'),('逍遥 ','男','18','4'),('七年级生','男','19','2')

 create table bbsPoint
 (
	ID nvarchar(10) ,
	point int 
 )
 
 --
 insert into bbsPoint(ID,point) select uName,uPoint from bbsUsers


 --
 select uName,uPoint into bbsUsers2 from bbsUsers 


 insert into bbsSection(sName,sUid) values ('技术交流  ','1'),('  读书世界 ','3'),('生活百科','1'),('八卦区 ','3')
 insert into bbsTopic(tUID,tSID,tTitle,tMsg,tTime,tCount) values ('2','4','范跑跑  ','谁是范跑跑','2008-7-8 ','1'),
 ('3','1','.NET','与JAVA的区别是什么呀？','2008-9-1','2'),('1','3','今年夏天最流行什么','有谁知道今年夏天最流行什么呀？',' 2008-9-10','0')

 insert into bbsReply(rUID,rMsg, rTime) values ('1','你是','2020.1.2'),('3','爱狗','2020.2.15'),('2','还是能','2020.5.25')
 select * from bbsSection
 select * from bbsUsers
 select * from bbsTopic
 select * from bbsReply

--1.查询出每个版块的版块编号，版主姓名和版块名称
select uName 版主姓名,sIDD 版块编号,sName 版块名称  from bbsSection C inner join bbsUsers B on C.sUid=B.UIDD
--2.查询出主贴的发帖时间在2008-8-15以后的主贴的发帖人编号，发帖人姓名，帖子的标题，帖子的内容和发帖时间
select tUID 发帖人编号, uName 发帖人姓名,tTitle 帖子的标题 ,tMsg 帖子的内容 ,tTime  发帖时间 from bbsTopic BT inner join bbsUsers BU on BT.tUID  =BU.UIDD where tTime>'2008-8-15'
--3.查询出年龄在20以下的版主的编号，版主的名称和版块的名称
select uName 版主姓名,sUid 版主编号,sName 版块名称  from bbsSection C inner join bbsUsers B on C.sUid=B.UIDD where uAge >18
--4.查询出回复数量最多的主贴的发帖人编号，发帖人姓名，主贴标题，主贴内容和回复数量
select tUID 发帖人编号,uName 发帖人姓名,tTitle 主贴标题 ,tMsg 主贴内容,tCount 回复数量 from bbsTopic C inner join bbsUsers B on C.tUID =B.UIDD where tCount=  (select max(tCount)from bbsTopic)
--5.在主贴表中查询每个版块中每个用户的发帖总数
select sName 板块名称,uName 用户名称 ,sum(tCount) 发帖总数 from  bbsTopic inner join bbsSection on sIDD=tSID  
inner join bbsUsers on tUID =UIDD group by sName ,uName

