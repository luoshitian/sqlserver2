
	use master
	go

	create database bbs
	on
	(
		name='bbs',
		filename='D:\bbs.mdf',
		size=5MB,
		filegrowth=10MB,
		maxsize=100MB	
	)
	log on
	(
		name='bbs_log',
		filename='D:\bbs_log.ldf',
		size=5MB,
		filegrowth=10MB,
		maxsize=100MB	
	)
	go

	use bbs
	go

	create table bbsUser
	(
		UID int identity,
		uName varchar(10) not null,
		uSex  varchar(2) not null,
		uAge  int not null,
		uPoint  int not null
	)

	alter table bbsUser add constraint PK_bbsUser_UID primary key(UID)
	alter table bbsUser add constraint UK_bbsUser_uName unique(uName)
	--alter table bbsUser add constraint CK_bbsUser_uSex check(uSex='男' or uSex='女')
	alter table bbsUser add constraint CK_bbsUser_uSex check(uSex in('男','女'))
	--alter table bbsUser add constraint CK_bbsUser_uAge check(uAge>=15 and uAge<=60)
	alter table bbsUser add constraint CK_bbsUser_uAge check(uAge between 15 and 60)
	alter table bbsUser add constraint CK_bbsUser_uPoint check(uPoint>=0)


	create table bbsTopic
	(
		tID  int primary key identity,
		tUID  int references bbsUser(UID),
		tSID  int references bbsSection(sID),
		tTitle  varchar(100) not null,
		tMsg  varchar(100) not null,
		tTime  datetime,
		tCount  int
	)

	create table bbsReply
	(
		rID  int primary key identity,
		rUID  int references bbsUser(UID),
		rTID  int references bbsTopic(tID),
		rMsg  text not null,
		rTime  datetime 
	)
	
	create table bbsSection
	(
		sID  int identity,
		sName  varchar(10) not null,
		sUid   int 
	)
	alter table bbsSection add constraint PK_bbsSection_sID primary key(sID) 
	alter table bbsSection add constraint FK_bbsSection_sUid foreign key(sUid) references bbsUser(UID)

	insert into bbsUser values ('小雨点','女',20,0),('逍遥','男',18,4),('七年级生','男',19,2)
	insert into bbsSection values ('技术交流',1),('读书世界',3),('生活百科',1),('八卦区',3)
	insert into bbsTopic values (2,4,'范跑跑','谁是范跑跑','2008-7-8',1)
	insert into bbsTopic values (3,1,'.NET','与JAVA的区别是什么呀？','2008-9-1',2),(1,3,'今年夏天最流行什么','有谁知道今年夏天最流行','2008-9-10',0)
	insert into bbsReply ( rMsg,  rTime, rUID,rTID )values ('范跑跑是',GETDATE(),1,1),
													  ('.NET与JAVA的区别是',GETDATE(),2,2),
													  ('今年夏天最流行是',GETDATE(),3,3)

	--1.查询出每个版块的版主编号，版主姓名和版块名称
		select sUid 版主编号,uName 用户名,sName 版块名称 from bbsSection 
	    inner join bbsUser on bbsSection.sUid = bbsUser.UID
	--2.查询出主贴的发帖时间在2008-9-15以后的主贴的发帖人编号，发帖人姓名，帖子的标题，帖子的内容和发帖时间
		select tUID 发帖人编号,uName 用户名,tTitle 贴子的标题, tMsg 帖子的内容,tTime 发帖时间  from bbsTopic 
		inner join bbsUser on bbsTopic.tUID = bbsUser.UID
		where tTime > '2008-9-15'
	--3.查询出年龄在20以下的版主的编号，版主的名称和版块的名称
		select uAge 年龄,sUid 版主编号,uName 用户名,sName 版块名称 from bbsSection 
		inner join bbsUser on bbsSection.sUid = bbsUser.UID
		where uAge < 20
	--4.查询出回复数量最多的主贴的发帖人编号，发帖人姓名，主贴标题，主贴内容和回复数量
		select top 1 max(tCount)回复数量最多 ,tUID 发帖人编号,uName 用户名,tTitle 贴子的标题, tMsg 帖子的内容,  tCount 回复数量 from bbsTopic
		inner join bbsUser on bbsTopic.tUID = bbsUser.UID
		group by tUID,uName,tTitle,tCount, tMsg
		order by tCount DESC
	--5.在主贴表中查询每个版块中每个用户的发帖总数
		select uName 用户名, sName 版块名称, count(tID)发帖总数 from bbsTopic
		inner join bbsUser on bbsTopic.tUID = bbsUser.UID
		inner join bbsSection on bbsTopic.tUID = bbsSection.sUid 
		group by uName ,sName

	select * from bbsUser
	select * from bbsSection
	select * from bbsTopic 
	select * from bbsReply
	 


  