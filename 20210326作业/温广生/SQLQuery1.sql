create database sss
on
(
	name='sss',
	FILENAME='D:\temp\sss.mdf',
	SIZE=5,
	MAXSIZE=50,
	FILEGROWTH=10%
)
log on
(
	NAME='sss_log',
	FILENAME='D:\temp\sss_log.ldf',
	SIZE=5,
	MAXSIZE=50,
	FILEGROWTH=10%

)
go
use sss
go

create table stuinfo
(
	StuNo varchar(20),
	stuName varchar(10) not null,
	stuAge int not null,
	stuAddress varchar(100),
	stuSeat int primary key identity,
	stuSex char(2) default(1) check(stuSex=0 or stuSex=1)
)
create table stuexam2
(
	examNo2 int primary key identity(1,1),
	stuNo2 varchar(20),
	writtenExam2 int,
	labExam2 int
)

insert into stuinfo(StuNo,stuName,stuAge,stuAddress,stuSex)
select 's2501','张秋利',20,'美国硅谷',1 union
select 's2502','李斯文',18,'湖北武汉',0 union
select 's2503','马文才',22,'湖南长沙',1 union
select 's2504','欧阳俊雄',21,'湖北武汉',0 union
select 's2505','梅超风',20,'湖北武汉',1 union
select 's2506','陈旋风',19,'美国硅谷',1 union
select 's2507','陈风',20,'美国硅谷',0

insert into stuexam2(stuNo2,writtenExam2,labExam2)
select 's2501',50,70 union
select 's2502',60,65 union
select 's2503',86,85 union
select 's2504',40,80 union
select 's2505',70,90 union
select 's2506',85,90

select * from stuinfo
select * from stuexam2

select * from stuinfo SI inner join stuexam2 SE on SI.StuNo=SE.stuNo2



--1.查询学生的姓名，年龄，笔试成绩和机试成绩
select StuNO,stuname,writtenExam2,labExam2 from stuinfo inner join stuexam2
on stuinfo.StuNo=stuexam2.stuNo2
--2.查询笔试和机试成绩都在60分以上的学生的学号，姓名，笔试成绩和机试成绩
select StuNO,stuname,writtenExam2,labExam2 from stuinfo inner join stuexam2
on stuinfo.StuNo=stuexam2.stuNo2 where writtenExam2>60 and labExam2>60
--3.查询所有学生的学号，姓名，笔试成绩，机试成绩，没有参加考试的学生的成绩以NULL值填充
---select StuNO,stuname,writtenExam2,labExam2 from stuinfo inner join stuexam2
on stuinfo.StuNo=stuexam2.stuNo2
--4.查询年龄在20以上（包括20）的学生的姓名，年龄，笔试成绩和机试成绩，并按笔试成绩降序排列
select stuname,stuAge,writtenExam2,labExam2 from stuinfo inner join stuexam2
on stuinfo.StuNo=stuexam2.stuNo2 where stuAge>=20 order by writtenExam2 desc
--5.查询男女生的机试平均分
--select xb 性别,SUM(yw) 语文总分,SUM(sx) 数学总分,SUM(yy) 英语总分,AVG(yw) 语文平均分,AVG(sx) 数学平均分,AVG(yy) 英语平均分 from xs left join cj on xs.xm=cj.xm group by xb
select stusex 性别,avg(labExam2) from stuinfo inner join stuexam2 on stuinfo.StuNo=stuexam2.stuNo2 group by stuSex
--6.查询男女生的笔试总分
select stusex 性别,sum(writtenExam2) from stuinfo inner join stuexam2 on stuinfo.StuNo=stuexam2.stuNo2 group by stuSex