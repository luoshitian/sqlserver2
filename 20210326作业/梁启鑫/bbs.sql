
use bbs9
go

--1.查询出每个版块的版主编号，版主姓名和版块名称

select sUid 版主编号,uName 版主姓名,sName 板块名称 from bbsSection inner join bbsUsers on bbsUsers.bbbsUID = bbsSection.sUid

--2.查询出主贴的发帖时间在2008-9-15以后的主贴的发帖人编号，发帖人姓名，帖子的标题，帖子的内容和发帖时间

select tUID,uName,tTitle,tMsg,tTime from bbsTopic inner join bbsUsers on bbsUsers.bbbsUID = bbsTopic.tUID where tTime>'2008-9-15'
--3.查询出年龄在20以下的版主的编号，版主的名称和版块的名称

select sUid 版主的编号,uName 版主的名称,sName 版块的名称 from bbsUsers left join bbsSection on bbsSection.sUid = bbsUsers.bbbsUID where uAge<20
--4.查询出回复数量最多的主贴的发帖人编号，发帖人姓名，主贴标题，主贴内容和回复数量

select tuid,uName,tTitle,tMsg,tCount from bbsTopic T inner join bbsUsers U on T.tUID = U.bbbsUID where tCount=(select max(tCount) from bbsTopic)
--5.在主贴表中查询每个版块中每个用户的发帖总数

select sName,uName,count(tUID) from bbsTopic 
inner join bbsSection on bbsTopic.tSID = bbsSection.bbssID
inner join bbsUsers on bbsTopic.tUID = bbsUsers.bbbsUID
group by sName,uName

select * from bbsSection
select * from bbsTopic
select * from bbsUsers