create database orders
on
(
	name = 'order',
	size = 5,
	maxsize = 500,
	filename = 'D:\新建文件夹3\order.mdf',
	filegrowth = 10%
)
log on
(
	name = 'order_LOG',
	size = 5,
	maxsize = 500,
	filename = 'D:\新建文件夹3\order.ldf',
	filegrowth = 10%
)
use orders
go
create table orders			--订单表
(
	orderId int primary key,  --订单编号
	orderDate smalldatetime		--订购日期
)
go
create table orderItem		--订购项目表
(
	itemName nvarchar(10) not null,		--产品名称
	theNumber  int not null,		--订购数量
	theMoney money not null,		--订购单价
	itemType nvarchar(4)  not null,--产品类别
	orderId	int references	orders(orderId)	not null,		--订单编号
	ItemiD int identity(1,1) primary key,		--项目编号
)
insert into orders(orderId,orderDate) values(1,'2008-01-12'),(2,'2008-02-10'),(3,'2008-02-15'),(4,'2008-03-10')
insert into orderItem(orderId,itemType,itemName,theNumber,theMoney)
select 1,'文具','笔',72,2 union
select 1,'文具','尺',10,1 union
select 1, '体育用品','篮球',1,56 union
select 2,'文具','笔',36,2 union
select 2,'文具','固体胶',20,3 union
select 2,'日常用品','透明胶',2,1 union
select 2,'体育用品','羽毛球',20,3 union
select 3,'文具','订书机',20,3 union
select 3,'文具','订书机',10,3 union
select 3,'文具','裁缝刀',5,5 union
select 4,'文具','笔',20,2 union
select 4,'文具','信纸',50,1 union
select 4,'日常用品','毛巾',4,5 union
select 4,'日常用品','透明胶',30,1 union
select 4,'体育用品','羽毛球',20,3 
select * from orders
select * from orderItem
--1.查询所有的订单的订单的编号，订单日期，订购产品的类别和订购的产品名称，订购数量和订购单价
select orderItem.orderId,orderDate,itemType,itemName,theNumber,theMoney from orders left join orderItem on orders.orderId=orderItem.orderId
--2.查询订购数量大于50的订单的编号，订单日期，订购产品的类别和订购的产品名称
select orderItem.orderId,orderDate,itemType,itemName,theNumber from orders left join orderItem on orders.orderId=orderItem.orderId where theNumber>50
--3.查询所有的订单的订单的编号，订单日期，订购产品的类别和订购的产品名称，订购数量和订购单价以及订购总价
select orderItem.orderId 订单的编号,orderDate 订单日期,itemType 订购产品的类别,theNumber 订购数量,theMoney 订购单价,theNumber*theMoney 订购总价 from orderItem left join orders on orders.orderId=orderItem.orderId
--4.查询单价大于等于5并且数量大于等于50的订单的订单的编号，订单日期，订购产品的类别和订购的产品名称，订购数量和订购单价以及订购总价
select orderItem.orderId 订单编号, orderDate 订单日期,itemType 订购产品的类别,itemName 产品名称,theNumber 订购数量,theMoney 订购单价,theNumber*theMoney 订购总价 from orderItem left join orders on orders.orderId=orderItem.orderId where theMoney>5 and theNumber >=50
--5.查询每个订单分别订购了几个产品，例如：
--		编号   订购产品数
--		  1           3
--        2           4
select orderId 编号,count(itemType) 订购产品数 from orderItem group by orderId
select orders.orderId 编号,count(itemType) 订购产品数 from orderItem left join  orders on orderItem.orderId=orders.orderId group by orders.orderId
--6.查询每个订单里的每个类别的产品分别订购了几次和总数量，例如：
-- 订单编号       产品类别     订购次数     总数量
--    1           文具            	2           82
--    1           体育用品        	1            1
--    2           文具            	2           56
--    2           体育用品        	1            2
--    2           日常用品        	1            20
select Distinct orderId 编号,itemType 产品类型,count(theNumber)订购产品数,sum(theNumber)总数量 from orderItem group by orderId ,itemType order by orderId asc