use master 
go
create database orderr
on
(
	name='orderr',
	filename= 'D:\orderr.mdf',
	size=10,
	maxsize=100,
	filegrowth=15%
)
log on
(
	name='orderr_log',
	filename= 'D:\orderr_log.ldf',
	size=10,
	maxsize=100,
	filegrowth=15%
)
go
use orderr
go
create table orders
(
	orderId int primary key identity,
	orderDate datetime  default(getdate())
)
create table orderItem
(
	ItemiD int primary key identity,
	orderId int references orders(orderId) ,
	itemType nvarchar(10),
	itemName nvarchar(10),
	theNumber  int ,
	theMoney money
)
insert into orders values(default),(default),(default),(default)
select * from orders
insert into orderItem  values ('1','文具','笔','72','2'),('1','文具','尺','10','1'),('1','体育用品','蓝球','1','56'),('2','文具','笔','36','2'),('2','文具','固体胶','20','3'),
('2','日常用品','透明胶','2','1'),('2','体育用品','羽毛球','20','3'),('3','文具','订书机','20','3'),('3','文具','订书针','10','3'),('3','文具','裁纸刀','5','5'),
('4','文具','笔','20','2'),('4','文具','信纸','50','1'),('4','日常用品','毛巾','4','5'),('4','日常用品','透明胶','30','1'),('4','体育用品','羽毛球','20','3')


select * from orderItem
select * from orders
--1.查询所有的订单的订单的编号，订单日期，订购产品的类别和订购的产品名称，订购数量和订购单价

select OI.orderId 订单编号 , orderDate 订单日期 , itemType 产品类型 , itemName 产品名称 ,theNumber 订购数量 , theMoney 订单单价  from orderItem OI 
	inner join orders O on  OI.orderId =O.orderId

--2.查询订购数量大于50的订单的编号，订单日期，订购产品的类别和订购的产品名称

select OI.orderId 订单编号 , orderDate 订单日期 , itemType 产品类型 , itemName 产品名称   from orderItem OI 
	inner join orders O on  OI.orderId =O.orderId where theNumber>50

--3.查询所有的订单的订单的编号，订单日期，订购产品的类别和订购的产品名称，订购数量和订购单价以及订购总价

select  OI.orderId 订单编号 , orderDate 订单日期 , itemType 产品类型 , itemName 产品名称 ,theNumber 订购数量 , theMoney 订单单价 from orderItem OI 
	inner join orders O on  OI.orderId =O.orderId 

--4.查询单价大于等于5并且数量大于等于50的订单的订单的编号，订单日期，订购产品的类别和订购的产品名称，订购数量和订购单价以及订购总价

select oi.orderId 订单编号,orderDate 订单时期,itemType 产品类型,itemName 产品名称,theNumber 订购数量,theMoney 订购单价 from orderItem oi 
	inner join  orders os on oi.orderId = os.orderId
		where theMoney >= 5 and theNumber >=50

--5.查询每个订单分别订购了几个产品，例如：
--			编号   订购产品数
--            1           3
--            2           4

select oi.orderId 订单编号,COUNT(ItemiD) 订购产品数 from orderItem oi 
	inner join orders os on oi.orderId = os.orderId 
		group by oi.orderId

--6.查询    每个订单    里的    每个类别    的产品分别订购了几次和总数量，例如：

-- 订单编号       产品类别     订购次数     总数量

--    1           文具            	2           82
--    1           体育用品        	1            1
--    2           文具            	2           56
--    2           体育用品        	1            2
--    2           日常用品        	1            20

select oi.orderId 订单编号,itemType 产品类别,COUNT(itemName) 订购次数,SUM(theNumber) 总数量 from orderItem oi 
	inner join orders os on oi.orderId = os.orderId
		group by oi.itemType,oi.orderId