--建数据库
create database Dong
on
(
	name='Dong',
	filename='D:\test\Dong.mdf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%

)
log on
(
	name='Dong_log',
	filename='D:\test\Dong_log.ldf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%

)
--使用创建出来的数据库
go
use Dong

go
create table orders
(
orderId int primary key identity(1,1),
orderDate datetime not null,
)
--在数据库创建表
go
create table orderItem
(
ItemiD int primary key identity(1,1),
orderId int references orders(orderId),
itemType varchar(20) not null,
itemName varchar(20) not null,
theNumber int not null,
theMoney money not null,
)
--测试查询 orders orderItem 表
go
select * from orders
select * from orderItem
--添加日期
go
insert into orders(orderDate) values('2008-01-12'),('2008-02-10'),('2008-02-15'),('2008-03-10')
--添加数据
go
insert into orderItem(orderId,itemType,itemName,theNumber,theMoney) values
(1,'文具','笔',72,2),
(1,'文具','尺',10,1),
(1,'体育用品','篮球',1,56),
(2,'文具','笔',36,2),
(2,'文具','固体胶',20,3),
(2,'日常用品','透明胶',2,1),
(2,'体育用品','羽毛球',20,3),
(3,'文具','订书机',20,3),
(3,'文具','订书针',10,3),
(3,'文具','裁纸刀',5,5),
(4,'文具','笔',20,2),
(4,'文具','信纸',50,1),
(4,'日常用品','毛巾',4,5),
(4,'日常用品','透明胶',30,1),
(4,'体育用品','羽毛球',20,3)


--使用上次作业的订单数据库，完成下列题目：

--1.查询所有的订单的订单的编号，订单日期，订购产品的类别和订购的产品名称，订购数量和订购单价
GO
select orders.orderId 订单编号,orderDate 订单日期,itemType 产品类别,itemName 产品名称,theNumber 订购数量,theMoney 单价 from orderItem inner join orders on orders.orderId = orderItem.orderId

--2.查询订购数量大于50的订单的编号，订单日期，订购产品的类别和订购的产品名称
go
select orders.orderId 订单编号,orderDate 订单日期,itemType 产品类别,itemName 产品名称 from orderItem inner join orders on orders.orderId = orderItem.orderId where theNumber>50

--3.查询所有的订单的订单的编号，订单日期，订购产品的类别和订购的产品名称，订购数量和订购单价以及订购总价
go
select orders.orderId 订单编号,orderDate 订单日期,itemType 产品类别,itemName 产品名称,theNumber 订购数量,theMoney 单价,sum(theNumber*theMoney)订购总价 from orderItem inner join orders on orderItem.orderId = orders.orderId group by theMoney,orders.orderId,orderDate,itemType,itemName,theNumber,theMoney order by orders.orderId

--4.查询单价大于等于5并且数量大于等于50的订单的订单的编号，订单日期，订购产品的类别和订购的产品名称，订购数量和订购单价以及订购总价
go
select  orders.orderId 订单编号,orderDate 订单日期,itemType 订购产品,itemName 产品名称,theNumber 订购数量,theMoney 订购单价,sum(theMoney*theNumber)订购总价 from orders inner join orderItem on orders.orderId=orderItem.orderId group by theMoney,orders.orderId,orderDate,itemType,itemName,theNumber,theMoney having theMoney>4 and theNumber>=3

--5.查询每个订单分别订购了几个产品，例如：
--			编号   订购产品数
--            1           3
--            2           4
go
select orders.orderId 编号,theNumber 订购产品数  from orderItem inner join orders on orderItem.orderId=orders.orderId

--6.查询每个订单里的每个类别的产品分别订购了几次和总数量，例如：

-- 订单编号       产品类别     订购次数     总数量

--    1           文具            	2           82
--    1           体育用品        	1            1
--    2           文具            	2           56
--    2           体育用品        	1            2
--    2           日常用品        	1            20
select orders.orderId 订单编号, itemType 产品类别,COUNT(*)订购次数,sum(theNumber)总数量 from orderItem inner join orders on orders.orderId = orderItem.orderId group by orders.orderId,itemType order by orders.orderId