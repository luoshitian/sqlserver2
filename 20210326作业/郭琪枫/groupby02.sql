--统计每个班的男生数

select ClassId,COUNT(StuSex) 男生人数 from StuInfo where StuSex='男' group by ClassId 

--统计每个班的男、女生数

select ClassId,StuSex,COUNT(StuSex) 人数 from StuInfo  group by ClassId,StuSex order by ClassId

--统计每个班的福建人数

select ClassId,StuProvince 省份,COUNT(StuId) 人数 from StuInfo where StuProvince='福建省' group by ClassId,StuProvince 

--统计每个班的各个省的总人数

select ClassId,StuProvince 省份,COUNT(StuProvince) 人数 from StuInfo  group by ClassId,StuProvince  order by ClassId


--统计每个省的女生数

select StuProvince 省份,COUNT(StuSex) 女生人数 from StuInfo where StuSex='女' group by StuProvince 

--统计每个省的男、女生数

select StuProvince 省份,StuSex 性别,COUNT(StuSex)  from StuInfo  group by StuProvince,StuSex

--统计每个学生的考试总分、平均分

select StuId,SUM(Score) 总分,AVG(Score) 平均分 from Scores group by StuId

--统计出考试总分大于620的学生的考试总分

select StuId,SUM(Score) 总分 from Scores group by StuId having SUM(Score)>620

--统计出每门考试成绩最高分和最低分

select CourseId,MAX(Score) 最高分,MIN(Score) 最低分 from Scores group by CourseId


--统计出每个学生的各门成绩的平均分

select StuId,CourseId,AVG(Score) 平均分 from Scores group by StuId,CourseId order by StuId