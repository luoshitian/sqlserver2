use master
go
create database Students
on
(
 name='Students',
 filename='D:\Stundents.mdf',
 size=6MB,
 maxsize=100MB,
 filegrowth=10Mb
)
log on
(
 name='Students_log',
 filename='D:\Stundents_log.ldf',
 size=6MB,
 maxsize=100MB,
 filegrowth=10Mb
)

use Students
go

create table StuInfo
(
	StuNo varchar(10),
	StuName nvarchar(20),
	StuAge varchar(10),
	StuAddress nvarchar(200),
	StuSeat char(10),
	StuSex	char(1) default(1) check(StuSex in(1,0))
)


create table StuExam
(
	ExamNo char(10),
	StuNo varchar(10),
	WittenExam int,
	LabExam int
)

insert into StuInfo(StuNo,StuName,StuAge,StuAddress,StuSeat,StuSex) values('s2501','张秋利',20,'美国硅谷',1,1),
('s2502','李斯文',18,'湖北武汉',2,0),
('s2503','马文才',22,'湖南长沙',3,1),
('s2504','欧阳俊雄',21,'湖北武汉',4,0),
('s2505','梅超风',20,'湖北武汉',5,1),
('s2506','陈旋风',19,'美国硅谷',6,1),
('s2507','陈风',20,'美国硅谷',7,0)

insert into StuExam(ExamNo,StuNo,WittenExam,LabExam) values(1,'s2501',50,70),
(2,'s2502',60,65),
(3,'s2503',86,85),
(4,'s2504',40,80),
(5,'s2505',70,90),
(6,'s2506',85,90)

select * from StuInfo
select * from StuExam
select 学号=StuNo,学生姓名=StuName,年龄=StuAge,家庭住址=StuAddress,座号=StuSeat,性别=StuSex from StuInfo

select StuName,StuAge,StuAddress from StuInfo

select StuNo,WittenExam,LabExam from StuExam

select 学号=StuNo,笔试成绩=WittenExam,机试成绩=LabExam from StuExam
select StuNo as 学号, WittenExam as 笔试成绩, LabExam as 机试成绩 from StuExam
select StuNo  学号, WittenExam  笔试成绩, LabExam  机试成绩 from StuExam

select StuName,StuAge,StuAddress from StuInfo
select StuName+'@'+StuAddress as 邮箱 from StuInfo

select StuNo,WittenExam,LabExam,WittenExam+LabExam as 总分 from StuExam

select StuAddress from StuInfo

select StuAge 所有年龄 from StuInfo

select * from StuInfo where StuSeat in (1,2,3)

select top 4 StuSeat,StuName from StuInfo

select top 50 percent * from StuInfo 

select * from StuInfo where StuAddress in ('湖北武汉')
select * from StuInfo where StuAge in (20)

select * from StuExam where LabExam>=60 and LabExam<=80 order by LabExam DESC

select * from StuInfo where StuAddress =('湖北武汉') or StuAddress =('湖南长沙')

select * from StuExam where WittenExam<70 or WittenExam>90 order by WittenExam DESC

select * from StuInfo where StuAge is null

select * from StuInfo where StuAge is not null

select * from StuInfo where StuName like '张%'

select * from StuInfo where StuAddress like '湖%'

select * from StuInfo where StuName like '张_'

select * from StuInfo where StuName like '__俊%'

select * from StuInfo order by StuAge DESC

select * from StuInfo order by StuAge DESC , StuSeat ASC

select top 1 * from StuExam order by WittenExam DESC

select top 1 * from StuExam order by LabExam ASC


--1.查询学生的姓名，年龄，笔试成绩和机试成绩

select StuName 姓名,StuAge 年龄,WittenExam 笔试成绩,LabExam 机试成绩
 from StuInfo SI inner join StuExam SE on SI.StuNo=SE.StuNo

--2.查询笔试和机试成绩都在60分以上的学生的学号，姓名，笔试成绩和机试成绩

select StuName 姓名,SI.StuNo 学号,WittenExam 笔试成绩,LabExam 机试成绩
 from StuInfo SI inner join StuExam SE on SI.StuNo=SE.StuNo where WittenExam>60 and LabExam>60

--3.查询所有学生的学号，姓名，笔试成绩，机试成绩，没有参加考试的学生的成绩以NULL值填充

select StuName 姓名,SI.StuNo 学号,WittenExam 笔试成绩,LabExam 机试成绩
 from StuInfo SI inner join StuExam SE on SI.StuNo=SE.StuNo

--4.查询年龄在20以上（包括20）的学生的姓名，年龄，笔试成绩和机试成绩，并按笔试成绩降序排列

select StuName 姓名,StuAge 年龄,WittenExam 笔试成绩,LabExam 机试成绩
 from StuInfo SI inner join StuExam SE on SI.StuNo=SE.StuNo where StuAge>=20 order by WittenExam DESC

--5.查询男女生的机试平均分

select StuSex 性别,AVG(LabExam) 平均成绩 from StuInfo SI inner join StuExam SE on SI.StuNo=SE.StuNo group by StuSex

--6.查询男女生的笔试总分

select StuSex 性别,SUM(WittenExam) 笔试总分 from StuInfo SI inner join StuExam SE on SI.StuNo=SE.StuNo group by StuSex
