use master
go
create database OrderInfo
on
(
 name='OrderInfo',
 filename='D:\OrderInfo.mdf',
 size=6MB,
 maxsize=100MB,
 filegrowth=10Mb
)
log on
(
 name='OrderInfo_log',
 filename='D:\OrderInfo_log.ldf',
 size=6MB,
 maxsize=100MB,
 filegrowth=10Mb
)

use OrderInfo
go

create table orders
(
	orderId int primary key identity(1,1),
	orderDate datetime
)

create table orderItem
(
	ItemiD int primary key identity(1,1),
	orderId int references orders(orderId),
	itemType nvarchar(20),
	itemName nvarchar(20),
	theNumber int,
	theMoney Money
)

select * from orderItem


insert into orders values('2008-01-12'),('2008-02-10'),('2008-02-15'),('2008-03-10')

insert into orderItem values(1,'文具','笔',72,2),
(1,'文具','尺',10,1),
(1,'体育用品','篮球',1,56),
(2,'文具','笔',36,2),
(2,'文具','固体胶',20,3),
(2,'日常用品','透明胶',2,1),
(2,'体育用品','羽毛球',20,3),
(3,'文具','订书机',20,3),
(3,'文具','订书针',10,3),
(3,'文具','裁纸刀',5,5),
(4,'文具','笔',20,2),
(4,'文具','信纸',50,1),
(4,'日常用品','毛巾',4,5),
(4,'日常用品','透明胶',30,1),
(4,'体育用品','羽毛球',20,3)

select SUM(theNumber) from dbo.orderItem 

select orderId,SUM(theNumber) 总数量,AVG(theMoney) 平均单价  from dbo.orderItem where orderId<3 group by orderId HAVING AVG(theMoney)<10

select orderId,SUM(theNumber) 总数量,AVG(theMoney) 平均单价  from dbo.orderItem group by orderId HAVING AVG(theMoney)<10 and SUM(theNumber)>50

select itemType,COUNT(itemType) 订购次数 from dbo.orderItem group by itemType 

select itemType,SUM(theNumber) 总数量,AVG(theMoney) 平均单价  from dbo.orderItem group by itemType HAVING SUM(theNumber)>100

select itemName,COUNT(itemName)订购次数, SUM(theNumber) 总数量,  AVG(theMoney) 平均单价 from dbo.orderItem group by itemName

--使用上次作业的订单数据库，完成下列题目：

--1.查询所有的订单的订单的编号，订单日期，订购产品的类别和订购的产品名称，订购数量和订购单价

select OS.orderId 订单的编号,orderDate 订单日期,itemType 产品的类别,itemName 订购的产品名称 from orders OS inner join orderItem OM on OS.orderId=OM.orderId

--2.查询订购数量大于50的订单的编号，订单日期，订购产品的类别和订购的产品名称

select OS.orderId 订单的编号,orderDate 订单日期,itemType 产品的类别,itemName 订购的产品名称 from orders OS inner join orderItem OM on OS.orderId=OM.orderId where theNumber>50

--3.查询所有的订单的订单的编号，订单日期，订购产品的类别和订购的产品名称，订购数量和订购单价以及订购总价

select OS.orderId 订单的编号,orderDate 订单日期,itemType 产品的类别,itemName 订购的产品名称,theNumber 订购数量,theMoney 订购单价,SUM(theNumber) 订购总价 
from orders OS inner join orderItem OM on OS.orderId=OM.orderId group by OS.orderId ,orderDate ,itemType ,itemName ,theNumber ,theMoney 

--4.查询单价大于等于5并且数量大于等于50的订单的订单的编号，订单日期，订购产品的类别和订购的产品名称，订购数量和订购单价以及订购总价

select OS.orderId 订单的编号,orderDate 订单日期,itemType 产品的类别,itemName 订购的产品名称,theNumber 订购数量,theMoney 订购单价,SUM(theNumber) 订购总价 
from orders OS inner join orderItem OM on OS.orderId=OM.orderId where theMoney>=5 and theNumber>=50 group by OS.orderId ,orderDate ,itemType ,itemName ,theNumber ,theMoney 

--5.查询每个订单分别订购了几个产品，例如：
--			编号   订购产品数
--                                            	  1           3
--                                            	  2           4

select OS.orderId 订单的编号,theNumber 订购数量 from orders OS inner join orderItem OM on OS.orderId=OM.orderId  group by OS.orderId ,theNumber

--6.查询每个订单里的每个类别的产品分别订购了几次和总数量，例如：

-- 订单编号       产品类别     订购次数     总数量

--    1           文具            	2           82
--    1           体育用品        	1            1
--    2           文具            	2           56
--    2           体育用品        	1            2
--    2           日常用品        	1            20

select OS.orderId 订单的编号,itemType 产品的类别,COUNT(*) 订购次数,SUM(theNumber) 总数量 from orders OS inner join orderItem OM on OS.orderId=OM.orderId 
group by OS.orderId,itemType order by  OS.orderId
