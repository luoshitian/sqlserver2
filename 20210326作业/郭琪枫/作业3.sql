use master
go
create database BBS
on
(
 name='BBS',
 filename='D:\BBS.mdf',
 size=6MB,
 maxsize=100MB,
 filegrowth=10Mb
)
log on
(
 name='BBS_log',
 filename='D:\BBS_log.ldf',
 size=6MB,
 maxsize=100MB,
 filegrowth=10Mb
)
go
use BBS
go

create table bbsUsers
(
	bbsUID int identity(1,1),
	uName varchar(10) not null,
	uSex  varchar(2) not null,
	uAge  int not null,
	uPoint  int not null
)

create table bbsSection
(
	sID int identity(1,1),
	sName  varchar(10) not null,
	sUid int
)

create table bbsTopic
(
	tID  int primary key identity(1,1),
	 tUID  int references bbsUsers(bbsUID),
	 tSID int references bbsSection(sID),
	  tTitle  varchar(100) not null,
	  tMsg  text not null,
	  tTime  datetime,
	  tCount  int
)

create table bbsReply
(
	rID int primary key identity(1,1),
	 rUID int references bbsUsers(bbsUID),
	 rTID int references bbsTopic(tID),
	 rMsg  text not null,
	 rTime  datetime 
)

alter table bbsUsers add constraint PK_bbsUsers_bbsUID primary key(bbsUID)
alter table bbsUsers add constraint UQ_bbsUsers_uName unique(uName)
alter table bbsUsers add constraint CK_bbsUsers_uSex check(uSex in('男','女'))
alter table bbsUsers add constraint CK_bbsUsers_uAge check(uAge>=15 and uAge<=60)
alter table bbsUsers add constraint CK_bbsUsers_uPoint check(uPoint>= 0)
alter table bbsSection add constraint PK_bbsSection_sID primary key(sID)
alter table bbsSection add constraint FK_bbsUsers_bbsUID foreign key(sUid) references bbsUsers(bbsUID) 

select * from bbsReply

insert into bbsUsers(uName,uSex,uAge,uPoint) values('小雨点','女',20,0),
('逍遥','男',18,4),
('七年级生','男',19,2)

select * from bbsUsers

select uName,uPoint into bbsPoint from bbsUsers

select * from bbsPoint

insert into bbsSection(sName,sUid) values('技术交流',1),('读书世界',3),('生活百科',1),('八卦区',3)

select * from bbsSection

insert into bbsTopic(tUID,tSID,tTitle,tMsg,tTime,tCount) values(2,4,'范跑跑','谁是范跑跑','2008-07-08',1),
(3,1,'.NET','与JAVA的区别是什么呀?','2008-09-01',2),
(1,3,'今年夏天最流行什么','有谁知道今年夏天最流行什么呀?','2008-09-10',0)

insert into bbsReply(rUID,rTID,rMsg,rTime) values(1,1,'不知道','2008-07-09'),
(1,2,'不知道','2008-09-09'),
(2,2,'不知道','2008-10-09')

select * from bbsReply
select * from bbsTopic
select * from bbsSection

--	5.因为会员“逍遥”发表了非法帖子，现将其从论坛中除掉，即删除该用户，请用语句实现（注意主外键，要删除主键，先要将引用了该主键的外键数据行删除）

delete from bbsReply where rUID=2
delete from bbsReply where rTID=1
delete from bbsTopic where tUID=2
delete from bbsUsers where bbsUID=2

update bbsUsers set uPoint=10 where bbsUID=1

select uPoint from bbsUsers
--	7.因为板块“生活百科”灌水的人太少，现决定取消该板块，即删除（注意主外键）
delete from bbsTopic where tSID=3
delete from bbsSection where sID=3

truncate table bbsReply


select * from bbsReply
alter table bbsTopic drop column tCount

alter table bbsUsers add telephone char(10) 

alter table bbsReply alter column rMsg varchar(200)

alter table bbsUsers drop constraint CK_bbsUsers_uPoint

update bbsUsers set uName='小雪' where bbsUID=1

update bbsUsers set uPoint=uPoint+100



select * from bbsUsers

select * into bbsUsers2 from bbsUsers

drop table bbsUsers2
truncate table bbsUsers2

--在论坛数据库中完成以下题目
--1.查询出每个版块的版主编号，版主姓名和版块名称

select bbsUID 版主编号,uName 版主姓名,sName  版块名称 from bbsUsers BU inner join bbsSection BS on bbsUID=sUid 

--2.查询出主贴的发帖时间在2008-9-15以后的主贴的发帖人编号，发帖人姓名，帖子的标题，帖子的内容和发帖时间

select bbsUID 发帖人编号,uName 发帖人姓名,tTitle 帖子的标题,tMsg 帖子的内容,tTime 发帖时间 from bbsUsers BU inner join bbsTopic BT on bbsUID=tUID 


--3.查询出年龄在20以下的版主的编号，版主的名称和版块的名称

select sID 版主的编号,uName 版主的名称,sName 版块的名称 from bbsUsers BU inner join bbsSection BS on bbsUID=sUid  where uAge<20

--4.查询出回复数量最多的主贴的发帖人编号，发帖人姓名，主贴标题，主贴内容和回复数量

select bbsUID 主贴的发帖人编号,uName 发帖人姓名,tTitle 主贴标题,tMsg 主贴内容,tCount 回复数量 from   bbsUsers BU inner join bbsTopic BT on bbsUID=tUID 
where tCount=(select MAX(tCount) from bbsTopic)


--5.在主贴表中查询每个版块中每个用户的发帖总数

select tID,tUID,COUNT(*) 发帖总数 from bbsTopic group by tID,tUID



 