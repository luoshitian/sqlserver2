use master
go
if exists(select*from sys.databases where name='Dwu')
drop database Dwu
create database Dwu
on
(
name='Dwu',
filename='D:\SQLcunchu\Dwu.mdf',
size=5mb,
maxsize=100mb,
filegrowth=10mb
)
log on
(
name='Dwu_log',
filename='D:\SQLcunchu\Dwu_log.ldf',
size=5mb,
maxsize=100mb,
filegrowth=10mb
)
go
use Dwu
go
create table stuinfo
(
stuNo char(5) primary key not null,
stuName nvarchar(20) not null,
stuAge char(3) not null,
stuAddress nvarchar(200), 
stuSeat int identity(1,1)not null,
stuSex char(1) check(stuSex in (1,0)) default(1) not null
)

insert into stuinfo(stuNo,stuName,stuAge,stuAddress,stuSex) values
('s2501','张秋利',20,'美国硅谷',1),
('s2502','李斯文',18,'湖北武汉',0),                 
('s2503','马文才',22,'湖南长沙',1),
('s2504','欧阳俊雄',21,'湖北武汉',0),
('s2505','梅超风',20,'湖北武汉',1),
('s2506','陈旋风',19,'美国硅谷',1),
('s2507','陈凤',20,'美国硅谷',0) 
create table stuexam
(
examNo int primary key identity(1,1),
stuNo char(5) references stuinfo(stuNo) not null,
writtenExam int not null,
labExam int not null
) 
insert stuexam(stuNo,writtenExam,labExam)values
('s2501',50,70),
('s2502',60,65),
('s2503',86,85),
('s2504',40,80),
('s2505',70,90),
('s2506',85,90)

select*from stuexam
select*from stuinfo
select stuName 学生姓名,stuAge 学生年龄,writtenExam 笔试成绩,labExam 机试成绩 from stuinfo inner join stuexam on stuinfo.stuNo = stuexam.stuNo--1.查询学生的姓名，年龄，笔试成绩和机试成绩

select stuinfo.stuNo 学生学号,stuName 学生姓名,writtenExam 笔试成绩,labExam 机试成绩 from stuinfo inner join stuexam on stuinfo.stuNo=stuexam.stuNo where writtenExam>=60 and labExam>=60--2.查询笔试和机试成绩都在60分以上的学生的学号，姓名，笔试成绩和机试成绩

select stuinfo.stuNo  学生学号,stuName 学生名字,writtenExam 笔试成绩,labExam 机试成绩 from stuexam left join stuinfo on stuinfo.stuNo=stuexam.stuNo--3.查询所有学生的学号，姓名，笔试成绩，机试成绩，没有参加考试的学生的成绩以NULL值填充

select stuName 学生姓名,stuAge 学生性别,writtenExam 笔试成绩,labExam 机试成绩 from stuinfo inner join stuexam on stuexam.stuNo=stuinfo.stuNo where stuAge>=20 order by writtenExam desc --4.查询年龄在20以上（包括20）的学生的姓名，年龄，笔试成绩和机试成绩，并按笔试成绩降序排列--

select stuSex 学生性别,avg(labExam) 平均机试成绩 from stuinfo right join stuExam on stuexam.stuNo=stuinfo.stuNo group by stuSex --5.查询男女生的机试平均分

select stuSex 学生性别,sum(writtenExam) 笔试成绩总分 from stuinfo left join stuexam on stuinfo.stuNo=stuexam.stuNo group by stuSex--6.查询男女生的笔试总分