use master
go
if exists(select*from sys.databases where name='bbs')
drop database bbs
create database bbs
on
(
name='bbs',
filename='D:\bbs.dmf',
size=5mb,
maxsize=100mb,
filegrowth=10%
)
log on
(
name='bbs_log',
filename='D:\bbs_log.lmf',
size=5mb,
maxsize=100mb,
filegrowth=10%
)
go
use bbs
go
--用户信息表（bbsUsers）
--	用户编号  UID int 主键  标识列
--	用户名    uName varchar(10)  唯一约束 不能为空
--	性别      uSex  varchar(2)  不能为空 只能是男或女
--	年龄      uAge  int  不能为空 范围15-60
--	积分      uPoint  int 不能为空  范围 >= 0
create table bbsUsers
(
UID int primary key identity(1,1),
uName varchar(10) unique not null,
uSex varchar(2) not null check(uSex in ('男' , '女')),
uAge int not null check(uAge >=15 and uAge <=60),
uPoint int not null check(uPoint>=0)
)
--主贴表（bbsTopic）
--	主贴编号  tID  int 主键  标识列，
--	发帖人编号  tUID  int 外键  引用用户信息表的用户编号
--	版块编号    tSID  int 外键  引用版块表的版块编号    （标明该贴子属于哪个版块）
--	贴子的标题  tTitle  varchar(100) 不能为空
--	帖子的内容  tMsg  text  不能为空
--	发帖时间    tTime  datetime  
--	回复数量    tCount  int
create table bbsSection
(
sID int primary key identity(1,1),
sName varchar(10) not null,
sUid int references bbsUsers(UID)
)
create table bbsTopic
(
tID int primary key identity(1,1),
tUID int references bbsUsers(UID),
tSID int references bbsSection(sID),
tTitle varchar(100) not null,
tMSg nvarchar(200) not null,
tTime datetime ,
tCount int
)
--回帖表（bbsReply）
--	回贴编号  rID  int 主键  标识列，
--	回帖人编号  rUID  int 外键  引用用户信息表的用户编号
--	对应主贴编号    rTID  int 外键  引用主贴表的主贴编号    （标明该贴子属于哪个主贴）
--	回帖的内容  rMsg  text  不能为空
--	回帖时间    rTime  datetime 
create table bbsReply
(
rID int primary key identity(1,1),
rUID int references bbsUsers(UID),
rTID int references bbsTopic(tID),
rMSg nvarchar(200) not null,
rTime datetime 
)
--板块表bbsSection）
--	版块编号  sID  int 标识列 主键
--	版块名称  sName  varchar(10)  不能为空
--	版主编号  sUid   int 外键  引用用户信息表的用户编号

--1.现在有3个会员注册成功，请用一次插入多行数据的方法向bbsUsers表种插入3行记录，记录值如下：
--	  小雨点  女  20  0
--	  逍遥    男  18  4
--	  七年级生  男  19  2
insert into bbsUsers
select '小雨点','女',20,0 union
select '逍遥','男',18,4union
select '七年级生','男',19,2
--	2.将bbsUsers表中的用户名和积分两列备份到新表bbsPoint表中，提示查询部分列:select 列名1，列名2 from 表名
select uName,uPoint into bbsPoint from bbsUsers
--	3.给论坛开设4个板块
--	  名称        版主名
--	  技术交流    小雨点
--	  读书世界    七年级生
--	  生活百科     小雨点
--	  八卦区       七年级生
insert into bbsSection 
select '技术交流','1' union
select '读书世界','3' union
select '生活百科','1' union
select '八卦区','3'
--	4.向主贴和回帖表中添加几条记录
	  
--	   主贴：

--	  发帖人    板块名    帖子标题                帖子内容                发帖时间   回复数量
--	  逍遥      八卦区     范跑跑                 谁是范跑跑              2008-7-8   1
--	  七年级生  技术交流   .NET                   与JAVA的区别是什么呀？  2008-9-1   2
--	  小雨点   生活百科    今年夏天最流行什么     有谁知道今年夏天最流行  2008-9-10  0
--						      什么呀？
insert into bbsTopic 
select '2','4','范跑跑','谁是范跑跑','2008-7-8',1 union	 
select '3','1','.NET','与JAVA的区别是什么呀？','2008-9-1',2 union
select '1','3','今年夏天最流行什么呀？','有谁知道今年夏天最流行什么呀？','2008-9-10',0 union	 

select '2','4','范跑跑','谁是范跑跑','2008-7-8',10
--	   回帖：
--	   分别给上面三个主贴添加对应的回帖，回帖的内容，时间，回帖人自定
insert into bbsReply
select '2','1','范跑跑是一个胆小鬼！','2009-1-4' union
select '3','2','JAVA头秃首选','2014-12-23' union
select '1','3','流行原宿风！','2020-1-3'

--	5.因为会员“逍遥”发表了非法帖子，现将其从论坛中除掉，即删除该用户，请用语句实现（注意主外键，要删除主键，先要将引用了该主键的外键数据行删除）

delete from bbsTopic where tUID=1
delete from bbsReply where rUID=1
delete from bbsSection where sName='逍遥'
--	6.因为小雨点发帖较多，将其积分增加10分
update bbsUsers set uPoint=uPoint+10 where UID=1
--	7.因为板块“生活百科”灌水的人太少，现决定取消该板块，即删除（注意主外键）
delete from bbsSection where sName='生活板块'
--	8.因回帖积累太多，现需要将所有的回帖删除
delete from bbsReply
select*from bbsTopic 
select*from bbsSection
select*from bbsReply 
select*from bbsUsers

select bbsSection.sUid 版主编号,uName 版主姓名,sName 板块名称 from bbsSection inner join bbsUsers on bbsSection.sUid=bbsUsers.UID--1.查询出每个版块的版主编号，版主姓名和版块名称

select tUID 发帖人编号,uName 发帖人姓名,tTitle 帖子的标题,tMSg 帖子的内容,tTime 发帖时间 from bbsTopic inner join bbsUsers on bbsTopic.tUID=bbsUsers.UID where tTime>'2008-9-15'--2.查询出主贴的发帖时间在2008-9-15以后的主贴的发帖人编号，发帖人姓名，帖子的标题，帖子的内容和发帖时间

select sUid 版主编号,uName 版主名称,sName 板块名称 from bbsSection inner join bbsUsers on bbsSection.sUid=bbsUsers.UID where uAge<20--3.查询出年龄在20以下的版主的编号，版主的名称和版块的名称

select top 1 tUid 发帖人编号,uName 发帖人姓名,tTitle 主贴标题,tMSg 主贴内容,max(tCount)回复数量 from bbsTopic right join bbsUsers on bbsTopic.tUID=bbsUsers.UID group by uName,tTitle,tMSg,tCount,tUID order by tUid DESC--4.查询出回复数量最多的主贴的发帖人编号，发帖人姓名，主贴标题，主贴内容和回复数量

select tUID 用户编号,uName 用户姓名,count(*)发帖总数 from bbsTopic inner join bbsUsers on bbsTopic.tUID=bbsUsers.UID group by tUID,uName--5.在主贴表中查询每个版块中每个用户的发帖总数