use master
go
if exists(select*from sys.databases where name='TestDB' )
drop database TestDB
create database TestDB
on
(
	name='TestDB',
	filename='D:\SQLcunchu\TestDB.mdf',
	size=5MB,
	maxsize=50MB,
	filegrowth=10%
)
log on
(   name='TestDB_log',
	filename='D:\SQLcunchu\TestDB_log.ldf',
	size=5MB,
	maxsize=50MB,
	filegrowth=10%
)
go
use TestDB
go
create table orders
(
orderId int primary key identity(1,1),
orderDate datetime 
)
create table orderItem
(
ItemiD int identity(1,1),
orderId int ,
itemType nvarchar(10),
itemName nvarchar(20),
theNumber int,
theMoney int
)
insert into orderItem values
(1,'文具','笔',72,2),(1,'文具','尺',10,1),
(1,'体育用品','篮球',1,56),(2,'文具','笔',36,2),
(2,'文具','固体胶',20,3),(2,'日常用品','笔透明胶',2,1),
(2,'体育用品','羽毛球',20,3),(3,'文具','订书机',20,3),
(3,'文具','订书针',10,3),(3,'文具','裁纸刀',5,5),
(4,'文具','笔',20,2),(4,'文具','信纸',50,1),
(4,'日常用品','毛巾',4,5),(4,'日常用品','透明胶',30,1),
(4,'体育用品','羽毛球',20,3)
insert into orders values
('2008-01-12'),
('2008-02-10'),
('2008-02-15'),
('2008-03-10')

select*from orders
select*from orderItem

select orders.orderId 订单编号,orderDate 订单日期,itemType  产品类别,itemName 产品名称,theNumber 订购数量,theMoney 订购单价 from orders inner join orderItem on orders.orderId=orderItem .orderId--1.查询所有的订单的订单的编号，订单日期，订购产品的类别和订购的产品名称，订购数量和订购单价

select orders.orderId 订单编号,orderDate 订单日期,itemType 订购类别,itemName 产品名称 from orders inner join orderItem on orders.orderId=orderItem .orderId where theNumber>50--2.查询订购数量大于50的订单的编号，订单日期，订购产品的类别和订购的产品名称

select orders.orderId 订单编号,orderDate 订单日期,itemType 订购类别,itemName 产品名称,theNumber 订购数量,theMoney 订购单价,sum(theMoney*theNumber)订购总价 from orders inner join orderItem on orders.orderId=orderItem .orderId group by orders .orderId,orderDate,itemType,itemName,theNumber,theMoney--3.查询所有的订单的订单的编号，订单日期，订购产品的类别和订购的产品名称，订购数量和订购单价以及订购总价

select orders.orderId 订单编号,orderDate 订单日期,itemType 订购类别,itemName 产品名称,theNumber 订购数量,theMoney 订购单价,sum(theMoney)订购总价 from orders inner join orderItem on orders.orderId=orderItem .orderId group by orders .orderId,orderDate,itemType,itemName,theNumber,theMoney having theMoney>=5 and sum(theNumber)>=50--4.查询单价大于等于5并且数量大于等于50的订单的订单的编号，订单日期，订购产品的类别和订购的产品名称，订购数量和订购单价以及订购总价

select orderId 编号,theNumber 订购产品数 from orderItem--5.查询每个订单分别订购了几个产品，例如：
--			编号   订购产品数
--           1           3
--           2           4

select orderId 订单编号,itemType 产品类别,theNumber 订购次数,sum(theNumber) 总数量 from orderItem group by orderId,itemType,theNumber--6.查询每个订单里的每个类别的产品分别订购了几次和总数量，例如：

--订单编号       产品类别     订购次数     总数量

--    1           文具            	2           82
--    1           体育用品        	1            1
--    2           文具            	2           56
--    2           体育用品        	1            2
--    2           日常用品        	1            20