use TestDB
go

select * from ClassInfo
select * from CourseInfo
select * from Scores
select * from StuInfo

--统计  每个班  男生数
select count(*)班级男生数 from StuInfo where(StuSex='男') group by ClassId
--统计 每个班  的  男、女生数
select classid,StuSex,count(*) from stuinfo group by classid,stusex order by ClassId
--统计每个班的福建人数
select count(*)班级男生数 from StuInfo where(StuProvince='福建省') group by ClassId
--统计每个班的各个省的总人数
select StuProvince,ClassId,count(*)人数 from StuInfo group by StuProvince,ClassId
--统计每个省的女生数
select StuProvince,StuSex,count(*)女生数 from StuInfo  where(StuSex='女')group by StuProvince,Stusex
--统计每个省的男、女生数
select StuProvince,StuSex,count(*)人数 from StuInfo group by StuProvince,Stusex
--统计每个学生的考试总分、平均分
select Stuid,avg(Score)平均分,sum(Score)总分 from Scores group by StuId
--统计出考试总分大于620 的 学生 的 考试总分
 select Stuid,sum(Score) from Scores group by Stuid having (sum(Score)>620)
--统计出每门考试成绩最高分和最低分
select courseID,max(Score)最高分,min(Score)最低分 from Scores group by courseID
--统计出  每个学生    各门成绩   平均分
select CourseId,Stuid,avg(score)平均分 from Scores group by CourseId,Stuid
select * from Scores