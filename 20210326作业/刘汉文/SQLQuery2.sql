use master
create database bbs
on
(
	name='bbs',
	filename='D:\test\bbs.mdf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%
)
log on
(
	name='bbs_log',
	filename='D:\test\bbs_log.ldf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%
)
go
use bbs
go
create table bbsUser
(
	uID int identity(1,1) not null,
	uName varchar(10)  not null,
	uSex  varchar(2) not null,
	uAge  int not null,
	uPoint  int not null
)



--添加约束
-- alter table 表名 add constraint 约束名  约束类型 
go
alter table bbsUser add constraint PK_bbsUser_uID primary key(uID)
alter table bbsUser add constraint UK_bbsUser_uName unique(uName)
alter table bbsUser add constraint CK_bbsUser_uSex check(uSex='男' or uSex='女')
alter table bbsUser add constraint CK_bbsUser_uAge check(uAge>=15 and uAge<=60)
alter table bbsUser add constraint CK_bbsUser_uPoint check(uPoint>=0)

go
alter table bbsSection add constraint PK_bbsSection_sID primary key(sID)
alter table bbsSection add constraint FK_bbsSection_sUid foreign key(sUid) references bbsUser(uID)

go
create table bbsTopic
(
	tID int primary key identity(1,1),
	tUID int references bbsUser(uID),
	tSID int references bbsSection(sID),
	tTitle varchar(100) not null,
	tMsg text not null,
	tTime datetime,
	tCount int
)
go
create table bbsReply
(
	 rID  int identity(1,1) primary key,
	 rUID  int references  bbsUser(uID),
	 rTID  int references bbsTopic(tID),
	 rMsg text not null,
	 rTime datetime
)

go
insert into bbsUser (uName,uSex,uAge,uPoint) values ('小雨点','女','20','0')
,('逍遥','男','18','4'),('七年级生','男','19','2')

go
select uName,uPoint into bbsPoint from bbsUser

go
insert into bbsSection (sName,sUid) values ('技术交流',1),
('读书世界',3),('生活百科',1),('八卦区',3)

go
insert into bbsTopic (tUID,tSID,tTitle,tMsg,tTime,tCount) values ('2','4','范跑跑','谁是范跑跑','2008-7-8','1'),
('3','1','.NET','谁是范跑跑','2008-9-1','2'),('1','3','.今年夏天最流行什么呀?','有谁知道今年夏天最流行','2008-9-10','0')

go
insert into bbsReply (rUID,rMsg,rTime) values ('1','范跑跑是谁啊','2021-1-1'),
('2','名字不一样','2021-1-2'),('3','EMO','2021-1-3')

go
select * from bbsUser
select * from bbsSection
select * from bbsTopic
select * from bbsReply

--在论坛数据库中完成以下题目
--1.查询出每个版块的版主编号，版主姓名和版块名称
go
select sUid 版主编号,uName 版主姓名,sName 版块名称 from bbsUser inner join bbsSection on bbsUser.uID= bbsSection.sUid

--2.查询出主贴的发帖时间在2008-9-15以后的主贴的发帖人编号，发帖人姓名，帖子的标题，帖子的内容和发帖时间
go
select uID 发帖人编号,uName 发贴人编号,tTitle 帖子标题,tMsg 帖子内容,tTime 发帖时间 from bbsTopic inner join bbsUser on bbsTopic.tSID=bbsUser.uID where tTime>'2008-9-1' 

--3.查询出年龄在20以下的版主的编号，版主的名称和版块的名称
go
select sID 版主编号,uName 版主名称,sName 版块吧名称,uAge 年龄 from bbsUser inner join bbsSection on bbsUser.uID=bbsSection.sUid where uAge<20

--4.查询出回复数量最多的主贴的发帖人编号，发帖人姓名，主贴标题，主贴内容和回复数量
go
select uID发帖人编号,uName 发帖人名字,tTitle 主贴标题,tMsg 主题内容,tCount 回复数量 from bbsTopic  inner join bbsUser on bbsTopic.tSID=bbsUser.uID where tCount= (select max(tCount) from bbsTopic)

--5.在主贴表中查询每个版块中每个用户的发帖总数
go
select tsid 版块编号,count(tSID) 发帖总数 from  bbsTopic group by tUID,tSID