
create database a
on
(
	name='a',
	filename='D:\bank\ATM1.mdf',
	size=5,
	maxsize=10,
	filegrowth=15%
)
log on
(
	name='a_log',
	filename='D:\bank\ATM1_log.ldf',
	size=5,
	maxsize=10,
	filegrowth=15%
)
go
use a
go

create table orders
(
--订单编号（orderId 主键）  订购日期（orderDate）
	orderId int primary key,
	orderDate datetime default(getdate())
)
--订购项目表（orderItem），列为：
--项目编号（ItemiD）订单编号（orderId）产品类别（itemType）
--产品名称（itemName） 订购数量（theNumber）  订购单价（theMoney）
create table orderItem
(
--订单编号（orderId 主键）  订购日期（orderDate）
	ItemiD int identity(1,1),
	orderId int,
	itemType varchar(20),
	itemName varchar(20),
	theNumber int, 
	theMoney int
)

select * from orders
select * from orderItem
insert into orderItem(orderId,itemType,itemName,theNumber,theMoney)
select 1,'文具','笔',72,2 union
select 1,'文具','尺',10,1 union
select 1,'体育用品','篮球',1,56 union
select 2,'文具','笔',36,2 union
select 2,'文具','固体胶',20,3 union
select 2,'日常用品','透明胶',2,1 union
select 2,'体育用品','羽毛球',20,3 union
select 3,'文具','订书机',20,3 union
select 3,'文具','订书针',10,3 union
select 3,'文具','栽纸刀',5,5 union
select 4,'文具','笔',20,2 union
select 4,'文具','信纸',50,1 union
select 4,'日常用品','毛巾',4,5 union
select 4,'日常用品','透明胶',30,1 union
select 4,'日常用品','透明胶',52,6 union
select 4,'体育用品','羽毛球',20,3 

insert into orders(orderId,orderDate)
select 1,'2008-01-12' union
select 2,'2008-02-10' union
select 3,'2008-02-15' union
select 4,'2008-03-10' 
use a
--1.查询所有的订单的订单的编号，订单日期，订购产品的类别和订购的产品名称，订购数量和订购单价
select ItemiD,orderDate,itemType,itemName,theNumber,theMoney from orders inner join orderItem on orders.orderId =orderItem.orderId
--2.查询订购数量大于50的订单的编号，订单日期，订购产品的类别和订购的产品名称
select ItemiD,orderDate,itemType,itemName,theNumber from orders inner join orderItem on orders.orderId =orderItem.orderId where theNumber>50
--3.查询所有的订单的订单的编号，订单日期，订购产品的类别和订购的产品名称，订购数量和订购单价以及订购总价
select ItemiD,orderDate,itemType,itemName,theNumber,theMoney,theNumber*theMoney 订购总价 from orders inner join orderItem on orders.orderId = orderItem.orderId 
--4.查询单价大于等于5并且数量大于等于50的订单的订单的编号，订单日期，订购产品的类别和订购的产品名称，订购数量和订购单价以及订购总价
select orderDate,itemType,itemName,theNumber,theMoney,theNumber*theMoney 订购总价 from orders inner join orderItem on orders.orderId = orderItem.orderId where theMoney>5 and theNumber>50
--5.查询每个订单分别订购了几个产品，例如：
select orderId 订单编号 ,count(theNumber)订购产品数 from orderItem group by orderId
	--		编号   订购产品数
                                            	  1           3
                                            	  2           4

--6.查询每个订单里的每个类别的产品分别订购了几次和总数量，例如：
select orderId 订单编号,itemName 产品名称,count(theNumber) 订购次数,sum(theNumber)总数量 from orderItem group by orderId,itemName
 订单编号       产品类别     订购次数     总数量

    1           文具            	2           82
    1           体育用品        	1            1
    2           文具            	2           56
    2           体育用品        	1            2
    2           日常用品        	1            20