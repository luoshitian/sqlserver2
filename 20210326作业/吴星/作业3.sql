
use bbs
go
create table bbsUsers--用户信息表
(
	UID01 int primary key identity(1,1),
	uName varchar(10) unique not null,
	uSex  varchar(2) check(uSex in('女','男')) not null,
	uAge  int check(uAge>=15 and uAge<=60) not null,
	uPoint int check(uPoint>=0) not null
)
insert into bbsUsers values('小雨点','女',20,0),('逍遥','男',18,4),('七年级生','男',19,2)
alter table bbsUsers add telephone varchar(20) check(len(telephone)=11)
update bbsUsers set telephone='12345678911' where uName='小雨点'
update bbsUsers set telephone='12345678912' where uName='七年级生'
alter table bbsUsers add constraint UK_bbsUsers_telephone unique(telephone)
alter table bbsUsers add constraint CK_bbsUsers_telephone check(len(telephone)=11)
alter table bbsUsers drop constraint CK__bbsUsers__uPoint__1367E606
update bbsUsers set uName='小雪' where uName='小雨点'


truncate table bbsUsers2
delete from bbsUsers2

select * from bbsUsers




create table bbsSection--版块表


(
	sID01 int primary key identity(1,1),
	sName varchar(10) not null,
	sUid int references bbsUsers(UID01)
)
alter table bbsSection add bbzName varchar(10)
insert into bbsSection(sName,bbzName) values('技术交流','小雨点'),('读书世界','七年级生'),('生活百科','小雨点'),('八卦区','七年级生')
select * from bbsSection


select sID01 版主的编号,bbzName 版主的名称,sName 版块的名称 from bbsSection inner join bbsUsers on bbsSection.sID01 = bbsUsers.UID01 where uAge<20

create table bbsTopic--主贴表
(
	tID int primary key identity(1,1),
	tUID int references bbsUsers(UID01),
	tSID int references bbsSection(sID01),
	tTitle varchar(100) not null,
	tMsg text not null,
	tTime date,
	tCount int
)
alter table bbsTopic add fatieName varchar(10)
alter table bbsTopic add bbzName varchar(10)

drop table bbsTopic

insert into bbsTopic values(1,null,'范跑跑!','谁是范跑跑','2008-7-8',3,'逍遥','八卦区'),(1,null,'范跑跑!','谁是范跑跑','2008-10-8',1,'逍遥','八卦区'), (2,null,'.NET','与JAVA的区别是什么呀？','2008-9-1',2,'七年级生','技术交流'),(3,null,'今年夏天最流行什么',' 有谁知道今年夏天最流行什么呀?','2008-9-10',1,'小雨点','生活百科')
select * from bbsTopic


select tUID 发帖人编号,tTitle 帖子的标题,uName 发帖人姓名,tMsg 帖子的内容,tTime 发帖时间 from bbsTopic inner join bbsPoint on bbsTopic.tUID= bbsPoint.UID01 where tTime>'2008-9-15'



create table bbsReply--回帖表
(
	rID int primary key identity(1,1),
	rUID int references bbsUsers(UID01),
	rTID int references bbsTopic(tID),
	rMsg text not null,
	rTime  datetime 
)
alter table bbsReply add huitieName varchar(10) not null
insert into bbsReply values(1,null,'范跑跑',2008-7-8 ,'1'),(2,null,'JAVA',2008-9-1 ,'2'),(2,null,'夏天',2008-9-10 ,'3')
alter table bbsReply alter column rMsg varchar(200) 

select * from bbsReply

create table bbsPoint
(
	UID01 int primary key identity(1,1),
	uName varchar(10) unique not null,
	uPoint  int check(uPoint>=0) not null
)
insert into bbsPoint select uName,uPoint from bbsUsers
select * from bbsPoint

--1.查询出每个版块的版主编号，版主姓名和版块名称
select sID01,bbzName,sName from bbsSection group by sID01,bbzName,sName
--2.查询出主贴的发帖时间在2008-9-15以后的主贴的发帖人编号，发帖人姓名，帖子的标题，帖子的内容和发帖时间
select tUID 发帖人编号,tTitle 帖子的标题,uName 发帖人姓名,tMsg 帖子的内容,tTime 发帖时间 from bbsTopic inner join bbsPoint on bbsTopic.tUID= bbsPoint.UID01 where tTime>'2008-9-15'
--3.查询出年龄在20以下的版主的编号，版主的名称和版块的名称
select sID01 版主的编号,bbzName 版主的名称,sName 版块的名称 from bbsSection inner join bbsUsers on bbsSection.sID01 = bbsUsers.UID01 where uAge<20
--4.查询出回复数量最多的主贴的发帖人编号，发帖人姓名，主贴标题，主贴内容和回复数量
select max(tCount) from bbsTopic
select rUID 发帖人编号,uName 发帖人姓名,tTitle 帖子的标题,tCount 回复数量, tMsg 帖子的内容 from  bbsReply inner join  bbsTopic on bbsReply.rUID =bbsTopic.tUID inner join bbsUsers on bbsReply.rUID= bbsUsers.UID01 where tCount=(select max(tCount) from bbsTopic)
--5.在主贴表中查询每个版块中每个用户的发帖总数
select tSID 每个版块,count (tUID) 每个用户 from bbsTopic group by tSID ,tUID