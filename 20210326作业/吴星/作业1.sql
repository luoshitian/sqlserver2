use Student
--1.查询学生的姓名，年龄，笔试成绩和机试成绩
select stuName,stuAge,wittenexam,labexam from Stuinfo002 inner join fraction on Stuinfo002.stuNo = fraction.stuNo

--2.查询笔试和机试成绩都在60分以上的学生的学号，姓名，笔试成绩和机试成绩
select Stuinfo002.stuNo,stuName,wittenexam,labexam from Stuinfo002 inner join fraction on  Stuinfo002.stuNo = fraction.stuNo where wittenexam>60 and labexam>60

--3.查询所有学生的学号，姓名，笔试成绩，机试成绩，没有参加考试的学生的成绩以NULL值填充
select Stuinfo002.stuNo,stuName,wittenexam,labexam from Stuinfo002 inner join fraction on  Stuinfo002.stuNo = fraction.stuNo where wittenexam>60 and labexam>60
--4.查询年龄在20以上（包括20）的学生的姓名，年龄，笔试成绩和机试成绩，并按笔试成绩降序排列
select stuName,stuAge,wittenexam,labexam from Stuinfo002 inner join fraction on Stuinfo002.stuNo = fraction.stuNo where stuAge>=20 order by wittenexam desc
--5.查询男女生的机试平均分
select  stuSex 性别,avg(labexam)机试平均分 from Stuinfo002 inner join fraction on Stuinfo002.stuNo = fraction.stuNo group by stuSex
--6.查询男女生的笔试总分
select stuSex 性别,sum(wittenexam)总分 from Stuinfo002 inner join fraction on Stuinfo002.stuNo = fraction.stuNo group by stuSex