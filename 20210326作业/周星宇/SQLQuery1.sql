use master
go

create database student
on
(
     name='student',
	 filename='D:\bbs.mdf',
	 size=5MB,
	 maxsize=50MB,
	 filegrowth=10%
)

log on
(
     name='student_log',
	 filename='D:\bbs_log.ldf',
	 size=5MB,
	 maxsize=50MB,
	 filegrowth=10%
)
go



use student 
go

create table studentinfo
(
    stuNO varchar(50) primary key not null,
    stuName varchar(10) unique not null,
	stuAge int,
	stuAddress nvarchar(20),
	stuSeat int not null,
	stuSex varchar(2) check(stuSex='0' or stuSex='1') not null
)

go

create table achievement
(
     examNO int primary key identity(1,1) not null,
	 stuNO varchar(50) references studentinfo(stuNO),
	 writtenExam int not null,
	 labExam int not null
)



insert into studentinfo(stuNO,stuName,stuAge,stuAddress,stuSeat,stuSex)
select 's2501','张秋利','20','美国硅谷','1','1' union
select 's2502','李斯文','18','湖北武汉','2','0' union
select 's2503','马文才','22','湖南长沙','3','1' union
select 's2504','欧阳俊雄','21','湖北武汉','4','0' union
select 's2505','梅超风','20','湖北武汉','5','1' union
select 's2506','陈旋风','19','美国硅谷','6','1' union
select 's2507','梅超风2','20','美国硅谷','7','0'
go



select * from studentinfo

insert into achievement
select 's2501', 50, 70 union
select 's2502', 60, 65 union
select 's2503', 86, 85 union
select 's2504', 40, 80 union
select 's2505', 70, 90 union
select 's2506', 85, 90

go



--1.查询学生的姓名，年龄，笔试成绩和机试成绩
select StuName ,StuAge ,writtenExam ,labExam  from StudentInfo
inner join achievement on StudentInfo.StuNo = achievement.StuNo

--2.查询笔试和机试成绩都在60分以上的学生的学号，姓名，笔试成绩和机试成绩
select StuName,writtenExam ,labExam from StudentInfo
inner join achievement on StudentInfo.StuNo = StudentInfo.StuNo
where labExam>60 and writtenExam>60 

--3.查询所有学生的学号，姓名，笔试成绩，机试成绩，没有参加考试的学生的成绩以NULL值填充
select StudentInfo.StuNo 学号,StuName 姓名,writtenExam 笔试成绩,labExam 机试成绩 from StudentInfo 
left join achievement on StudentInfo.StuNo= StudentInfo.StuNo

--4.查询年龄在20以上（包括20）的学生的姓名，年龄，笔试成绩和机试成绩，并按笔试成绩降序排列
select StuName,StuAge,writtenExam,labExam from StudentInfo 
inner join achievement on StudentInfo.stuNo = StudentInfo.stuNo 
where stuAge>=20 order by writtenExam desc

--5.查询男女生的机试平均分
select StuSex,avg(labExam)平均分 from StudentInfo
inner join achievement on StudentInfo.stuNo = StudentInfo.stuNo group by stuSex

--6.查询男女生的笔试总分
select StuSex,SUM(writtenExam)笔试总分 from StudentInfo
inner join achievement on StudentInfo.stuNo = StudentInfo.stuNo group by stuSex
