select*from StuInfo
select*from ClassInfo
select*from CourseInfo 
select*from Scores
--统计每个班的男生数
select ClassId,count(*) StuSex from StuInfo where StuSex='男' group by ClassId 

--统计每个班的男、女生数
select ClassId ,StuSex, count(StuSex) StuSex from StuInfo group by ClassId ,StuSex order by ClassId

--统计每个班的福建人数
select ClassId,count(StuProvince) StuProvince from StuInfo where StuProvince='福建省' group by ClassId

--统计每个班的各个省的总人数
select ClassId ,StuProvince, count(StuProvince) StuProvince from StuInfo group by ClassId ,StuProvince order by ClassId

--统计每个省的女生数
select StuProvince, count(StuSex) StuSex from StuInfo where StuSex='女' group by StuProvince

--统计每个省的男、女生数
select StuProvince,StuSex, count(StuSex) StuSex from StuInfo group by StuProvince ,StuSex order by StuProvince

--统计每个学生的考试总分、平均分
select StuId  ,AVG(Score),SUM(Score) StuId from Scores group by StuId

--统计出考试总分大于620的学生的考试总分 
select StuId,SUM(Score) StuID from Scores group by StuId having SUM(Score)>620

--统计出每门考试成绩最高分和最低分
select CourseId,max(Score) ,min(Score) from Scores group by CourseId

--统计出每个学生的各门成绩的平均分
select StuId,CourseId,AVG(Score) from Scores GROUP BY StuId,CourseId ORDER BY StuId