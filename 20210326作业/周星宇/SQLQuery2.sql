use master
go
create database webshop
on
(
     name='webshop',
	 filename='D:\webshop.mdf',
	 size=5MB,
	 maxsize=50MB,
	 filegrowth=10%
)

log on
(
     name='webshop_log',
	 filename='D:\webshop_log.ldf',
	 size=5MB,
	 maxsize=50MB,
	 filegrowth=10%
)
go


use webshop 
go

create table orders
(
     orderID int primary key identity(1,1) ,
	 orderDate datetime
)


go

create table orderItem
(
      ItemiD int primary key identity(1,1) ,
	  orderId int,
	  itemType varchar(20),
	  itemName nvarchar(10),
	  theNumber int,
	  theMoney money
)

set identity_insert orders ON--打开
insert into orders(orderId,orderDate) values ('1','2008-1-12'),('2','2008-2-10'),('3','2008-2-15'),('4','2008-3-10')
select * from orders

set identity_insert orderItem ON--打开
insert into orderItem(ItemiD,orderId,itemType,itemName,theNumber,theMoney)
select '1','1','文具','笔','72','2' union
select '2','1','文具','尺','11','1' union
select '3','1','体育用品','篮球','1','56' union
select '4','2','文具','笔','36','2' union
select '5','2','文具','固体胶','20','3' union
select '6','2','日常用品','透明胶','20','1' union
select '7','2','体育用品','羽毛球','20','3' union
select '8','3','文具','订书机','20','3' union
select '9','3','文具','订书钉','10','3' union
select '10','3','文具','裁纸刀','5','5' union
select '11','4','文具','笔','20','2' union
select '12','4','文具','信纸','50','1' union
select '13','4','日常用品','毛巾','4','5' union
select '14','4','日常用品','透明胶','30','1' union
select '15','4','体育用品','羽毛球','20','3'
select * from orderItem




select * from orders
select * from orderItem
 
--1.查询所有的订单的订单的编号，订单日期，订购产品的类别和订购的产品名称，订购数量和订购单价
select orderDate ,itemType ,itemName,theNumber,theMoney  from orderItem
inner join orders on orderItem.orderId = orders.orderId

--2.查询订购数量大于50的订单的编号，订单日期，订购产品的类别和订购的产品名称
select orderDate ,itemType, itemName from orders
inner join orderItem on orders.orderId = orderItem.orderId
where theNumber>50


--3.查询所有的订单的订单的编号，订单日期，订购产品的类别和订购的产品名称，订购数量和订购单价以及订购总价
select orders.orderId,orderDate,itemType,itemName,theNumber,theMoney * theNumber 订购总价 from orderItem
inner join orders on orderItem.orderId = orders.orderId


--4.查询单价大于等于5并且数量大于等于50的订单的订单的编号，订单日期，订购产品的类别和订购的产品名称，订购数量和订购单价以及订购总价
select  orderDate ,itemType, itemName,theNumber,theMoney,theMoney * theNumber 订购总价 from orders
inner join orderItem on orders.orderId = orderItem.orderId
where theMoney>=5 and theNumber>=50

--5.查询每个订单分别订购了几个产品，例如：
--			编号   订购产品数
--           1          3            
--           2          4                  	 
select orders.orderId  编号,count(*) as 订购产品数  from orderItem
inner join orders on orderItem.orderId = orders.orderId
group by orders.orderId


--6.查询每个订单里的每个类别的产品分别订购了几次和总数量，例如：

-- 订单编号       产品类别     订购次数     总数量

--    1           文具            	2           82
--    1           体育用品        	1            1
--    2           文具            	2           56
--   2           体育用品        	1            2
--   2           日常用品        	1            20
select orders.orderId as 订单编号,itemType as 产品类别,count(*)as 订购次数,sum(theNumber) as 总数量 from orderItem
inner join orders on orderItem.orderId = orders.orderId
group by orders.orderId,itemType
order by orders.orderId
