use master
go

create database Dwu
on
(
name='Dwu',
filename='D:\test\Dwu.mdf',
size=5mb,
maxsize=100mb,
filegrowth=10mb
)
log on
(
name='Dwu_log',
filename='D:\test\Dwu_log.ldf',
size=5mb,
maxsize=100mb,
filegrowth=10mb
)

go
use Dwu
go

create table stuinfo
(
stuNo char(5) primary key not null,
stuName nvarchar(20) not null,
stuAge char(3) not null,
stuAddress nvarchar(200), 
stuSeat int identity(1,1)not null,
stuSex char(1) check(stuSex in (1,0)) default(1) not null
)
go

insert into stuinfo(stuNo,stuName,stuAge,stuAddress,stuSex) values
('s2501','张秋利',20,'美国硅谷',1),
('s2502','李斯文',18,'湖北武汉',0),                 
('s2503','马文才',22,'湖南长沙',1),
('s2504','欧阳俊雄',21,'湖北武汉',0),
('s2505','梅超风',20,'湖北武汉',1),
('s2506','陈旋风',19,'美国硅谷',1),
('s2507','陈凤',20,'美国硅谷',0) 
go

create table stuexam
(
examNo int primary key identity(1,1),
stuNo char(5) references stuinfo(stuNo) not null,
writtenExam int not null,
labExam int not null
) 
go

insert stuexam(stuNo,writtenExam,labExam)values
('s2501',50,70),
('s2502',60,65),
('s2503',86,85),
('s2504',40,80),
('s2505',70,90),
('s2506',85,90)
go
--数据如图片1,使用上次作业的数据

--1.查询学生的姓名，年龄，笔试成绩和机试成绩
select stuName,stuAge,writtenExam,labExam from stuinfo inner join stuexam on stuexam.stuNo = stuinfo.stuNo

--2.查询笔试和机试成绩都在60分以上的学生的学号，姓名，笔试成绩和机试成绩
select stuName,stuAge, writtenExam,labExam from stuexam inner join stuinfo on stuexam.stuNo = stuinfo.stuNo
where writtenExam>60 and labExam>60

--3.查询所有学生的学号，姓名，笔试成绩，机试成绩，没有参加考试的学生的成绩以NULL值填充
select stuname,stuAge ,writtenExam,labExam from stuexam left join stuinfo on stuexam.stuNo = stuinfo.stuNo

--4.查询年龄在20以上（包括20）的学生的姓名，年龄，笔试成绩和机试成绩，并按笔试成绩降序排列
select stuName,stuAge,writtenExam,labExam  from stuexam inner join stuinfo on stuexam.stuNo = stuinfo.stuNo
where stuAge>=20
order by writtenExam desc

--5.查询男女生的机试平均分
select stuSex,AVG(labExam)机试 from stuexam inner join stuinfo on stuexam.stuNo = stuinfo.stuNo
group by stuSex

--6.查询男女生的笔试总分
--1.查询所有订单订购的所有物品数量总和
--select  '所有物品综合',sum(theNumber) as 订购数量  from orderItem
select stuSex ,SUM(writtenExam)总分 from stuexam inner join stuinfo on stuexam.stuNo=stuinfo.stuNo
group by stuSex

