--统计每个班的男生数
select ClassId ,count(StuSex)男生数 from dbo.StuInfo where StuSex='男' group by ClassId
--统计每个班的男、女生数
select ClassId StuSex,count(StuSex) from dbo.StuInfo group by ClassId ,StuSex
--统计每个班的福建人数
select ClassId,count(*)福建人数 from dbo.StuInfo where StuProvince='福建省' group by ClassId
--统计每个班的各个省的总人数
select StuProvince,count(*) from dbo.StuInfo group by StuProvince
--统计每个省的女生数
select StuProvince,count(*)女生数 from dbo.StuInfo  where StuSex='女' group by StuProvince
--统计每个省的男、女生数
select StuProvince StuSex,count(StuSex) from dbo.StuInfo group by StuProvince, StuSex order by StuProvince
--统计每个学生的考试总分、平均分
select StuId,sum(Score)考试总分, avg(Score)平均分 from dbo.Scores group by StuId
--统计出考试总分大于620的学生的考试总分
select StuId,sum(Score)考试总分 from  dbo.Scores group by StuId having sum(Score)>620
--统计出每门考试成绩最高分和最低分
select  CourseId,max(Score)最高分,min(Score)最低分 from dbo.Scores group by CourseId
--统计出每个学生的各门成绩的平均分
select StuId,CourseID ,avg(Score)各门成绩的平均分 from  dbo.Scores group by StuId, CourseID order by StuId