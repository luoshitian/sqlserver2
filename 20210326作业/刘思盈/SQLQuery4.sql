	use master
	go

	create database DingDan
	on
	(
	name = 'DingDan' ,
	filename = 'D:\DingDan.mdf',
	size = 5MB,
	MAXsize = 50MB,
	filegrowth = 10%
	)
	log	on
	(
	name = 'DingDan_log' ,
	filename = 'D:\DingDan_log.ldf',
	size = 5MB,
	MAXsize = 50MB,
	filegrowth = 10%
	)
	go

	use DingDan
	go
	create table OrdersIofo
	(
	orderId int primary key identity(1,1),
	orderDate datetime
	)
	create table OrderItemInfo
	(
	ItemiD int primary key identity(1,1),
	orderId  int references  OrdersIofo(orderId),
	itemType  varchar(10) not null,
	itemName varchar(10)not null,
	theNumber int,
	theMoney money
	)
	insert into OrdersIofo values('2008-01-12'),
							 ('2008-02-10'),
							 ('2008-02-15'),
							 ('2008-03-10')

	insert into OrderItemInfo values(1,'文具','笔',72,2),
									(1,'文具','尺',10,1),
									(1,'体育用品','篮球',1,56),
									(2,'文具','笔',36,2),
									(2,'文具','固体胶',20,3),
									(2,'日常用品','透明胶',2,1),
									(2,'体育用品','羽毛球',20,3),
									(3,'文具','订书机',20,3),
									(3,'文具','订书机',10,3),
									(3,'文具','裁纸刀',5,5),
									(4,'文具','笔',20,2),
									(4,'文具','信纸',50,1),
									(4,'日常用品','毛巾',4,5),
									(4,'日常用品','透明胶',30,1),
									(4,'体育用品','羽毛球',20,3)
	
--	使用上次作业的订单数据库，完成下列题目：

--1.查询所有的订单的订单的编号，订单日期，订购产品的类别和订购的产品名称，订购数量和订购单价
	select * from OrderItemInfo
	select * from OrdersIofo
	select OI.orderId 订单编号, orderDate 订单日期,itemType 产品类别,itemName 产品名称,theNumber  订购数量, theMoney 订购单价 from OrderItemInfo OI inner join OrdersIofo on OI.orderId=OrdersIofo.orderId
--2.查询订购数量大于50的订单的编号，订单日期，订购产品的类别和订购的产品名称
	select OI.orderId 订单编号, orderDate 订单日期,itemType 产品类别,itemName 产品名称 from OrderItemInfo OI inner join OrdersIofo on OI.orderId=OrdersIofo.orderId WHERE theNumber>50
--3.查询所有的订单的订单的编号，订单日期，订购产品的类别和订购的产品名称，订购数量和订购单价以及订购总价
	select OI.orderId 订单编号, orderDate 订单日期,itemType 产品类别,itemName 产品名称,theNumber  订购数量, theMoney 订购单价, theMoney*theNumber 订购总价 from OrderItemInfo OI inner join OrdersIofo on OI.orderId=OrdersIofo.orderId
--4.查询单价大于等于5并且数量大于等于50的订单的订单的编号，订单日期，订购产品的类别和订购的产品名称，订购数量和订购单价以及订购总价
	select OI.orderId 订单编号, orderDate 订单日期,itemType 产品类别,itemName 产品名称,theNumber  订购数量, theMoney 订购单价, theMoney*theNumber 订购总价 from OrderItemInfo OI inner join OrdersIofo on OI.orderId=OrdersIofo.orderId  where theMoney>5 and theNumber>5

--5.查询每个订单分别订购了几个产品，例如：
--			编号   订购产品数
--                                            	  1           3
--                                            	  2           4
select orderId 编号 , theNumber 订购产品数  from OrderItemInfo group by orderId,theNumber,itemType
--6.查询每个订单里的每个类别的产品分别订购了几次和总数量，例如：

-- 订单编号       产品类别     订购次数     总数量

--    1           文具            	2           82
--    1           体育用品        	1            1
--    2           文具            	2           56
--    2           体育用品        	1            2
--    2           日常用品        	1            20
	
	select  orderId 编号 ,itemType 产品类别,count(theNumber) 订购次数 ,sum(theNumber) 总数量 from OrderItemInfo group by orderId,itemType order by orderId