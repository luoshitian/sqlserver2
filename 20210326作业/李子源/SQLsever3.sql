use master
go

create database Dwu
on
(
name='Dwu',
filename='D:\test\Dwu.mdf',
size=5mb,
maxsize=100mb,
filegrowth=10mb
)
log on
(
name='Dwu_log',
filename='D:\test\Dwu_log.ldf',
size=5mb,
maxsize=100mb,
filegrowth=10mb
)
use Dwu
go

create table stuinfo
(
stuNo char(5) primary key not null,
stuName nvarchar(20) not null,
stuAge char(3) not null,
stuAddress nvarchar(200), 
stuSeat int identity(1,1)not null,
stuSex char(1) check(stuSex in (1,0)) default(1) not null
)
go

insert into stuinfo(stuNo,stuName,stuAge,stuAddress,stuSex) values
('s2501','������',20,'�������',1),
('s2502','��˹��',18,'�����人',0),                 
('s2503','���Ĳ�',22,'���ϳ�ɳ',1),
('s2504','ŷ������',21,'�����人',0),
('s2505','÷����',20,'�����人',1),
('s2506','������',19,'�������',1),
('s2507','�·�',20,'�������',0) 
go

create table stuexam
(
examNo int primary key identity(1,1),
stuNo char(5) references stuinfo(stuNo) not null,
writtenExam int not null,
labExam int not null
) 
go

insert stuexam(stuNo,writtenExam,labExam)values
('s2501',50,70),
('s2502',60,65),
('s2503',86,85),
('s2504',40,80),
('s2505',70,90),
('s2506',85,90)
go

select stuName,stuAge,writtenExam,labExam from stuexam
inner join stuinfo on stuexam.stuNo = stuinfo.stuNo
go


select stuName,stuAge,writtenExam,labExam from stuexam
inner join stuinfo on stuexam.stuNo = stuinfo.stuNo
where writtenExam > 60 and labExam > 60
go

select stuName,stuAge,writtenExam,labExam from stuinfo
left join stuexam on stuexam.stuNo = stuinfo.stuNo
go

select stuName,stuAge,writtenExam,labExam from stuexam
inner join stuinfo on stuexam.stuNo = stuinfo.stuNo
where stuAge >= 20
order by writtenExam desc 
go

select stuSex,avg(labExam) from stuexam
inner join stuinfo on stuexam.stuNo = stuinfo.stuNo
group by stuSex
go

select stuSex,sum(writtenExam) from stuexam
inner join stuinfo on stuexam.stuNo = stuinfo.stuNo
group by stuSex
go