create database Student
on
(
	name='Student',
	filename='D:\Student.mdf',
	size=10mb,
	maxsize=100mb,
	filegrowth=10mb
	)

log on
(
	name='Student_log',
	filename='D:\Student_log.ldf',
	size=10mb,
	maxsize=100mb,
	filegrowth=10mb
	)
go
use Student
go

create table ClassInfo
(
	ClassId int primary key identity(1,1),
	ClassName int unique not null,
	OpenTime int not null,
	ClassDescribe text
)

go
use Student 
go

create table StudentInfo
(
	StudentId int primary key identity(1,1),
	StudentName varchar(10) check(StudentName>2) unique,
	StudentSex varchar(2) check(StudentSex=' ' or StudentSex='1') default(' ') not null,
	StudentAge int check(StudentAge>15 or StudentAge<40) not null,
	StudentAddress  text default('湖北武汉'),
	StudentClassId int references ClassInfo(ClassId),
)

create table CourseInfo
(
	CourseId int primary key identity(1,1),
	CourseName varchar(20) unique not null,
	CoueseDescribe text

)

create table PerformanceInfo
(
	 PerformanceId int primary key identity(1,1),
	PerformanceStudentNumber int references StudentInfo(StudentId) not null,
	PerformanceClassNumber int references CourseInfo(CourseId) not null,
	performance int check(performance>0 or performance<100)
)


--数据如图片1,使用上次作业的数据

--1.查询学生的姓名，年龄，笔试成绩和机试成绩
select StudentName,StudentAge from StudentInfo

--2.查询笔试和机试成绩都在60分以上的学生的学号，姓名，笔试成绩和机试成绩
select StudentName from StudentInfo where elam>60

--3.查询所有学生的学号，姓名，笔试成绩，机试成绩，没有参加考试的学生的成绩以NULL值填充
select StudentName from StudentInfo 

--4.查询年龄在20以上（包括20）的学生的姓名，年龄，笔试成绩和机试成绩，并按笔试成绩降序排列
select StudentName,StudentAge from StudentInfo where StudentAge>20 order by elam DESC

--5.查询男女生的机试平均分
select StudentSex,avg(elam) from StudentInfo group by StudentSex

--6.查询男女生的笔试总分
select StudentSex,sum(elam) from StudentInfo StudentSex