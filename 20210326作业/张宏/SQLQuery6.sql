use master
go

create database DEMO
on
(
	name='DEMO'
	filename='D:\bank\Demo3.mdf',
	size=10mb,
	maxsize=100mb,
	filegrowth=10mb,
)

log on
(
	name='DEMO_log'
	filename='D:\bank\Demo3_log.ldf',
	size=10mb,
	maxsize=100mb,
	filegrowth=10mb,
)
go

use DEMO
go

create table orders
(
	orderId int primary key identity(1,1),
	orderDate datetime default(getdate()),
)

create table orderItem
(
	ItemiD int primary key identity(1,1),
	orderId int foreign key references orders(orderId),
	itemType nvarchar(10) not null,
	itemName nvarchar(20) not null,
	theNumber varchar(20) not null,
	theMoney money not null,
)

insert into orders(orderId,orderDate) values(1,2008-01-12),(2,2008-02-10),(3,2008-02-15),(4,2008-03-10)
insert into orders(ItemiD,orderId,itemType,itemName,theNumber,theMoney) values(1,1,'文具','笔','72',2)

select * from orders
select * from orderItem


--1.查询所有订单订购的所有物品数量总和
select sum(orderId) from orders

--2.查询 每个订单订购的所有物品的 数量和 以及 平均单价       订单编号小于3的，平均单价小于10
select orderItem,avg(theMoney)平均单价,sum(theNumber) from orderItem where orderId<3 group by orderId having avg(theMoney)<10

--3.查询 每个订单订购的所有物品 数量和 以及 平均单价 平均单价小于10并且总数量大于 50 
select orderItem,avg(theMoney)平均单价 from orderItem where avg(theMoney)<10 group by theMoney having avg(theNumber)>50

--4.查询每种类别的产品分别订购了几次，
select 

--5.查询每种类别的产品的订购总数量在100以上的订购总数量和平均单价
select itemType,avg(theMoney)平均单价 from orderItem where sum(theNumber)>100 

--6.查询每种产品的订购次数，订购总数量和订购的平均单价
select itemName,sum(orderId) ,avg(theMoney)from orderItem group by itemName

--使用上次作业的订单数据库，完成下列题目：

--1.查询所有的订单的订单的编号，订单日期，订购产品的类别和订购的产品名称，订购数量和订购单价
select * from orderItem 

--2.查询订购数量大于50的订单的编号，订单日期，订购产品的类别和订购的产品名称
select orderId,itemType,itemName from orderItem where orderId>50

--3.查询所有的订单的订单的编号，订单日期，订购产品的类别和订购的产品名称，订购数量和订购单价以及订购总价
select *,sum(theMoney) from orderItem

--4.查询单价大于等于5并且数量大于等于50的订单的订单的编号，订单日期，订购产品的类别和订购的产品名称，订购数量和订购单价以及订购总价
select *,sum(theMoney) from orderItem where theMoney>5 and theNumber>50

--5.查询每个订单分别订购了几个产品，例如：
--			编号   订购产品数
--                                            	  1           3
--                                            	  2           4
select orderId,theNumber from orderItem group by orderId

--6.查询每个订单里的每个类别的产品分别订购了几次和总数量，例如：
select orderId,itemType,sum(theNumber),orderId from orderItem group by orderId,itemType

-- 订单编号       产品类别     订购次数     总数量

--    1           文具            	2           82
--    1           体育用品        	1            1
--    2           文具            	2           56
--    2           体育用品        	1            2