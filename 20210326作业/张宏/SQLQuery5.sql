use master 
go

create database bbs
on
(
	name='bbs',
	filename='D:\bbs.mdf',
	size=10mb,
	maxsize=100mb,
	filegrowth=10mb
)

log on
(
	name='bbs_log',
	filename='D:\bbs_log.mdf',
	size=10mb,
	maxsize=100mb,
	filegrowth=10mb
)
go

use bbs

go

create table bbsUsers
(
	UID int primary key identity(1,1),
	uName varchar(10) unique not null,
    uSex varchar(2) check(uSex='男' or uSex='女')  not null,
	uAge int check(uAge>15 and uAge<60) not null,
	uPoint int check(uPoint>=0) not null
)

create table bbsSection
(
	sID  int primary key identity(1,1),
	sName  varchar(10) not null,
	sUid   int foreign key references bbsUsers(UID),
)

create table bbsTopic
(
	 tID  int primary key identity(1,1),
	 tUID  int references bbsUsers(UID),
	 tSID  int references bbsSection(sID),
	 tTitle  varchar(100) not null,
	 tMsg  text  not null,
	 Time  datetime,
	 tCount  int
)

create table bbsReply
(
	 rID  int primary key identity(1,1),
	 rUID  int references bbsUsers (UID),
	 rTID  int references bbsTopic(tID),
	 rMsg  text not null,
	 rTime  datetime 
)

--1.现在有3个会员注册成功，请用一次插入多行数据的方法向bbsUsers表种插入3行记录，记录值如下：
--  小雨点  女  20  0
--  逍遥    男  18  4
--	  七年级生  男  19  2
insert into bbsUsers values('小雨点','女',20,0),('逍遥','男',18,4),('七年级生','男',19,2)
select * from bbsUsers



--2.将bbsUsers表中的用户名和积分两列备份到新表bbsPoint表中，提示查询部分列:select 列名1，列名2 from 表名
select uName,uPoint into  bbsPoint from bbsUsers
insert into bbsPoint select uName,uPoint  from bbsUsers
select * from bbsPoint

--  读书世界    七年级生
--  生活百科     小雨点
--  八卦区       七年级生
insert into bbsSection values('技术交流',1),('读书世界',3),('生活百科',1),('八卦区',3)
select * from bbsSection
select * from bbsPoint

--4.向主贴和回帖表中添加几条记录
	  
		--主贴：

		--发帖人    板块名    帖子标题                帖子内容                发帖时间   回复数量
		--逍遥      八卦区     范跑跑                 谁是范跑跑              2008-7-8   1
		--七年级生  技术交流   .NET                   与JAVA的区别是什么呀？  2008-9-1   2
		--小雨点   生活百科    今年夏天最流行什么     ’有谁知道今年夏天最流行‘  2008-9-10  0
		--						什么呀？
insert into bbsTopic values(2,4,'范跑跑','谁是范跑跑',' 2008-7-8 ',1),(6,1,'NET','与JAVA的区别是什么呀？','2008-9-1',2)
insert into bbsTopic values(4,3,'今年夏天最流行什么 ','有谁知道今年夏天最流行',' 2008-9-10',0)

--insert into bbsTopic values (2,5,'范跑跑','谁是范跑跑','2008-7-8',1)
--insert into bbsTopic values (3,2,'.NET','与JAVA的区别是什么呀？','2008-9-1',2),(1,4,'今年夏天最流行什么','有谁知道今年夏天最流行','2008-9-10',0)
select * from bbsTopic

  --回帖：
	 --  分别给上面三个主贴添加对应的回帖，回帖的内容，时间，回帖人自定

--在论坛数据库中完成以下题目
--1.查询出每个版块的版主编号，版主姓名和版块名称
select UID,uName,uPoint from bbsUsers group by UID
--2.查询出主贴的发帖时间在2008-9-15以后的主贴的发帖人编号，发帖人姓名，帖子的标题，帖子的内容和发帖时间
select tUID,tTitle,tMsg,Time from bbsTopic where Time>2008-9-15
--3.查询出年龄在20以下的版主的编号，版主的名称和版块的名称
select UID,uName from bbsUsers where uAge<20
--4.查询出回复数量最多的主贴的发帖人编号，发帖人姓名，主贴标题，主贴内容和回复数量
select tUID,tTitle,tMsg,tCount from bbsTopic order by tCount DESC
--5.在主贴表中查询每个版块中每个用户的发帖总数
select tID,tUID,sum(tCount) from bbsTopic group by tID,tUID



