
--统计每个班的男生数
select '男生的数量',count(StuSex) from StuInfo group by StuSex having StuSex='男'


--统计每个班的男、女生数

select ClassId,'男生的数量',count(StuSex) from StuInfo  where StuSex='男' group by ClassId  
union
select ClassId,'女生的数量',count(StuSex) from StuInfo  where StuSex='女' group by ClassId

 

 --统计每个班的男、女生数

select ClassId,Stusex,count(StuSex) from StuInfo  group by ClassId,StuSex
 

 --统计每个班的福建人数
select ClassId,count(StuProvince)福建省的人数 from StuInfo group by ClassId,StuProvince having StuProvince='福建省'


--统计每个班的各个省的总人数
select ClassId,StuProvince,count(StuProvince) from StuInfo group by ClassId,StuProvince order by ClassId 


--统计每个省的女生数
select StuProvince,count(StuSex)女生人数 from StuInfo  where StuSex='女' group by StuProvince


--统计每个省的男、女生数
select StuProvince,StuSex,count(StuSex)人数 from StuInfo group by StuProvince,StuSex


--统计每个学生的考试总分、平均分
select StuId 学号,sum(Score)总分,avg(Score)平均分 from Scores group by StuId

--统计出考试总分大于620的学生的考试总分
select StuId 学号,sum(Score)总分 from Scores  group by StuId having sum(Score)>620 


--统计出每门考试成绩最高分和最低分
select CourseId,max(Score)最高分,min(Score)最低分 from Scores group by CourseId 


--统计出每个学生的各门成绩的平均分
select StuId,CourseId,avg(score) from Scores group by StuId,CourseId order by StuId