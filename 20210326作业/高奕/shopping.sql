use master 
go

create database shopping
on
(
	name='shopping',
	filename='D:\Demo\shopping.mdf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%
)
log on
(
	name='shopping_log',
	filename='D:\Demo\shopping_log.ldf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%
)
go

use shopping
go

create table  orders
(
	orderId int primary key identity(1,1) ,
	orderDate datetime default(getdate())
)

create table orderItem
(
	ItemiD	int primary key identity(1,1),
	orderId	int references orders(orderId) not null,
	itemType  nvarchar(4) check(itemType='文具' or itemType='日常用品' or itemType='体育用品' ) not null,
	itemName nvarchar(4) not null,
	theNumber int not null,
	theMoney money not null
)

insert into orders values (default),(default),(default),(default)


insert into orderItem (orderId,itemType,itemName,theNumber,theMoney) values (1 ,'文具',' 笔' ,72, 2)
,(1,'文具','尺',10,1)
,(1,'体育用品','篮球',1,56)
,(2,'文具','笔',36,2)
,(2,'文具','固体胶',20,3)
,(2,'日常用品','透明胶',2,1)
,(2,'体育用品','羽毛球',20,3)
,(3,'文具','订书机',20,3)
,(3,'文具','订书针',10,3)
,(3,'文具','裁纸刀',5,5)
,(4,'文具','笔',20,2)
,(4,'文具','信纸',50,1)
,(4,'日常用品','毛巾',4,5)
,(4,'日常用品','透明胶',30,1)
,(4,'体育用品','羽毛球',20,3)



select * from orderItem
select * from orders



--使用上次作业的订单数据库，完成下列题目：

--1.查询所有的订单的订单的编号，订单日期，订购产品的类别和订购的产品名称，订购数量和订购单价
select orders.orderId,orderDate,itemType,itemName,theNumber,theMoney from orders inner join orderItem on orders.orderId=orderItem.orderId


--2.查询订购数量大于50的订单的编号，订单日期，订购产品的类别和订购的产品名称
select orders.orderId,orderDate,itemType,itemName from orders inner join orderItem on orders.orderId=orderItem.orderId where theNumber>=50

--3.查询所有的订单的订单的编号，订单日期，订购产品的类别和订购的产品名称，订购数量和订购单价以及订购总价
select  orders.orderId,orderDate,itemType,itemName,theNumber,theMoney,sum(theMoney*theNumber)订购总价 from orders inner join orderItem on orders.orderId=orderItem.orderId group by theMoney,orders.orderId,orderDate,itemType,itemName,theNumber,theMoney


--4.查询单价大于等于5并且数量大于等于50的订单的订单的编号，订单日期，订购产品的类别和订购的产品名称，订购数量和订购单价以及订购总价
select  orders.orderId,orderDate,itemType,itemName,theNumber,theMoney,sum(theMoney*theNumber)订购总价 from orders inner join orderItem on orders.orderId=orderItem.orderId group by theMoney,orders.orderId,orderDate,itemType,itemName,theNumber,theMoney having theMoney>4 and theNumber>=3


--5.查询每个订单分别订购了几个产品，例如：
--			编号   订购产品数
--                                            	  1           3
--                                            	  2           4
select orders.orderId,theNumber  from orderItem inner join orders on orderItem.orderId=orders.orderId

--6.查询 每个订单里的 每个类别的产品 分别订购了几次和总数量，例如：

-- 订单编号       产品类别     订购次数     总数量

--    1           文具            	2           82
--    1           体育用品        	1            1
--    2           文具            	2           56
--    2           体育用品        	1            2
--    2           日常用品        	1            20

select orderId,itemType,count(theNumber) from orderItem group by orderId,itemType order by orderId
